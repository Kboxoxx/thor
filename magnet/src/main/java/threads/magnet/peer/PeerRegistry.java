package threads.magnet.peer;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import threads.magnet.LogUtils;
import threads.magnet.Settings;
import threads.magnet.event.EventSink;
import threads.magnet.metainfo.Torrent;
import threads.magnet.metainfo.TorrentId;
import threads.magnet.net.Peer;
import threads.magnet.net.PeerId;
import threads.magnet.torrent.TorrentDescriptor;
import threads.magnet.torrent.TorrentRegistry;

public record PeerRegistry(PeerId localId, TorrentRegistry torrentRegistry, EventSink eventSink,
                           Set<PeerSourceFactory> extraPeerSourceFactories,
                           ScheduledExecutorService executor) {

    private static final String TAG = PeerRegistry.class.getSimpleName();


    public static PeerRegistry create(TorrentRegistry torrentRegistry, EventSink eventSink, PeerId peerId) {
        ScheduledExecutorService executor =
                Executors.newSingleThreadScheduledExecutor(r ->
                        new Thread(r, "bt.peer.peer-collector"));

        return new PeerRegistry(peerId, torrentRegistry, eventSink, new HashSet<>(), executor);
    }

    public void startup() {
        executor.scheduleWithFixedDelay(this::collectAndVisitPeers, 1,
                Settings.peerDiscoveryInterval.toMillis(), TimeUnit.MILLISECONDS);
    }

    public void shutdown() {
        executor.shutdown();
        executor.shutdownNow();
    }

    public void addPeerSourceFactory(PeerSourceFactory factory) {
        extraPeerSourceFactories.add(factory);
    }

    private void collectAndVisitPeers() {
        torrentRegistry.getTorrentIds().forEach(torrentId -> {
            TorrentDescriptor descriptor = torrentRegistry.getDescriptor(torrentId);
            if (descriptor != null && descriptor.isActive()) {
                Torrent torrent = torrentRegistry.getTorrent(torrentId);

                // disallow querying peer sources other than the tracker for private torrents
                if ((torrent == null || !torrent.isPrivate()) && !extraPeerSourceFactories.isEmpty()) {
                    extraPeerSourceFactories.forEach(factory ->
                            queryPeerSource(torrentId, factory.getPeerSource(torrentId)));
                }
            }
        });
    }

    private void queryPeerSource(TorrentId torrentId, PeerSource peerSource) {
        try {
            if (peerSource.update()) {
                Collection<Peer> discoveredPeers = peerSource.getPeers();
                Set<Peer> addedPeers = new HashSet<>();
                Iterator<Peer> iter = discoveredPeers.iterator();
                while (iter.hasNext()) {
                    Peer peer = iter.next();
                    if (!addedPeers.contains(peer)) {
                        addPeer(torrentId, peer);
                        addedPeers.add(peer);
                    }
                    iter.remove();
                }
            }
        } catch (Exception e) {
            LogUtils.error(TAG, "Error when querying peer source: " + peerSource, e);
        }
    }

    public void addPeer(TorrentId torrentId, Peer peer) {
        if (peer.isPortUnknown()) {
            throw new IllegalArgumentException("Peer's port is unknown: " + peer);
        } else if (peer.getPort() < 0 || peer.getPort() > 65535) {
            throw new IllegalArgumentException("Invalid port: " + peer.getPort());
        }
        eventSink.firePeerDiscovered(torrentId, peer);
    }

}
