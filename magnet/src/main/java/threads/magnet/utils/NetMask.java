package threads.magnet.utils;

import static threads.magnet.utils.Functional.unchecked;

import java.net.InetAddress;
import java.util.Objects;

public record NetMask(byte[] address, int mask) {


    public static NetMask createNetMask(InetAddress addr, int mask) {
        byte[] address = addr.getAddress();
        if (address.length * 8 < mask)
            throw new IllegalArgumentException("mask cannot cover more bits than the length of the network address");
        return new NetMask(address, mask);
    }

    public static NetMask fromString(String toParse) {
        String[] parts = toParse.split("/");
        return NetMask.createNetMask(Objects.requireNonNull(unchecked(()
                -> InetAddress.getByName(parts[0]))), Integer.parseInt(parts[1]));
    }

    public boolean contains(InetAddress toTest) {
        byte[] other = toTest.getAddress();

        if (address.length != other.length)
            return false;

        for (int i = 0; i < mask / 8; i++) {
            if (address[i] != other[i])
                return false;
        }

        if (mask % 8 == 0)
            return true;

        int offset = mask / 8;

        int probeMask = (0xff00 >> mask % 8) & 0xff;

        return (address[offset] & probeMask) == (other[offset] & probeMask);
    }
}
