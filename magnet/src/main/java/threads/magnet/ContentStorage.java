package threads.magnet;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ThreadLocalRandom;

import threads.magnet.data.Storage;
import threads.magnet.data.StorageUnit;
import threads.magnet.event.EventBus;
import threads.magnet.metainfo.TorrentFile;


public class ContentStorage implements Storage {
    private final EventBus eventBus;
    private final File root;

    public ContentStorage(File root, EventBus eventbus) {
        this.eventBus = eventbus;
        this.root = root;
    }

    public static int nextFreePort() {
        int port = ThreadLocalRandom.current().nextInt(4001, 65535);
        while (true) {
            if (isLocalPortFree(port)) {
                return port;
            } else {
                port = ThreadLocalRandom.current().nextInt(4001, 65535);
            }
        }
    }

    private static boolean isLocalPortFree(int port) {
        try {
            new ServerSocket(port).close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }


    public File getRootFile() {
        if (!root.exists()) {
            throw new IllegalArgumentException("data directory does not exists");
        }
        return root;
    }

    EventBus getEventBus() {
        return eventBus;
    }

    @Override
    public StorageUnit getUnit(TorrentFile torrentFile) {
        try {
            return new ContentStorageUnit(this, torrentFile);
        } catch (Throwable e) {
            throw new IllegalArgumentException(e);
        }
    }
}
