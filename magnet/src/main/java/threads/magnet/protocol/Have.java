package threads.magnet.protocol;

/**
 * @since 1.0
 */
public record Have(int pieceIndex) implements Message {

    /**
     * @since 1.0
     */
    public Have {

        if (pieceIndex < 0) {
            throw new InvalidMessageException("Illegal argument: piece index (" + pieceIndex + ")");
        }

    }

    /**
     * @since 1.0
     */
    @Override
    public int pieceIndex() {
        return pieceIndex;
    }


    @Override
    public String toString() {
        return "[" + this.getClass().getSimpleName() + "] piece index {" + pieceIndex + "}";
    }

    @Override
    public Integer getMessageId() {
        return StandardBittorrentProtocol.HAVE_ID;
    }
}
