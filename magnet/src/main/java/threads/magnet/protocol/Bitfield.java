package threads.magnet.protocol;

public record Bitfield(byte[] bitfield) implements Message {

    /**
     * @since 1.0
     */
    public Bitfield {
    }

    /**
     * @since 1.0
     */
    @Override
    public byte[] bitfield() {
        return bitfield;
    }


    @Override
    public String toString() {
        return "[" + this.getClass().getSimpleName() + "] bitfield {" + bitfield.length + " bytes}";
    }

    @Override
    public Integer getMessageId() {
        return StandardBittorrentProtocol.BITFIELD_ID;
    }
}
