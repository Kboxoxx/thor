package threads.magnet;

import java.io.File;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.Selector;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import threads.magnet.data.ChunkVerifier;
import threads.magnet.data.DataDescriptorFactory;
import threads.magnet.data.digest.JavaSecurityDigester;
import threads.magnet.dht.DHTHandshakeHandler;
import threads.magnet.dht.DHTPeerSourceFactory;
import threads.magnet.dht.DHTService;
import threads.magnet.event.EventBus;
import threads.magnet.magnet.UtMetadataMessageHandler;
import threads.magnet.net.BitfieldConnectionHandler;
import threads.magnet.net.ConnectionHandlerFactory;
import threads.magnet.net.ConnectionSource;
import threads.magnet.net.DataReceiver;
import threads.magnet.net.HandshakeHandler;
import threads.magnet.net.MessageDispatcher;
import threads.magnet.net.PeerConnectionFactory;
import threads.magnet.net.PeerConnectionPool;
import threads.magnet.net.PeerId;
import threads.magnet.net.SharedSelector;
import threads.magnet.net.SocketChannelConnectionAcceptor;
import threads.magnet.net.buffer.BufferManager;
import threads.magnet.net.extended.ExtendedProtocolHandshakeHandler;
import threads.magnet.net.pipeline.BufferedPieceRegistry;
import threads.magnet.net.pipeline.ChannelPipelineFactory;
import threads.magnet.peer.PeerRegistry;
import threads.magnet.peerexchange.PeerExchangeMessageHandler;
import threads.magnet.peerexchange.PeerExchangePeerSourceFactory;
import threads.magnet.protocol.HandshakeFactory;
import threads.magnet.protocol.Message;
import threads.magnet.protocol.StandardBittorrentProtocol;
import threads.magnet.protocol.extended.AlphaSortedMapping;
import threads.magnet.protocol.extended.ExtendedHandshakeFactory;
import threads.magnet.protocol.extended.ExtendedMessage;
import threads.magnet.protocol.extended.ExtendedProtocol;
import threads.magnet.protocol.handler.MessageHandler;
import threads.magnet.protocol.handler.PortMessageHandler;
import threads.magnet.torrent.BlockCache;
import threads.magnet.torrent.DataWorker;
import threads.magnet.torrent.TorrentRegistry;

public final class Runtime {

    private static final int STEP = 2 << 22; // 8 MB
    public final MessageDispatcher messageDispatcher;
    public final ConnectionSource connectionSource;
    public final PeerRegistry peerRegistry;
    public final TorrentRegistry torrentRegistry;
    public final Set<IAgent> messagingAgents;
    public final DataWorker dataWorker;
    public final PeerConnectionPool connectionPool;
    public final BufferedPieceRegistry bufferedPieceRegistry;

    private final EventBus eventBus;
    private final ContentStorage storage;
    private final DHTService dhtService;
    private final SharedSelector selector;
    private final DHTPeerSourceFactory dhtPeerSourceFactory;
    private final PeerExchangePeerSourceFactory peerExchangePeerSourceFactory;
    private final DataReceiver dataReceiver;

    public Runtime(File root) throws Exception {
        this.eventBus = new EventBus();
        this.storage = new ContentStorage(root, eventBus);
        this.peerExchangePeerSourceFactory = new PeerExchangePeerSourceFactory(eventBus);
        this.selector = new SharedSelector(Selector.open());

        JavaSecurityDigester digester = new JavaSecurityDigester("SHA-1", STEP);
        ChunkVerifier chunkVerifier = new ChunkVerifier(eventBus, digester);
        DataDescriptorFactory dataDescriptorFactory = new DataDescriptorFactory(chunkVerifier);

        this.torrentRegistry = new TorrentRegistry(dataDescriptorFactory);

        PeerId peerId = PeerId.createPeerId(new IdentityService().getID());
        int acceptorPort = ContentStorage.nextFreePort();
        this.peerRegistry = PeerRegistry.create(torrentRegistry, eventBus, peerId);


        dataReceiver = new DataReceiver(selector);
        BufferManager bufferManager = new BufferManager();
        bufferedPieceRegistry = new BufferedPieceRegistry();
        ChannelPipelineFactory channelPipelineFactory =
                new ChannelPipelineFactory(bufferManager, bufferedPieceRegistry);


        dhtService = new DHTService(peerId, torrentRegistry, eventBus, acceptorPort);


        Map<String, MessageHandler<? extends ExtendedMessage>> handlersByTypeName = new HashMap<>();
        handlersByTypeName.put("ut_pex", new PeerExchangeMessageHandler());
        handlersByTypeName.put("ut_metadata", new UtMetadataMessageHandler());
        AlphaSortedMapping messageTypeMapping = new AlphaSortedMapping(handlersByTypeName);


        Map<Integer, MessageHandler<?>> extraHandlers = new HashMap<>();
        extraHandlers.put(PortMessageHandler.PORT_ID, new PortMessageHandler());
        extraHandlers.put(ExtendedProtocol.EXTENDED_MESSAGE_ID,
                new ExtendedProtocol(messageTypeMapping, handlersByTypeName));
        MessageHandler<Message> bittorrentProtocol = new StandardBittorrentProtocol(extraHandlers);


        List<HandshakeHandler> handshakeHandlers = new ArrayList<>();
        // add default handshake handlers to the beginning of the connection handling chain
        handshakeHandlers.add(new DHTHandshakeHandler(dhtService.getPort()));
        handshakeHandlers.add(new BitfieldConnectionHandler(torrentRegistry));
        handshakeHandlers.add(new ExtendedProtocolHandshakeHandler(
                new ExtendedHandshakeFactory(torrentRegistry, messageTypeMapping, acceptorPort)
        ));

        ConnectionHandlerFactory connectionHandlerFactory =
                new ConnectionHandlerFactory(new HandshakeFactory(peerRegistry),
                        torrentRegistry, handshakeHandlers);


        PeerConnectionFactory peerConnectionFactory = new PeerConnectionFactory(
                selector, connectionHandlerFactory,
                channelPipelineFactory, bittorrentProtocol,
                torrentRegistry, bufferManager,
                dataReceiver, eventBus
        );

        connectionPool = new PeerConnectionPool(eventBus);


        Set<SocketChannelConnectionAcceptor> connectionAcceptors = new HashSet<>();
        InetSocketAddress localAddress = new InetSocketAddress(
                InetAddress.getLoopbackAddress(), acceptorPort);

        connectionAcceptors.add(new SocketChannelConnectionAcceptor(selector,
                peerConnectionFactory, localAddress));


        dhtPeerSourceFactory = new DHTPeerSourceFactory(dhtService);
        peerRegistry.addPeerSourceFactory(dhtPeerSourceFactory);


        this.dataWorker = new DataWorker(torrentRegistry,
                chunkVerifier, new BlockCache(torrentRegistry, eventBus));


        this.connectionSource = ConnectionSource.create(connectionAcceptors,
                peerConnectionFactory, connectionPool);
        this.messageDispatcher = new MessageDispatcher(connectionPool, torrentRegistry);
        this.messagingAgents = new HashSet<>();
    }

    public ContentStorage storage() {
        return storage;
    }

    public EventBus eventBus() {
        return eventBus;
    }

    void shutdown() {
        dhtService.shutdown();
        selector.close();
        peerRegistry.shutdown();
        connectionPool.shutdown();
        messageDispatcher.shutdown();
        dhtPeerSourceFactory.shutdown();
        peerExchangePeerSourceFactory.shutdown();
        dataWorker.shutdown();
        connectionSource.shutdown();
        dataReceiver.shutdown();
        torrentRegistry.shutdown();
    }

    void startup() {
        dhtService.startup();
        peerRegistry.startup();
        connectionPool.startup();
        messageDispatcher.startup();
        peerExchangePeerSourceFactory.startup();
        connectionSource.startup();
        dataReceiver.startup();
    }

}
