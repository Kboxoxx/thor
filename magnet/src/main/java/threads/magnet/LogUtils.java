package threads.magnet;


import java.util.Objects;

public interface LogUtils {
    String TAG = LogUtils.class.getSimpleName();

    @SuppressWarnings("SameReturnValue")
    static boolean isDebug() {
        return false;
    }

    static void debug(String tag, String message) {
        if (isDebug()) {
            System.out.println(tag + " " + message);
        }
    }

    static void warning(String tag, String message) {
        if (isDebug()) {
            System.out.println(tag + " " + Objects.requireNonNullElse(message, "No warning message defined"));
        }
    }

    static void info(String tag, String message) {
        if (isDebug()) {
            System.out.println(tag + " " + message);
        }
    }

    static void error(String tag, String message) {
        if (isDebug()) {
            System.err.println(tag + " " + message);
        }
    }

    static void error(String tag, String message, Throwable throwable) {
        if (isDebug()) {
            System.err.println(tag + " " + message);
            throwable.printStackTrace(System.err);
        }
    }

    static void error(String tag, Throwable throwable) {
        if (isDebug()) {
            System.err.println(tag + " " + throwable.getLocalizedMessage());
            throwable.printStackTrace(System.err);
        }
    }
}
