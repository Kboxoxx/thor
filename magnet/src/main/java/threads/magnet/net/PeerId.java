package threads.magnet.net;

import java.util.Arrays;
import java.util.Objects;

import threads.magnet.protocol.Protocols;

public record PeerId(byte[] bytes) {

    private static final int PEER_ID_LENGTH = 20;

    public static PeerId createPeerId(byte[] peerId) {
        Objects.requireNonNull(peerId);
        if (peerId.length != PEER_ID_LENGTH) {
            throw new RuntimeException("Illegal peer ID length: " + peerId.length);
        }
        return new PeerId(peerId);
    }

    /**
     * @return Standrad peer ID length in BitTorrent.
     * @since 1.0
     */
    public static int length() {
        return PEER_ID_LENGTH;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(bytes);
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null || !PeerId.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        return (obj == this) || Arrays.equals(bytes, ((PeerId) obj).bytes());
    }


    @Override
    public String toString() {
        return Protocols.toHex(bytes);
    }
}
