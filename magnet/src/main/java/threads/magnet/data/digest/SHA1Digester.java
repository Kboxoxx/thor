package threads.magnet.data.digest;

public interface SHA1Digester {

    static JavaSecurityDigester rolling(int step) {
        if (step <= 0) {
            throw new IllegalArgumentException("Invalid step: " + step);
        }
        return JavaSecurityDigester.createJavaSecurityDigester("SHA-1", step);
    }
}
