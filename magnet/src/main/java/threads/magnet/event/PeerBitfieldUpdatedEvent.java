package threads.magnet.event;

import threads.magnet.net.ConnectionKey;


public class PeerBitfieldUpdatedEvent extends BaseEvent {

    private final ConnectionKey connectionKey;


    PeerBitfieldUpdatedEvent(long id, long timestamp, ConnectionKey connectionKey) {
        super(id, timestamp);
        this.connectionKey = connectionKey;
    }


    @Override
    public String toString() {
        return "[" + this.getClass().getSimpleName() + "]  connection key {" + connectionKey + "}";
    }
}
