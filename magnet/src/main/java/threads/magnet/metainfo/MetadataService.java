package threads.magnet.metainfo;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import threads.magnet.LogUtils;
import threads.magnet.bencoding.BEInteger;
import threads.magnet.bencoding.BEList;
import threads.magnet.bencoding.BEMap;
import threads.magnet.bencoding.BEObject;
import threads.magnet.bencoding.BEParser;
import threads.magnet.bencoding.BEString;
import threads.magnet.bencoding.BEType;


public interface MetadataService {
    String TAG = MetadataService.class.getSimpleName();
    String INFOMAP_KEY = "info";
    String TORRENT_NAME_KEY = "name";
    String CHUNK_SIZE_KEY = "piece length";
    String CHUNK_HASHES_KEY = "pieces";
    String TORRENT_SIZE_KEY = "length";
    String FILES_KEY = "files";
    String FILE_SIZE_KEY = "length";
    String FILE_PATH_ELEMENTS_KEY = "path";
    String PRIVATE_KEY = "private";
    String CREATION_DATE_KEY = "creation date";
    String CREATED_BY_KEY = "created by";

    /**
     * Calculate SHA-1 digest of a byte array.
     *
     * @since 1.0
     */
    static byte[] getSha1Digest(byte[] bytes) {
        MessageDigest crypto;
        try {
            crypto = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Unexpected error", e);
        }
        return crypto.digest(bytes);
    }

    static Torrent buildTorrent(byte[] bs) {
        try (BEParser parser = BEParser.create(bs)) {
            if (parser.readType() != BEType.MAP) {
                throw new RuntimeException("Invalid metainfo format -- expected a map, got: "
                        + parser.readType().name().toLowerCase());
            }

            BEMap metadata = parser.readMap();
            BEMap infoDictionary;
            Map<String, BEObject> root = metadata.value();
            if (root.containsKey(INFOMAP_KEY)) {
                // standard BEP-3 format
                infoDictionary = (BEMap) root.get(INFOMAP_KEY);
            } else {
                // BEP-9 exchanged metadata (just the info dictionary)
                infoDictionary = metadata;
            }
            Objects.requireNonNull(infoDictionary);
            TorrentSource source = infoDictionary::content;


            TorrentId torrentId = TorrentId.createTorrentId(getSha1Digest(infoDictionary.content()));

            Map<String, BEObject> infoMap = infoDictionary.value();

            String name = "";
            if (infoMap.get(TORRENT_NAME_KEY) != null) {
                byte[] data = ((BEString) Objects.requireNonNull(
                        infoMap.get(TORRENT_NAME_KEY))).content();
                name = new String(data, StandardCharsets.UTF_8);
            }

            BigInteger chunkSize = ((BEInteger) Objects.requireNonNull(
                    infoMap.get(CHUNK_SIZE_KEY))).value();


            byte[] chunkHashes = ((BEString) Objects.requireNonNull(infoMap.get(CHUNK_HASHES_KEY))).content();


            List<TorrentFile> torrentFiles = new ArrayList<>();
            long size;
            if (infoMap.get(TORRENT_SIZE_KEY) != null) {
                BigInteger torrentSize = ((BEInteger) Objects.requireNonNull(
                        infoMap.get(TORRENT_SIZE_KEY))).value();
                size = torrentSize.longValue();

            } else {
                List<BEObject> files =
                        ((BEList) Objects.requireNonNull(infoMap.get(FILES_KEY))).value();
                BigInteger torrentSize = BigInteger.ZERO;
                for (BEObject object : files) {
                    BEMap file = (BEMap) object;
                    Map<String, BEObject> fileMap = file.value();

                    BigInteger fileSize = ((BEInteger) Objects.requireNonNull(
                            fileMap.get(FILE_SIZE_KEY))).value();

                    torrentSize = torrentSize.add(fileSize);

                    List<BEObject> objectList =
                            ((BEList) Objects.requireNonNull(
                                    fileMap.get(FILE_PATH_ELEMENTS_KEY))).value();

                    List<BEString> pathElements = new ArrayList<>();
                    objectList.forEach(beObject -> pathElements.add((BEString) beObject));

                    torrentFiles.add(TorrentFile.createTorrentFile(
                            fileSize.longValue(), pathElements.stream()
                                    .map(BEString::string)
                                    .collect(Collectors.toList())));
                }

                size = torrentSize.longValue();
            }
            boolean isPrivate = false;
            if (infoMap.get(PRIVATE_KEY) != null) {
                if (BigInteger.ONE.equals(
                        ((BEInteger) Objects.requireNonNull(infoMap.get(PRIVATE_KEY))).value())) {
                    isPrivate = true;
                }
            }
            long creationDate = System.currentTimeMillis();
            if (root.get(CREATION_DATE_KEY) != null) {

                // TODO: some torrents contain bogus values here (like 101010101010), which causes an exception
                try {
                    BigInteger epochMilli = ((BEInteger) Objects.requireNonNull(
                            root.get(CREATION_DATE_KEY))).value();
                    creationDate = epochMilli.intValue();
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
            String createdBy = "";

            if (root.get(CREATED_BY_KEY) != null) {
                createdBy = ((BEString) Objects.requireNonNull(
                        root.get(CREATED_BY_KEY))).string();
            }


            return Torrent.createTorrent(torrentId, name, source, torrentFiles,
                    chunkHashes, size, chunkSize.longValue(), isPrivate, creationDate, createdBy);

        } catch (Exception e) {
            throw new RuntimeException("Invalid metainfo format", e);
        }
    }
}
