package threads.magnet.processor;


import threads.magnet.data.Bitfield;
import threads.magnet.data.Storage;
import threads.magnet.magnet.MagnetUri;
import threads.magnet.metainfo.Torrent;
import threads.magnet.metainfo.TorrentId;
import threads.magnet.torrent.Assignments;
import threads.magnet.torrent.BitfieldCollectingConsumer;
import threads.magnet.torrent.MessageRouter;
import threads.magnet.torrent.PieceSelector;
import threads.magnet.torrent.PieceStatistics;
import threads.magnet.torrent.RarestFirstSelector;
import threads.magnet.torrent.TorrentSessionState;

public class Context {

    private final MagnetUri magnetUri;
    private final PieceSelector pieceSelector = RarestFirstSelector.randomizedRarest();
    private final Storage storage;
    private final TorrentId torrentId;

    private volatile Torrent torrent;
    private volatile TorrentSessionState state;
    private volatile MessageRouter router;
    private volatile Bitfield bitfield;
    private volatile Assignments assignments;
    private volatile PieceStatistics pieceStatistics;
    private volatile BitfieldCollectingConsumer bitfieldConsumer;


    public Context(MagnetUri magnetUri, Storage storage) {
        this.magnetUri = magnetUri;
        this.torrentId = magnetUri.getTorrentId();
        this.storage = storage;
    }

    public Context(Torrent torrent, Storage storage) {
        this.magnetUri = null;
        this.torrent = torrent;
        this.torrentId = torrent.torrentId();
        this.storage = storage;
    }

    PieceSelector getPieceSelector() {
        return pieceSelector;
    }

    public Storage getStorage() {
        return storage;
    }

    public Torrent getTorrent() {
        return torrent;
    }

    public void setTorrent(Torrent torrent) {
        this.torrent = torrent;
    }

    public TorrentSessionState getState() {
        return state;
    }

    public void setState(TorrentSessionState state) {
        this.state = state;
    }

    MessageRouter getRouter() {
        return router;
    }

    void setRouter(MessageRouter router) {
        this.router = router;
    }

    public Bitfield getBitfield() {
        return bitfield;
    }

    public void setBitfield(Bitfield bitfield) {
        this.bitfield = bitfield;
    }

    Assignments getAssignments() {
        return assignments;
    }

    void setAssignments(Assignments assignments) {
        this.assignments = assignments;
    }

    public PieceStatistics getPieceStatistics() {
        return pieceStatistics;
    }

    public void setPieceStatistics(PieceStatistics pieceStatistics) {
        this.pieceStatistics = pieceStatistics;
    }

    public BitfieldCollectingConsumer getBitfieldConsumer() {
        return bitfieldConsumer;
    }

    public void setBitfieldConsumer(BitfieldCollectingConsumer bitfieldConsumer) {
        this.bitfieldConsumer = bitfieldConsumer;
    }

    public TorrentId getTorrentId() {
        return torrentId;
    }

    public MagnetUri getMagnetUri() {
        return magnetUri;
    }

}
