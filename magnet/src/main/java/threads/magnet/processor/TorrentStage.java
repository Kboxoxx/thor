package threads.magnet.processor;

import threads.magnet.event.EventSink;
import threads.magnet.metainfo.TorrentId;
import threads.magnet.torrent.BitfieldCollectingConsumer;


public class TorrentStage extends TerminateOnErrorProcessingStage {
    private final EventSink eventSink;

    public TorrentStage(ProcessingStage next, EventSink eventSink) {
        super(next);
        this.eventSink = eventSink;
    }

    @Override
    protected void doExecute(Context context) {
        TorrentId torrentId = context.getTorrentId();

        // need to also receive Bitfields and Haves (without validation for the number of pieces...)
        BitfieldCollectingConsumer bitfieldConsumer = new BitfieldCollectingConsumer();
        context.getRouter().registerMessagingAgent(bitfieldConsumer);

        eventSink.fireMetadataAvailable(torrentId);

        context.setBitfieldConsumer(bitfieldConsumer);
    }


    @Override
    public ProcessingEvent after() {
        return ProcessingEvent.TORRENT_FETCHED;
    }
}
