package threads.magnet.processor;

import threads.magnet.Runtime;

public interface TorrentProcessorFactory {


    /**
     * @noinspection ExtractMethodRecommender
     */
    static ChainProcessor createMagnetProcessor(Runtime runtime) {


        ProcessingStage stage4 = new ProcessTorrentStage(
                runtime.torrentRegistry, runtime.eventBus());

        ProcessingStage stage3 = new ChooseFilesStage(stage4, runtime.torrentRegistry);

        ProcessingStage stage2 = new InitializeMagnetTorrentProcessingStage(stage3,
                runtime.connectionPool,
                runtime.torrentRegistry,
                runtime.dataWorker,
                runtime.bufferedPieceRegistry,
                runtime.eventBus());


        ProcessingStage stage1 = new FetchMetadataStage(stage2,
                runtime.torrentRegistry,
                runtime.peerRegistry,
                runtime.eventBus());


        ProcessingStage stage0 = new CreateSessionStage(stage1,
                runtime.torrentRegistry,
                runtime.eventBus(),
                runtime.connectionSource,
                runtime.messageDispatcher,
                runtime.messagingAgents);

        return new ChainProcessor(stage0, new TorrentContextFinalizer(runtime.torrentRegistry, runtime.eventBus()));
    }

    /**
     * @noinspection ExtractMethodRecommender
     */
    static ChainProcessor createTorrentProcessor(Runtime runtime) {

        ProcessingStage stage4 = new ProcessTorrentStage(
                runtime.torrentRegistry, runtime.eventBus());

        ProcessingStage stage3 = new ChooseFilesStage(stage4, runtime.torrentRegistry);

        ProcessingStage stage2 = new InitializeTorrentProcessingStage(stage3,
                runtime.connectionPool,
                runtime.torrentRegistry,
                runtime.dataWorker,
                runtime.bufferedPieceRegistry,
                runtime.eventBus());


        ProcessingStage stage1 = new TorrentStage(stage2,
                runtime.eventBus());


        ProcessingStage stage0 = new CreateSessionStage(stage1,
                runtime.torrentRegistry,
                runtime.eventBus(),
                runtime.connectionSource,
                runtime.messageDispatcher,
                runtime.messagingAgents);

        return new ChainProcessor(stage0, new TorrentContextFinalizer(
                runtime.torrentRegistry, runtime.eventBus()));
    }
}
