package threads.magnet.kad.tasks;


import threads.magnet.kad.Key;
import threads.magnet.kad.Node;
import threads.magnet.kad.RPCServer;

public abstract class IteratingTask extends TargetedTask {

    final ClosestSet closest;
    final IterativeLookupCandidates todo;

    IteratingTask(Key target, RPCServer srv, Node node) {
        super(target, srv, node);
        todo = new IterativeLookupCandidates(target, node.getDHT().getMismatchDetector());
        todo.setNonReachableCache(node.getDHT().getUnreachableCache());
        todo.setSpamThrottle(node.getDHT().getServerManager().getOutgoingRequestThrottle());
        closest = new ClosestSet(target);
    }

    @Override
    public int getTodoCount() {
        return (int) todo.allCand().filter(todo.lookupFilter).count();
    }


}
