package threads.magnet.bencoding;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * BEncoded string.
 *
 * @since 1.0
 */
public record BEString(byte[] content) implements BEObject {

    @Override
    public void writeTo(OutputStream out) throws IOException {
        BEEncoder.encode(this, out);
    }

    public String string() {
        return new String(content, StandardCharsets.UTF_8);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(content);
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof BEString)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        return Arrays.equals(content, ((BEString) obj).content());
    }


    @Override
    public String toString() {
        return string();
    }
}
