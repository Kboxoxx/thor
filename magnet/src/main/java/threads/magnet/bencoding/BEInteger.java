package threads.magnet.bencoding;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;

/**
 * BEncoded integer.
 *
 * <p>«BEP-3: The BitTorrent Protocol Specification» defines integers
 * as unsigned numeric values with an arbitrary number of digits.
 *
 * @since 1.0
 */
public record BEInteger(BigInteger value) implements BEObject {

    @Override
    public void writeTo(OutputStream out) throws IOException {
        BEEncoder.encode(this, out);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof BEInteger)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        return value.equals(((BEInteger) obj).value);
    }


    @Override
    public String toString() {
        return value.toString(10);
    }
}
