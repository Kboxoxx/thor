package threads.magnet;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

import threads.magnet.metainfo.MetadataService;
import threads.magnet.metainfo.Torrent;

public class TorrentDownloadTest {
    private static final String TAG = TorrentDownloadTest.class.getSimpleName();
    final String path = "src/test/resources";

    @Test
    public void downloadTest() throws Exception {

        File resources = new File(path);

        File file = new File(resources, "lubuntu-24.04-desktop-amd64.iso.torrent");

        long now = System.currentTimeMillis();

        try (InputStream inputStream = new FileInputStream(file)) {
            Torrent torrent = MetadataService.buildTorrent(inputStream.readAllBytes());
            Objects.requireNonNull(torrent);


            String property = System.getProperty("java.io.tmpdir");
            File dataDir = new File(property);
            if (!dataDir.exists()) {
                throw new IOException("Tmpdir does not exists");
            }

            File root = new File(dataDir, torrent.name());
            if (!torrent.singleFile()) {
                if (!root.exists()) {
                    boolean success = root.mkdir();
                    assertTrue(success);
                }
                dataDir = root;
            }

            Client client = Client.create(dataDir, torrent);

            // now start torrent download
            client.start((torrentSessionState) -> {

                long completePieces = torrentSessionState.getPiecesComplete();
                long totalPieces = torrentSessionState.getPiecesTotal();
                int progress = (int) ((completePieces * 100.0f) / totalPieces);

                LogUtils.error(TAG, "progress : " + progress +
                        " pieces : " + completePieces + "/" + totalPieces);


                if ((now + 10000) < System.currentTimeMillis()) { // 10 sec, stop
                    try {
                        client.stop();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            }, 1000);
        }
    }
}
