package tech.lp2p.cert;

final class DERGraphicString extends ASN1GraphicString {
    DERGraphicString(byte[] contents) {
        super(contents);
    }
}
