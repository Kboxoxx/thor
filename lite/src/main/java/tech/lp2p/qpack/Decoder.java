package tech.lp2p.qpack;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.nio.charset.StandardCharsets;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public final class Decoder {


    public static final int PREFIX_LENGTH = 3;

    public static List<Map.Entry<String, String>> decodeStream(InputStream inputStream) throws IOException {
        PushbackInputStream pushbackInputStream = new PushbackInputStream(inputStream, 16);
        List<Map.Entry<String, String>> headers = new ArrayList<>();

        // https://tools.ietf.org/html/draft-ietf-quic-qpack-07#section-4.5.1
        // "Header Block Prefix"
        parsePrefixedInteger(8, pushbackInputStream);
        parsePrefixedInteger(7, pushbackInputStream);

        int instruction = pushbackInputStream.read();
        pushbackInputStream.unread(instruction);
        while (instruction >= 0) {  // EOF returns -1
            Map.Entry<String, String> entry;
            if ((instruction & 0x80) == 0x80) {
                entry = parseIndexedHeaderField(pushbackInputStream);
            } else if ((instruction & 0xc0) == 0x40) {
                entry = parseLiteralHeaderFieldWithNameReference(pushbackInputStream);
            } else if ((instruction & 0xe0) == 0x20) {
                entry = parseLiteralHeaderFieldWithoutNameReference(pushbackInputStream);
            } else {
                throw new IllegalStateException("Error: unknown instruction: " + instruction);
            }

            if (entry != null) {
                headers.add(entry);
            }
            instruction = pushbackInputStream.read();
            pushbackInputStream.unread(instruction);
        }

        return headers;
    }

    // https://tools.ietf.org/html/draft-ietf-quic-qpack-07#section-4.1.1
    // "The prefixed integer from Section 5.1 of [RFC7541] is used heavily
    //   throughout this document.  The format from [RFC7541] is used
    //   unmodified.  QPACK implementations MUST be able to decode integers up
    //   to 62 bits long."
    private static long parsePrefixedInteger(int prefixLength, InputStream input) throws IOException {
        int maxPrefix = (int) (Math.pow(2, prefixLength) - 1);
        int initialValue = read(input) & maxPrefix;
        if (initialValue < maxPrefix) {
            return initialValue;
        }

        long value = initialValue;
        int factor = 0;
        byte next;
        do {
            next = read(input);
            value += ((long) (next & 0x7f) << factor);
            factor += 7;
        }
        while ((next & 0x80) == 0x80);

        return value;
    }

    // https://tools.ietf.org/html/draft-ietf-quic-qpack-07#section-4.5.2
    private static Map.Entry<String, String> parseIndexedHeaderField(PushbackInputStream inputStream) throws IOException {
        byte first = read(inputStream);
        inputStream.unread(first);
        boolean inStaticTable = (first & 0x40) == 0x40;
        int index = (int) parsePrefixedInteger(6, inputStream);

        if (inStaticTable) {
            return StaticTable.lookupNameValue(index);
        } else {
            return null;
        }
    }


    // https://tools.ietf.org/html/draft-ietf-quic-qpack-07#section-4.5.4
    private static Map.Entry<String, String> parseLiteralHeaderFieldWithNameReference(PushbackInputStream inputStream) throws IOException {
        byte first = read((inputStream));
        inputStream.unread(first);
        boolean inStaticTable = (first & 0x10) == 0x10;
        int nameIndex = (int) parsePrefixedInteger(4, inputStream);
        if (!inStaticTable) {
            throw new IllegalStateException("non static ref in parseLiteralHeaderFieldWithNameReference");
        }
        String name = StaticTable.lookupName(nameIndex);

        String value = parseStringValue(inputStream);

        return new AbstractMap.SimpleEntry<>(name, value);
    }

    // https://tools.ietf.org/html/draft-ietf-quic-qpack-07#section-4.5.6
    private static Map.Entry<String, String> parseLiteralHeaderFieldWithoutNameReference(PushbackInputStream inputStream) throws IOException {
        String name = parseStringValue(PREFIX_LENGTH, inputStream);
        String value = parseStringValue(inputStream);
        return new AbstractMap.SimpleEntry<>(name, value);
    }

    private static String parseStringValue(PushbackInputStream inputStream) throws IOException {
        byte firstByte = read(inputStream);
        inputStream.unread(firstByte);
        boolean huffmanEncoded = (firstByte & 0x80) == 0x80;
        int valueLength = (int) parsePrefixedInteger(7, inputStream);
        byte[] rawValue = new byte[valueLength];
        readExact(inputStream, rawValue);
        return huffmanEncoded ? Huffman.decode(rawValue) : new String(rawValue, StandardCharsets.ISO_8859_1);
    }

    private static String parseStringValue(@SuppressWarnings("SameParameterValue") int prefixLength,
                                           PushbackInputStream inputStream) throws IOException {
        int huffmanFlagMask = switch (prefixLength) {
            case 3 -> 0x08;
            case 5 -> 0x20;
            default ->
                    throw new IllegalStateException("no huffman flag mask for prefix " + prefixLength);
        };
        byte firstByte = read(inputStream);
        inputStream.unread(firstByte);
        boolean huffmanEncoded = (firstByte & huffmanFlagMask) == huffmanFlagMask;
        int length = (int) parsePrefixedInteger(prefixLength, inputStream);
        byte[] rawBytes = new byte[length];
        readExact(inputStream, rawBytes);
        return huffmanEncoded ? Huffman.decode(rawBytes) : new String(rawBytes, StandardCharsets.ISO_8859_1);
    }

    private static byte read(InputStream stream) throws IOException {
        int value = stream.read();
        if (value == -1) {
            throw new EOFException();
        } else {
            return (byte) value;
        }
    }

    private static void readExact(InputStream stream, byte[] data) throws IOException {
        int read = stream.readNBytes(data, 0, data.length);
        if (read != data.length) {
            throw new EOFException();
        }
    }
}
