package tech.lp2p.qpack;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

/**
 * Decoder for Huffman code as specified by <a href="https://datatracker.ietf.org/doc/html/rfc7541#appendix-B">...</a>.
 * The decoding is implemented by nested lookup tables, where each lookup key is 8 bits. As the given Huffman code
 * has a maximum code length of 30 bits, the maximum nesting is 4 levels.
 * For example, the code for '\n' (decimal 10) is |11111111|11111111|11111111|111100, this requires four lookups: the
 * 1st, 2nd and 3rd point to the table that contains an entry for 0b111100xx.
 * As most codes are not an exact multiple of 8, the lookup must take into account the "don't care" bits. To simplify
 * the lookup, all values for the don't cares are included in the table.
 * For example, the code for '%' is 0b010101 (6 bits), and the table contains 4 entries (0b01010100, 0b01010101,
 * 0b01010110, 0b01010111), all pointing to the same table entry for '%'.
 */
public final class Huffman {

    private static final String huffmancode = """
            |11111111|11000                             1ff8  [13]
            |11111111|11111111|1011000                7fffd8  [23]
            |11111111|11111111|11111110|0010         fffffe2  [28]
            |11111111|11111111|11111110|0011         fffffe3  [28]
            |11111111|11111111|11111110|0100         fffffe4  [28]
            |11111111|11111111|11111110|0101         fffffe5  [28]
            |11111111|11111111|11111110|0110         fffffe6  [28]
            |11111111|11111111|11111110|0111         fffffe7  [28]
            |11111111|11111111|11111110|1000         fffffe8  [28]
            |11111111|11111111|11101010               ffffea  [24]
            |11111111|11111111|11111111|111100      3ffffffc  [30]
            |11111111|11111111|11111110|1001         fffffe9  [28]
            |11111111|11111111|11111110|1010         fffffea  [28]
            |11111111|11111111|11111111|111101      3ffffffd  [30]
            |11111111|11111111|11111110|1011         fffffeb  [28]
            |11111111|11111111|11111110|1100         fffffec  [28]
            |11111111|11111111|11111110|1101         fffffed  [28]
            |11111111|11111111|11111110|1110         fffffee  [28]
            |11111111|11111111|11111110|1111         fffffef  [28]
            |11111111|11111111|11111111|0000         ffffff0  [28]
            |11111111|11111111|11111111|0001         ffffff1  [28]
            |11111111|11111111|11111111|0010         ffffff2  [28]
            |11111111|11111111|11111111|111110      3ffffffe  [30]
            |11111111|11111111|11111111|0011         ffffff3  [28]
            |11111111|11111111|11111111|0100         ffffff4  [28]
            |11111111|11111111|11111111|0101         ffffff5  [28]
            |11111111|11111111|11111111|0110         ffffff6  [28]
            |11111111|11111111|11111111|0111         ffffff7  [28]
            |11111111|11111111|11111111|1000         ffffff8  [28]
            |11111111|11111111|11111111|1001         ffffff9  [28]
            |11111111|11111111|11111111|1010         ffffffa  [28]
            |11111111|11111111|11111111|1011         ffffffb  [28]
            |010100                                       14  [ 6]
            |11111110|00                                 3f8  [10]
            |11111110|01                                 3f9  [10]
            |11111111|1010                               ffa  [12]
            |11111111|11001                             1ff9  [13]
            |010101                                       15  [ 6]
            |11111000                                     f8  [ 8]
            |11111111|010                                7fa  [11]
            |11111110|10                                 3fa  [10]
            |11111110|11                                 3fb  [10]
            |11111001                                     f9  [ 8]
            |11111111|011                                7fb  [11]
            |11111010                                     fa  [ 8]
            |010110                                       16  [ 6]
            |010111                                       17  [ 6]
            |011000                                       18  [ 6]""";
    private static final int KEY_SIZE = 8;
    public static final int TABLE_SIZE = (int) Math.pow(2, KEY_SIZE);
    private static final TableEntry[] lookupTable;

    static {
        lookupTable = new TableEntry[TABLE_SIZE];
        Map<String, Integer> codeTable = new HashMap<>();
        try {
            InputStream resourceAsStream = new ByteArrayInputStream(huffmancode.getBytes());
            BufferedReader reader = new BufferedReader(new InputStreamReader(resourceAsStream));

            String line;
            int index = 0;
            line = reader.readLine();
            while (line != null) {
                codeTable.put(extractBitPattern(line), index);
                index++;
                line = reader.readLine();
            }
        } catch (IOException e) {
            // Impossible when library is build correctly.
            throw new RuntimeException("Corrupt library, missing internal resource.");
        }

        codeTable.forEach((key, value) -> addToLookupTable(lookupTable, key, value));
    }

    /**
     * Decodes a string of Huffman encoded bytes.
     */
    public static String decode(byte[] bytes) {
        StringBuilder string = new StringBuilder(bytes.length);
        BitBuffer buffer = new BitBuffer(bytes);
        while (buffer.hasRemaining()) {
            TableEntry symbol = lookup(lookupTable, buffer);
            if (symbol != null) {
                string.append(symbol.character);
            }
        }
        return string.toString();
    }

    /**
     * Performs (recursive) symbol lookup for the first character in the buffer with the given table.
     *
     * @param table  the lookup table used for the lookup (is an argument to allow for recursion)
     * @param buffer the buffer containing the bits that will be decoded.
     * @return the symbol represented by the code or null if there is no match
     */
    private static TableEntry lookup(TableEntry[] table, BitBuffer buffer) {
        int key = (int) buffer.peek() & 0xff;
        TableEntry mappedSymbol = table[key];
        if (mappedSymbol.isSymbol()) {
            buffer.shift(mappedSymbol.codeLength);
            return mappedSymbol;
        } else if (buffer.remaining() >= KEY_SIZE) {
            if (mappedSymbol.subTable == null) {
                throw new IllegalStateException("Missing subtable!");
            }
            buffer.shift(KEY_SIZE);
            return lookup(mappedSymbol.subTable, buffer);
        } else {
            // End of buffer contains some non-character bits (probably just 1's), as total length of character encodings
            // in the buffer is not a multiple of 8.
            buffer.shift(buffer.remaining());
            return null;
        }
    }

    /**
     * Adds a symbol with the given code to the lookup table recursively. If the code length is larger than 8, the
     * symbol will not be added to the table directly, but indirectly via one or more linked (nested) tables.
     *
     * @param table       the table to add the symbol to
     * @param code        the code to add as a String of 1's and 0's
     * @param symbolValue the symbol symbolValue to add (integer representation)
     */
    private static void addToLookupTable(TableEntry[] table, String code, int symbolValue) {
        if (code.length() <= KEY_SIZE) {
            int codeValue = parseBits(code, code.length());
            TableEntry mappedSymbol = new TableEntry(symbolValue, code.length());
            generateCodeKeys(codeValue, code.length())
                    .forEach(key -> table[key] = mappedSymbol);
        } else {
            int prefixCode = parseBits(code, KEY_SIZE);
            String suffix = code.substring(KEY_SIZE);
            if (table[prefixCode] == null) {
                table[prefixCode] = new TableEntry();
            }
            addToLookupTable(table[prefixCode].subTable, suffix, symbolValue);
        }
    }

    /**
     * Generates keys for the given code. As a code can be less than 8 bits, keys must be generated for all values for
     * the LSB's that are not part of the key. For example, if the code is 0b001100 (6 bits), keys are generated for all
     * values of the 2 least significant bits: 0b00110000, 0b00110001, 0b00110010, 0b00110011
     */
    private static IntStream generateCodeKeys(int codeValue, int bits) {
        int baseValue = codeValue << (KEY_SIZE - bits);
        int maxAddition = (int) Math.pow(2, KEY_SIZE - bits);
        return IntStream.range(0, maxAddition).map(addition -> baseValue | addition);
    }

    private static int parseBits(String code, int count) {
        return Integer.parseInt(code.substring(0, count), 2);
    }

    private static String extractBitPattern(String line) {
        int firstSpace = line.indexOf(' ');
        return line.substring(0, firstSpace).replaceAll("\\|", "");
    }

    private static class TableEntry {

        final char character;
        final int codeLength;
        final TableEntry[] subTable;

        public TableEntry(int character, int codeLength) {
            this.character = (char) character;
            this.codeLength = codeLength;
            this.subTable = null;
        }

        public TableEntry() {
            this.character = 0;
            this.codeLength = 0;
            subTable = new TableEntry[(int) Math.pow(2, KEY_SIZE)];
        }

        boolean isSymbol() {
            return subTable == null;
        }
    }
}
