package tech.lp2p.qpack;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// https://tools.ietf.org/html/draft-ietf-quic-qpack-07#section-3.1
// "The static table consists of a predefined static list of header
//   fields, each of which has a fixed index over time."
// "All entries in the static table have a name and a value.  However,
//   values can be empty (that is, have a length of 0)."
public final class StaticTable {

    private static final String statictable = """
               | 0    | :authority                  |                              |
               |      |                             |                              |
               | 1    | :path                       | /                            |
               |      |                             |                              |
               | 2    | age                         | 0                            |
               |      |                             |                              |
               | 3    | content-disposition         |                              |
               |      |                             |                              |
               | 4    | content-length              | 0                            |
               |      |                             |                              |
               | 5    | cookie                      |                              |
               |      |                             |                              |
               | 6    | date                        |                              |
               |      |                             |                              |
               | 7    | etag                        |                              |
               |      |                             |                              |
               | 8    | if-modified-since           |                              |
               |      |                             |                              |
               | 9    | if-none-match               |                              |
               |      |                             |                              |
               | 10   | last-modified               |                              |
               |      |                             |                              |
               | 11   | link                        |                              |
               |      |                             |                              |
               | 12   | location                    |                              |
               |      |                             |                              |
               | 13   | referer                     |                              |
               |      |                             |                              |
               | 14   | set-cookie                  |                              |
               |      |                             |                              |
               | 15   | :method                     | CONNECT                      |
               |      |                             |                              |
               | 16   | :method                     | DELETE                       |
               |      |                             |                              |
               | 17   | :method                     | GET                          |
               |      |                             |                              |
               | 18   | :method                     | HEAD                         |
               |      |                             |                              |
               | 19   | :method                     | OPTIONS                      |
               |      |                             |                              |
               | 20   | :method                     | POST                         |
               |      |                             |                              |
               | 21   | :method                     | PUT                          |
               |      |                             |                              |
               | 22   | :scheme                     | http                         |
               |      |                             |                              |
               | 23   | :scheme                     | https                        |
               |      |                             |                              |
               | 24   | :status                     | 103                          |
               |      |                             |                              |
               | 25   | :status                     | 200                          |
               |      |                             |                              |
               | 26   | :status                     | 304                          |
               |      |                             |                              |
               | 27   | :status                     | 404                          |
               |      |                             |                              |
               | 28   | :status                     | 503                          |
               |      |                             |                              |
               | 29   | accept                      | */*                          |
               |      |                             |                              |
               | 30   | accept                      | application/dns-message      |
               |      |                             |                              |
               | 31   | accept-encoding             | gzip, deflate, br            |
               |      |                             |                              |
               | 32   | accept-ranges               | bytes                        |
               |      |                             |                              |
               | 33   | access-control-allow-       | cache-control                |
               |      | headers                     |                              |
               |      |                             |                              |
               | 34   | access-control-allow-       | content-type                 |
               |      | headers                     |                              |
               |      |                             |                              |
               | 35   | access-control-allow-origin | *                            |
               |      |                             |                              |
               | 36   | cache-control               | max-age=0                    |
               |      |                             |                              |
               | 37   | cache-control               | max-age=2592000              |
               |      |                             |                              |
               | 38   | cache-control               | max-age=604800               |
               |      |                             |                              |
               | 39   | cache-control               | no-cache                     |
               |      |                             |                              |
               | 40   | cache-control               | no-store                     |
               |      |                             |                              |
               | 41   | cache-control               | public, max-age=31536000     |
               |      |                             |                              |
               | 42   | content-encoding            | br                           |
               |      |                             |                              |
               | 43   | content-encoding            | gzip                         |
               |      |                             |                              |
               | 44   | content-type                | application/dns-message      |
               |      |                             |                              |
               | 45   | content-type                | application/javascript       |
               |      |                             |                              |
               | 46   | content-type                | application/json             |
               |      |                             |                              |
               | 47   | content-type                | application/x-www-form-      |
               |      |                             | urlencoded                   |
               |      |                             |                              |
               | 48   | content-type                | image/gif                    |
               |      |                             |                              |
               | 49   | content-type                | image/jpeg                   |
               |      |                             |                              |
               | 50   | content-type                | image/png                    |
               |      |                             |                              |
               | 51   | content-type                | text/css                     |
               |      |                             |                              |
               | 52   | content-type                | text/html; charset=utf-8     |
               |      |                             |                              |
               | 53   | content-type                | text/plain                   |
               |      |                             |                              |
               | 54   | content-type                | text/plain;charset=utf-8     |
               |      |                             |                              |
               | 55   | range                       | bytes=0-                     |
               |      |                             |                              |
               | 56   | strict-transport-security   | max-age=31536000             |
               |      |                             |                              |
               | 57   | strict-transport-security   | max-age=31536000;            |
               |      |                             | includesubdomains            |
               |      |                             |                              |
               | 58   | strict-transport-security   | max-age=31536000;            |
               |      |                             | includesubdomains; preload   |
               |      |                             |                              |
               | 59   | vary                        | accept-encoding              |
               |      |                             |                              |
               | 60   | vary                        | origin                       |
               |      |                             |                              |
               | 61   | x-content-type-options      | nosniff                      |
               |      |                             |                              |
               | 62   | x-xss-protection            | 1; mode=block                |
               |      |                             |                              |
               | 63   | :status                     | 100                          |
               |      |                             |                              |
               | 64   | :status                     | 204                          |
               |      |                             |                              |
               | 65   | :status                     | 206                          |
               |      |                             |                              |
               | 66   | :status                     | 302                          |
               |      |                             |                              |
               | 67   | :status                     | 400                          |
               |      |                             |                              |
               | 68   | :status                     | 403                          |
               |      |                             |                              |
               | 69   | :status                     | 421                          |
               |      |                             |                              |
               | 70   | :status                     | 425                          |
               |      |                             |                              |
               | 71   | :status                     | 500                          |
               |      |                             |                              |
               | 72   | accept-language             |                              |
               |      |                             |                              |
               | 73   | access-control-allow-       | FALSE                        |
               |      | credentials                 |                              |
               |      |                             |                              |
               | 74   | access-control-allow-       | TRUE                         |
               |      | credentials                 |                              |
               |      |                             |                              |
               | 75   | access-control-allow-       | *                            |
               |      | headers                     |                              |
               |      |                             |                              |
               | 76   | access-control-allow-       | get                          |
               |      | methods                     |                              |
               |      |                             |                              |
               | 77   | access-control-allow-       | get, post, options           |
               |      | methods                     |                              |
               |      |                             |                              |
               | 78   | access-control-allow-       | options                      |
               |      | methods                     |                              |
               |      |                             |                              |
               | 79   | access-control-expose-      | content-length               |
               |      | headers                     |                              |
               |      |                             |                              |
               | 80   | access-control-request-     | content-type                 |
               |      | headers                     |                              |
               |      |                             |                              |
               | 81   | access-control-request-     | get                          |
               |      | method                      |                              |
               |      |                             |                              |
               | 82   | access-control-request-     | post                         |
               |      | method                      |                              |
               |      |                             |                              |
               | 83   | alt-svc                     | clear                        |
               |      |                             |                              |
               | 84   | authorization               |                              |
               |      |                             |                              |
               | 85   | content-security-policy     | script-src 'none'; object-   |
               |      |                             | src 'none'; base-uri 'none'  |
               |      |                             |                              |
               | 86   | early-data                  | 1                            |
               |      |                             |                              |
               | 87   | expect-ct                   |                              |
               |      |                             |                              |
               | 88   | forwarded                   |                              |
               |      |                             |                              |
               | 89   | if-range                    |                              |
               |      |                             |                              |
               | 90   | origin                      |                              |
               |      |                             |                              |
               | 91   | purpose                     | prefetch                     |
               |      |                             |                              |
               | 92   | server                      |                              |
               |      |                             |                              |
               | 93   | timing-allow-origin         | *                            |
               |      |                             |                              |
               | 94   | upgrade-insecure-requests   | 1                            |
               |      |                             |                              |
               | 95   | user-agent                  |                              |
               |      |                             |                              |
               | 96   | x-forwarded-for             |                              |
               |      |                             |                              |
               | 97   | x-frame-options             | deny                         |
               |      |                             |                              |
               | 98   | x-frame-options             | sameorigin                   |\
            """;
    private final static String[] names = new String[100];
    private final static String[] values = new String[100];

    static {
        Pattern empty = Pattern.compile("\\|\\s+\\|\\s+\\|\\s+\\|");
        Pattern nameOnly = Pattern.compile("\\|\\s*(\\d+)\\s*" + "\\|\\s*([^|]+)\\s*" + "\\|\\s+\\|");
        Pattern nameValue = Pattern.compile("\\|\\s*(\\d+)\\s*" + "\\|\\s*([^|]+)\\s*" + "\\|\\s*([^|]+)\\s*\\|");
        Pattern continuation = Pattern.compile("\\|\\s+" + "\\|\\s*([^|]*)\\s*" + "\\|\\s*([^|]*)\\s*\\|");

        try {
            InputStream resourceAsStream = new ByteArrayInputStream(statictable.getBytes());
            BufferedReader reader = new BufferedReader(new InputStreamReader(resourceAsStream));

            String line;
            int lastIndex = 0;
            line = reader.readLine();
            while (line != null) {
                line = line.trim();
                if (!empty.matcher(line).matches()) {
                    if (nameOnly.matcher(line).matches()) {
                        Matcher m = nameOnly.matcher(line);
                        //noinspection ResultOfMethodCallIgnored
                        m.matches();
                        names[Integer.parseInt(m.group(1).trim())] = m.group(2).trim();
                        values[Integer.parseInt(m.group(1).trim())] = "";
                        lastIndex = Integer.parseInt(m.group(1).trim());
                    } else if (nameValue.matcher(line).matches()) {
                        Matcher m = nameValue.matcher(line);
                        //noinspection ResultOfMethodCallIgnored
                        m.matches();
                        names[Integer.parseInt(m.group(1).trim())] = m.group(2).trim();
                        values[Integer.parseInt(m.group(1).trim())] = m.group(3).trim();
                        lastIndex = Integer.parseInt(m.group(1).trim());
                    } else if (continuation.matcher(line).matches()) {
                        Matcher m = continuation.matcher(line);
                        //noinspection ResultOfMethodCallIgnored
                        m.matches();
                        String namePart = m.group(1).trim();
                        String valuePart = m.group(2).trim();
                        if (!namePart.isBlank()) {
                            names[lastIndex] = names[lastIndex] + namePart;
                        }
                        if (!valuePart.isBlank()) {
                            values[lastIndex] = values[lastIndex] + valuePart;
                        }
                    } else {
                        throw new RuntimeException("Internal error: parsing static table definition failed.");
                    }
                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            // Impossible when library is build correctly.
            throw new RuntimeException("Corrupt library, missing internal resource.");
        }
    }

    public static String lookupName(int index) {
        String result = names[index];
        if (result == null) {
            throw new IllegalStateException("HTTP_QPACK_DECOMPRESSION_FAILED");
        }
        return result;
    }

    public static int findByNameAndValue(String name, String value) {
        Objects.requireNonNull(name);
        Objects.requireNonNull(value);
        int firstMatch = -1;
        for (int i = 0; i < names.length; i++) {
            if (name.equals(names[i])) {
                if (firstMatch < 0) {
                    firstMatch = i;
                }
                if (value.equals(values[i])) {
                    return i;
                }
            }
        }
        return firstMatch;
    }

    public static Map.Entry<String, String> lookupNameValue(int index) {
        if (names[index] != null) {
            return new AbstractMap.SimpleImmutableEntry<>(names[index], values[index]);
        } else {
            throw new IllegalStateException("HTTP_QPACK_DECOMPRESSION_FAILED");
        }
    }
}
