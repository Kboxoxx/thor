package tech.lp2p.relay;


import java.net.ConnectException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import tech.lp2p.Lite;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Key;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.dht.DhtService;
import tech.lp2p.utils.Utils;

public interface RelayDialer {


    static Info hopConnect(Lite host, PeerId peerId,
                           Consumer<Peeraddr> tries, Consumer<Peeraddr> failures,
                           int timeout) throws ConnectException {


        AtomicReference<Info> done = new AtomicReference<>();

        Key key = peerId.createKey();

        ExecutorService service = Executors.newSingleThreadExecutor();
        try {
            service.execute(() -> DhtService.findClosestPeers(host, key, peeraddr -> {

                tries.accept(peeraddr);

                try {
                    Connection connection = RelayService.hopConnect(host, peeraddr, peerId);
                    // maybe a hop connect was already set before
                    if (done.compareAndSet(null,
                            new Info(connection, peeraddr))) {
                        service.shutdownNow();
                    } else {
                        // this could only happen in very rare cases
                        connection.close();
                        service.shutdownNow();
                    }
                } catch (Throwable throwable) {
                    failures.accept(peeraddr);
                    Utils.debug("RelayDialer hopConnect failure " + throwable.getMessage());
                }
            }));

            service.shutdown();
            if (!service.awaitTermination(timeout, TimeUnit.SECONDS)) {
                service.shutdownNow();
            }
        } catch (InterruptedException interruptedException) {
            service.shutdownNow();
        }

        Info info = done.get();
        if (info == null) {
            throw new ConnectException("No hop connection established");
        }
        return info;
    }

    record Info(Connection connection, Peeraddr relay) {
    }
}
