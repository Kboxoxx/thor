package tech.lp2p.http3;

import java.io.InputStream;
import java.util.concurrent.CountDownLatch;
import java.util.function.Consumer;

import tech.lp2p.core.Status;
import tech.lp2p.quic.Connection;
import tech.lp2p.quic.Stream;


public class Http3ClientConnection extends Http3Connection {
    private final CountDownLatch settingsFrameReceived;
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private InputStream serverPushStream;
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private boolean settingsEnableConnectProtocol;
    private Consumer<Stream> bidirectionalStreamHandler;

    public Http3ClientConnection(Connection connection) {
        super(connection);

        settingsFrameReceived = new CountDownLatch(1);
    }

    @SuppressWarnings("unused")
    public void registerBidirectionalStreamHandler(Consumer<Stream> streamHandler) {
        bidirectionalStreamHandler = streamHandler;
    }

    @Override
    protected void registerStandardStreamHandlers() {
        super.registerStandardStreamHandlers();
        unidirectionalStreamHandler.put((long) STREAM_TYPE_PUSH_STREAM,
                this::setServerPushStream);
    }

    private void setServerPushStream(InputStream stream) {
        serverPushStream = stream;
    }

    @Override
    protected SettingsFrame processControlStream(InputStream controlStream) {
        SettingsFrame settingsFrame = super.processControlStream(controlStream);
        if (settingsFrame != null) {
            // Read settings that only apply to client role.
            settingsEnableConnectProtocol = settingsFrame.isSettingsEnableConnectProtocol();
        }

        // Unblock who is waiting for settings frame to be received.
        settingsFrameReceived.countDown();

        return settingsFrame;
    }


    @Override
    protected void handleBidirectionalStream(Stream stream, byte[] frame) {
        if (bidirectionalStreamHandler != null) {
            bidirectionalStreamHandler.accept(stream);
        } else {
            // https://www.rfc-editor.org/rfc/rfc9114.html#name-bidirectional-streams
            // "Clients MUST treat receipt of a server-initiated bidirectional stream as a
            //  connection error of type H3_STREAM_CREATION_ERROR unless such an extension has
            //  been negotiated."
            connectionError(Status.H3_STREAM_CREATION_ERROR);
        }
    }
}