package tech.lp2p.http3;

/**
 * A HTTP3 frame with unknown type.
 */
public final class UnknownFrame implements Http3Frame {
}
