package tech.lp2p.http3;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import tech.lp2p.core.Method;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Request;
import tech.lp2p.core.RequestHandler;
import tech.lp2p.core.Response;
import tech.lp2p.core.Scheme;
import tech.lp2p.core.Status;
import tech.lp2p.quic.ApplicationProtocolConnection;
import tech.lp2p.quic.ServerConnection;
import tech.lp2p.quic.Stream;
import tech.lp2p.quic.StreamHandler;
import tech.lp2p.utils.Utils;


/**
 * Http connection serving HTTP requests using a given HttpRequestHandler.
 */
public final class Http3ServerConnection extends Http3Connection implements ApplicationProtocolConnection {


    private final RequestHandler requestHandler;

    public Http3ServerConnection(ServerConnection quicConnection, RequestHandler requestHandler) {
        super(quicConnection);
        this.requestHandler = requestHandler;

        startControlStream();
    }

    static List<Http3Frame> parseHttp3Frames(InputStream requestStream) throws IOException {
        // https://tools.ietf.org/html/draft-ietf-quic-http-34#section-4.1
        // "An HTTP message (request or response) consists of:
        //   1.  the header section, sent as a single HEADERS frame (see Section 7.2.2),
        //   2.  optionally, the content, if present, sent as a series of DATA frames (see Section 7.2.1), and
        //   3.  optionally, the trailer section, if present, sent as a single HEADERS frame."
        List<Http3Frame> receivedFrames = new ArrayList<>();

        Http3Frame frame;
        while ((frame = readFrame(requestStream)) != null) {
            receivedFrames.add(frame);
        }

        return receivedFrames;
    }

    private static void sendStatus(int statusCode, Stream outputStream) {
        HeadersFrame headersFrame = new HeadersFrame(
                HeadersFrame.PSEUDO_HEADER_STATUS, Integer.toString(statusCode));
        outputStream.writeOutput(headersFrame.toBytes(), true);
    }

    @Override
    protected void handleBidirectionalStream(Stream stream, byte[] frame) {
        // https://tools.ietf.org/html/draft-ietf-quic-http-34#section-6.1
        // "All client-initiated bidirectional streams are used for HTTP requests and responses."
        // https://tools.ietf.org/html/draft-ietf-quic-http-34#section-4.1
        // "A client sends an HTTP request on a request stream, which is a client-initiated bidirectional QUIC
        //  stream; see Section 6.1. A client MUST send only a single request on a given stream."
        InputStream requestStream = new ByteArrayInputStream(frame);
        try {
            List<Http3Frame> receivedFrames = parseHttp3Frames(requestStream);
            byte[] data = handleHttpRequest(receivedFrames);
            stream.writeOutput(data, true);
        } catch (IOException ioError) {
            stream.stopLoading(H3_INTERNAL_ERROR);
            sendStatus(Status.SERVER_INTERNAL_ERROR.code(), stream);
        } catch (HttpError httpError) {
            stream.stopLoading(httpError.getStatusCode());
            sendStatus(Status.SERVER_INTERNAL_ERROR.code(), stream);
        }
    }

    private byte[] handleHttpRequest(List<Http3Frame> receivedFrames) throws IOException, HttpError {
        HeadersFrame headersFrame = (HeadersFrame) receivedFrames.stream()
                .filter(f -> f instanceof HeadersFrame)
                .findFirst()
                .orElseThrow(() ->
                        new HttpError(Status.H3_MESSAGE_ERROR));

        Method method = null;
        String headerMethod = headersFrame.getPseudoHeader(HeadersFrame.PSEUDO_HEADER_METHOD);
        if (headerMethod != null && !headerMethod.isEmpty()) {
            method = Method.valueOf(headerMethod.toUpperCase());
        }
        if (method == null) {
            throw new HttpError(Status.H3_MESSAGE_ERROR);
        }


        String schemeRaw = headersFrame.getPseudoHeader(HeadersFrame.PSEUDO_HEADER_SCHEME);
        if (schemeRaw == null) {
            throw new HttpError(Status.H3_MESSAGE_ERROR);
        }
        Scheme scheme = Scheme.valueOf(schemeRaw);

        String authority = headersFrame.getPseudoHeader(
                HeadersFrame.PSEUDO_HEADER_AUTHORITY);
        if (authority == null) {
            throw new HttpError(Status.H3_MESSAGE_ERROR);
        }

        String path = headersFrame.getPseudoHeader(HeadersFrame.PSEUDO_HEADER_PATH);
        if (path == null) {
            throw new HttpError(Status.H3_MESSAGE_ERROR);
        }

        byte[] payload = Utils.BYTES_EMPTY;
        Optional<Http3Frame> optionalDataFrame = receivedFrames.stream()
                .filter(f -> f instanceof DataFrame)
                .findFirst();
        if (optionalDataFrame.isPresent()) {
            payload = ((DataFrame) optionalDataFrame.get()).getPayload();
        }


        PeerId peerId = connection.remotePeerId();
        Peeraddr peeraddr = Peeraddr.create(peerId, connection.remoteAddress());

        // timeout (zero) and keepAlive and maxResponseValue NOT RELEVANT
        Request request = new Request(peeraddr, path, scheme,
                method, headersFrame.headers(), Duration.ZERO, false,
                Integer.MAX_VALUE, payload);

        Response response = requestHandler.handleRequest(request);

        HeadersFrame headers = new HeadersFrame(HeadersFrame.PSEUDO_HEADER_STATUS,
                Integer.toString(response.status().code()), response.headers());

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            outputStream.write(headers.toBytes());
            byte[] data = response.body();
            if (data != null && data.length > 0) {
                outputStream.write(new DataFrame(data).toBytes());
            }
            return outputStream.toByteArray();
        }
    }

    @Override
    public Function<Stream, StreamHandler> getStreamHandler() {
        return stream -> new HttpStreamHandler(this);
    }
}
