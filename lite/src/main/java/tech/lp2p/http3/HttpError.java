package tech.lp2p.http3;


import tech.lp2p.core.Status;

public final class HttpError extends Exception {

    private final int statusCode;

    public HttpError(Status statusCode) {
        super(statusCode.name());
        this.statusCode = statusCode.code();
    }

    public int getStatusCode() {
        return statusCode;
    }
}
