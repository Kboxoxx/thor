package tech.lp2p.http3;


public sealed interface Http3Frame permits SettingsFrame, UnknownFrame, DataFrame, HeadersFrame {
}
