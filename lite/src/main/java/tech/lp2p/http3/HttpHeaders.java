package tech.lp2p.http3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.BiPredicate;

import tech.lp2p.core.Headers;

public final class HttpHeaders implements Headers {
    public static final HttpHeaders NO_HEADERS = new HttpHeaders(Map.of());
    private final Map<String, List<String>> headers;

    public HttpHeaders() {
        this.headers = new TreeMap<>();
    }

    private HttpHeaders(Map<String, List<String>> headers) {
        this.headers = headers;
    }

    public static HttpHeaders of(Map<String, List<String>> headerMap, BiPredicate<String, String> filter) {
        Objects.requireNonNull(headerMap);
        Objects.requireNonNull(filter);
        return headersOf(headerMap, filter);
    }

    private static int entryHash(Map.Entry<String, List<String>> e) {
        String key = e.getKey();
        List<String> value = e.getValue();
        int keyHash = key.toLowerCase(Locale.ROOT).hashCode();
        int valueHash = value.hashCode();
        return keyHash ^ valueHash;
    }

    private static HttpHeaders headersOf(Map<String, List<String>> map, BiPredicate<String, String> filter) {
        TreeMap<String, List<String>> other = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        TreeSet<String> notAdded = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
        ArrayList<String> tempList = new ArrayList<>();
        map.forEach((key, value) -> {
            String headerName = Objects.requireNonNull(key).trim();
            if (headerName.isEmpty()) {
                throw new IllegalArgumentException("empty key");
            } else {
                List<String> headerValues = Objects.requireNonNull(value);
                headerValues.forEach((headerValue) -> {
                    headerValue = Objects.requireNonNull(headerValue).trim();
                    if (filter.test(headerName, headerValue)) {
                        tempList.add(headerValue);
                    }

                });
                if (tempList.isEmpty()) {
                    if (other.containsKey(headerName) || notAdded.contains(headerName.toLowerCase(Locale.ROOT))) {
                        throw new IllegalArgumentException("duplicate key: " + headerName);
                    }

                    notAdded.add(headerName.toLowerCase(Locale.ROOT));
                } else if (other.put(headerName, List.copyOf(tempList)) != null) {
                    throw new IllegalArgumentException("duplicate key: " + headerName);
                }

                tempList.clear();
            }
        });
        return other.isEmpty() ? NO_HEADERS : new HttpHeaders(Collections.unmodifiableMap(other));
    }

    public Optional<String> firstValue(String name) {
        return this.allValues(name).stream().findFirst();
    }

    public OptionalLong firstValueAsLong(String name) {
        return this.allValues(name).stream().mapToLong(Long::valueOf).findFirst();
    }

    public List<String> allValues(String name) {
        Objects.requireNonNull(name);
        List<String> values = this.map().get(name);
        return values != null ? values : List.of();
    }

    public Map<String, List<String>> map() {
        return this.headers;
    }

    @Override
    public void add(String key, String value) {
        this.headers.computeIfAbsent(key, (k) -> new ArrayList<>(1)).add(value);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof HttpHeaders that)) {
            return false;
        } else {
            return this.map().equals(that.map());
        }
    }

    public int hashCode() {
        int h = 0;

        Map.Entry<String, List<String>> e;
        for (Iterator<Map.Entry<String, List<String>>> var2 =
             this.map().entrySet().iterator();
             var2.hasNext(); h += entryHash(e)) {
            e = var2.next();
        }

        return h;
    }

    public String toString() {
        return "Headers { " + this.map().toString() + " }";
    }
}

