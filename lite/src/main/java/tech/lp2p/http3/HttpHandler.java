package tech.lp2p.http3;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.net.ProtocolException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import tech.lp2p.core.Request;
import tech.lp2p.core.Response;
import tech.lp2p.core.Status;
import tech.lp2p.quic.VariableLengthInteger;

public interface HttpHandler {


    static byte[] createMessage(Request request) throws IOException {

        // https://www.rfc-editor.org/rfc/rfc9114.html#name-request-pseudo-header-field
        // "All HTTP/3 requests MUST include exactly one value for the :method, :scheme,
        // and :path pseudo-header fields, unless the request is a CONNECT request;"
        // "If the :scheme pseudo-header field identifies a scheme that has a mandatory
        // authority component (including  "http" and "https"), the request MUST
        // contain either an :authority pseudo-header field or a Host header field."
        Map<String, String> pseudoHeaders = Map.of(
                ":method", request.method().name(),
                ":scheme", request.scheme().name().toLowerCase(),
                ":authority", request.peeraddr().authority(),
                ":path", request.path()
        );
        HeadersFrame headersFrame = new HeadersFrame(pseudoHeaders);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(headersFrame.toBytes());

        DataFrame dataFrame = new DataFrame(request.body());
        out.write(dataFrame.toBytes());

        return out.toByteArray();

    }


    static byte[] readExact(InputStream inputStream, int length) throws IOException {
        byte[] data = inputStream.readNBytes(length);
        if (data.length != length) {
            throw new EOFException("Stream closed by peer");
        }
        return data;
    }


    static Http3Frame readFrame(InputStream input) throws IOException {
        // PushbackInputStream only buffers the unread bytes, so it's safe to use it as a
        // temporary wrapper for the input stream.
        PushbackInputStream inputStream = new PushbackInputStream(input, 1);
        int firstByte = inputStream.read();
        if (firstByte == -1) {
            return null;
        }
        inputStream.unread(firstByte);

        long frameType = VariableLengthInteger.parseLong(inputStream);
        int payloadLength = VariableLengthInteger.parse(inputStream);

        return switch ((int) frameType) {
            case Http3Connection.FRAME_TYPE_HEADERS ->
                    new HeadersFrame().parsePayload(readExact(inputStream, payloadLength));
            case Http3Connection.FRAME_TYPE_DATA ->
                    new DataFrame().parsePayload(readExact(inputStream, payloadLength));
            case Http3Connection.FRAME_TYPE_SETTINGS -> new SettingsFrame().parsePayload(
                    ByteBuffer.wrap(readExact(inputStream, payloadLength)));
            case Http3Connection.FRAME_TYPE_GOAWAY,
                 Http3Connection.FRAME_TYPE_CANCEL_PUSH,
                 Http3Connection.FRAME_TYPE_MAX_PUSH_ID,
                 Http3Connection.FRAME_TYPE_PUSH_PROMISE ->
                    throw new IllegalStateException("Frame type " + frameType + " not yet implemented");
            default -> {
                // https://www.rfc-editor.org/rfc/rfc9114.html#extensions
                // "Extensions are permitted to use new frame types (Section 7.2), ...."
                // "Implementations MUST ignore unknown or unsupported values in all extensible protocol elements."
                //noinspection ResultOfMethodCallIgnored
                inputStream.skip(payloadLength);
                yield new UnknownFrame();
            }
        };
    }

    private static HeadersFrame readHeadersFrame(
            InputStream responseStream, ResponseFramesSequenceChecker frameSequenceChecker)
            throws IOException {
        Http3Frame frame = readFrame(responseStream);
        if (frame == null) {
            throw new EOFException("end of stream");
        } else if (frame instanceof HeadersFrame) {
            frameSequenceChecker.gotHeader();
            return (HeadersFrame) frame;
        } else if (frame instanceof DataFrame) {
            frameSequenceChecker.gotData();
            return null;  // Will not happen, checker will throw exception
        } else {
            ResponseFramesSequenceChecker.gotOther(frame);
            // If it gets here, the unknown frame is ignored, continue to (try to) read a headers frame
            return readHeadersFrame(responseStream, frameSequenceChecker);
        }
    }

    static Response receiveResponse(byte[] payload) throws IOException {

        ResponseFramesSequenceChecker frameSequenceChecker = new ResponseFramesSequenceChecker();
        InputStream responseStream = new ByteArrayInputStream(payload);
        HeadersFrame headersFrame = readHeadersFrame(responseStream, frameSequenceChecker);
        Objects.requireNonNull(headersFrame);
        HttpResponseInfo responseInfo = new HttpResponseInfo(headersFrame);
        Status status = Status.get(responseInfo.statusCode);

        if (status != Status.OK) {
            return Response.create(status, responseInfo.headers);
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();


        readout(responseStream, frameSequenceChecker, responseInfo, outputStream);

        return Response.create(Status.OK, responseInfo.headers, outputStream.toByteArray());
    }

    private static void readout(InputStream inputStream,
                                ResponseFramesSequenceChecker frameSequenceChecker,
                                HttpResponseInfo responseInfo,
                                OutputStream outputStream) throws IOException {

        DataFrame dataFrame;
        do {
            dataFrame = readDataFrame(frameSequenceChecker, responseInfo, inputStream);
            if (dataFrame != null) {
                frameSequenceChecker.gotData();

                outputStream.write(dataFrame.getPayload());
            } else {
                frameSequenceChecker.done();
                outputStream.close();
            }
        } while (dataFrame != null);

    }

    private static DataFrame readDataFrame(ResponseFramesSequenceChecker frameSequenceChecker,
                                           HttpResponseInfo responseInfo,
                                           InputStream inputStream) throws IOException {
        Http3Frame frame;

        frame = readFrame(inputStream);
        if (frame == null) {
            return null;
        }
        if (frame instanceof DataFrame) {
            return (DataFrame) frame;
        }
        if (frame instanceof HeadersFrame) {
            frameSequenceChecker.gotHeader();
            responseInfo.add((HeadersFrame) frame);
            // No additional frames expected, but if more frames follow, an error must be generated, so
        } else {
            ResponseFramesSequenceChecker.gotOther(frame);
            // If it gets here, the unknown frame is ignored, continue to (try to) read a data frame
        }
        return readDataFrame(frameSequenceChecker, responseInfo, inputStream);
    }

    class HttpResponseInfo {
        private final int statusCode;
        private HttpHeaders headers;

        public HttpResponseInfo(HeadersFrame headersFrame) {
            headers = HttpHeaders.of(headersFrame.headers().map(), (a, b) -> true);
            String statusCodeHeader = headersFrame.getPseudoHeader(HeadersFrame.PSEUDO_HEADER_STATUS);
            statusCode = Integer.parseInt(statusCodeHeader);
        }


        public void add(HeadersFrame headersFrame) {
            Map<String, List<String>> mergedHeadersMap = new HashMap<>();
            mergedHeadersMap.putAll(headers.map());
            mergedHeadersMap.putAll(headersFrame.headers().map());
            headers = HttpHeaders.of(mergedHeadersMap, (a, b) -> true);
        }
    }

    class ResponseFramesSequenceChecker {

        ResponseStatus status = ResponseStatus.INITIAL;

        public ResponseFramesSequenceChecker() {
        }

        public static void gotOther(Http3Frame frame) throws ProtocolException {
            if (!(frame instanceof UnknownFrame)) {
                throw new ProtocolException("only header and body frames" +
                        " are allowed on response stream");
            }
        }

        void gotHeader() throws ProtocolException {
            if (status == ResponseStatus.INITIAL) {
                status = ResponseStatus.GOT_HEADER;
            } else if (status == ResponseStatus.GOT_HEADER) {
                throw new ProtocolException("Header frame is not allowed after initial header frame");
            } else if (status == ResponseStatus.GOT_HEADER_AND_DATA) {
                status = ResponseStatus.GOT_HEADER_AND_DATA_AND_TRAILING_HEADER;
            } else if (status == ResponseStatus.GOT_HEADER_AND_DATA_AND_TRAILING_HEADER) {
                throw new ProtocolException("Header frame is not allowed after trailing header frame");
            }
        }

        public void gotData() throws ProtocolException {
            if (status == ResponseStatus.INITIAL) {
                throw new ProtocolException("Missing header frame");
            } else if (status == ResponseStatus.GOT_HEADER) {
                status = ResponseStatus.GOT_HEADER_AND_DATA;
            } else if (status == ResponseStatus.GOT_HEADER_AND_DATA_AND_TRAILING_HEADER) {
                throw new ProtocolException("Data frame not allowed after trailing header frame");
            } // others No status change
        }

        public void done() throws ProtocolException {
            if (status == ResponseStatus.INITIAL) {
                throw new ProtocolException("Missing header frame");
            }
        }

        enum ResponseStatus {
            INITIAL,
            GOT_HEADER,
            GOT_HEADER_AND_DATA,
            GOT_HEADER_AND_DATA_AND_TRAILING_HEADER,
        }
    }
}
