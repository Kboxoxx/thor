package tech.lp2p.http3;

import tech.lp2p.Lite;
import tech.lp2p.quic.Stream;
import tech.lp2p.quic.StreamHandler;

public class HttpStreamHandler implements StreamHandler {
    private final Http3Connection http3Connection;

    public HttpStreamHandler(Http3Connection http3Connection) {
        this.http3Connection = http3Connection;
    }

    @Override
    public void terminated(Stream stream) {
    }

    @Override
    public void fin(Stream stream) {
    }

    @Override
    public boolean readFully(Stream stream) {
        return !stream.isUnidirectional();
    }

    @Override
    public void data(Stream stream, byte[] frame) {
        if (frame.length > 0) {
            http3Connection.handleIncomingStream(stream, frame);
        }
    }

    @Override
    public long delimiter(Stream stream) {
        if (stream.isUnidirectional()) {
            return Lite.DEFAULT_MAX_HEADER_SIZE;
        } else {
            return 2 * Lite.CHUNK_DATA;
        }
    }
}
