package tech.lp2p.http3;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import tech.lp2p.core.Headers;
import tech.lp2p.qpack.Decoder;
import tech.lp2p.qpack.Encoder;
import tech.lp2p.quic.VariableLengthInteger;


// https://www.rfc-editor.org/rfc/rfc9114.html#section-7.2.2
public final class HeadersFrame implements Http3Frame {

    // https://www.rfc-editor.org/rfc/rfc9114.html#name-request-pseudo-header-field
    public static final String PSEUDO_HEADER_METHOD = ":method";
    public static final String PSEUDO_HEADER_SCHEME = ":scheme";
    public static final String PSEUDO_HEADER_AUTHORITY = ":authority";
    public static final String PSEUDO_HEADER_PATH = ":path";
    // https://www.rfc-editor.org/rfc/rfc9114.html#name-response-pseudo-header-fiel
    public static final String PSEUDO_HEADER_STATUS = ":status";
    private final Map<String, String> pseudoHeaders;
    private HttpHeaders httpHeaders;

    public HeadersFrame() {
        pseudoHeaders = new HashMap<>();
        httpHeaders = HttpHeaders.of(Collections.emptyMap(), (a, b) -> true);
    }

    public HeadersFrame(String pseudoHeader, String value) {
        pseudoHeaders = new HashMap<>();
        pseudoHeaders.put(pseudoHeader, value);
        httpHeaders = HttpHeaders.of(Collections.emptyMap(), (a, b) -> true);
    }

    public HeadersFrame(String pseudoHeader, String value, Headers headers) {
        pseudoHeaders = new HashMap<>();
        pseudoHeaders.put(pseudoHeader, value);
        httpHeaders = HttpHeaders.of(headers.map(), (a, b) -> true);
    }

    public HeadersFrame(Map<String, String> pseudoHeaders) {
        if (pseudoHeaders.keySet().stream().anyMatch(key -> !key.startsWith(":"))) {
            throw new IllegalArgumentException("Pseudo headers must start with ':'");
        }
        this.pseudoHeaders = Objects.requireNonNull(pseudoHeaders);
        this.httpHeaders = HttpHeaders.of(Collections.emptyMap(), (a, b) -> true);
    }

    private static List<String> mapValue(Map.Entry<String, String> entry) {
        return List.of(entry.getValue());
    }

    private static List<String> mergeValues(List<String> value1, List<String> value2) {
        List<String> result = new ArrayList<>();
        result.addAll(value1);
        result.addAll(value2);
        return result;
    }

    public byte[] toBytes() {
        List<Map.Entry<String, String>> qpackHeaders = new ArrayList<>();
        addPseudoHeaders(qpackHeaders);
        addHeaders(qpackHeaders);

        ByteBuffer compressedHeaders = Encoder.compressHeaders(qpackHeaders);
        compressedHeaders.flip();

        ByteBuffer payloadLength = ByteBuffer.allocate(4);
        VariableLengthInteger.encode(compressedHeaders.limit(), payloadLength);
        payloadLength.flip();

        byte[] data = new byte[1 + payloadLength.limit() + compressedHeaders.limit()];
        data[0] = 0x01;  // Header frame
        payloadLength.get(data, 1, payloadLength.limit());
        compressedHeaders.get(data, 1 + payloadLength.limit(), compressedHeaders.limit());

        return data;
    }

    public HeadersFrame parsePayload(byte[] headerBlock) throws IOException {
        List<Map.Entry<String, String>> headersList = Decoder.decodeStream(new ByteArrayInputStream(headerBlock));
        Map<String, List<String>> headersMap = headersList.stream()
                .collect(Collectors.toMap(Map.Entry::getKey, HeadersFrame::mapValue, HeadersFrame::mergeValues));
        // https://www.rfc-editor.org/rfc/rfc9114#name-http-control-data
        // "Pseudo-header fields are not HTTP fields."
        extractPseudoHeaders(headersMap);
        httpHeaders = HttpHeaders.of(headersMap, (key, value) -> !key.startsWith(":"));
        return this;
    }

    private void extractPseudoHeaders(Map<String, List<String>> headersMap) {
        headersMap.entrySet().stream()
                .filter(entry -> entry.getKey().startsWith(":"))
                .forEach(entry -> pseudoHeaders.put(entry.getKey(), entry.getValue().get(0)));
    }

    private void addPseudoHeaders(List<Map.Entry<String, String>> qpackHeaders) {
        pseudoHeaders.entrySet().forEach(qpackHeaders::add);
    }

    private void addHeaders(List<Map.Entry<String, String>> qpackHeaders) {
        httpHeaders.map().forEach((key, value1) -> {
            String value = String.join(",", value1);
            // https://tools.ietf.org/html/draft-ietf-quic-http-28#4.1.1
            // "As in HTTP/2, characters in field names MUST be converted to lowercase prior to their encoding."
            qpackHeaders.add(new AbstractMap.SimpleEntry<>(key.toLowerCase(), value));
        });
    }

    public String getPseudoHeader(String header) {
        return pseudoHeaders.get(header);
    }

    public HttpHeaders headers() {
        return httpHeaders;
    }

}
