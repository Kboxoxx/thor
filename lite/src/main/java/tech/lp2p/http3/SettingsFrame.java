package tech.lp2p.http3;


import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import tech.lp2p.quic.VariableLengthInteger;

// https://www.rfc-editor.org/rfc/rfc9114.html#name-settings
public final class SettingsFrame implements Http3Frame {

    // https://www.rfc-editor.org/rfc/rfc9114.html#name-settings
    public static final int SETTINGS_FRAME_TYPE = 0x04;

    // https://www.rfc-editor.org/rfc/rfc9204.html#name-settings-registration
    public static final int QPACK_MAX_TABLE_CAPACITY = 0x01;
    public static final int QPACK_BLOCKED_STREAMS = 0x07;

    // https://www.rfc-editor.org/rfc/rfc9220#name-iana-considerations
    public static final int SETTINGS_ENABLE_CONNECT_PROTOCOL = 0x08;

    private int qpackMaxTableCapacity;
    private int qpackBlockedStreams;
    private boolean settingsEnableConnectProtocol;
    private Map<Long, Long> additionalSettings = new HashMap<>();

    public SettingsFrame(int qpackMaxTableCapacity, int qpackBlockedStreams) {
        this(qpackMaxTableCapacity, qpackBlockedStreams, false);
    }

    public SettingsFrame(int qpackMaxTableCapacity, int qpackBlockedStreams, boolean enableConnectProtocol) {
        this.qpackMaxTableCapacity = qpackMaxTableCapacity;
        this.qpackBlockedStreams = qpackBlockedStreams;
        this.settingsEnableConnectProtocol = enableConnectProtocol;
    }

    public SettingsFrame() {
        this(0, 0, false);
    }

    public SettingsFrame parsePayload(ByteBuffer buffer) throws IOException {
        while (buffer.remaining() > 0) {
            // https://www.rfc-editor.org/rfc/rfc9114.html#name-settings
            // "The payload of a SETTINGS frame consists of zero or more parameters. Each parameter consists of
            //  a setting identifier and a value, both encoded as QUIC variable-length integers."
            try {
                long identifier = VariableLengthInteger.parseLong(buffer);
                long value = VariableLengthInteger.parseLong(buffer);
                if (identifier == QPACK_MAX_TABLE_CAPACITY) {
                    qpackMaxTableCapacity = (int) value;
                } else if (identifier == QPACK_BLOCKED_STREAMS) {
                    qpackBlockedStreams = (int) value;
                } else if (identifier == SETTINGS_ENABLE_CONNECT_PROTOCOL) {
                    // https://www.rfc-editor.org/rfc/rfc9220#name-iana-considerations
                    // "Value: 0x08
                    //  Setting Name: SETTINGS_ENABLE_CONNECT_PROTOCOL"
                    if (value == 1L) {
                        // https://www.rfc-editor.org/rfc/rfc8441#section-3
                        // "The value of the parameter MUST be 0 or 1."
                        // "Upon receipt of SETTINGS_ENABLE_CONNECT_PROTOCOL with a value of 1, a client MAY use the
                        //  Extended CONNECT as defined in this document when creating new streams."
                        settingsEnableConnectProtocol = true;
                    }
                } else {
                    // https://www.rfc-editor.org/rfc/rfc9114.html#name-settings
                    // "An implementation MUST ignore any parameter with an identifier it does not understand."
                    additionalSettings.put(identifier, value);
                }
            } catch (Throwable e) {
                throw new IOException(e);
            }
        }
        return this;
    }

    public ByteBuffer getBytes() {
        ByteBuffer serializedParams = ByteBuffer.allocate(3 * 8);

        serializedParams.put((byte) QPACK_MAX_TABLE_CAPACITY);
        VariableLengthInteger.encode(qpackMaxTableCapacity, serializedParams);

        serializedParams.put((byte) QPACK_BLOCKED_STREAMS);
        VariableLengthInteger.encode(qpackBlockedStreams, serializedParams);

        if (settingsEnableConnectProtocol) {
            serializedParams.put((byte) SETTINGS_ENABLE_CONNECT_PROTOCOL);
            VariableLengthInteger.encode(1, serializedParams);
        }
        additionalSettings.forEach((key, value) -> {
            VariableLengthInteger.encode(key, serializedParams);
            VariableLengthInteger.encode(value, serializedParams);
        });

        int paramLength = serializedParams.position();
        ByteBuffer buffer = ByteBuffer.allocate(1 + VariableLengthInteger.bytesNeeded(paramLength) + paramLength);
        buffer.put((byte) SETTINGS_FRAME_TYPE);
        VariableLengthInteger.encode(paramLength, buffer);
        buffer.put(serializedParams.array(), 0, paramLength);
        return buffer;
    }

    public int getQpackMaxTableCapacity() {
        return qpackMaxTableCapacity;
    }

    public int getQpackBlockedStreams() {
        return qpackBlockedStreams;
    }

    public boolean isSettingsEnableConnectProtocol() {
        return settingsEnableConnectProtocol;
    }

    public void addAdditionalSettings(Map<Long, Long> settingsParameters) {
        this.additionalSettings = settingsParameters;
    }

    @SuppressWarnings("unused")
    public Long getParameter(long identifier) {
        return additionalSettings.get(identifier);
    }
}
