package tech.lp2p.http3;

import static tech.lp2p.http3.SettingsFrame.QPACK_BLOCKED_STREAMS;
import static tech.lp2p.http3.SettingsFrame.QPACK_MAX_TABLE_CAPACITY;
import static tech.lp2p.http3.SettingsFrame.SETTINGS_ENABLE_CONNECT_PROTOCOL;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Request;
import tech.lp2p.core.Response;
import tech.lp2p.core.Status;
import tech.lp2p.quic.Connection;
import tech.lp2p.quic.Requester;
import tech.lp2p.quic.Stream;
import tech.lp2p.quic.StreamHandler;
import tech.lp2p.quic.VariableLengthInteger;


public class Http3Connection implements tech.lp2p.core.Connection {

    // https://www.rfc-editor.org/rfc/rfc9114.html#name-stream-types
    public static final int STREAM_TYPE_CONTROL_STREAM = 0x00;
    public static final int STREAM_TYPE_PUSH_STREAM = 0x01;

    // https://www.rfc-editor.org/rfc/rfc9204.html#name-stream-type-registration
    public static final int STREAM_TYPE_QPACK_ENCODER = 0x02;
    public static final int STREAM_TYPE_QPACK_DECODER = 0x03;

    // https://www.rfc-editor.org/rfc/rfc9114.html#name-http-3-error-codes
    // "No error. This is used when the connection or stream needs to be closed, but there is no error to signal.
    @SuppressWarnings("unused")
    public static final int H3_NO_ERROR = 0x0100;
    // "Peer violated protocol requirements in a way that does not match a more specific error code
    // or endpoint declines to use the more specific error code."
    @SuppressWarnings("unused")
    public static final int H3_GENERAL_PROTOCOL_ERROR = 0x0101;
    // "An internal error has occurred in the HTTP stack."
    public static final int H3_INTERNAL_ERROR = 0x0102;


    // A frame was received that was not permitted in the current state or on the current stream.
    @SuppressWarnings("unused")
    public static final int H3_FRAME_UNEXPECTED = 0x0105;

    // The endpoint detected that its peer is exhibiting a behavior that might be generating excessive load.
    @SuppressWarnings("unused")
    public static final int H3_EXCESSIVE_LOAD = 0x0107;
    // A stream ID or push ID was used incorrectly, such as exceeding a limit, reducing a limit, or being reused.
    @SuppressWarnings("unused")
    public static final int H3_ID_ERROR = 0x0108;
    // An endpoint detected an error in the payload of a SETTINGS frame.
    @SuppressWarnings("unused")
    public static final int H3_SETTINGS_ERROR = 0x0109;

    // A server rejected a request without performing any application processing.
    @SuppressWarnings("unused")
    public static final int H3_REQUEST_REJECTED = 0x010b;
    // The request or its response (including pushed response) is cancelled.
    @SuppressWarnings("unused")
    public static final int H3_REQUEST_CANCELLED = 0x010c;
    // The client's stream terminated without containing a fully formed request.
    @SuppressWarnings("unused")
    public static final int H3_REQUEST_INCOMPLETE = 0x010d;

    // The TCP connection established in response to a CONNECT request was reset or abnormally closed.
    @SuppressWarnings("unused")
    public static final int H3_CONNECT_ERROR = 0x010f;
    // The requested operation cannot be served over HTTP/3. The peer should retry over HTTP/1.1."
    @SuppressWarnings("unused")
    public static final int H3_VERSION_FALLBACK = 0x0110;

    // https://www.rfc-editor.org/rfc/rfc9114.html#name-frame-types
    public static final int FRAME_TYPE_DATA = 0x00;
    public static final int FRAME_TYPE_HEADERS = 0x01;
    public static final int FRAME_TYPE_CANCEL_PUSH = 0x03;
    public static final int FRAME_TYPE_SETTINGS = 0x04;
    public static final int FRAME_TYPE_PUSH_PROMISE = 0x05;
    public static final int FRAME_TYPE_GOAWAY = 0x07;
    public static final int FRAME_TYPE_MAX_PUSH_ID = 0x0d;
    private static final List<Long> internalSettingsParameterIds = List.of(
            (long) QPACK_MAX_TABLE_CAPACITY,
            (long) QPACK_BLOCKED_STREAMS,
            (long) SETTINGS_ENABLE_CONNECT_PROTOCOL
    );
    protected final Connection connection;
    protected final Map<Long, Long> settingsParameters = new HashMap<>();
    protected final Map<Long, Consumer<InputStream>> unidirectionalStreamHandler = new HashMap<>();
    private final StreamHandler uniStreamHandler = new UniStreamHandler();
    @SuppressWarnings("unused")
    protected InputStream peerEncoderStream;
    @SuppressWarnings("unused")
    protected int peerQpackBlockedStreams;
    @SuppressWarnings("unused")
    protected int peerQpackMaxTableCapacity;


    public Http3Connection(Connection connection) {
        this.connection = connection;

        registerStandardStreamHandlers();

    }

    private static boolean isReservedStreamType(long streamType) {
        // https://www.rfc-editor.org/rfc/rfc9114.html#stream-grease
        // Stream types of the format 0x1f * N + 0x21 for non-negative integer values of N are reserved
        return (streamType - 0x21) % 0x1f == 0;
    }

    /**
     * Reads one HTTP3 frame from the given input stream (if any).
     *
     * @param input the input stream to read from.
     * @return the frame read, or null if no frame is available due to end of stream.
     */
    protected static Http3Frame readFrame(InputStream input) throws IOException {
        // PushbackInputStream only buffers the unread bytes, so it's safe to use it as a temporary
        // wrapper for the input stream.
        PushbackInputStream inputStream = new PushbackInputStream(input, 1);
        int firstByte = inputStream.read();
        if (firstByte == -1) {
            return null;
        }
        inputStream.unread(firstByte);

        long frameType = VariableLengthInteger.parseLong(inputStream);
        int payloadLength = VariableLengthInteger.parse(inputStream);

        return switch ((int) frameType) {
            case FRAME_TYPE_HEADERS ->
                    new HeadersFrame().parsePayload(readExact(inputStream, payloadLength));
            case FRAME_TYPE_DATA ->
                    new DataFrame().parsePayload(readExact(inputStream, payloadLength));
            case FRAME_TYPE_SETTINGS ->
                    new SettingsFrame().parsePayload(ByteBuffer.wrap(readExact(inputStream, payloadLength)));
            case FRAME_TYPE_GOAWAY, FRAME_TYPE_CANCEL_PUSH, FRAME_TYPE_MAX_PUSH_ID,
                 FRAME_TYPE_PUSH_PROMISE ->
                    throw new IllegalStateException("Frame type " + frameType + " not yet implemented");
            default -> {
                // https://www.rfc-editor.org/rfc/rfc9114.html#extensions
                // "Extensions are permitted to use new frame types (Section 7.2), ...."
                // "Implementations MUST ignore unknown or unsupported values in all extensible protocol elements."
                //noinspection ResultOfMethodCallIgnored
                inputStream.skip(payloadLength);
                yield new UnknownFrame();
            }
        };
    }

    protected static byte[] readExact(InputStream inputStream, int length) throws IOException {
        byte[] data = inputStream.readNBytes(length);
        if (data.length != length) {
            throw new EOFException("Stream closed by peer");
        }
        return data;
    }

    @SuppressWarnings("unused")
    public void registerUnidirectionalStreamType(long streamType, Consumer<InputStream> handler) {
        // ensure the stream type is not one of the standard types
        if (streamType >= 0x00 && streamType <= 0x03) {
            throw new IllegalArgumentException("Cannot register standard stream type");
        }
        if (isReservedStreamType(streamType)) {
            throw new IllegalArgumentException("Cannot register reserved stream type");
        }
        unidirectionalStreamHandler.put(streamType, handler);
    }

    @SuppressWarnings("unused")
    public Stream createUnidirectionalStream(long streamType) throws IOException {
        // ensure the stream type is not one of the standard types
        if (streamType >= 0x00 && streamType <= 0x03) {
            throw new IllegalArgumentException("Cannot create standard stream type");
        }
        if (isReservedStreamType(streamType)) {
            throw new IllegalArgumentException("Cannot create reserved stream type");
        }
        try {
            Stream quicStream = connection.createStream(uniStreamHandler, false);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            VariableLengthInteger.write(streamType, outputStream);
            quicStream.writeOutput(outputStream.toByteArray(), false);
            return quicStream;
        } catch (Throwable throwable) {
            throw new IOException(throwable);
        }
    }

    @SuppressWarnings("unused")
    public void addSettingsParameter(long identifier, long value) {
        if (identifier < 0) {
            throw new IllegalArgumentException("Identifier must be a positive integer");
        }
        if (internalSettingsParameterIds.contains(identifier)) {
            throw new IllegalArgumentException("Cannot overwrite internal settings parameter");
        }
        settingsParameters.put(identifier, value);
    }

    protected void registerStandardStreamHandlers() {
        // https://www.rfc-editor.org/rfc/rfc9114.html#name-unidirectional-streams
        // "Two stream types are defined in this document: control streams (Section 6.2.1) and push streams (Section 6.2.2).
        // https://www.rfc-editor.org/rfc/rfc9114.html#name-control-streams
        // "A control stream is indicated by a stream type of 0x00."
        unidirectionalStreamHandler.put((long) STREAM_TYPE_CONTROL_STREAM,
                this::processControlStream);
        // "[QPACK] defines two additional stream types. Other stream types can be defined by extensions to HTTP/3;..."
        // https://www.rfc-editor.org/rfc/rfc9204.html#name-encoder-and-decoder-streams
        // "An encoder stream is a unidirectional stream of type 0x02."
        unidirectionalStreamHandler.put((long) STREAM_TYPE_QPACK_ENCODER,
                this::setPeerEncoderStream);

        unidirectionalStreamHandler.put((long) STREAM_TYPE_QPACK_DECODER, httpStream -> {
        });
    }

    protected void handleUnidirectionalStream(Stream quicStream, byte[] frame) {
        InputStream stream = new ByteArrayInputStream(frame);
        long streamType;
        // https://www.rfc-editor.org/rfc/rfc9114.html#name-unidirectional-streams
        // "Unidirectional streams, in either direction, are used for a range of purposes. The purpose is indicated by
        //  a stream type, which is sent as a variable-length integer at the start of the stream."
        try {
            streamType = VariableLengthInteger.parseLong(stream);
        } catch (IOException ioError) {
            // https://www.rfc-editor.org/rfc/rfc9114.html#name-unidirectional-streams
            // "A receiver MUST tolerate unidirectional streams being closed or reset prior to the reception of the
            //  unidirectional stream header."
            return;
        }
        Consumer<InputStream> streamHandler = unidirectionalStreamHandler.get(streamType);
        if (streamHandler != null) {
            streamHandler.accept(stream);
        } else {
            // https://www.rfc-editor.org/rfc/rfc9114.html#name-unidirectional-streams
            // "If the stream header indicates a stream type that is not supported by the recipient, the remainder
            //  of the stream cannot be consumed as the semantics are unknown. Recipients of unknown stream types MUST
            //  either abort reading of the stream or discard incoming data without further processing. If reading is
            //  aborted, the recipient SHOULD use the H3_STREAM_CREATION_ERROR error code "
            quicStream.stopLoading(Status.H3_STREAM_CREATION_ERROR.code());
        }
    }

    public void startControlStream() {
        try {
            // https://www.rfc-editor.org/rfc/rfc9114.html#name-control-streams
            // "Each side MUST initiate a single control stream at the beginning of the
            // connection and send its SETTINGS frame as the first frame on this stream."

            // SO FAR the stream is not reused (maybe somewhere later)
            Stream clientControlStream = connection.createStream(uniStreamHandler, false);

            SettingsFrame settingsFrame = new SettingsFrame(0, 0);
            settingsFrame.addAdditionalSettings(settingsParameters);
            byte[] serializedSettings = settingsFrame.getBytes().array();
            ByteBuffer buffer = ByteBuffer.allocate(1 + serializedSettings.length);
            buffer.put((byte) STREAM_TYPE_CONTROL_STREAM);
            buffer.put(serializedSettings);
            byte[] data = buffer.array();


            // https://www.rfc-editor.org/rfc/rfc9114.html#name-control-streams
            // "The sender MUST NOT close the control stream, and the receiver MUST NOT request
            // that the sender close the control stream."
            clientControlStream.writeOutput(data, false);

        } catch (Throwable e) {
            // QuicStream's output stream will never throw an IOException, unless stream is closed.
            // https://www.rfc-editor.org/rfc/rfc9114.html#name-control-streams
            // "If either control stream is closed at any point, this MUST be treated as a
            // connection error of type H3_CLOSED_CRITICAL_STREAM."
            connectionError(Status.H3_CLOSED_CRITICAL_STREAM);
        }
    }

    protected SettingsFrame processControlStream(InputStream controlStream) {
        try {
            // https://www.rfc-editor.org/rfc/rfc9114.html#name-control-streams
            // "Each side MUST initiate a single control stream at the beginning of the connection and send its SETTINGS
            //  frame as the first frame on this stream."
            long frameType = VariableLengthInteger.parseLong(controlStream);
            // "If the first frame of the control stream is any other frame type, this MUST be treated as a connection error
            //  of type H3_MISSING_SETTINGS."
            if (frameType != (long) FRAME_TYPE_SETTINGS) {
                connectionError(Status.H3_MISSING_SETTINGS);
                return null;
            }
            int frameLength = VariableLengthInteger.parse(controlStream);
            byte[] payload = readExact(controlStream, frameLength);

            SettingsFrame settingsFrame = new SettingsFrame().parsePayload(ByteBuffer.wrap(payload));
            peerQpackMaxTableCapacity = settingsFrame.getQpackMaxTableCapacity();
            peerQpackBlockedStreams = settingsFrame.getQpackBlockedStreams();
            return settingsFrame;
        } catch (IOException e) {
            // "If either control stream is closed at any point, this MUST be treated as a
            //  connection error of type H3_CLOSED_CRITICAL_STREAM."
            connectionError(Status.H3_CLOSED_CRITICAL_STREAM);
            return null;
        }
    }

    void setPeerEncoderStream(InputStream stream) {
        peerEncoderStream = stream;
    }

    // https://www.rfc-editor.org/rfc/rfc9114.html#name-error-handling
    protected void connectionError(Status http3ErrorCode) {
        connection.close(http3ErrorCode);
    }

    public void handleIncomingStream(Stream stream, byte[] frame) {
        if (stream.isUnidirectional()) {
            handleUnidirectionalStream(stream, frame);
        } else {
            handleBidirectionalStream(stream, frame);
        }
    }

    protected void handleBidirectionalStream(Stream stream, byte[] frame) {
    }

    @Override
    public ALPN alpn() {
        return ALPN.h3;
    }

    @Override
    public void close() {
        connection.close();
    }

    @Override
    public InetSocketAddress remoteAddress() {
        return connection.remoteAddress();
    }

    @Override
    public boolean isConnected() {
        return connection.isConnected();
    }

    @Override
    public Response send(Request request) {
        try {
            byte[] msg = HttpHandler.createMessage(request);

            keepAlive(request.keepAlive());

            byte[] data = Requester.createStream(connection, request.maxResponseSize())
                    .request(msg, request.timeout().toSeconds());
            return HttpHandler.receiveResponse(data);
        } catch (TimeoutException timeoutException) {
            return Response.create(Status.REQUEST_TIMEOUT);
        } catch (InterruptedException interruptedException) {
            return Response.create(Status.CLIENT_CLOSED_REQUEST);
        } catch (IOException exception) {
            return Response.create(Status.BAD_REQUEST);
        }
    }

    private void keepAlive(boolean keepAlive) {
        if (keepAlive) {
            connection.enableKeepAlive();
        } else {
            connection.disableKeepAlive();
        }
    }

    private class UniStreamHandler implements StreamHandler {
        @Override
        public void terminated(Stream stream) {
            // should never be terminated
            connectionError(Status.H3_CLOSED_CRITICAL_STREAM);
        }

        @Override
        public void fin(Stream stream) {
            // should never be closed
            connectionError(Status.H3_CLOSED_CRITICAL_STREAM);
        }

        @Override
        public boolean readFully(Stream stream) {
            return false;
        }

        @Override
        public void data(Stream stream, byte[] byteArray) {
            // no data expected
            connectionError(Status.H3_CLOSED_CRITICAL_STREAM);
        }

        @Override
        public long delimiter(Stream stream) {
            return 0;
        }
    }
}
