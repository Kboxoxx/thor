package tech.lp2p.http3;


import java.nio.ByteBuffer;

import tech.lp2p.quic.VariableLengthInteger;


// https://www.rfc-editor.org/rfc/rfc9114.html#name-data
public final class DataFrame implements Http3Frame {

    private ByteBuffer payload;

    public DataFrame() {
        payload = ByteBuffer.allocate(0);
    }

    public DataFrame(byte[] payload) {
        this.payload = ByteBuffer.wrap(payload);
    }


    public byte[] toBytes() {
        int payloadLength = payload.limit();
        ByteBuffer lengthBuffer = ByteBuffer.allocate(8);
        int varIntLength = VariableLengthInteger.encode(payloadLength, lengthBuffer);
        int dataLength = 1 + varIntLength + payloadLength;
        byte[] data = new byte[dataLength];
        data[0] = 0x00;
        lengthBuffer.flip();  // Prepare for reading length.
        lengthBuffer.get(data, 1, varIntLength);
        payload.get(data, 1 + varIntLength, payloadLength);
        payload.rewind();
        return data;
    }

    public DataFrame parsePayload(byte[] payload) {
        this.payload = ByteBuffer.wrap(payload);
        return this;
    }

    public byte[] getPayload() {
        int payloadLength = payload.limit();
        if (payloadLength == payload.array().length) {
            return payload.array();
        } else {
            byte[] payloadBytes = new byte[payloadLength];
            payload.mark();
            payload.get(payloadBytes);
            payload.reset();
            return payloadBytes;
        }
    }

    @Override
    public String toString() {
        int payloadLength = payload.limit() - payload.position();
        return "DataFrame[" + payloadLength + "]";
    }
}
