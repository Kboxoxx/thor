package tech.lp2p.utils;


import com.google.protobuf.MessageLite;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import tech.lp2p.core.Progress;
import tech.lp2p.core.Protocol;


public interface Utils {
    boolean DEBUG = false;
    boolean ERROR = true;

    String STRING_EMPTY = "";
    byte[] BYTES_EMPTY = new byte[0];


    X509Certificate[] CERTIFICATES_EMPTY = new X509Certificate[0];

    static byte[] concat(byte[]... chunks) throws IllegalStateException {
        int length = 0;
        for (byte[] chunk : chunks) {
            if (length > Integer.MAX_VALUE - chunk.length) {
                throw new IllegalStateException("exceeded size limit");
            }
            length += chunk.length;
        }
        byte[] res = new byte[length];
        int pos = 0;
        for (byte[] chunk : chunks) {
            System.arraycopy(chunk, 0, res, pos, chunk.length);
            pos += chunk.length;
        }
        return res;
    }


    static int nextFreePort() {
        int port = ThreadLocalRandom.current().nextInt(4001, 65535);
        if (isLocalPortFree(port)) {
            return port;
        } else {
            return nextFreePort();
        }
    }

    private static boolean isLocalPortFree(int port) {
        try {
            new ServerSocket(port).close();
            return true;
        } catch (Throwable ignore) {
            return false;
        }
    }


    /**
     * Returns a datagram socket with a valid valid free port
     *
     * @param port preferred port for the socket
     * @return a datagram socket with a valid valid free port
     */
    static DatagramSocket getSocket(int port) {
        try {
            return new DatagramSocket(port);
        } catch (Throwable ignore) {
            return getSocket(nextFreePort());
        }
    }

    /**
     * Returns the values from each provided array combined into a single array. For example, {@code
     * concat(new int[] {a, b}, new int[] {}, new int[] {c}} returns the array {@code {a, b, c}}.
     *
     * @param arrays zero or more {@code int} arrays
     * @return a single array containing all the values from the source arrays, in order
     */
    static int[] concat(int[]... arrays) {
        int length = 0;
        for (int[] array : arrays) {
            length += array.length;
        }
        int[] result = new int[length];
        int pos = 0;
        for (int[] array : arrays) {
            System.arraycopy(array, 0, result, pos, array.length);
            pos += array.length;
        }
        return result;
    }


    static int unsignedVariantSize(int value) {
        int remaining = value >> 7;
        int count = 0;
        while (remaining != 0) {
            remaining >>= 7;
            count++;
        }
        return count + 1;
    }

    static void writeUnsignedVariant(ByteBuffer out, int value) {
        int remaining = value >>> 7;
        while (remaining != 0) {
            out.put((byte) ((value & 0x7f) | 0x80));
            value = remaining;
            remaining >>>= 7;
        }
        out.put((byte) (value & 0x7f));
    }


    static byte[] encode(MessageLite message) {
        try (ByteArrayOutputStream buf = new ByteArrayOutputStream()) {
            message.writeDelimitedTo(buf);
            return buf.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    static byte[] encode(byte[] data) {
        int dataLength = unsignedVariantSize(data.length);
        ByteBuffer buffer = ByteBuffer.allocate(dataLength + data.length);
        writeUnsignedVariant(buffer, data.length);
        buffer.put(data);
        return buffer.array();
    }


    static byte[] encodeProtocols(Protocol... protocols) {
        try (ByteArrayOutputStream buf = new ByteArrayOutputStream()) {
            for (Protocol protocol : protocols) {
                byte[] data = protocol.name().getBytes(StandardCharsets.UTF_8);
                int length = data.length + 1; // 1 is "\n"
                int dataLength = unsignedVariantSize(length);
                ByteBuffer buffer = ByteBuffer.allocate(dataLength);
                writeUnsignedVariant(buffer, length);
                buf.write(buffer.array());
                buf.write(data);
                buf.write('\n');
            }
            return buf.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    static byte[] encode(MessageLite message, Protocol... protocols) {
        try (ByteArrayOutputStream buf = new ByteArrayOutputStream()) {
            for (Protocol protocol : protocols) {
                byte[] data = protocol.name().getBytes(StandardCharsets.UTF_8);
                int length = data.length + 1; // 1 is "\n"
                int dataLength = unsignedVariantSize(length);
                ByteBuffer buffer = ByteBuffer.allocate(dataLength);
                writeUnsignedVariant(buffer, length);
                buf.write(buffer.array());
                buf.write(data);
                buf.write('\n');
            }
            message.writeDelimitedTo(buf);
            return buf.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }


    static long copy(InputStream source, OutputStream sink,
                     Progress progress, long size) throws IOException {
        long nread = 0L;
        byte[] buf = new byte[4096];
        int remember = 0;
        int n;
        while ((n = source.read(buf)) > 0) {
            sink.write(buf, 0, n);
            nread += n;

            if (size > 0) {
                int percent = (int) ((nread * 100.0f) / size);
                if (percent > remember) {
                    remember = percent;
                    progress.setProgress(percent);
                }
            }
        }
        return nread;
    }

    static void debug(String message) {
        if (DEBUG) {
            System.err.println(message);
        }
    }

    static void error(Throwable throwable) {
        if (ERROR) {
            throwable.printStackTrace(System.err);
        }
    }


    static void checkArgument(int val, int cmp, String message) {
        if (val != cmp) {
            throw new IllegalArgumentException(message);
        }
    }

    static void checkArgument(Object val, Object cmp, String message) {
        if (!Objects.equals(val, cmp)) {
            throw new IllegalArgumentException(message);
        }
    }

    static void checkTrue(boolean condition, String message) {
        if (!condition) {
            throw new IllegalArgumentException(message);
        }
    }

    static void runnable(Runnable runnable, int timeout) {
        ExecutorService service = Executors.newSingleThreadExecutor();
        try {
            service.execute(runnable);
            service.shutdown();
            if (!service.awaitTermination(timeout, TimeUnit.SECONDS)) {
                service.shutdownNow();
            }
        } catch (InterruptedException interruptedException) {
            service.shutdownNow();
        }
    }

}
