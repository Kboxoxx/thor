package tech.lp2p.dag;


import java.io.IOException;
import java.util.Objects;
import java.util.Stack;

import tech.lp2p.core.Cid;
import tech.lp2p.proto.Merkledag;
import tech.lp2p.proto.Unixfs;
import tech.lp2p.utils.Utils;

public record DagWalker(Merkledag.PBNode root) {


    static DagWalker createWalker(Merkledag.PBNode node) {
        return new DagWalker(node);
    }

    private static boolean up(DagVisitor dagVisitor) {

        if (dagVisitor.isPresent()) {
            dagVisitor.popStage();
        } else {
            return false;
        }
        if (dagVisitor.isPresent()) {
            boolean result = nextChild(dagVisitor);
            if (result) {
                return true;
            } else {
                return up(dagVisitor);
            }
        } else {
            return false;
        }
    }

    private static boolean nextChild(DagVisitor dagVisitor) {
        DagStage dagStage = dagVisitor.peekStage();
        Merkledag.PBNode activeNode = dagStage.node();

        if (dagStage.index().get() + 1 < activeNode.getLinksCount()) {
            dagStage.incrementIndex();
            return true;
        }

        return false;
    }

    private static boolean down(DagFetch dagFetch, DagVisitor dagVisitor)
            throws IOException {

        Merkledag.PBNode child = fetchChild(dagFetch, dagVisitor);
        if (child != null) {
            dagVisitor.pushActiveNode(child);
            return true;
        }
        return false;
    }


    private static Merkledag.PBNode fetchChild(DagFetch dagFetch, DagVisitor dagVisitor)
            throws IOException {
        DagStage dagStage = dagVisitor.peekStage();
        Merkledag.PBNode activeNode = dagStage.node();
        int index = dagStage.index().get();
        Objects.requireNonNull(activeNode);

        if (index >= activeNode.getLinksCount()) {
            return null;
        }

        return DagService.getNode(dagFetch, child(activeNode, index));
    }

    private static Result seek(DagFetch dagFetch, Stack<DagStage> stack, int offset)
            throws IOException {

        Utils.checkTrue(offset >= 0, "invalid offset");

        if (offset == 0) {
            return new Result(stack, 0);
        }

        int left = offset;
        DagStage peek = stack.peek();
        Merkledag.PBNode node = peek.node();

        if (node.getLinksCount() > 0) {
            // Internal node, should be a `mdag.ProtoNode` containing a
            // `unixfs.FSNode` (see the `balanced` package for more details).
            Unixfs.Data unixData = DagReader.getData(node);

            // Internal nodes have no data, so just iterate through the
            // sizes of its children (advancing the child index of the
            // `dagWalker`) to find where we need to go down to next in
            // the search
            for (int i = 0; i < unixData.getBlocksizesCount(); i++) {

                long childSize = unixData.getBlocksizes(i);

                if (childSize > left) {
                    stack.peek().setIndex(i);

                    Merkledag.PBNode fetched = DagService.getNode(
                            dagFetch, child(node, i));
                    stack.push(DagStage.createDagStage(fetched));

                    return seek(dagFetch, stack, left);
                }
                left -= (int) childSize;
            }
        }

        return new Result(stack, left);
    }


    private static Cid child(Merkledag.PBNode node, int index) {
        return Cid.createCid(node.getLinks(index).getHash().toByteArray());
    }


    public DagStage next(DagFetch dagFetch, DagVisitor dagVisitor) throws IOException {

        if (!dagVisitor.isRootVisited(true)) {
            DagStage dagStage = dagVisitor.peekStage();
            Objects.requireNonNull(dagStage);
            if (dagStage.node().equals(root)) {
                return dagStage;
            }
        }

        if (dagVisitor.isPresent()) {
            boolean success = down(dagFetch, dagVisitor);
            if (success) {
                DagStage dagStage = dagVisitor.peekStage();
                Objects.requireNonNull(dagStage);
                return dagStage;
            }

            success = up(dagVisitor);

            if (success) {
                return next(dagFetch, dagVisitor);
            }
        }
        return null; // done, nothing left
    }

    Result seek(DagFetch dagFetch, int offset) throws IOException {

        Stack<DagStage> stack = new Stack<>();
        stack.push(DagStage.createDagStage(root()));

        return seek(dagFetch, stack, offset);

    }

    record Result(Stack<DagStage> stack, int left) {
    }

}

