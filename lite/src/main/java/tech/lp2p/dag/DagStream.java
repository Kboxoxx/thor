package tech.lp2p.dag;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Dir;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Info;
import tech.lp2p.core.Raw;
import tech.lp2p.proto.Merkledag;
import tech.lp2p.proto.Unixfs;
import tech.lp2p.utils.Utils;


public interface DagStream {


    static Info info(BlockStore blockStore, Cid cid) throws IOException {
        Merkledag.PBNode node = DagService.getNode(blockStore, cid);
        Objects.requireNonNull(node);
        return info(cid, node);
    }


    static Info info(Cid cid, Merkledag.PBNode node) throws IOException {
        Unixfs.Data unixData = DagService.info(node);
        if (unixData.getType() == Unixfs.Data.DataType.Directory) {
            return new Dir(cid, unixData.getFilesize(), unixData.getName());
        }
        if (unixData.getType() == Unixfs.Data.DataType.File) {
            return new Fid(cid, unixData.getFilesize(), unixData.getName());
        }
        if (unixData.getType() == Unixfs.Data.DataType.Raw) {
            return new Raw(cid, unixData.getFilesize(), Utils.STRING_EMPTY);
        }
        throw new IllegalStateException();
    }


    static Dir createEmptyDirectory(BlockStore blockStore, String name) {
        return DagService.createEmptyDirectory(blockStore, name);
    }


    static Dir addToDirectory(BlockStore blockStore, Cid directory, Info info)
            throws IOException {

        byte[] block = blockStore.getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = DagService.node(block);
        Objects.requireNonNull(dirNode);
        return DagService.addChild(blockStore, dirNode, info);

    }


    static Dir updateDirectory(BlockStore blockStore, Cid directory,
                               Info info) throws IOException {
        byte[] block = blockStore.getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = DagService.node(block);
        Objects.requireNonNull(dirNode);
        return DagService.updateDirectory(blockStore, dirNode, info);
    }


    static Dir createDirectory(BlockStore blockStore, String name, List<Info> links) {
        return DagService.createDirectory(blockStore, name, links);
    }

    static Dir renameDirectory(BlockStore blockStore, Cid directory,
                               String name) throws IOException {
        byte[] block = blockStore.getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = DagService.node(block);
        Objects.requireNonNull(dirNode);
        return DagService.renameDirectory(blockStore, dirNode, name);
    }


    static Dir removeFromDirectory(BlockStore blockStore, Cid directory,
                                   Info info) throws IOException {
        byte[] block = blockStore.getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = DagService.node(block);
        Objects.requireNonNull(dirNode);
        return DagService.removeChild(blockStore, dirNode, info);
    }


    static List<Cid> blocks(BlockStore blockStore, Cid cid) throws IOException {
        List<Cid> result = new ArrayList<>();

        byte[] block = blockStore.getBlock(cid);
        if (block != null) { // block is locally available
            Merkledag.PBNode node = DagService.node(block);
            List<Merkledag.PBLink> links = node.getLinksList();

            for (Merkledag.PBLink link : links) {
                Cid child = Cid.createCid(link.getHash().toByteArray());
                result.add(child);
                result.addAll(blocks(blockStore, child));
            }
        }
        return result;
    }

    static void removeBlocks(BlockStore blockStore, Cid cid) throws IOException {
        byte[] block = blockStore.getBlock(cid);
        if (block != null) { // block is locally available
            Merkledag.PBNode node = DagService.node(block);
            List<Merkledag.PBLink> links = node.getLinksList();

            for (Merkledag.PBLink link : links) {
                Cid child = Cid.createCid(link.getHash().toByteArray());
                removeBlocks(blockStore, child);
            }
            blockStore.deleteBlock(cid);
        }
    }

    static List<Info> childs(BlockStore blockStore, Dir dir) throws IOException {
        Merkledag.PBNode node = DagService.getNode(blockStore, dir.cid());
        Objects.requireNonNull(node);
        Unixfs.Data unixData = DagService.info(node);
        DagService.checkDirectory(unixData);

        List<Info> result = new ArrayList<>();
        List<Merkledag.PBLink> links = node.getLinksList();
        for (Merkledag.PBLink link : links) {
            String name = link.getName();
            Merkledag.PBLinkType type = link.getType();
            Cid cid = Cid.createCid(link.getHash().toByteArray());
            if (type != null) {
                switch (type) {
                    case Dir -> result.add(new Dir(cid, link.getTsize(), name));
                    case File -> result.add(new Fid(cid, link.getTsize(), name));
                }
            }
        }
        return result;
    }

    static List<Raw> raws(BlockStore blockStore, Fid fid) throws IOException {
        Merkledag.PBNode node = DagService.getNode(blockStore, fid.cid());
        Objects.requireNonNull(node);
        Unixfs.Data unixData = DagService.info(node);
        if (unixData.getType() != Unixfs.Data.DataType.File) {
            throw new IllegalStateException("invalid node");
        }
        List<Raw> result = new ArrayList<>();
        List<Merkledag.PBLink> links = node.getLinksList();
        for (Merkledag.PBLink link : links) {
            result.add(new Raw(Cid.createCid(link.getHash().toByteArray()),
                    link.getTsize(), Utils.STRING_EMPTY));
        }
        return result;
    }

    static boolean hasChild(DagFetch dagFetch, Dir dir,
                            String name) throws IOException {
        Merkledag.PBNode node = DagService.getNode(dagFetch, dir.cid());
        Objects.requireNonNull(node);
        Unixfs.Data unixData = DagService.info(node);
        DagService.checkDirectory(unixData);
        return DagReader.hasLink(node, name);
    }

    static boolean hasChild(BlockStore blockStore, Dir dir,
                            String name) throws IOException {
        Merkledag.PBNode node = DagService.getNode(blockStore, dir.cid());
        Objects.requireNonNull(node);
        Unixfs.Data unixData = DagService.info(node);
        DagService.checkDirectory(unixData);
        return DagReader.hasLink(node, name);
    }


    static Fid readInputStream(BlockStore blockStore, String name,
                               DagInputStream dagInputStream) throws IOException {

        return DagService.createFromStream(blockStore, name, dagInputStream);
    }

}
