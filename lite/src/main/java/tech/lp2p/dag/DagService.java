package tech.lp2p.dag;

import com.google.protobuf.ByteString;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import tech.lp2p.Lite;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Dir;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Hash;
import tech.lp2p.core.Info;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Response;
import tech.lp2p.core.Status;
import tech.lp2p.proto.Merkledag;
import tech.lp2p.proto.Unixfs;
import tech.lp2p.utils.Utils;

public interface DagService {


    private static Merkledag.PBNode createRawNode(byte[] bytes) {
        Utils.checkTrue(bytes.length <= Lite.CHUNK_DATA, "Illegal size");
        Unixfs.Data.Builder data = Unixfs.Data.newBuilder().setType(Unixfs.Data.DataType.Raw)
                .setFilesize(bytes.length)
                .setData(ByteString.copyFrom(bytes));

        Merkledag.PBNode.Builder pbn = Merkledag.PBNode.newBuilder();
        pbn.setData(ByteString.copyFrom(data.build().toByteArray()));

        return pbn.build();
    }

    static DirectoryNode createDirectory(String name, List<Info> infos) {
        Unixfs.Data.Builder builder = Unixfs.Data.newBuilder()
                .setName(name)
                .setType(Unixfs.Data.DataType.Directory);
        Merkledag.PBNode.Builder pbn = Merkledag.PBNode.newBuilder();
        long fileSize = 0;
        for (Info info : infos) {
            Utils.checkTrue(!info.isRaw(), "Raw type can not be added to directory");

            long size = info.size();
            fileSize = fileSize + size;
            builder.addBlocksizes(size);


            Merkledag.PBLink.Builder lnb = createLink(info);

            pbn.addLinks(lnb.build());
        }
        builder.setFilesize(fileSize);
        byte[] unixData = builder.build().toByteArray();
        pbn.setData(ByteString.copyFrom(unixData));
        return new DirectoryNode(pbn.build(), fileSize, name);

    }

    static DirectoryNode createDirectory(String name) {
        byte[] unixData = Unixfs.Data.newBuilder()
                .setType(Unixfs.Data.DataType.Directory)
                .setName(name)
                .build().toByteArray();
        Merkledag.PBNode.Builder pbn = Merkledag.PBNode.newBuilder();
        pbn.setData(ByteString.copyFrom(unixData));
        return new DirectoryNode(pbn.build(), 0, name);
    }

    static Unixfs.Data info(Merkledag.PBNode node) throws IOException {
        return DagReader.getData(node);
    }


    static Merkledag.PBNode getNode(DagFetch dagFetch, Cid cid) throws IOException {
        byte[] block = dagFetch.blockStore().getBlock(cid);
        if (block != null) {
            return node(block);
        }

        Response response = dagFetch.getBlock(cid);
        if (response.status() == Status.OK) {
            // only Multicodec.DAG_PB is supported here
            Cid cmp = Cid.createCid(Hash.createHash(response.body()));

            if (!Objects.equals(cmp, cid)) {
                throw new IOException("Invalid Cid block returned");
            }
            Merkledag.PBNode node = Merkledag.PBNode.parseFrom(response.body()); // only to check if valid
            dagFetch.blockStore().storeBlock(cmp, node.toByteArray());
            return node;
        }

        throw new IOException("Cid could not be fetched");
    }


    static Merkledag.PBNode getNode(BlockStore blockStore, Cid cid) throws IOException {
        byte[] block = blockStore.getBlock(cid);
        Objects.requireNonNull(block, "Cid is not available in block store");
        return node(block);
    }


    static Merkledag.PBNode node(byte[] block) throws IOException {
        return Merkledag.PBNode.parseFrom(block);
    }

    static Raw createRaw(BlockStore blockStore, byte[] data) {
        Merkledag.PBNode node = DagService.createRawNode(data);
        byte[] bytes = node.toByteArray();
        Cid cid = Cid.createCid(Hash.createHash(bytes));
        blockStore.storeBlock(cid, bytes);
        return new Raw(cid, data.length, Utils.STRING_EMPTY);
    }

    static Cid createRaw(byte[] data) {
        Merkledag.PBNode node = DagService.createRawNode(data);
        byte[] bytes = node.toByteArray();
        return Cid.createCid(Hash.createHash(bytes));
    }

    static Dir createDirectory(BlockStore blockStore, String name, List<Info> links) {
        DagService.DirectoryNode directory = DagService.createDirectory(name, links);
        byte[] bytes = directory.node().toByteArray();
        Cid cid = Cid.createCid(Hash.createHash(bytes));
        blockStore.storeBlock(cid, bytes);
        return new Dir(cid, directory.size(), directory.name());
    }

    static Dir createEmptyDirectory(BlockStore blockStore, String name) {
        DagService.DirectoryNode directory = DagService.createDirectory(name);
        byte[] bytes = directory.node().toByteArray();
        Cid cid = Cid.createCid(Hash.createHash(bytes));
        blockStore.storeBlock(cid, bytes);
        return new Dir(cid, directory.size(), directory.name());
    }

    static void checkDirectory(Unixfs.Data unixData) {
        if (unixData.getType() != Unixfs.Data.DataType.Directory) {
            throw new IllegalArgumentException("not a directory");
        }
    }


    static Dir addChild(BlockStore blockStore, Merkledag.PBNode dirNode, Info info)
            throws IOException {

        Unixfs.Data unixData = DagReader.getData(dirNode);
        checkDirectory(unixData);

        long dirSize = unixData.getFilesize();

        Merkledag.PBLink.Builder lnb = createLink(info);

        long newDirSize = dirSize + info.size();

        Unixfs.Data.Builder builder = unixData.toBuilder();
        builder.setFilesize(newDirSize);
        builder.addBlocksizes(info.size());

        Merkledag.PBNode.Builder pbn = dirNode.toBuilder();
        pbn.addLinks(lnb.build());
        Unixfs.Data updateUnixData = builder.build();
        pbn.setData(ByteString.copyFrom(updateUnixData.toByteArray()));

        byte[] bytes = pbn.build().toByteArray();
        Cid cid = Cid.createCid(Hash.createHash(bytes));
        blockStore.storeBlock(cid, bytes);

        return new Dir(cid, updateUnixData.getFilesize(), updateUnixData.getName());
    }

    private static Merkledag.PBLink.Builder createLink(Info info) {
        if (info.isDir()) {
            return Merkledag.PBLink.newBuilder()
                    .setName(info.name())
                    .setType(Merkledag.PBLinkType.Dir)
                    .setHash(ByteString.copyFrom(info.cid().hash()))
                    .setTsize(info.size());
        }
        if (info.isFid()) {
            return Merkledag.PBLink.newBuilder()
                    .setName(info.name())
                    .setType(Merkledag.PBLinkType.File)
                    .setHash(ByteString.copyFrom(info.cid().hash()))
                    .setTsize(info.size());
        }
        if (info.isRaw()) {
            return Merkledag.PBLink.newBuilder()
                    .setHash(ByteString.copyFrom(info.cid().hash()))
                    .setTsize(info.size());
        }
        throw new IllegalStateException("not supported link");
    }


    static Dir updateDirectory(BlockStore blockStore, Merkledag.PBNode dirNode,
                               Info info) throws IOException {

        Unixfs.Data unixData = DagReader.getData(dirNode);
        checkDirectory(unixData);

        long filesize = 0L;

        Merkledag.PBNode.Builder pbn = dirNode.toBuilder();
        Unixfs.Data.Builder builder = unixData.toBuilder();
        builder.clearBlocksizes();

        for (int index = 0; index < pbn.getLinksCount(); index++) {
            Merkledag.PBLink pbLink = dirNode.getLinks(index);
            if (Objects.equals(pbLink.getName(), info.name())) {
                pbn.removeLinks(index); // remove the update link when available
            } else {
                filesize = filesize + pbLink.getTsize();
                builder.addBlocksizes(pbLink.getTsize());
            }
        }

        // added the updated link
        Merkledag.PBLink.Builder lnb = createLink(info);
        pbn.addLinks(lnb);

        filesize = filesize + info.size();
        builder.addBlocksizes(info.size());
        // end update link

        builder.setFilesize(filesize);
        Unixfs.Data updateUnixData = builder.build();
        pbn.setData(ByteString.copyFrom(updateUnixData.toByteArray()));
        byte[] bytes = pbn.build().toByteArray();
        Cid cid = Cid.createCid(Hash.createHash(bytes));
        blockStore.storeBlock(cid, bytes);

        return new Dir(cid, updateUnixData.getFilesize(), updateUnixData.getName());
    }

    static Dir removeChild(BlockStore blockStore, Merkledag.PBNode dirNode,
                           Info info) throws IOException {

        Unixfs.Data unixData = DagReader.getData(dirNode);
        checkDirectory(unixData);

        long filesize = 0L;

        Merkledag.PBNode.Builder pbn = dirNode.toBuilder();
        Unixfs.Data.Builder builder = unixData.toBuilder();
        builder.clearBlocksizes();

        for (int index = 0; index < pbn.getLinksCount(); index++) {
            Merkledag.PBLink link = dirNode.getLinks(index);
            if (Objects.equals(link.getName(), info.name())) {
                pbn.removeLinks(index);
            } else {
                filesize = filesize + link.getTsize();
                builder.addBlocksizes(link.getTsize());
            }
        }

        builder.setFilesize(filesize);
        Unixfs.Data updateUnixData = builder.build();
        pbn.setData(ByteString.copyFrom(updateUnixData.toByteArray()));

        byte[] bytes = pbn.build().toByteArray();
        Cid cid = Cid.createCid(Hash.createHash(bytes));
        blockStore.storeBlock(cid, bytes);

        return new Dir(cid, updateUnixData.getFilesize(), updateUnixData.getName());

    }


    static Fid createFromStream(BlockStore blockStore, String name,
                                DagInputStream reader) throws IOException {
        return DagWriter.trickle(name, reader, blockStore);
    }

    static Dir renameDirectory(BlockStore blockStore, Merkledag.PBNode dirNode, String name)
            throws IOException {
        Unixfs.Data unixData = DagReader.getData(dirNode);
        checkDirectory(unixData);

        Merkledag.PBNode.Builder pbn = dirNode.toBuilder();
        Unixfs.Data.Builder builder = unixData.toBuilder();
        builder.setName(name);
        Unixfs.Data updateUnitData = builder.build();
        pbn.setData(ByteString.copyFrom(updateUnitData.toByteArray()));

        byte[] bytes = pbn.build().toByteArray();
        Cid cid = Cid.createCid(Hash.createHash(bytes));
        blockStore.storeBlock(cid, bytes);

        return new Dir(cid, updateUnitData.getFilesize(), updateUnitData.getName());
    }

    record DirectoryNode(Merkledag.PBNode node, long size, String name) {
    }


}
