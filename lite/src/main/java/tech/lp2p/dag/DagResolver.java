package tech.lp2p.dag;


import java.io.IOException;
import java.util.List;
import java.util.Objects;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Info;
import tech.lp2p.proto.Merkledag;


public interface DagResolver {


    static Info resolvePath(DagFetch dagFetch, Cid root,
                            List<String> path) throws IOException {

        Cid cid = resolveToLastNode(dagFetch, root, path);
        Objects.requireNonNull(cid);
        Merkledag.PBNode node = DagService.getNode(dagFetch, cid);
        Objects.requireNonNull(node);
        return DagStream.info(cid, node);
    }


    static Info resolvePath(BlockStore blockStore, Cid root,
                            List<String> path) throws IOException {

        Cid cid = resolveToLastNode(blockStore, root, path);
        Objects.requireNonNull(cid);
        Merkledag.PBNode node = DagService.getNode(blockStore, cid);
        Objects.requireNonNull(node);
        return DagStream.info(cid, node);
    }


    private static Cid resolveToLastNode(BlockStore blockStore, Cid root,
                                         List<String> path) throws IOException {

        if (path.isEmpty()) {
            return root;
        }

        Cid cid = root;
        Merkledag.PBNode node = DagService.getNode(blockStore, cid);

        for (String name : path) {
            Merkledag.PBLink lnk = DagReader.getLinkByName(node, name);
            cid = Cid.createCid(lnk.getHash().toByteArray());
            node = DagService.getNode(blockStore, cid);
        }

        return cid;

    }


    private static Cid resolveToLastNode(DagFetch dagFetch, Cid root,
                                         List<String> path) throws IOException {

        if (path.isEmpty()) {
            return root;
        }

        Cid cid = root;

        Merkledag.PBNode node = DagService.getNode(dagFetch, cid);
        for (String name : path) {
            Merkledag.PBLink lnk = DagReader.getLinkByName(node, name);
            cid = Cid.createCid(lnk.getHash().toByteArray());
            node = DagService.getNode(dagFetch, cid);
        }

        return cid;

    }


}
