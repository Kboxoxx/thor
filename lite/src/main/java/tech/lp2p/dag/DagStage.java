package tech.lp2p.dag;

import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.proto.Merkledag;

public record DagStage(Merkledag.PBNode node, AtomicInteger index) {

    public static DagStage createDagStage(Merkledag.PBNode node) {
        return new DagStage(node, new AtomicInteger(0));
    }

    public void incrementIndex() {
        index.incrementAndGet();
    }


    public void setIndex(int value) {
        index.set(value);
    }

}
