package tech.lp2p.dag;

import java.util.Stack;
import java.util.concurrent.atomic.AtomicBoolean;

import tech.lp2p.proto.Merkledag;

public record DagVisitor(Stack<DagStage> stack, AtomicBoolean rootVisited) {

    public static DagVisitor createDagVisitor(Merkledag.PBNode root) {
        DagVisitor dagVisitor = new DagVisitor(new Stack<>(), new AtomicBoolean(false));
        dagVisitor.pushActiveNode(root);
        return dagVisitor;
    }

    public void reset(Stack<DagStage> dagStages) {
        stack.clear();
        stack.addAll(dagStages);
        rootVisited.set(false);
    }

    public void pushActiveNode(Merkledag.PBNode node) {
        stack.push(DagStage.createDagStage(node));
    }

    public void popStage() {
        stack.pop();
    }

    public DagStage peekStage() {
        return stack.peek();
    }

    public boolean isRootVisited(boolean visited) {
        return rootVisited.getAndSet(visited);
    }

    public boolean isPresent() {
        return !stack.isEmpty();
    }
}
