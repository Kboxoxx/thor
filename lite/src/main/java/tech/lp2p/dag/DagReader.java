package tech.lp2p.dag;


import com.google.protobuf.ByteString;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.proto.Merkledag;
import tech.lp2p.proto.Unixfs;

public record DagReader(DagVisitor dagVisitor, DagWalker dagWalker,
                        AtomicInteger left, long size) {

    public static DagReader create(Merkledag.PBNode node) throws IOException {
        long size = 0;

        Unixfs.Data unixData = DagReader.getData(node);

        switch (unixData.getType()) {
            case Raw, File -> size = unixData.getFilesize();
        }


        DagWalker dagWalker = DagWalker.createWalker(node);
        return new DagReader(DagVisitor.createDagVisitor(dagWalker.root()), dagWalker,
                new AtomicInteger(0), size);

    }


    public static Merkledag.PBLink getLinkByName(Merkledag.PBNode node, String name) {
        for (Merkledag.PBLink link : node.getLinksList()) {
            if (Objects.equals(link.getName(), name)) {
                return link;
            }
        }
        throw new IllegalStateException("Link with " + name + " not found");
    }


    public static boolean hasLink(Merkledag.PBNode node, String name) {

        for (Merkledag.PBLink link : node.getLinksList()) {
            if (Objects.equals(link.getName(), name)) {
                return true;
            }
        }
        return false;

    }


    public static Unixfs.Data getData(Merkledag.PBNode node) throws IOException {
        return Unixfs.Data.parseFrom(node.getData().toByteArray());
    }

    private static ByteString readUnixNodeData(Unixfs.Data unixData, int position) {

        return switch (unixData.getType()) {
            case Directory, File, Raw -> unixData.getData().substring(position);
            default -> throw new IllegalStateException("found %s node in unexpected place " +
                    unixData.getType().name());
        };
    }

    public void seek(DagFetch dagFetch, int offset) throws IOException {
        DagWalker.Result result = dagWalker.seek(dagFetch, offset);
        this.left.set(result.left());
        this.dagVisitor.reset(result.stack());
    }


    public ByteString loadNextData(DagFetch dagFetch) throws IOException {

        int left = this.left.getAndSet(0);
        if (left > 0) {
            DagStage dagStage = dagVisitor.peekStage();
            Merkledag.PBNode node = dagStage.node();

            if (node.getLinksCount() == 0) {
                return readUnixNodeData(DagReader.getData(node), left);
            }
        }

        while (true) {
            DagStage dagStage = dagWalker.next(dagFetch, dagVisitor);
            if (dagStage == null) {
                // done nothing left
                return ByteString.EMPTY;
            }

            Merkledag.PBNode node = dagStage.node();
            if (node.getLinksCount() > 0) {
                continue;
            }

            return readUnixNodeData(DagReader.getData(node), 0);
        }
    }
}
