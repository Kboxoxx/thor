package tech.lp2p.dag;

import com.google.protobuf.ByteString;

import java.io.IOException;

import tech.lp2p.Lite;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Hash;
import tech.lp2p.core.Info;
import tech.lp2p.core.Raw;
import tech.lp2p.proto.Merkledag;
import tech.lp2p.proto.Unixfs;


interface DagWriter {
    int DEPTH_REPEAT = 4;
    int LINKS_PER_BLOCK = 1000; // kubo it is 175 [but not big performance gain]


    private static long fillNodeLayer(DagInputStream reader,
                                      BlockStore blockStore,
                                      Unixfs.Data.Builder data,
                                      Merkledag.PBNode.Builder pbn) throws IOException {
        byte[] bytes = new byte[Lite.CHUNK_DATA];
        int numChildren = 0;
        long size = 0L;
        while ((numChildren < LINKS_PER_BLOCK && !reader.done())) {
            int read = reader.read(bytes);
            if (read > 0) {
                if (reader.done() && numChildren == 0) {
                    data.setData(ByteString.copyFrom(bytes, 0, read));
                    size = size + read;
                    break;
                } else {
                    createRawBlock(blockStore, data, pbn, bytes, read);
                    size = size + read;
                }
            }
            numChildren++;
        }
        return size;
    }

    private static void createRawBlock(BlockStore blockStore,
                                       Unixfs.Data.Builder data,
                                       Merkledag.PBNode.Builder pbn,
                                       byte[] bytes, int read) {
        Unixfs.Data.Builder unixfs =
                Unixfs.Data.newBuilder().setType(Unixfs.Data.DataType.Raw)
                        .setFilesize(read)
                        .setData(ByteString.copyFrom(bytes, 0, read));

        Merkledag.PBNode.Builder builder = Merkledag.PBNode.newBuilder();
        builder.setData(ByteString.copyFrom(unixfs.build().toByteArray()));

        byte[] block = builder.build().toByteArray();
        Cid cid = Cid.createCid(Hash.createHash(block));
        blockStore.storeBlock(cid, block);
        addChild(data, pbn, cid, read);
    }


    static Fid trickle(String name, DagInputStream reader,
                       BlockStore blockStore) throws IOException {
        return (Fid) fillTrickleRec(reader, blockStore, Unixfs.Data.newBuilder()
                .setType(Unixfs.Data.DataType.File)
                .setName(name)
                .setFilesize(0L), Merkledag.PBNode.newBuilder(), -1);
    }


    private static Info fillTrickleRec(DagInputStream reader, BlockStore blockStore,
                                       Unixfs.Data.Builder data, Merkledag.PBNode.Builder pbn,
                                       int maxDepth) throws IOException {
        // Always do this, even in the base case
        long size = fillNodeLayer(reader, blockStore, data, pbn);

        for (int depth = 1; maxDepth == -1 || depth < maxDepth; depth++) {
            if (reader.done()) {
                break;
            }

            for (int repeatIndex = 0; repeatIndex < DEPTH_REPEAT && !reader.done(); repeatIndex++) {

                Info result = fillTrickleRec(reader, blockStore, Unixfs.Data.newBuilder()
                        .setType(Unixfs.Data.DataType.Raw)
                        .setFilesize(0L), Merkledag.PBNode.newBuilder(), depth);

                long fileSize = result.size();
                addChild(data, pbn, result.cid(), fileSize);
                size = size + fileSize;
            }
        }

        data.setFilesize(size);
        pbn.setData(ByteString.copyFrom(data.build().toByteArray()));
        Merkledag.PBNode node = pbn.build();


        byte[] bytes = node.toByteArray();
        Cid cid = Cid.createCid(Hash.createHash(bytes));
        blockStore.storeBlock(cid, bytes);

        if (data.getType() == Unixfs.Data.DataType.File) {
            String name = data.getName();
            return new Fid(cid, size, name);
        } else {
            return new Raw(cid, size, "");
        }
    }


    private static void addChild(Unixfs.Data.Builder data,
                                 Merkledag.PBNode.Builder pbn,
                                 Cid cid, long fileSize) {
        Merkledag.PBLink.Builder lnb = Merkledag.PBLink.newBuilder().setTsize(fileSize);
        lnb.setHash(ByteString.copyFrom(cid.hash()));

        pbn.addLinks(lnb.build());
        data.addBlocksizes(fileSize);
    }
}
