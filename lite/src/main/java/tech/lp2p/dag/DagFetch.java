package tech.lp2p.dag;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Response;

public interface DagFetch {


    Response getBlock(Cid cid);


    BlockStore blockStore();

}
