/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.tls;


import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.X509TrustManager;

public record TlsServerEngineFactory(X509Certificate[] x509Certificates,
                                     PrivateKey certificateKey) {


    public static TlsServerEngineFactory createTlsServerEngineFactory(
            X509Certificate x509Certificate, PrivateKey privateKey) {
        X509Certificate[] x509Certificates = new X509Certificate[1];
        x509Certificates[0] = x509Certificate;
        return new TlsServerEngineFactory(x509Certificates, privateKey);
    }

    public TlsServerEngine createServerEngine(X509TrustManager trustManager,
                                              ServerMessageSender serverMessageSender,
                                              TlsStatusEventHandler tlsStatusHandler) {

        TlsServerEngine tlsServerEngine = new TlsServerEngine(trustManager, x509Certificates, certificateKey,
                serverMessageSender, tlsStatusHandler);
        tlsServerEngine.addSupportedCiphers(List.of(CipherSuite.TLS_AES_128_GCM_SHA256));
        return tlsServerEngine;
    }
}
