package tech.lp2p;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.net.ssl.X509TrustManager;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Certificate;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Dir;
import tech.lp2p.core.FileStore;
import tech.lp2p.core.Info;
import tech.lp2p.core.Keys;
import tech.lp2p.core.Limit;
import tech.lp2p.core.MemoryPeers;
import tech.lp2p.core.Network;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Progress;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Protocols;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Request;
import tech.lp2p.core.RequestHandler;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.ResolveInfo;
import tech.lp2p.core.Responder;
import tech.lp2p.core.Response;
import tech.lp2p.core.Status;
import tech.lp2p.core.TrustManager;
import tech.lp2p.crypto.Ed25519Sign;
import tech.lp2p.dag.DagResolver;
import tech.lp2p.dag.DagService;
import tech.lp2p.dag.DagStream;
import tech.lp2p.http3.Http3ApplicationProtocolFactory;
import tech.lp2p.ident.IdentifyHandler;
import tech.lp2p.ident.IdentifyPushHandler;
import tech.lp2p.lite.LiteApplicationProtocolFactory;
import tech.lp2p.lite.LiteConnectionFactory;
import tech.lp2p.lite.LiteFetchManager;
import tech.lp2p.lite.LiteProgressStream;
import tech.lp2p.lite.LiteReader;
import tech.lp2p.lite.LiteReaderStream;
import tech.lp2p.lite.LiteRequestHandler;
import tech.lp2p.lite.LiteService;
import tech.lp2p.lite.LiteStreamHandler;
import tech.lp2p.quic.ServerConnector;
import tech.lp2p.relay.RelayDialer;
import tech.lp2p.relay.RelayHopHandler;
import tech.lp2p.relay.RelayService;
import tech.lp2p.relay.RelayStopHandler;
import tech.lp2p.utils.Utils;


public final class Lite implements AutoCloseable {
    public static final int DEFAULT_MAX_HEADER_SIZE = 10 * 1024;
    public static final int CHUNK_BASIS = 1024;
    public static final int CHUNK_DEFAULT = 4 * Lite.CHUNK_BASIS;
    public static final int CHUNK_DATA = CHUNK_BASIS * 50;
    public static final int GRACE_PERIOD = 15;
    public static final int DHT_ALPHA = 30;
    public static final int DHT_CONCURRENCY = 5;
    public static final int TIMEOUT = 5; // in seconds
    public static final String ROOT_PATH = "/";
    public static final String AGENT = "lite/1.0.0/";

    public static final long LIMIT_BYTES = 1024;
    public static final int LIMIT_DURATION = 15 * 50; // in seconds [15 min]


    private final Certificate certificate;
    private final X509TrustManager trustManager;
    private final String agent;
    private final Peeraddrs bootstrap;
    private final PeerStore peerStore;
    private final BlockStore blockStore;

    private final Consumer<Reservation> reservationGain;
    private final Consumer<Reservation> reservationLost;


    private final Protocols protocols = new Protocols();
    private final LiteConnectionFactory connectionFactory = new LiteConnectionFactory(this);
    private final LiteRequestHandler requestHandler = new LiteRequestHandler(this);
    private final ServerConnector serverConnector;
    private final DatagramSocket socket;


    private Lite(Certificate certificate,
                 X509TrustManager trustManager,
                 Peeraddrs bootstrap,
                 PeerStore peerStore,
                 BlockStore blockStore,
                 Limit limit,
                 String agent,
                 Function<PeerId, Boolean> isGated,
                 Consumer<Peeraddr> connectionGain,
                 Consumer<Peeraddr> connectionLost,
                 Consumer<Reservation> reservationGain,
                 Consumer<Reservation> reservationLost,
                 DatagramSocket socket) {

        Objects.requireNonNull(certificate);
        Objects.requireNonNull(trustManager);
        Objects.requireNonNull(agent);
        Objects.requireNonNull(blockStore);
        Objects.requireNonNull(limit);
        Objects.requireNonNull(bootstrap);
        Objects.requireNonNull(peerStore);
        Objects.requireNonNull(isGated);
        Objects.requireNonNull(connectionGain);
        Objects.requireNonNull(connectionLost);
        Objects.requireNonNull(reservationGain);
        Objects.requireNonNull(reservationLost);
        Objects.requireNonNull(socket);

        this.blockStore = blockStore;
        this.reservationGain = reservationGain;
        this.reservationLost = reservationLost;
        this.bootstrap = bootstrap;
        this.agent = agent;
        this.certificate = certificate;
        this.trustManager = trustManager;
        this.peerStore = peerStore;
        this.socket = socket;


        protocols.put(Protocol.MULTISTREAM_PROTOCOL, new LiteStreamHandler());
        protocols.put(Protocol.IDENTITY_PROTOCOL, new IdentifyHandler(this));
        if (Protocol.IDENTITY_PUSH_PROTOCOL.enabled()) {
            protocols.put(Protocol.IDENTITY_PUSH_PROTOCOL, new IdentifyPushHandler());
        }

        protocols.put(Protocol.RELAY_PROTOCOL_STOP, new RelayStopHandler(this, isGated));
        protocols.put(Protocol.RELAY_PROTOCOL_HOP, new RelayHopHandler(this, limit));

        this.serverConnector = new ServerConnector(socket, trustManager(),
                certificate().x509Certificate(),
                certificate().certificateKey(), connectionLost);


        serverConnector.registerApplicationProtocol(ALPN.libp2p.name(),
                new LiteApplicationProtocolFactory(isGated, connectionGain,
                        new Responder(protocols)));

        serverConnector.registerApplicationProtocol(ALPN.h3.name(),
                new Http3ApplicationProtocolFactory(
                        isGated, connectionGain, requestHandler.createRequestHandler()));

        serverConnector.start();


    }

    public static Keys generateKeys() {
        try {
            Ed25519Sign.KeyPair keyPair = Ed25519Sign.KeyPair.newKeyPair();
            return new Keys(PeerId.create(keyPair.getPublicKey()),
                    keyPair.getPrivateKey());
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    public static Certificate generateCertificate() throws Exception {
        return Certificate.createCertificate(generateKeys());
    }

    public static Cid decodeCid(String cid) throws Exception {
        return Cid.decodeCid(cid);
    }

    public static PeerId decodePeerId(String pid) throws Exception {
        return PeerId.decodePeerId(pid);
    }

    // Note : the cid is not stored in the block store
    public static Cid rawCid(String string) {
        return DagService.createRaw(string.getBytes(StandardCharsets.UTF_8));
    }

    public static Peeraddr createPeeraddr(PeerId peerId, InetAddress inetAddress, int port) {
        return Peeraddr.create(peerId, inetAddress.getAddress(), port);
    }

    public static Peeraddr createPeeraddr(String peerId, String address, int port)
            throws Exception {
        return createPeeraddr(decodePeerId(peerId), InetAddress.getByName(address), port);
    }

    // Reservation feature is only possible when a public Inet6Address is available
    public static boolean reservationFeaturePossible() {
        return !Network.publicInet6Addresses().isEmpty();
    }

    public static Lite.Builder newBuilder() {
        return new Builder();
    }

    public static Lite newLite() throws Exception {
        return Lite.newBuilder().build();
    }

    public Peeraddrs reservationPeeraddrs() {
        return LiteService.reservationPeeraddrs(this);
    }

    public Set<Reservation> reservations() {
        return LiteService.reservations(this);
    }

    public Reservation doReservation(Peeraddr relay) throws Exception {
        return LiteService.hopReserve(this, relay);
    }

    public boolean hasReservations() {
        return !reservations().isEmpty();
    }

    public void doReservation(int maxReservation, int timeout) {
        LiteService.hopReserve(this, maxReservation, timeout);
    }

    public Reservation refreshReservation(Reservation reservation) throws Exception {
        return LiteService.refreshReservation(this, reservation);
    }

    public void closeReservation(Reservation reservation) {
        LiteService.closeReservation(this, reservation);
    }

    public boolean hasHopReserve(PeerId peerId) {
        return LiteService.hasHopReserve(serverConnector, peerId);
    }

    public void closeConnections(PeerId peerId) {
        for (tech.lp2p.quic.Connection connection : serverConnector.serverConnections(peerId)) {
            connection.close();
        }
    }

    public boolean hasConnection(PeerId peerId) {
        return !serverConnector.serverConnections(peerId).isEmpty();
    }

    public int numConnections() {
        return serverConnector.numConnections();
    }

    public boolean hasReservation(PeerId peerId) {
        return LiteService.hasReservation(serverConnector, peerId);
    }

    public Peeraddr loopbackAddress() {
        return Peeraddr.create(peerId(), new InetSocketAddress(
                InetAddress.getLoopbackAddress(), port()));
    }

    public int port() {
        return socket.getLocalPort();
    }

    public void addRequestHandler(String path, RequestHandler requestHandler) {
        this.requestHandler.addRequestHandler(path, requestHandler);
    }


    public Optional<Cid> fetchRoot(Peeraddr peeraddr) {
        Request request = Request.post(peeraddr, Lite.ROOT_PATH,
                Duration.ofSeconds(Lite.TIMEOUT), true, 128);
        Response response = send(request);
        if (response.status() == Status.OK) {
            return Optional.of(Cid.createCid(response.body()));
        }
        return Optional.empty();
    }

    public InputStream getInputStream(Peeraddr peeraddr, Cid cid) throws IOException {
        LiteFetchManager fetchManager = new LiteFetchManager(this, peeraddr);
        LiteReader liteReader = LiteReader.getReader(fetchManager, cid);
        return new LiteReaderStream(liteReader);
    }

    public InputStream getInputStream(Peeraddr peeraddr, Cid cid, Progress progress) throws IOException {
        LiteFetchManager fetchManager = new LiteFetchManager(this, peeraddr);
        LiteReader liteReader = LiteReader.getReader(fetchManager, cid);
        return new LiteProgressStream(liteReader, progress);
    }

    public Info resolvePath(Peeraddr peeraddr, Cid root, List<String> path) throws IOException {
        LiteFetchManager fetchManager = new LiteFetchManager(this, peeraddr);
        return DagResolver.resolvePath(fetchManager, root, path);
    }

    public String fetchText(Peeraddr peeraddr, Cid cid, Progress progress) throws IOException {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(peeraddr, outputStream, cid, progress);
            return outputStream.toString();
        }
    }

    public String fetchText(Peeraddr peeraddr, Raw raw) throws IOException {
        return fetchText(peeraddr, raw.cid());
    }

    public String fetchText(Peeraddr peeraddr, Cid cid) throws IOException {
        return fetchText(peeraddr, cid, progress -> {
        });
    }

    public byte[] fetchData(Peeraddr peeraddr, Raw raw) throws IOException {
        return fetchData(peeraddr, raw.cid());
    }

    public byte[] fetchData(Peeraddr peeraddr, Cid cid) throws IOException {
        return fetchData(peeraddr, cid, progress -> {
        });
    }

    public byte[] fetchData(Peeraddr peeraddr, Cid cid, Progress progress) throws IOException {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(peeraddr, outputStream, cid, progress);
            return outputStream.toByteArray();
        }
    }

    public void fetchToOutputStream(Peeraddr peeraddr, OutputStream outputStream,
                                    Cid cid, Progress progress) throws IOException {
        LiteFetchManager fetchManager = new LiteFetchManager(this, peeraddr);
        LiteReader.storeToStream(fetchManager, cid, outputStream, progress);
    }

    public void fetchToFile(Peeraddr peeraddr, File file, Cid cid,
                            Progress progress) throws IOException {
        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fetchToOutputStream(peeraddr, fileOutputStream, cid, progress);
        }
    }

    public boolean hasChild(Peeraddr peeraddr, Dir dir, String name) throws IOException {
        LiteFetchManager fetchManager = new LiteFetchManager(this, peeraddr);
        return DagStream.hasChild(fetchManager, dir, name);
    }


    public X509TrustManager trustManager() {
        return trustManager;
    }


    public PeerStore peerStore() {
        return peerStore;
    }


    public Peeraddrs peeraddrs() {
        return Peeraddrs.peeraddrs(peerId(), port());
    }

    public PeerId peerId() {
        return certificate.keys().peerId();
    }


    public Protocols protocols() {
        return protocols;
    }


    public Peeraddrs bootstrap() {
        return bootstrap;
    }


    public Certificate certificate() {
        return certificate;
    }


    public String agent() {
        return agent;
    }


    public ResolveInfo resolveAddress(PeerId peerId, Consumer<Peeraddr> tries,
                                      Consumer<Peeraddr> failures,
                                      int timeout) throws ConnectException {
        RelayDialer.Info info = RelayDialer.hopConnect(this, peerId, tries, failures, timeout);
        Peeraddr peeraddr = Peeraddr.create(peerId, info.connection().remoteAddress());
        connectionFactory.insert(peeraddr, info.connection());
        return new ResolveInfo(peeraddr, info.relay());
    }

    public Peeraddr resolveAddress(Peeraddr relay, PeerId target) throws ConnectException {
        Connection connection = RelayService.hopConnect(this, relay, target);
        Peeraddr peeraddr = Peeraddr.create(target, connection.remoteAddress());
        connectionFactory.insert(peeraddr, connection);
        return peeraddr;
    }


    public BlockStore blockStore() {
        return blockStore;
    }


    public Response send(Request request) {
        try {
            Connection connection = connectionFactory.connection(request);
            return connection.send(request);
        } catch (TimeoutException timeoutException) {
            return Response.create(Status.REQUEST_TIMEOUT);
        } catch (ConnectException connectException) {
            return Response.create(Status.SERVICE_UNAVAILABLE);
        } catch (InterruptedException interruptedException) {
            return Response.create(Status.CLIENT_CLOSED_REQUEST);
        }
    }


    public Consumer<Reservation> reservationGain() {
        return reservationGain;
    }


    public Consumer<Reservation> reservationLost() {
        return reservationLost;
    }

    @Override
    public void close() {
        connectionFactory.shutdown();
        serverConnector.shutdown();
    }

    public ServerConnector serverConnector() {
        return serverConnector;
    }

    public static class Builder {
        private X509TrustManager trustManager = new TrustManager();
        private String agent = Lite.AGENT;
        private Peeraddrs bootstrap = new Peeraddrs();
        private PeerStore peerStore = new MemoryPeers();
        private Certificate certificate;
        private Limit limit = new Limit(Lite.LIMIT_BYTES, Lite.LIMIT_DURATION);
        private BlockStore blockStore = null;
        private int port = 0;
        private Function<PeerId, Boolean> isGated = peerId -> false;
        private Consumer<Peeraddr> connectionGain = peeraddr -> {
        };
        private Consumer<Peeraddr> connectionLost = peeraddr -> {
        };
        private Consumer<Reservation> reservationGain = reservation -> {
        };
        private Consumer<Reservation> reservationLost = reservation -> {
        };

        public Builder agent(String agent) {
            Objects.requireNonNull(agent);
            this.agent = agent;
            return this;
        }

        // only used when starting a server
        public Builder limit(Limit limit) {
            Objects.requireNonNull(limit);
            this.limit = limit;
            return this;
        }

        // only used when starting a server
        public Builder port(int port) {
            Utils.checkTrue(port > 0, "Port not well defined");
            this.port = port;
            return this;
        }

        public Builder certificate(Certificate certificate) {
            Objects.requireNonNull(certificate);
            this.certificate = certificate;
            return this;
        }

        public Builder trustManager(X509TrustManager trustManager) {
            Objects.requireNonNull(trustManager);
            this.trustManager = trustManager;
            return this;
        }

        public Builder bootstrap(Peeraddrs bootstrap) {
            Objects.requireNonNull(bootstrap);
            this.bootstrap = bootstrap;
            return this;
        }

        public Builder peerStore(PeerStore peerStore) {
            Objects.requireNonNull(peerStore);
            this.peerStore = peerStore;
            return this;
        }

        public Builder blockStore(BlockStore blockStore) {
            Objects.requireNonNull(blockStore);
            this.blockStore = blockStore;
            return this;
        }

        // only used when running a server
        public Builder isGated(Function<PeerId, Boolean> isGated) {
            Objects.requireNonNull(isGated);
            this.isGated = isGated;
            return this;
        }

        // only used when running a server
        public Builder connectionGain(Consumer<Peeraddr> connectionGain) {
            Objects.requireNonNull(connectionGain);
            this.connectionGain = connectionGain;
            return this;
        }

        // only used when running a server
        public Builder connectionLost(Consumer<Peeraddr> connectionLost) {
            Objects.requireNonNull(connectionLost);
            this.connectionLost = connectionLost;
            return this;
        }

        // only used when running a server
        public Builder reservationLost(Consumer<Reservation> reservationLost) {
            Objects.requireNonNull(reservationLost);
            this.reservationLost = reservationLost;
            return this;
        }

        // only used when running a server
        public Builder reservationGain(Consumer<Reservation> reservationGain) {
            Objects.requireNonNull(reservationGain);
            this.reservationGain = reservationGain;
            return this;
        }

        public Lite build() throws Exception {
            if (certificate == null) {
                certificate = Lite.generateCertificate();
            }
            if (blockStore == null) {
                blockStore = new FileStore();
            }
            if (port == 0) {
                port = Utils.nextFreePort();
            }

            DatagramSocket socket = Utils.getSocket(port);

            return new Lite(certificate, trustManager, bootstrap, peerStore,
                    blockStore, limit, agent, isGated, connectionGain,
                    connectionLost, reservationGain, reservationLost, socket);
        }
    }
}
