package tech.lp2p.lite;

import java.net.InetSocketAddress;
import java.util.concurrent.TimeoutException;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Request;
import tech.lp2p.core.Response;
import tech.lp2p.core.Status;
import tech.lp2p.quic.Connection;
import tech.lp2p.quic.Requester;

public class LiteConnection implements tech.lp2p.core.Connection {
    protected final Connection connection;

    public LiteConnection(Connection connection) {
        this.connection = connection;
    }

    @Override
    public ALPN alpn() {
        return ALPN.libp2p;
    }

    @Override
    public void close() {
        connection.close();
    }

    @Override
    public InetSocketAddress remoteAddress() {
        return connection.remoteAddress();
    }

    @Override
    public boolean isConnected() {
        return connection.isConnected();
    }

    @Override
    public Response send(Request request) {
        keepAlive(request.keepAlive());
        try {
            byte[] data = Requester.createStream(connection, request.maxResponseSize())
                    .request(request.body(), request.timeout().toSeconds());

            byte[] response = LiteHandler.receiveResponse(data);

            return Response.create(Status.OK, response);
        } catch (TimeoutException timeoutException) {
            return Response.create(Status.REQUEST_TIMEOUT);
        } catch (InterruptedException interruptedException) {
            return Response.create(Status.CLIENT_CLOSED_REQUEST);
        }
    }

    public Connection quic() {
        return connection;
    }

    private void keepAlive(boolean keepAlive) {
        if (keepAlive) {
            connection.enableKeepAlive();
        } else {
            connection.disableKeepAlive();
        }
    }
}
