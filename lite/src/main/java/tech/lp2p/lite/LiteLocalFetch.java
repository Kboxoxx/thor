package tech.lp2p.lite;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Response;
import tech.lp2p.core.Status;
import tech.lp2p.dag.DagFetch;

public record LiteLocalFetch(BlockStore blockStore) implements DagFetch {

    @Override
    public Response getBlock(Cid cid) {
        return Response.create(Status.SERVICE_UNAVAILABLE);
    }
}
