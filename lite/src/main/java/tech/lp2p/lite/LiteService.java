package tech.lp2p.lite;

import java.net.ConnectException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Key;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Status;
import tech.lp2p.dht.DhtService;
import tech.lp2p.quic.Connection;
import tech.lp2p.quic.ConnectionBuilder;
import tech.lp2p.quic.Requester;
import tech.lp2p.quic.ServerConnector;
import tech.lp2p.relay.RelayService;
import tech.lp2p.utils.Utils;

public interface LiteService {

    static void hopReserve(Lite host, int maxReservation, int timeout) {

        Set<PeerId> validRelay = new HashSet<>();
        HashMap<Peeraddr, Connection> refreshRequired = new HashMap<>();


        // check if reservations are still valid and not expired
        for (tech.lp2p.quic.Connection connection : host.serverConnector().clientConnections()) {
            Reservation reservation = (Reservation) connection.getAttribute(
                    Requester.RESERVATION);
            if (reservation != null) {
                validRelay.add(reservation.peerId());  // still valid
                if (reservation.updateRequired()) {
                    // update the limited connection
                    refreshRequired.put(reservation.peeraddr(), connection);
                }
            } else {
                removeReservation(host, connection, null);
            }
        }

        AtomicInteger valid = new AtomicInteger(validRelay.size());

        ExecutorService service = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());
        try {
            for (Map.Entry<Peeraddr, tech.lp2p.quic.Connection> entry : refreshRequired.entrySet()) {
                service.execute(() -> {
                    tech.lp2p.quic.Connection connection = entry.getValue();
                    Peeraddr relay = entry.getKey();
                    try {
                        if (Thread.currentThread().isInterrupted()) {
                            return;
                        }
                        connection.setAttribute(Requester.RESERVATION,
                                RelayService.hopReserve(connection, relay, host.peerId()));

                        valid.incrementAndGet();
                    } catch (Throwable throwable) {
                        Reservation reservation = (Reservation) connection.getAttribute(
                                Requester.RESERVATION);
                        removeReservation(host, connection, reservation);
                    }
                });

            }

            Key key = host.peerId().createKey();
            service.execute(() -> {
                // fill up reservations [not yet enough]
                DhtService.findClosestPeers(host, key, peeraddr -> {

                    // there is a valid reservation
                    if (validRelay.contains(peeraddr.peerId())) {
                        return;
                    }

                    if (valid.get() > maxReservation) {
                        // no more reservations
                        return; // just return, let the refresh mechanism finished
                    }

                    try {
                        hopReserve(host, peeraddr);

                        if (valid.incrementAndGet() > maxReservation) {
                            // done
                            service.shutdownNow();
                        }
                    } catch (Exception e) {
                        Utils.debug("Exception hopReserve " + e.getMessage());
                    }
                });
            });

            service.shutdown();
            if (!service.awaitTermination(timeout, TimeUnit.SECONDS)) {
                service.shutdownNow();
            }
        } catch (InterruptedException interruptedException) {
            service.shutdownNow();
        }
    }


    private static void removeReservation(Lite host, tech.lp2p.quic.Connection connection, Reservation reservation) {
        connection.removeAttribute(Requester.RESERVATION);
        if (reservation != null) {
            host.reservationLost().accept(reservation);
        }
    }

    static Reservation refreshReservation(Lite host, Reservation reservation) throws Exception {
        for (tech.lp2p.quic.Connection connection : host.serverConnector().clientConnections()) {
            Reservation stored = (Reservation) connection.getAttribute(Requester.RESERVATION);
            if (Objects.equals(stored, reservation)) {
                try {
                    Reservation refreshed = RelayService.hopReserve(
                            connection, stored.peeraddr(), host.peerId());

                    connection.setAttribute(Requester.RESERVATION, refreshed);
                    return refreshed;
                } catch (Throwable throwable) {
                    removeReservation(host, connection, stored);
                    throw throwable;
                }
            }
        }
        throw new ConnectException("Reservation is not valid anymore");
    }

    static Reservation hopReserve(Lite host, Peeraddr relay) throws Exception {

        tech.lp2p.core.Connection connection = ConnectionBuilder.connect(host, ALPN.libp2p,
                relay, true);
        tech.lp2p.quic.Connection quic = ((LiteConnection) connection).quic();
        quic.enableKeepAlive();
        quic.setCloseConsumer(conn -> {
            Reservation reservation = (Reservation) conn.getAttribute(Requester.RESERVATION);
            removeReservation(host, conn, reservation);
        });
        try {
            Reservation reservation = RelayService.hopReserve(quic, relay, host.peerId());
            quic.setAttribute(Requester.RESERVATION, reservation);
            host.reservationGain().accept(reservation);
            return reservation;
        } catch (Throwable throwable) {
            connection.close();
            throw throwable;
        }
    }

    static Peeraddrs reservationPeeraddrs(Lite host) {
        Peeraddrs result = new Peeraddrs();
        for (Reservation reservation : reservations(host)) {
            result.add(reservation.peeraddr());
        }
        return result;
    }

    static boolean hasReservation(ServerConnector serverConnector, PeerId peerId) {
        for (tech.lp2p.quic.Connection connection : serverConnector.clientConnections(peerId)) {
            Reservation reservation = (Reservation) connection.getAttribute(Requester.RESERVATION);
            if (reservation != null) {
                return true;
            }
        }
        return false;
    }

    static boolean hasHopReserve(ServerConnector serverConnector, PeerId peerId) {
        for (tech.lp2p.quic.Connection connection : serverConnector.serverConnections(peerId)) {
            if (connection.hasAttribute(Requester.RELAYED)) {
                return true;
            }
        }
        return false;
    }

    static void closeReservation(Lite host, Reservation reservation) {
        for (tech.lp2p.quic.Connection connection : host.serverConnector().clientConnections()) {
            Reservation stored = (Reservation) connection.getAttribute(Requester.RESERVATION);
            if (Objects.equals(stored, reservation)) {
                removeReservation(host, connection, stored);
                connection.close(Status.CLOSE);
            }
        }
    }

    static Set<Reservation> reservations(Lite host) {
        Set<Reservation> result = new HashSet<>();
        for (tech.lp2p.quic.Connection connection : host.serverConnector().clientConnections()) {
            Reservation reservation = (Reservation) connection.getAttribute(
                    Requester.RESERVATION);
            if (reservation != null) {
                result.add(reservation);
            } else {
                removeReservation(host, connection, null);
            }
        }
        return result;
    }
}
