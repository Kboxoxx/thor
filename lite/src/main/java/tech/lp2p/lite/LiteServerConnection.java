package tech.lp2p.lite;

import java.util.function.Function;

import tech.lp2p.quic.ApplicationProtocolConnection;
import tech.lp2p.quic.Connection;
import tech.lp2p.quic.Stream;
import tech.lp2p.quic.StreamHandler;

public class LiteServerConnection extends LiteConnection implements ApplicationProtocolConnection {
    private final Function<Stream, StreamHandler> streamHandlerFunction;

    public LiteServerConnection(Connection connection,
                                Function<Stream, StreamHandler> streamHandlerFunction) {
        super(connection);
        this.streamHandlerFunction = streamHandlerFunction;
    }

    @Override
    public Function<Stream, StreamHandler> getStreamHandler() {
        return streamHandlerFunction;
    }
}
