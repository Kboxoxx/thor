package tech.lp2p.lite;

import java.nio.ByteBuffer;

import tech.lp2p.quic.StreamState;
import tech.lp2p.utils.Utils;

public class LiteHandler {

    public static byte[] receiveResponse(byte[] data) {
        return transform(ByteBuffer.wrap(data));
    }

    private static byte[] transform(ByteBuffer bytes) {
        if (bytes.remaining() == 0) {
            return Utils.BYTES_EMPTY;
        }
        byte[] frame = StreamState.unsignedVarintReader(bytes);

        if (frame.length == 0) {
            return Utils.BYTES_EMPTY;
        } else {
            bytes.get(frame);

            if (!StreamState.isProtocol(frame)) {
                return frame;
            }

            return transform(bytes);
        }
    }
}
