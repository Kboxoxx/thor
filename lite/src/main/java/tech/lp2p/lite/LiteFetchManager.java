package tech.lp2p.lite;


import java.time.Duration;

import tech.lp2p.Lite;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Request;
import tech.lp2p.core.Response;
import tech.lp2p.dag.DagFetch;


public record LiteFetchManager(Lite host, Peeraddr peeraddr) implements DagFetch {

    @Override
    public Response getBlock(Cid cid) {
        Request request = Request.post(peeraddr, Lite.ROOT_PATH,
                Duration.ofSeconds(Lite.GRACE_PERIOD), true,
                Lite.CHUNK_BASIS + Lite.CHUNK_DATA, cid.hash());
        return host.send(request);
    }

    @Override
    public BlockStore blockStore() {
        return host.blockStore();
    }
}
