package tech.lp2p.lite;


import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import tech.lp2p.Lite;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Hash;
import tech.lp2p.core.Method;
import tech.lp2p.core.Request;
import tech.lp2p.core.RequestHandler;
import tech.lp2p.core.Response;
import tech.lp2p.core.Status;

public class LiteRequestHandler implements RequestHandler {
    private final Lite host;
    private final ConcurrentHashMap<String, RequestHandler> handlers = new ConcurrentHashMap<>();

    public LiteRequestHandler(Lite host) {
        this.host = host;
    }

    @Override
    public Response handleRequest(Request request) {
        try {
            if (!Objects.equals(request.path(), Lite.ROOT_PATH)) {
                return Response.create(Status.NOT_IMPLEMENTED);
            }

            if (Objects.requireNonNull(request.method()) == Method.POST) {

                byte[] payload = request.body();

                if (payload.length == 0) { // root request
                    Hash hash = host.peerId();
                    byte[] blockData = host.blockStore().getBlock(hash);

                    if (blockData == null) {
                        return Response.create(Status.NOT_FOUND);
                    }
                    if (blockData.length != 32) { // body must be 32 bit (hash)
                        return Response.create(Status.BAD_REQUEST);
                    }

                    return Response.create(Status.OK, blockData);
                }

                if (payload.length != 32) { // body must be 32 bit (hash)
                    return Response.create(Status.BAD_REQUEST);
                }

                Hash hash = new Cid(payload);
                byte[] blockData = host.blockStore().getBlock(hash);

                if (blockData == null) {
                    return Response.create(Status.NOT_FOUND);
                }

                return Response.create(Status.OK, blockData);
            }
            return Response.create(Status.METHOD_NOT_ALLOWED);
        } catch (Throwable throwable) {
            return Response.create(Status.SERVER_INTERNAL_ERROR);
        }
    }

    public void addRequestHandler(String path, RequestHandler requestHandler) {
        handlers.put(path, requestHandler);
    }

    private RequestHandler requestHandler(String path) {
        return handlers.get(path);
    }

    public RequestHandler createRequestHandler() {
        return request -> {
            try {
                String path = request.path();
                if (path.isEmpty() || Objects.equals(Lite.ROOT_PATH, path)) {
                    return handleRequest(request);
                }

                RequestHandler handler = requestHandler(path);
                if (handler != null) {
                    return handler.handleRequest(request);
                } else {
                    return Response.create(Status.NOT_IMPLEMENTED);
                }
            } catch (Throwable throwable) {
                return Response.create(Status.SERVER_INTERNAL_ERROR);
            }
        };
    }
}
