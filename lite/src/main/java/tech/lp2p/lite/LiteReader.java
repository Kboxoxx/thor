package tech.lp2p.lite;

import com.google.protobuf.ByteString;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Objects;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Progress;
import tech.lp2p.dag.DagFetch;
import tech.lp2p.dag.DagReader;
import tech.lp2p.dag.DagService;
import tech.lp2p.proto.Merkledag;

public record LiteReader(DagReader dagReader, DagFetch dagFetch) {


    public static void storeToStream(LiteFetchManager fetchManager, Cid cid,
                                     OutputStream outputStream, Progress progress) throws IOException {
        LiteReader liteReader = LiteReader.getReader(fetchManager, cid);
        long totalRead = 0L;
        int remember = 0;
        long size = liteReader.getSize();

        do {
            ByteString buffer = liteReader.loadNextData();
            if (buffer.isEmpty()) {
                return;
            }
            outputStream.write(buffer.toByteArray());

            // calculate progress
            totalRead += buffer.size();

            if (size > 0) {
                int percent = (int) ((totalRead * 100.0f) / size);
                if (remember < percent) {
                    remember = percent;
                    progress.setProgress(percent);
                }
            }

        } while (true);
    }

    public static LiteReader getReader(LiteFetchManager fetchManager, Cid cid) throws IOException {
        return LiteReader.createReader(fetchManager, cid);
    }


    public static LiteReader getReader(BlockStore blockStore, Cid cid) throws IOException {
        return LiteReader.createReader(new LiteLocalFetch(blockStore), cid);
    }

    public static LiteReader createReader(DagFetch dagFetch, Cid cid) throws IOException {

        Merkledag.PBNode top = DagService.getNode(dagFetch, cid);
        Objects.requireNonNull(top);
        DagReader dagReader = DagReader.create(top);

        return new LiteReader(dagReader, dagFetch);
    }


    public ByteString loadNextData() throws IOException {
        return dagReader.loadNextData(dagFetch);
    }

    public void seek(int position) throws IOException {
        dagReader.seek(dagFetch, position);
    }

    public long getSize() {
        return this.dagReader.size();
    }
}
