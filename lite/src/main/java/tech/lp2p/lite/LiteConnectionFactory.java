package tech.lp2p.lite;


import java.net.ConnectException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeoutException;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Request;
import tech.lp2p.quic.ConnectionBuilder;
import tech.lp2p.utils.Utils;


public class LiteConnectionFactory {

    private final Lite host;
    private final Map<Peeraddr, Connection> connections = new ConcurrentHashMap<>();

    public LiteConnectionFactory(Lite host) {
        this.host = host;
    }


    public Connection connection(Request request)
            throws ConnectException, InterruptedException, TimeoutException {

        Peeraddr peeraddr = request.peeraddr();
        Connection connection = connections.get(peeraddr);
        if (connection != null) {
            if (connection.isConnected()) {
                return connection;
            }
            connections.remove(peeraddr);
        }
        connection = ConnectionBuilder.connect(host, ALPN.h3, peeraddr, true);
        connections.put(peeraddr, connection);
        return connection;
    }

    public void shutdown() {
        try {
            for (Connection connection : connections.values()) {
                connection.close();
            }
        } catch (Throwable throwable) {
            Utils.error(throwable);
        } finally {
            connections.clear();
        }
    }

    public void insert(Peeraddr peeraddr, Connection connection) {
        try {
            connections.put(peeraddr, connection);
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }
}
