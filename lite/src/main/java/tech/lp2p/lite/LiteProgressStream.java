package tech.lp2p.lite;

import java.io.IOException;

import tech.lp2p.core.Progress;

public final class LiteProgressStream extends LiteReaderStream {

    private final Progress progress;
    private final long size;
    private int remember = 0;


    public LiteProgressStream(LiteReader liteReader, Progress progress) {
        super(liteReader);
        this.progress = progress;
        this.size = liteReader.getSize();
    }


    @Override
    void loadNextData() throws IOException {
        super.loadNextData();
        if (buffer != null) {
            if (size > 0) {
                int percent = (int) ((pos * 100.0f) / size);
                if (remember < percent) {
                    remember = percent;
                    progress.setProgress(percent);
                }
            }
        }
    }
}
