package tech.lp2p.lite;

import com.google.protobuf.ByteString;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;


public class LiteReaderStream extends InputStream {

    private final LiteReader liteReader;
    protected ByteString buffer = null;
    protected int pos;
    private int bufferPos = 0;

    public LiteReaderStream(LiteReader liteReader) {
        this.liteReader = liteReader;
    }


    @Override
    public int available() {
        long size = liteReader.getSize();
        return (int) size;
    }

    void loadNextData() throws IOException {
        bufferPos = 0;
        buffer = liteReader.loadNextData();
    }

    @Override
    public int read() throws IOException {

        if (buffer == null) { // initial the buffer is null
            loadNextData();
        }
        if (buffer.isEmpty()) {
            return -1;
        }
        if (bufferPos < buffer.size()) {
            return buffer.byteAt(bufferPos++) & 0xFF;
        } else {
            pos += buffer.size(); // all 'old' buffer is read
            loadNextData();
            if (buffer.isEmpty()) {
                return -1;
            }
            if (bufferPos < buffer.size()) {
                return buffer.byteAt(bufferPos++) & 0xFF;
            }
            return -1;
        }
    }


    public byte[] readAllBytes() throws IOException {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream(available())) {
            if (buffer == null) { // initial the buffer is null
                loadNextData();
            }
            while (!buffer.isEmpty()) {
                outputStream.write(buffer.toByteArray());
                loadNextData();
            }
            return outputStream.toByteArray();
        }
    }


    @Override
    public long skip(long n) throws IOException {
        pos += (int) (bufferPos + n);
        liteReader.seek(pos);
        loadNextData();
        return pos;
    }
}
