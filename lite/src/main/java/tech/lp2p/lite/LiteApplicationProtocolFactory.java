package tech.lp2p.lite;


import java.util.function.Consumer;
import java.util.function.Function;

import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Responder;
import tech.lp2p.core.Status;
import tech.lp2p.quic.AlpnLibp2pResponder;
import tech.lp2p.quic.ApplicationProtocolConnection;
import tech.lp2p.quic.ApplicationProtocolConnectionFactory;
import tech.lp2p.quic.Parameters;
import tech.lp2p.quic.ServerConnection;
import tech.lp2p.quic.Settings;
import tech.lp2p.quic.TransportError;

public class LiteApplicationProtocolFactory implements ApplicationProtocolConnectionFactory {
    private final Function<PeerId, Boolean> isGated;
    private final Consumer<Peeraddr> connectConsumer;
    private final Responder responder;

    public LiteApplicationProtocolFactory(Function<PeerId, Boolean> isGated,
                                          Consumer<Peeraddr> connectConsumer, Responder responder) {
        this.isGated = isGated;
        this.connectConsumer = connectConsumer;
        this.responder = responder;
    }

    @Override
    public ApplicationProtocolConnection createConnection(ServerConnection connection)
            throws TransportError {

        PeerId peerId = connection.remotePeerId();

        if (isGated.apply(peerId)) {
            throw new TransportError(Status.GATED);
        }

        try {
            connectConsumer.accept(Peeraddr.create(peerId, connection.remoteAddress()));
        } catch (Throwable throwable) {
            throw new TransportError(Status.INTERNAL_ERROR);
        }

        return new LiteServerConnection(connection,
                quicStream -> AlpnLibp2pResponder.create(responder));
    }

    @Override
    public Parameters parameters() {
        return Parameters.createLibP2p(Settings.SERVER_INITIAL_RTT);
    }
}
