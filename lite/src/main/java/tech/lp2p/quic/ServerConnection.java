package tech.lp2p.quic;

import static tech.lp2p.quic.Connection.Status.Connected;
import static tech.lp2p.quic.Level.App;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.tls.ApplicationLayerProtocolNegotiationExtension;
import tech.lp2p.tls.CertificateMessage;
import tech.lp2p.tls.CertificateRequestMessage;
import tech.lp2p.tls.CertificateVerifyMessage;
import tech.lp2p.tls.DecodeErrorException;
import tech.lp2p.tls.EncryptedExtensions;
import tech.lp2p.tls.ErrorAlert;
import tech.lp2p.tls.Extension;
import tech.lp2p.tls.FinishedMessage;
import tech.lp2p.tls.MissingExtensionAlert;
import tech.lp2p.tls.NoApplicationProtocolAlert;
import tech.lp2p.tls.ServerHello;
import tech.lp2p.tls.ServerMessageSender;
import tech.lp2p.tls.TlsServerEngine;
import tech.lp2p.tls.TlsStatusEventHandler;
import tech.lp2p.utils.Utils;


public final class ServerConnection extends Connection {

    private final TlsServerEngine tlsEngine;
    private final ApplicationProtocolRegistry applicationProtocolRegistry;
    private final ServerConnector serverConnector;
    private final CidServerManager cidServerManager;
    private final AtomicReference<ALPN> negotiatedApplicationProtocol =
            new AtomicReference<>(ALPN.libp2p);
    private PeerId remotePeerId;
    private ApplicationProtocolConnection applicationProtocolConnection;

    ServerConnection(ServerConnector serverConnector, int version,
                     InetSocketAddress remoteAddress, Integer scid, Number dcid) {
        super(version, false, Settings.INITIAL_MAX_DATA, Settings.SERVER_INITIAL_RTT,
                serverConnector.datagramSocket(), remoteAddress);
        this.applicationProtocolRegistry = serverConnector.applicationProtocolRegistry();
        this.serverConnector = serverConnector;
        this.tlsEngine = serverConnector.tlsServerEngineFactory().createServerEngine(
                serverConnector.trustManager(), new CryptoMessageSender(), new StatusEventHandler());

        initializeCryptoStreams(tlsEngine);
        computeInitialKeys(dcid);

        Consumer<TransportError> transportError = (error) ->
                immediateCloseWithError(Level.App, error);
        this.cidServerManager = new CidServerManager(
                scid, dcid, serverConnector,
                sendRequestQueue(Level.App), transportError);

        startRequester();

    }

    private static String[] getRequestedProtocols(Extension[] extensions) throws MissingExtensionAlert {
        Extension alpnExtension = null;
        for (Extension ext : extensions) {
            if (ext instanceof ApplicationLayerProtocolNegotiationExtension) {
                alpnExtension = ext;
                break;
            }

        }
        if (alpnExtension == null) {
            throw new MissingExtensionAlert("missing application layer protocol negotiation extension");
        }
        // "When using ALPN, endpoints MUST immediately close a connection (...) if an application protocol is not negotiated."
        return ((ApplicationLayerProtocolNegotiationExtension) alpnExtension).protocols();
    }

    private X509Certificate remoteCertificate() {
        return tlsEngine.remoteCertificate();
    }

    public Peeraddr remotePeeraddr() {
        return Peeraddr.create(remotePeerId(), remoteAddress());
    }

    @Override
    public ALPN alpn() {
        return negotiatedApplicationProtocol.get();
    }

    public PeerId remotePeerId() {
        if (!hasRemotePeerId()) {
            X509Certificate certificate = remoteCertificate();
            remotePeerId = PeerId.extractPeerId(certificate);
        }
        return remotePeerId;
    }

    public boolean hasRemotePeerId() {
        return remotePeerId != null;
    }

    @Override
    void abortConnection(Throwable error) {
        Utils.debug("Aborting server connection " +
                remoteAddress().toString() +
                " because of error " + error.getMessage());
        terminate();
    }

    @Override
    Integer activeScid() {
        return cidServerManager.initialScid();
    }

    @Override
    public Number activeDcid() {
        return cidServerManager.activeDcid();
    }


    @Override
    boolean process(PacketHeader packetHeader, long timeReceived) throws IOException {
        switch (packetHeader.level()) {
            case Handshake -> {
                return processHandshake(packetHeader, timeReceived);
            }
            case Initial -> {
                return processFrames(packetHeader, timeReceived);
            }
            case App -> {
                cidServerManager.registerCidInUse(packetHeader.dcid());
                return processFrames(packetHeader, timeReceived);
            }
        }
        return false;
    }

    @Override
    void process(FrameReceived.HandshakeDoneFrame ignoredHandshakeDoneFrame) {
        throw new IllegalStateException("Server Connection does not process a HandshakeDoneFrame");
    }

    private boolean processHandshake(PacketHeader packetHeader, long timeReceived) throws IOException {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-8.1
        // "In particular, receipt of a packet protected with Handshake keys confirms that the peer successfully processed
        //  an Initial packet. Once an endpoint has successfully processed a Handshake packet from the peer, it can consider
        //  the peer address to have been validated."


        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.2.2.1
        // "A server stops sending and processing Initial packets when it receives its first Handshake packet. "
        discard(Level.Initial);  // Only discards when not yet done.

        // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-4.9.1
        // (-> a server MUST discard Initial keys when it first successfully processes a Handshake packet)
        // 4.9.1. Discarding Initial Keys
        // Packets protected with Initial secrets (Section 5.2) are not authenticated,
        // meaning that an attacker could spoof packets with the intent to disrupt a connection.
        // To limit these attacks, Initial packet protection keys are discarded more aggressively
        // than other keys.
        //
        // The successful use of Handshake packets indicates that no more Initial packets need to
        // be exchanged, as these keys can only be produced after receiving all CRYPTO frames from
        // Initial packets. Thus, a client MUST discard Initial keys when it first sends a
        // Handshake packet and a server MUST discard Initial keys when it first successfully
        // processes a Handshake packet. Endpoints MUST NOT send Initial packets after this point.
        //
        // This results in abandoning loss recovery state for the Initial encryption level and
        // ignoring any outstanding Initial packets.
        discardInitialKeys();


        return processFrames(packetHeader, timeReceived);
    }

    @Override
    public void process(FrameReceived.NewConnectionIdFrame newConnectionIdFrame) {
        cidServerManager.process(newConnectionIdFrame);
    }

    @Override
    void process(FrameReceived.NewTokenFrame newTokenFrame) {
        throw new IllegalStateException("Server Connection does not process a NewTokenFrame");
    }

    @Override
    public void process(FrameReceived.RetireConnectionIdFrame retireConnectionIdFrame, Number dcid) {
        cidServerManager.process(retireConnectionIdFrame, dcid);
    }

    @Override
    Function<Stream, StreamHandler> getStreamHandler() {
        return Objects.requireNonNull(applicationProtocolConnection).getStreamHandler();
    }

    @Override
    public void terminate() {
        super.terminate();
        serverConnector.removeConnection(this);
    }

    private void validateAndProcess(TransportParameters remoteTransportParameters) throws DecodeErrorException {

        if (remoteTransportParameters.maxUdpPayloadSize() < 1200) {
            throw new DecodeErrorException("maxUdpPayloadSize transport parameter is invalid");
        }
        if (remoteTransportParameters.ackDelayExponent() > 20) {
            throw new DecodeErrorException("ackDelayExponent transport parameter is invalid");
        }
        if (remoteTransportParameters.maxAckDelay() > 16384) { // 16384 = 2^14 ()
            throw new DecodeErrorException("maxAckDelay value of 2^14 or greater are invalid.");
        }
        if (remoteTransportParameters.activeConnectionIdLimit() < 2) {
            throw new DecodeErrorException("activeConnectionIdLimit transport parameter is invalid");
        }
        if (!cidServerManager.validateInitialDcid(remoteTransportParameters.initialScid())) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-7.3
            // "An endpoint MUST treat absence of the initial_source_connection_id transport parameter from either
            //  endpoint (...) as a connection error of payloadType TRANSPORT_PARAMETER_ERROR."
            // "An endpoint MUST treat the following as a connection error of payloadType TRANSPORT_PARAMETER_ERROR or
            //  PROTOCOL_VIOLATION: a mismatch between values received from a peer in these transport parameters and the
            //  value sent in the corresponding Destination or Source Connection ID fields of Initial packets."
            throw new DecodeErrorException("Validation connection ids failed");
        }

        determineIdleTimeout(Settings.MAX_IDLE_TIMEOUT, remoteTransportParameters.maxIdleTimeout());

        cidServerManager.remoteCidLimit(remoteTransportParameters.activeConnectionIdLimit());

        init(remoteTransportParameters.initialMaxData(),
                remoteTransportParameters.initialMaxStreamDataBidiLocal(),
                remoteTransportParameters.initialMaxStreamDataBidiRemote(),
                remoteTransportParameters.initialMaxStreamDataUni());

        setInitialMaxStreamsBidi(remoteTransportParameters.initialMaxStreamsBidi());
        setInitialMaxStreamsUni(remoteTransportParameters.initialMaxStreamsUni());

        remoteMaxAckDelay = remoteTransportParameters.maxAckDelay();
    }

    public boolean isClosed() {
        return connectionState.get().isClosed();
    }


    Collection<CidInfo> sourceConnectionIds() {
        return cidServerManager.sourceCidInfos();
    }

    private class StatusEventHandler implements TlsStatusEventHandler {

        @Override
        public void handshakeSecretsKnown() {
            computeHandshakeSecrets(tlsEngine, tlsEngine.getSelectedCipher());
        }

        @Override
        public void handshakeFinished() {
            computeApplicationSecrets(tlsEngine, tlsEngine.getSelectedCipher());

            // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-4.1.2
            // "the TLS handshake is considered confirmed at the server when the handshake completes"
            discard(Level.Handshake);

            // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-4.9.2
            // "An endpoint MUST discard its handshake keys when the TLS handshake is confirmed"
            // 4.9.2. Discarding Handshake Keys
            // An endpoint MUST discard its handshake keys when the TLS handshake is confirmed
            // (Section 4.1.2).
            discardHandshakeKeys();


            // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-4.9.2
            // "The server MUST send a HANDSHAKE_DONE frame as soon as it completes the handshake."
            addRequest(App, Frame.HANDSHAKE_DONE);

            HandshakeState state = handshakeState.updateAndGet(handshakeState -> {
                if (handshakeState.transitionAllowed(HandshakeState.Confirmed)) {
                    return HandshakeState.Confirmed;
                }
                return handshakeState;
            });

            if (state == HandshakeState.Confirmed) {
                handshakeStateChangedEvent(HandshakeState.Confirmed);
            } else {
                throw new IllegalStateException("Handshake server state cannot be set to Confirmed");
            }

            try {
                applicationProtocolConnection = applicationProtocolRegistry.startApplicationProtocolConnection(
                        negotiatedApplicationProtocol.get().name(),
                        ServerConnection.this);

                cidServerManager.handshakeFinished();

                connectionState.set(Connected);
            } catch (TransportError transportError) {
                immediateCloseWithError(Level.App, transportError);
            }

        }


        @Override
        public void extensionsReceived(Extension[] extensions) throws ErrorAlert {
            // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-8.1
            // "Unless another mechanism is used for agreeing on an application protocol, endpoints MUST use ALPN for this purpose."
            String[] requestedProtocols = getRequestedProtocols(extensions);
            Optional<String> applicationProtocol = applicationProtocolRegistry.selectSupportedApplicationProtocol(
                    Arrays.asList(requestedProtocols));
            applicationProtocol
                    .map(protocol -> {
                        // Add negotiated protocol to TLS response (Encrypted Extensions message)
                        tlsEngine.addServerExtensions(
                                ApplicationLayerProtocolNegotiationExtension.create(protocol));
                        return protocol;
                    })
                    .map(selectedProtocol -> negotiatedApplicationProtocol.updateAndGet(s -> ALPN.valueOf(selectedProtocol)))
                    .orElseThrow(() -> new NoApplicationProtocolAlert(Arrays.asList(requestedProtocols)));


            // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-8.2
            // "endpoints that receive ClientHello or EncryptedExtensions messages without the quic_transport_parameters extension
            //  MUST close the connection with an error of payloadType 0x16d (equivalent to a fatal TLS missing_extension alert"
            TransportParametersExtension tpExtension = null;
            for (Extension ext : extensions) {
                if (ext instanceof TransportParametersExtension) {
                    tpExtension = (TransportParametersExtension) ext;
                    break;
                }

            }

            if (tpExtension != null) {
                TransportParameters transportParameters = tpExtension.getTransportParameters();
                validateAndProcess(transportParameters);
                remoteDelayScale.set(transportParameters.ackDelayScale());

            } else {
                throw new MissingExtensionAlert("missing quic transport parameters extension");
            }

            TransportParameters.VersionInformation versionInformation = null;
            if (Version.isV2(version)) {
                int[] otherVersions = {Version.V2, Version.V1};
                versionInformation = new TransportParameters.VersionInformation(
                        Version.V2, otherVersions);
            }

            Parameters parameters = applicationProtocolRegistry.parameters(
                    applicationProtocol.get());
            init(parameters);
            TransportParameters serverTransportParams = TransportParameters.createServer(
                    cidServerManager.originalDcid(),
                    cidServerManager.initialScid(),
                    Settings.ACTIVE_CONNECTION_ID_LIMIT,
                    parameters,
                    versionInformation);


            TlsServerEngine.setSelectedApplicationLayerProtocol(negotiatedApplicationProtocol.get().name());
            tlsEngine.addServerExtensions(TransportParametersExtension.create(
                    version, serverTransportParams, false));

        }
    }

    private class CryptoMessageSender implements ServerMessageSender {
        @Override
        public void send(ServerHello sh) {
            getCryptoStream(Level.Initial).write(sh);
        }

        @Override
        public void send(EncryptedExtensions ee) {
            getCryptoStream(Level.Handshake).write(ee);
        }

        @Override
        public void send(CertificateRequestMessage cm) {
            getCryptoStream(Level.Handshake).write(cm);
        }

        @Override
        public void send(CertificateMessage cm) {
            getCryptoStream(Level.Handshake).write(cm);
        }

        @Override
        public void send(CertificateVerifyMessage cv) {
            getCryptoStream(Level.Handshake).write(cv);
        }

        @Override
        public void send(FinishedMessage finished) {
            getCryptoStream(Level.Handshake).write(finished);
        }

    }
}
