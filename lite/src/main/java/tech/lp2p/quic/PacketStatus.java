package tech.lp2p.quic;


final class PacketStatus {

    private final long timeSent;
    private final int size;
    private final Packet packet;
    private volatile boolean lost;
    private volatile boolean acked;

    PacketStatus(Packet packet, long timeSent, int size) {
        this.timeSent = timeSent;
        this.size = size;
        this.packet = packet;
    }

    public int size() {
        return size;
    }

    boolean acked() {
        return acked;
    }

    boolean setAcked() {
        if (!acked && !lost) {
            acked = true;
            return true;
        } else {
            return false;
        }
    }

    boolean inFlight() {
        return !acked && !lost;
    }

    boolean setLost() {
        if (!acked && !lost) {
            lost = true;
            return true;
        } else {
            return false;
        }
    }

    public long timeSent() {
        return timeSent;
    }

    public Packet packet() {
        return packet;
    }

}

