package tech.lp2p.quic;

import java.net.ConnectException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

import javax.net.ssl.X509TrustManager;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Certificate;
import tech.lp2p.core.Network;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Responder;
import tech.lp2p.http3.Http3ClientConnection;
import tech.lp2p.http3.Http3Connection;
import tech.lp2p.http3.HttpStreamHandler;
import tech.lp2p.lite.LiteConnection;
import tech.lp2p.tls.CipherSuite;

public final class ConnectionBuilder {


    public static tech.lp2p.core.Connection connect(Lite host, ALPN alpn, Peeraddr address,
                                                    boolean validatePeer)
            throws ConnectException, InterruptedException, TimeoutException {

        InetAddress inetAddress;
        try {
            inetAddress = InetAddress.getByAddress(address.address());
        } catch (Throwable throwable) {
            throw new ConnectException("invalid address " + address.peerId());
        }
        Objects.requireNonNull(inetAddress);

        return connect(host, alpn, address.peerId(), inetAddress, address.port(),
                validatePeer);
    }

    public static tech.lp2p.core.Connection connect(Lite host, ALPN alpn, PeerId peerId,
                                                    InetAddress inetAddress, int port,
                                                    boolean validatePeer // todo validatePeer remove
    )
            throws ConnectException, InterruptedException, TimeoutException {


        int initialRtt = Settings.INITIAL_RTT;
        if (Network.isLocalAddress(inetAddress)) {
            initialRtt = 100;
        }

        Certificate certificate = host.certificate();
        X509TrustManager trustManager = getX509TrustManager(host, validatePeer);

        if (alpn == ALPN.h3) {
            Parameters parameters = Parameters.createH3(initialRtt);
            AtomicReference<Http3Connection> http3Connection = new AtomicReference<>(null);
            ClientConnection clientConnection = ClientConnection.create(parameters,
                    inetAddress.getHostName(), new InetSocketAddress(inetAddress, port),
                    Version.V1, List.of(CipherSuite.TLS_AES_128_GCM_SHA256),
                    trustManager, certificate.x509Certificate(), certificate.certificateKey(),
                    stream -> new HttpStreamHandler(http3Connection.get()),
                    host.serverConnector());

            http3Connection.set(new Http3ClientConnection(clientConnection));
            clientConnection.connect(Lite.TIMEOUT);
            http3Connection.get().startControlStream();


            if (validatePeer) {
                PeerId client = clientConnection.remotePeerId();
                if (!Objects.equals(client, peerId)) {
                    throw new ConnectException("Invalid PeerId received.");
                }
            }

            return http3Connection.get();
        } else {
            Parameters parameters = Parameters.createLibP2p(initialRtt);
            Responder responder = new Responder(host.protocols());
            Objects.requireNonNull(responder, "Responder not defined for libp2p ALPN");

            ClientConnection clientConnection = ClientConnection.create(parameters,
                    inetAddress.getHostName(), new InetSocketAddress(inetAddress, port),
                    Version.V1, List.of(CipherSuite.TLS_AES_128_GCM_SHA256),
                    trustManager, certificate.x509Certificate(), certificate.certificateKey(),
                    quicStream -> AlpnLibp2pResponder.create(responder),
                    host.serverConnector());
            clientConnection.connect(Lite.TIMEOUT);

            if (validatePeer) {
                PeerId client = clientConnection.remotePeerId();
                if (!Objects.equals(client, peerId)) {
                    throw new ConnectException("Invalid PeerId received.");
                }
            }

            return new LiteConnection(clientConnection);
        }
    }

    private static X509TrustManager getX509TrustManager(Lite host, boolean validatePeer) {
        X509TrustManager trustManager;
        if (validatePeer) {
            trustManager = host.trustManager();
        } else {
            trustManager = new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] x509Certificates, String s) {

                }

                @Override
                public void checkServerTrusted(X509Certificate[] x509Certificates, String s) {

                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            };
        }
        return trustManager;
    }
}
