package tech.lp2p.quic;


public interface Version {
    int V1 = 0x00000001;
    int V2 = 0x709a50c4;


    static byte[] toBytes(int versionId) {
        return VariableLengthInteger.numToBytes(versionId, Integer.BYTES);
    }

    static boolean isV2(int versionId) {
        return versionId == V2;
    }


}
