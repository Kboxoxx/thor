package tech.lp2p.quic;


import java.util.Objects;


final class ScidRegistry extends CidRegistry {

    ScidRegistry() {
        super(new CidInfo(0,
                VariableLengthInteger.generateNumber(Integer.BYTES),
                null, CidStatus.IN_USE));
    }

    CidInfo generateNew() {
        int sequenceNr = maxSequenceNr() + 1;
        CidInfo newCid = new CidInfo(sequenceNr,
                VariableLengthInteger.generateNumber(Integer.BYTES),
                null, CidStatus.NEW);
        cidInfos().add(newCid);
        return newCid;
    }

    /**
     * Registers a connection id for being used.
     *
     * @return true is the connection id is new (newly used), false otherwise.
     */
    boolean registerUsedConnectionId(Number usedCid) {
        if (Objects.equals(getInitial(), usedCid)) {
            return false;
        } else {
            // Register previous connection id as used
            cidInfos().stream()
                    .filter(cid -> Objects.equals(cid.cid(), getInitial()))
                    .forEach(cid -> cid.cidStatus(CidStatus.USED));

            // Check if new connection id is newly used
            boolean wasNew = cidInfos().stream()
                    .filter(cid -> Objects.equals(cid.cid(), usedCid))
                    .anyMatch(cid -> cid.cidStatus() == CidStatus.NEW);
            // Register current connection id as current
            cidInfos().stream()
                    .filter(cid -> Objects.equals(cid.cid(), usedCid))
                    .forEach(cid -> cid.cidStatus(CidStatus.IN_USE));

            return wasNew;
        }
    }
}


