package tech.lp2p.quic;


import tech.lp2p.core.ALPN;

public record Parameters(ALPN alpn, int initialRtt, int initialMaxData,
                         int initialMaxStreamDataUni, int initialMaxStreamsUni,
                         int initialMaxStreamsBidi,
                         int maxTotalPeerInitiatedUnidirectionalStreams,
                         int maxTotalPeerInitiatedBidirectionalStreams) {

    // https://www.rfc-editor.org/rfc/rfc9114.html#name-unidirectional-streams
    // "Therefore, the transport parameters sent by both clients and servers MUST allow the
    // peer to create at least three unidirectional streams."
    private static final int H3_MAX_STREAMS_UNI = 3;

    // https://www.rfc-editor.org/rfc/rfc9114.html#name-unidirectional-streams
    // "These transport parameters SHOULD also provide at least 1,024 bytes of flow-control
    // credit to each unidirectional stream."
    private static final int H3_MAX_STREAM_DATA_UNI = 1024;


    public static Parameters createH3(int initialRtt) {
        return new Parameters(ALPN.h3, initialRtt, Settings.INITIAL_MAX_DATA,
                H3_MAX_STREAM_DATA_UNI, H3_MAX_STREAMS_UNI, Settings.MAX_STREAMS_BIDI,
                H3_MAX_STREAMS_UNI, Integer.MAX_VALUE);
    }

    public static Parameters createLibP2p(int initialRtt) {
        return new Parameters(ALPN.libp2p, initialRtt,
                Settings.INITIAL_MAX_DATA, Settings.INITIAL_MAX_STREAM_DATA, Settings.MAX_STREAMS_UNI,
                Settings.MAX_STREAMS_BIDI, Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

}