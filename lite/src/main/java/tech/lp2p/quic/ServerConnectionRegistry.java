package tech.lp2p.quic;

@SuppressWarnings("unused")
interface ServerConnectionRegistry {

    void registerConnection(Connection connection, Integer cid);

    void deregisterConnection(Connection connection, Integer cid);

    void registerAdditionalConnectionId(Integer currentCid, Integer cid);

    void deregisterConnectionId(Integer cid);
}
