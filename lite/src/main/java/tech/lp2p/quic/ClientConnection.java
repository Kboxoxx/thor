package tech.lp2p.quic;


import static tech.lp2p.quic.Level.App;
import static tech.lp2p.quic.Level.Handshake;
import static tech.lp2p.quic.Level.Initial;
import static tech.lp2p.quic.TransportError.Code.PROTOCOL_VIOLATION;
import static tech.lp2p.quic.TransportError.Code.TRANSPORT_PARAMETER_ERROR;
import static tech.lp2p.quic.TransportError.Code.VERSION_NEGOTIATION_ERROR;

import java.io.IOException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.net.ssl.X509TrustManager;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.PeerId;
import tech.lp2p.tls.ApplicationLayerProtocolNegotiationExtension;
import tech.lp2p.tls.BadRecordMacAlert;
import tech.lp2p.tls.CertificateMessage;
import tech.lp2p.tls.CertificateVerifyMessage;
import tech.lp2p.tls.CertificateWithPrivateKey;
import tech.lp2p.tls.CipherSuite;
import tech.lp2p.tls.ClientHello;
import tech.lp2p.tls.ClientMessageSender;
import tech.lp2p.tls.EarlyDataExtension;
import tech.lp2p.tls.Extension;
import tech.lp2p.tls.FinishedMessage;
import tech.lp2p.tls.TlsClientEngine;
import tech.lp2p.tls.TlsStatusEventHandler;
import tech.lp2p.utils.Utils;

public final class ClientConnection extends Connection {

    private final TlsClientEngine tlsEngine;
    private final CidClientManager cidClientManager;
    private final CountDownLatch handshakeFinishedCondition = new CountDownLatch(1);
    private final TransportParameters transportParams;
    private final ALPN alpn;
    private final Function<Stream, StreamHandler> streamHandlerFunction;
    private final ServerConnector serverConnector;
    private PeerId remotePeerId;

    private ClientConnection(Parameters parameters, String serverName,
                             InetSocketAddress remoteAddress, int version,
                             List<CipherSuite> cipherSuites, X509TrustManager trustManager,
                             X509Certificate clientCertificate, PrivateKey clientCertificateKey,
                             Function<Stream, StreamHandler> streamHandlerFunction,
                             ServerConnector serverConnector) {

        super(version, true, parameters.initialMaxData(), parameters.initialRtt(),
                serverConnector.datagramSocket(), remoteAddress);
        this.alpn = parameters.alpn();
        this.streamHandlerFunction = streamHandlerFunction;
        this.serverConnector = serverConnector;

        Consumer<TransportError> transportError = (error) ->
                immediateCloseWithError(App, error);


        this.cidClientManager = new CidClientManager(sendRequestQueue(App), transportError);


        TransportParameters.VersionInformation versionInformation = null;
        if (Version.isV2(version)) {
            int[] otherVersions = {Version.V2, Version.V1};
            versionInformation = new TransportParameters.VersionInformation(
                    Version.V2, otherVersions);
        }

        this.transportParams = TransportParameters.createClient(
                cidClientManager.initialScid(),
                Settings.ACTIVE_CONNECTION_ID_LIMIT, parameters, versionInformation);

        Extension tpExtension = TransportParametersExtension.create(
                this.version, transportParams, true);
        Extension aplnExtension = ApplicationLayerProtocolNegotiationExtension.create(alpn.name());
        this.tlsEngine = new TlsClientEngine(serverName, trustManager, cipherSuites,
                List.of(tpExtension, aplnExtension),
                new CryptoMessageSender(), new StatusEventHandler());
        initializeCryptoStreams(tlsEngine);

        tlsEngine.setHostnameVerifier((hostname, serverCertificate) -> true);

        tlsEngine.setClientCertificateCallback(authorities ->
                new CertificateWithPrivateKey(clientCertificate, clientCertificateKey));
    }

    public static ClientConnection create(Parameters parameters, String serverName,
                                          InetSocketAddress remoteAddress, int version,
                                          List<CipherSuite> cipherSuites,
                                          X509TrustManager trustManager,
                                          X509Certificate clientCertificate,
                                          PrivateKey clientCertificateKey,
                                          Function<Stream, StreamHandler> streamHandlerFunction,
                                          ServerConnector serverConnector) {

        return new ClientConnection(parameters, serverName, remoteAddress, version,
                cipherSuites, trustManager, clientCertificate,
                clientCertificateKey, streamHandlerFunction, serverConnector);

    }

    public void connect(int timeout) throws InterruptedException, ConnectException, TimeoutException {

        try {
            startHandshake();
        } catch (Throwable throwable) {
            abortHandshake();
            throw new ConnectException("Error : " + throwable.getMessage());
        }

        try {
            boolean handshakeFinished = handshakeFinishedCondition.await(timeout, TimeUnit.SECONDS);
            if (!handshakeFinished) {
                abortHandshake();
                throw new TimeoutException("Connection timed out after " + timeout + " s");
            } else if (connectionState.get() != Status.Connected) {
                abortHandshake();
                throw new ConnectException("Handshake error");
            }
        } catch (InterruptedException interruptedException) {
            abortHandshake();
            throw new InterruptedException("Interrupt exception occur");  // Should not happen.
        }
    }

    private void startHandshake() throws BadRecordMacAlert {

        computeInitialKeys(cidClientManager.initialDcid());

        // start the services
        Integer scid = cidClientManager.initialScid();
        serverConnector.registerConnection(this, scid);

        startRequester();

        tlsEngine.startHandshake();

    }

    @Override
    public ALPN alpn() {
        return alpn;
    }

    public PeerId remotePeerId() {
        if (!hasRemotePeerId()) {
            X509Certificate certificate = remoteCertificate();
            remotePeerId = PeerId.extractPeerId(certificate);
        }
        return remotePeerId;
    }

    public boolean hasRemotePeerId() {
        return remotePeerId != null;
    }

    private void abortHandshake() {
        connectionState.set(Status.Failed);
        clearRequests();
        terminate();
    }


    @Override
    public boolean process(PacketHeader packetHeader, long timeReceived) throws IOException {
        switch (packetHeader.level()) {
            case Handshake -> {
                return processFrames(packetHeader, timeReceived);
            }
            case Initial -> {
                cidClientManager.registerInitialCid(packetHeader.scid());
                return processFrames(packetHeader, timeReceived);
            }
            case App -> {
                cidClientManager.registerCidInUse(packetHeader.dcid());
                return processFrames(packetHeader, timeReceived);
            }
        }
        return false;
    }

    @Override
    public void process(FrameReceived.HandshakeDoneFrame handshakeDoneFrame) {

        HandshakeState state = handshakeState.updateAndGet(handshakeState -> {
            if (handshakeState.transitionAllowed(HandshakeState.Confirmed)) {
                return HandshakeState.Confirmed;
            }
            return handshakeState;
        });

        if (state == HandshakeState.Confirmed) {
            handshakeStateChangedEvent(HandshakeState.Confirmed);
        } else {
            throw new IllegalStateException("Handshake state cannot be set to Confirmed");
        }

        discard(Level.Handshake);

        // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-4.9.2
        // "An endpoint MUST discard its handshake keys when the TLS handshake is confirmed"
        // 4.9.2. Discarding Handshake Keys
        // An endpoint MUST discard its handshake keys when the TLS handshake is confirmed
        // (Section 4.1.2).
        discardHandshakeKeys();

    }

    @Override
    void process(FrameReceived.NewConnectionIdFrame newConnectionIdFrame) {
        cidClientManager.process(newConnectionIdFrame);
    }

    @Override
    void process(FrameReceived.NewTokenFrame newTokenFrame) {
        // not yet supported
    }


    @Override
    void process(FrameReceived.RetireConnectionIdFrame retireConnectionIdFrame, Number dcid) {
        cidClientManager.process(retireConnectionIdFrame, dcid);
    }

    @Override
    public void immediateCloseWithError(Level level, TransportError transportError) {
        super.immediateCloseWithError(level, transportError);
    }


    @Override
    Function<Stream, StreamHandler> getStreamHandler() {
        return streamHandlerFunction;
    }

    /**
     * Closes the connection by discarding all connection state. Do not call directly, should be called after
     * closing state or draining state ends.
     */
    @Override
    public void terminate() {
        super.terminate();
        handshakeFinishedCondition.countDown();

        Integer scid = cidClientManager.initialScid();
        serverConnector.deregisterConnection(this, scid);

    }

    private void validateAndProcess(TransportParameters remoteTransportParameters) {

        if (remoteTransportParameters.maxUdpPayloadSize() < 1200) {
            immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR));
            return;
        }
        if (remoteTransportParameters.ackDelayExponent() > 20) {
            immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR));
            return;
        }
        if (remoteTransportParameters.maxAckDelay() > 16384) { // 16384 = 2^14 ()
            immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR));
            return;
        }
        if (remoteTransportParameters.activeConnectionIdLimit() < 2) {
            immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR));
            return;
        }


        // https://tools.ietf.org/html/draft-ietf-quic-transport-29#section-7.3
        // "An endpoint MUST treat absence of the initial_source_connection_id
        //   transport parameter from either endpoint or absence of the
        //   original_destination_connection_id transport parameter from the
        //   server as a connection error of payloadType TRANSPORT_PARAMETER_ERROR."
        if (remoteTransportParameters.initialScid() == null ||
                remoteTransportParameters.originalDcid() == null) {

            immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR));
            return;
        }

        // https://tools.ietf.org/html/draft-ietf-quic-transport-29#section-7.3
        // "An endpoint MUST treat the following as a connection error of payloadType
        // TRANSPORT_PARAMETER_ERROR or PROTOCOL_VIOLATION:
        // a mismatch between values received from a peer in these transport parameters and the
        // value sent in the
        // corresponding Destination or Source Connection ID fields of Initial packets."

        if (!Objects.equals(cidClientManager.initialDcid(),
                remoteTransportParameters.initialScid())) {
            immediateCloseWithError(Handshake, new TransportError(PROTOCOL_VIOLATION));
            return;
        }

        if (!Objects.equals(cidClientManager.originalDcid(),
                remoteTransportParameters.originalDcid())) {
            immediateCloseWithError(Handshake, new TransportError(PROTOCOL_VIOLATION));
            return;
        }


        TransportParameters.VersionInformation versionInformation = remoteTransportParameters.versionInformation();
        if (versionInformation != null) {
            if (versionInformation.chosenVersion() != version) {
                // https://www.ietf.org/archive/id/draft-ietf-quic-version-negotiation-08.html
                // "clients MUST validate that the server's Chosen Version is equal to the negotiated version; if they do not
                //  match, the client MUST close the connection with a version negotiation error. "

                immediateCloseWithError(Handshake,
                        new TransportError(VERSION_NEGOTIATION_ERROR));
                return;
            }
        }

        this.remoteDelayScale.set(remoteTransportParameters.ackDelayScale());


        init(remoteTransportParameters.initialMaxData(),
                remoteTransportParameters.initialMaxStreamDataBidiLocal(),
                remoteTransportParameters.initialMaxStreamDataBidiRemote(),
                remoteTransportParameters.initialMaxStreamDataUni()
        );


        setInitialMaxStreamsBidi(remoteTransportParameters.initialMaxStreamsBidi());
        setInitialMaxStreamsUni(remoteTransportParameters.initialMaxStreamsUni());

        remoteMaxAckDelay = remoteTransportParameters.maxAckDelay();
        cidClientManager.remoteCidLimit(remoteTransportParameters.activeConnectionIdLimit());

        determineIdleTimeout(transportParams.maxIdleTimeout(), remoteTransportParameters.maxIdleTimeout());

        cidClientManager.initialStatelessResetToken(remoteTransportParameters.statelessResetToken());


        if (remoteTransportParameters.retrySourceConnectionId() != null) {
            immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR));
        }
    }


    /**
     * Abort connection due to a fatal error in this client. No message is sent to peer; just inform client it's all over.
     *
     * @param error the exception that caused the trouble
     */
    @Override
    void abortConnection(Throwable error) {

        connectionState.set(Status.Closing);

        Utils.debug("Aborting client connection " +
                remoteAddress().toString() +
                " because of error " + error.getMessage());

        clearRequests();
        terminate();
    }


    @Override
    boolean checkForStatelessResetToken(ByteBuffer buffer) {
        byte[] tokenCandidate = new byte[16];
        buffer.position(buffer.limit() - 16);
        buffer.get(tokenCandidate);
        return cidClientManager.isStatelessResetToken(tokenCandidate);
    }

    @Override
    Number activeScid() {
        return cidClientManager.activeScid();
    }

    @Override
    Number activeDcid() {
        return cidClientManager.activeDcid();
    }

    private void validateALPN(String[] protocols) {
        for (String protocol : protocols) {
            if (Objects.equals(protocol, alpn.name())) {
                return; // done all good
            }
        }
        immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR));
    }

    private X509Certificate remoteCertificate() {
        return tlsEngine.remoteCertificate();
    }

    private class StatusEventHandler implements TlsStatusEventHandler {

        @Override
        public void handshakeSecretsKnown() {
            // Server Hello provides a new secret, so:
            computeHandshakeSecrets(tlsEngine, tlsEngine.getSelectedCipher());
            HandshakeState state = handshakeState.updateAndGet(handshakeState -> {
                if (handshakeState.transitionAllowed(HandshakeState.HasHandshakeKeys)) {
                    return HandshakeState.HasHandshakeKeys;
                }
                return handshakeState;
            });

            if (state == HandshakeState.HasHandshakeKeys) {
                handshakeStateChangedEvent(HandshakeState.HasHandshakeKeys);
            } else {
                throw new IllegalStateException("Handshake state cannot be set to HasHandshakeKeys");
            }
        }

        @Override
        public void handshakeFinished() {
            // note this is not 100% correct, it discards only when handshake is finished,
            // not when the first handshake message is written [but fine for now !!!]

            // https://tools.ietf.org/html/draft-ietf-quic-tls-29#section-4.11.1
            // "Thus, a client MUST discard Initial keys when it first sends a Handshake packet (...).
            // This results in abandoning loss recovery state for the Initial encryption level and
            // ignoring any outstanding Initial packets."
            discard(Level.Initial);

            // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-4.9.1
            // -> (Thus, a client MUST discard Initial keys when it first sends a Handshake)
            // 4.9.1. Discarding Initial Keys
            // Packets protected with Initial secrets (Section 5.2) are not authenticated,
            // meaning that an attacker could spoof packets with the intent to disrupt a connection.
            // To limit these attacks, Initial packet protection keys are discarded more aggressively
            // than other keys.
            //
            // The successful use of Handshake packets indicates that no more Initial packets need to
            // be exchanged, as these keys can only be produced after receiving all CRYPTO frames from
            // Initial packets. Thus, a client MUST discard Initial keys when it first sends a
            // Handshake packet and a server MUST discard Initial keys when it first successfully
            // processes a Handshake packet. Endpoints MUST NOT send Initial packets after this point.
            //
            // This results in abandoning loss recovery state for the Initial encryption level and
            // ignoring any outstanding Initial packets.
            discardInitialKeys();


            computeApplicationSecrets(tlsEngine, tlsEngine.getSelectedCipher());

            HandshakeState state = handshakeState.updateAndGet(handshakeState -> {
                if (handshakeState.transitionAllowed(HandshakeState.HasAppKeys)) {
                    return HandshakeState.HasAppKeys;
                }
                return handshakeState;
            });

            if (state == HandshakeState.HasAppKeys) {
                handshakeStateChangedEvent(HandshakeState.HasAppKeys);
            } else {
                throw new IllegalStateException("Handshake state cannot be set to HasAppKeys");
            }


            connectionState.set(Status.Connected);
            handshakeFinishedCondition.countDown();
        }


        @Override
        public void extensionsReceived(Extension[] extensions) {
            for (Extension ex : extensions) {
                if (ex instanceof EarlyDataExtension) {
                    Utils.debug("Server has accepted early data. Should not happen");
                } else if (ex instanceof TransportParametersExtension transportParametersExtension) {
                    validateAndProcess(transportParametersExtension.getTransportParameters());
                } else if (ex instanceof ApplicationLayerProtocolNegotiationExtension alpnExtension) {
                    validateALPN(alpnExtension.protocols());
                } else {
                    Utils.debug("not handled extension received " + ex.toString());
                }
            }
        }
    }

    private class CryptoMessageSender implements ClientMessageSender {
        @Override
        public void send(ClientHello clientHello) {
            CryptoStream cryptoStream = getCryptoStream(Initial);
            cryptoStream.write(clientHello);
            flush();
            connectionState.set(Status.Handshaking);
        }

        @Override
        public void send(FinishedMessage finished) {
            CryptoStream cryptoStream = getCryptoStream(Handshake);
            cryptoStream.write(finished);
            flush();
        }

        @Override
        public void send(CertificateMessage certificateMessage) {
            CryptoStream cryptoStream = getCryptoStream(Handshake);
            cryptoStream.write(certificateMessage);
            flush();
        }

        @Override
        public void send(CertificateVerifyMessage certificateVerifyMessage) {
            CryptoStream cryptoStream = getCryptoStream(Handshake);
            cryptoStream.write(certificateVerifyMessage);
            flush();
        }
    }

}
