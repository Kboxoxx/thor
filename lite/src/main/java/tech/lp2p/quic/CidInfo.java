package tech.lp2p.quic;


final class CidInfo implements Comparable<CidInfo> {

    private final int sequenceNumber;
    private volatile Number cid;
    private volatile byte[] statelessResetToken;
    private volatile CidStatus cidStatus;


    CidInfo(int sequenceNumber, Number cid, byte[] statelessResetToken, CidStatus status) {
        this.sequenceNumber = sequenceNumber;
        this.cid = cid;
        this.cidStatus = status;
        this.statelessResetToken = statelessResetToken;
    }

    void statelessResetToken(byte[] statelessResetToken) {
        this.statelessResetToken = statelessResetToken;
    }

    int sequenceNumber() {
        return sequenceNumber;
    }

    public Number cid() {
        return cid;
    }

    CidStatus cidStatus() {
        return cidStatus;
    }

    byte[] getStatelessResetToken() {
        return statelessResetToken;
    }

    void cidStatus(CidStatus newStatus) {
        cidStatus = newStatus;
    }

    @Override
    public int compareTo(CidInfo o) {
        return Integer.compare(sequenceNumber, o.sequenceNumber);
    }

    public void cid(Number cid) {
        this.cid = cid;
    }
}

