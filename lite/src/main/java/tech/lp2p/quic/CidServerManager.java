package tech.lp2p.quic;

import static tech.lp2p.quic.TransportError.Code.CONNECTION_ID_LIMIT_ERROR;
import static tech.lp2p.quic.TransportError.Code.FRAME_ENCODING_ERROR;
import static tech.lp2p.quic.TransportError.Code.PROTOCOL_VIOLATION;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;


final class CidServerManager {

    private final ServerConnectionRegistry connectionRegistry;
    private final SendRequestQueue sendRequestQueue;
    private final Consumer<TransportError> transportError;
    private final ScidRegistry scidRegistry;
    private final DcidRegistry dcidRegistry;
    private final Integer initialScid;
    private final Integer initialDcid;
    private final Number originalDcid;
    /**
     * The maximum numbers of connection IDs this endpoint can use; determined by the TP
     * supplied by the peer
     */
    private final AtomicInteger remoteCidLimit = new AtomicInteger(Settings.ACTIVE_CONNECTION_ID_LIMIT);
    /**
     * The maximum number of peer connection IDs this endpoint is willing to maintain;
     * advertised in TP sent by this endpoint
     */
    private final int activeCidLimit;


    /**
     * Creates a connection ID manager for server role.
     */
    CidServerManager(Integer initialScid, Number originalDcid,
                     ServerConnectionRegistry connectionRegistry,
                     SendRequestQueue sendRequestQueue,
                     Consumer<TransportError> transportError) {
        this.originalDcid = originalDcid;
        this.activeCidLimit = Settings.ACTIVE_CONNECTION_ID_LIMIT;
        this.connectionRegistry = connectionRegistry;
        this.sendRequestQueue = sendRequestQueue;
        this.transportError = transportError;
        this.scidRegistry = new ScidRegistry();
        this.initialScid = (Integer) scidRegistry.getInitial();
        this.dcidRegistry = new DcidRegistry(initialScid);
        this.initialDcid = initialScid;
    }

    void handshakeFinished() {
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-issuing-connection-ids
        // "An endpoint SHOULD ensure that its peer has a sufficient number of available and unused connection IDs."
        // "The initial connection ID issued by an endpoint is sent in the Source Connection ID field of the long
        //  packet header (Section 17.2) during the handshake."
        int max = remoteCidLimit.get();
        for (int i = 1; i < max; i++) {
            sendNewCid();
        }
    }

    void process(FrameReceived.NewConnectionIdFrame frame) {

        // https://www.rfc-editor.org/rfc/rfc9000.html#name-new_connection_id-frames
        // "An endpoint that is sending packets with a zero-length Destination Connection ID MUST treat receipt of a
        //  NEW_CONNECTION_ID frame as a connection error of payloadType PROTOCOL_VIOLATION."
        if (dcidRegistry == null) {
            transportError.accept(new TransportError(PROTOCOL_VIOLATION));
            return;
        }

        // https://www.rfc-editor.org/rfc/rfc9000.html#name-new_connection_id-frames
        // "Receiving a value in the Retire Prior To field that is greater than that in the Sequence Number field MUST
        //  be treated as a connection error of payloadType FRAME_ENCODING_ERROR."
        if (frame.retirePriorTo() > frame.sequenceNr()) {
            transportError.accept(new TransportError(FRAME_ENCODING_ERROR));
            return;
        }
        CidInfo cidInfo = dcidRegistry.cidInfo(frame.sequenceNr());
        if (cidInfo == null) {

            boolean added = dcidRegistry.registerNewConnectionId(frame.sequenceNr(),
                    frame.connectionId(), frame.statelessResetToken());
            if (!added) {
                // https://www.rfc-editor.org/rfc/rfc9000.html#name-new_connection_id-frames
                // "An endpoint that receives a NEW_CONNECTION_ID frame with a sequence number smaller than the Retire Prior To
                //  field of a previously received NEW_CONNECTION_ID frame MUST send a corresponding RETIRE_CONNECTION_ID
                //  frame that retires the newly received connection ID, "
                sendRetireCid(frame.sequenceNr());
            }
        } else if (!Objects.equals(cidInfo.cid(), frame.connectionId())) {
            // https://www.rfc-editor.org/rfc/rfc9000.html#name-new_connection_id-frames
            // "... or if a sequence number is used for different connection IDs, the endpoint MAY treat that receipt as a
            //  connection error of payloadType PROTOCOL_VIOLATION."
            transportError.accept(new TransportError(PROTOCOL_VIOLATION));
            return;
        }
        if (frame.retirePriorTo() > 0) {
            List<Integer> retired = dcidRegistry.retireAllBefore(frame.retirePriorTo());
            retired.forEach(this::sendRetireCid);
        }
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-issuing-connection-ids
        // "After processing a NEW_CONNECTION_ID frame and adding and retiring active connection IDs, if the number of
        //  active connection IDs exceeds the value advertised in its active_connection_id_limit transport parameter, an
        //  endpoint MUST close the connection with an error of payloadType CONNECTION_ID_LIMIT_ERROR."
        if (dcidRegistry.getActiveCids() > activeCidLimit) {
            transportError.accept(new TransportError(CONNECTION_ID_LIMIT_ERROR));
        }
    }

    void process(FrameReceived.RetireConnectionIdFrame frame, Number dcid) {
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-retire_connection_id-frames
        // "Receipt of a RETIRE_CONNECTION_ID frame containing a sequence number greater than any previously sent to the
        //  peer MUST be treated as a connection error of payloadType PROTOCOL_VIOLATION."
        if (frame.sequenceNumber() > scidRegistry.maxSequenceNr()) {
            transportError.accept(new TransportError(PROTOCOL_VIOLATION));
            return;
        }
        int sequenceNr = frame.sequenceNumber();
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-retire_connection_id-frames
        // "The sequence number specified in a RETIRE_CONNECTION_ID frame MUST NOT refer to the
        //  Destination Connection ID field of the packet in which the frame is contained.
        //  The peer MAY treat this as a connection error of payloadType PROTOCOL_VIOLATION."
        if (Objects.equals(Objects.requireNonNull(
                        scidRegistry.cidInfo(sequenceNr)).cid(),
                dcid)) {
            transportError.accept(new TransportError(PROTOCOL_VIOLATION));
            return;
        }

        Integer retiredCid = scidRegistry.retireCid(sequenceNr);
        // If not retired already
        if (retiredCid != null) {
            connectionRegistry.deregisterConnectionId(retiredCid);
            // https://www.rfc-editor.org/rfc/rfc9000.html#name-issuing-connection-ids
            // "An endpoint SHOULD supply a new connection ID when the peer retires a connection ID."
            if (scidRegistry.getActiveCids() < remoteCidLimit.get()) {
                sendNewCid();
            }
        }
    }

    /**
     * Register the active connection ID limit of the peer (as received by this endpoint as TP active_connection_id_limit)
     * and determine the maximum number of peer connection ID's this endpoint is willing to maintain.
     * "This is an integer value specifying the maximum number of connection IDs from the peer that an endpoint is
     * willing to store.", so it puts an upper bound to the number of connection IDs this endpoint can generate.
     */
    void remoteCidLimit(int remoteCidLimit) {
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-issuing-connection-ids
        // "An endpoint MUST NOT provide more connection IDs than the peer's limit."
        this.remoteCidLimit.set(remoteCidLimit);
    }

    /**
     * Generate, register and send a new connection ID (that identifies this endpoint).
     */
    void sendNewCid() {
        CidInfo cidInfo = scidRegistry.generateNew();
        Integer cid = (Integer) cidInfo.cid();
        Objects.requireNonNull(cid);
        Integer active = (Integer) scidRegistry.getActive();
        Objects.requireNonNull(active);
        connectionRegistry.registerAdditionalConnectionId(active, cid);
        sendRequestQueue.addRequest(Frame.createNewConnectionIdFrame(cidInfo.sequenceNumber(),
                0, cid));
    }

    /**
     * Send a retire connection ID frame, that informs the peer the given connection ID will not be used by this
     * endpoint anymore for addressing the peer.
     */
    private void sendRetireCid(Integer seqNr) {
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-retransmission-of-informati
        // "Likewise, retired connection IDs are sent in RETIRE_CONNECTION_ID frames and retransmitted if the packet
        //  containing them is lost."
        sendRequestQueue.addRequest(Frame.createRetireConnectionsIdFrame(seqNr));
    }


    public Collection<CidInfo> sourceCidInfos() {
        return scidRegistry.cidInfos();
    }


    /**
     * Returns the (peer's) connection ID that is currently used by this endpoint to address the peer.
     */

    Number activeDcid() {
        return dcidRegistry.getActive();
    }

    /**
     * Retrieves the initial connection used by this endpoint. This is the value that the endpoint included in the
     * Source Connection ID field of the first Initial packet it sends/send for the connection.
     *
     * @return the initial connection id
     */
    Integer initialScid() {
        return initialScid;
    }

    /**
     * Returns the original destination connection ID, i.e. the connection ID the client used as destination in its
     * very first initial packet.
     */
    Number originalDcid() {
        return originalDcid;
    }

    /**
     * Validates the given connection ID equals the initial peer connection ID.
     *
     * @return true if given connection ID equals the initial peer connection ID, false otherwise.
     */
    boolean validateInitialDcid(Number dcid) {
        return Objects.equals(dcid, initialDcid);
    }

    /**
     * Registers that the given connection is used by the peer (as destination connection ID) to send messages to this
     * endpoint.
     *
     * @param cid the connection ID used
     */
    void registerCidInUse(Number cid) {

        if (scidRegistry.registerUsedConnectionId(cid)) {
            // New connection id, not used before.
            // https://www.rfc-editor.org/rfc/rfc9000.html#name-issuing-connection-ids
            // "If an endpoint provided fewer connection IDs than the peer's active_connection_id_limit, it MAY supply
            //  a new connection ID when it receives a packet with a previously unused connection ID."
            if (scidRegistry.getActiveCids() < remoteCidLimit.get()) {
                sendNewCid();
            }
        }
    }

}
