package tech.lp2p.quic;

import java.nio.ByteBuffer;
import java.util.Objects;

import tech.lp2p.utils.Utils;

public abstract class StreamState {


    byte[] frame = null;
    int frameIndex = 0;
    boolean reset = false;


    public static boolean isProtocol(byte[] data) {
        if (data.length > 2) {
            return data[0] == '/' && data[data.length - 1] == '\n';
        }
        return false;
    }

    public static byte[] unsignedVarintReader(ByteBuffer data) {
        Objects.requireNonNull(data);
        if (data.remaining() == 0) {
            return Utils.BYTES_EMPTY;
        }
        return new byte[readUnsignedVariant(data)];
    }

    private static int readUnsignedVariant(ByteBuffer in) {
        int result = 0;
        int cur;
        int count = 0;
        do {
            cur = in.get() & 0xff;
            result |= (cur & 0x7f) << (count * 7);
            count++;
        } while (((cur & 0x80) == 0x80) && count < 5);
        if ((cur & 0x80) == 0x80) {
            throw new IllegalStateException("invalid unsigned variant sequence");
        }
        return result;
    }


    static void iteration(StreamState streamState, Stream stream, ByteBuffer bytes) throws Exception {

        if (!streamState.reset) {
            if (streamState.length() == 0) {
                streamState.frame = unsignedVarintReader(bytes);
                streamState.frameIndex = 0;
                if (streamState.length() < 0) {
                    streamState.frame = null;
                    streamState.frameIndex = 0;
                    throw new Exception("invalid length of < 0 : " + streamState.length());
                } else {

                    int read = Math.min(streamState.length(), bytes.remaining());
                    for (int i = 0; i < read; i++) {
                        streamState.frame[streamState.frameIndex] = bytes.get();
                        streamState.frameIndex++;
                    }

                    if (read == streamState.length()) {
                        byte[] frame = Objects.requireNonNull(streamState.frame);
                        streamState.frame = null;
                        streamState.accept(stream, frame);
                    }

                    // check for a next iteration
                    if (bytes.remaining() > 0) {
                        iteration(streamState, stream, bytes);
                    }
                }
            } else {
                byte[] frame = Objects.requireNonNull(streamState.frame);
                int remaining = streamState.frame.length - streamState.frameIndex;
                int read = Math.min(remaining, bytes.remaining());
                for (int i = 0; i < read; i++) {
                    streamState.frame[streamState.frameIndex] = bytes.get();
                    streamState.frameIndex++;
                }
                remaining = streamState.frame.length - streamState.frameIndex;
                if (remaining == 0) { // frame is full
                    streamState.frame = null;
                    streamState.frameIndex++;
                    streamState.accept(stream, frame);
                }
                // check for a next iteration
                if (bytes.remaining() > 0) {
                    iteration(streamState, stream, bytes);
                }
            }
        }
    }

    void reset() {
        frame = null;
        reset = true;
    }

    protected abstract void accept(Stream stream, byte[] frame) throws Exception;

    int length() {
        if (frame != null) {
            return frame.length;
        }
        return 0;
    }


    @Override
    public String toString() {
        return "State{" + "frameIndex=" + frameIndex + ", reset=" + reset + '}';
    }
}
