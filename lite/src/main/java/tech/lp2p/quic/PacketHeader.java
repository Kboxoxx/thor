package tech.lp2p.quic;


public record PacketHeader(Level level, int version, Number dcid, Integer scid,
                           byte[] framesBytes, long packetNumber, Keys updated) {

    boolean hasUpdatedKeys() {
        return updated != null;
    }
}
