package tech.lp2p.quic;

import java.net.UnknownServiceException;
import java.nio.ByteBuffer;

import tech.lp2p.core.Protocol;
import tech.lp2p.core.Responder;
import tech.lp2p.core.Status;

public record AlpnLibp2pResponder(Responder responder,
                                  Libp2pState streamState) implements StreamHandler {


    public static AlpnLibp2pResponder create(Responder responder) {
        return new AlpnLibp2pResponder(responder, new Libp2pState(responder));
    }

    @Override
    public void data(Stream stream, byte[] data) {
        try {
            StreamState.iteration(streamState, stream, ByteBuffer.wrap(data));
        } catch (UnknownServiceException unknownServiceException) {
            stream.resetStream(Status.PROTOCOL_NEGOTIATION_FAILED);
        } catch (Throwable throwable) {
            stream.resetStream(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public long delimiter(Stream stream) {
        return Responder.delimiter();
    }

    @Override
    public void terminated(Stream stream) {
        streamState.reset();
        responder.terminated(stream, streamState.protocol());
    }

    @Override
    public void fin(Stream stream) {
        streamState.reset();
        responder.fin(stream, streamState.protocol());
    }


    @Override
    public boolean readFully(Stream stream) {
        return false;
    }


    static class Libp2pState extends StreamState {
        private final Responder responder;
        Protocol protocol = null;

        Libp2pState(Responder responder) {
            this.responder = responder;
        }


        public Protocol protocol() {
            return protocol;
        }

        public void accept(Stream stream, byte[] frame) throws Exception {
            if (frame.length > 0) {
                if (isProtocol(frame)) {
                    String name = new String(frame, 0, frame.length - 1);
                    protocol = Protocol.name(name);
                    responder.protocol(stream, protocol);
                } else {
                    responder.data(stream, protocol, frame);
                }
            }
        }
    }
}
