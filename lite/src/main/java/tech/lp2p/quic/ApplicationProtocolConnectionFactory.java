package tech.lp2p.quic;

public interface ApplicationProtocolConnectionFactory {
    ApplicationProtocolConnection createConnection(ServerConnection connection) throws TransportError;

    Parameters parameters();
}

