package tech.lp2p.quic;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.security.SecureRandom;


// https://tools.ietf.org/html/draft-ietf-quic-transport-20#section-16
public interface VariableLengthInteger {

    static void write(long value, OutputStream outputStream) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        int numberOfBytes = VariableLengthInteger.encode(value, buffer);
        buffer.flip();
        for (int i = 0; i < numberOfBytes; i++) {
            outputStream.write(buffer.get());
        }
    }

    static Number generateNumber(int cidLength) {
        if (cidLength == Integer.BYTES) {
            return new SecureRandom().nextInt();
        }
        return new SecureRandom().nextLong();
    }

    static int bytesToInt(byte[] data) {
        int value = 0;
        for (byte datum : data) {
            value = (value << 8) | (datum & 0xff);
        }
        return value;
    }

    static byte[] numToBytes(long value, int length) {
        byte[] bytes = new byte[length];
        for (int i = 0; i < length; i++) {
            bytes[length - i - 1] = (byte) (value & 0xff);
            value >>= 8;
        }
        return bytes;
    }

    static byte[] numToBytes(Number number) {
        if (number instanceof Long) {
            return numToBytes(number.longValue(), Long.BYTES);
        } else if (number instanceof Integer) {
            return numToBytes(number.intValue(), Integer.BYTES);
        } else {
            throw new IllegalStateException("not supported number");
        }

    }

    static int length(Number number) {
        if (number instanceof Long) {
            return Long.BYTES;
        } else if (number instanceof Integer) {
            return Integer.BYTES;
        } else {
            throw new IllegalStateException("not supported number");
        }
    }

    /**
     * Parses a variable length integer and returns the value as in int. Throws an exception when the actual value is
     * larger than <code>Integer.MAX_VALUE</code>, so only use it in cases where a large value can be considered an
     * error, e.g. when the QUIC specification defines a smaller range for a specific integer.
     * Note that smaller values (needlessly) encoded in eight bytes, are parsed correctly.
     */
    static int parse(ByteBuffer buffer) throws IOException {
        long value = parseLong(buffer);
        if (value <= Integer.MAX_VALUE) {
            return (int) value;
        } else {
            // If value can be larger than int, parseLong should have called.
            throw new IOException("value to large for Java int");
        }
    }


    static int parse(InputStream inputStream) throws IOException {
        long value = parseLong(inputStream);
        if (value <= Integer.MAX_VALUE) {
            return (int) value;
        } else {
            throw new RuntimeException("value to large for Java int");
        }
    }

    static long parseLong(InputStream inputStream) throws IOException {
        long value;
        int firstLengthByte = inputStream.read();
        if (firstLengthByte == -1) {
            throw new EOFException();
        }
        switch ((firstLengthByte & 0xc0) >> 6) {
            case 0:
                value = firstLengthByte;
                break;
            case 1:
                int nextByte = inputStream.read();
                if (nextByte == -1) {
                    throw new EOFException();
                }
                value = ((long) (firstLengthByte & 0x3f) << 8) | (nextByte & 0xff);
                break;
            case 2:
                int byte2 = inputStream.read();
                int byte3 = inputStream.read();
                int byte4 = inputStream.read();
                if (byte2 == -1 || byte3 == -1 || byte4 == -1) {
                    throw new EOFException();
                }
                value = ((long) (firstLengthByte & 0x3f) << 24) | ((byte2 & 0xff) << 16) | ((byte3 & 0xff) << 8) | (byte4 & 0xff);
                break;
            case 3:
                byte[] rawBytes = new byte[8];
                rawBytes[0] = (byte) (firstLengthByte & 0x3f);
                int bytesRead = 0;
                while (bytesRead != 7) {
                    int read = inputStream.read(rawBytes, 1 + bytesRead, 7 - bytesRead);
                    if (read > 0) {
                        bytesRead += read;
                    } else {
                        throw new EOFException();
                    }
                }
                value = ByteBuffer.wrap(rawBytes).getLong();
                break;
            default:
                // Impossible, just to satisfy the compiler
                throw new IOException("Impossible");
        }
        return value;
    }

    static long parseLong(ByteBuffer buffer) throws IOException {
        if (buffer.remaining() < 1) {
            throw new IOException("Invalid size encoding");
        }

        long value;
        byte firstLengthByte = buffer.get();
        switch ((firstLengthByte & 0xc0) >> 6) {
            case 0 -> value = firstLengthByte;
            case 1 -> {
                if (buffer.remaining() < 1) {
                    throw new IOException("Invalid size encoding");
                }
                buffer.position(buffer.position() - 1);
                value = buffer.getShort() & 0x3fff;
            }
            case 2 -> {
                if (buffer.remaining() < 3) {
                    throw new IOException("Invalid size encoding");
                }
                buffer.position(buffer.position() - 1);
                value = buffer.getInt() & 0x3fffffff;
            }
            case 3 -> {
                if (buffer.remaining() < 7) {
                    throw new IOException("Invalid size encoding");
                }
                buffer.position(buffer.position() - 1);
                value = buffer.getLong() & 0x3fffffffffffffffL;
            }
            default ->
                // Impossible, just to satisfy the compiler
                    throw new IOException("Not handled size encoding");
        }
        return value;
    }


    static int bytesNeeded(long value) {
        if (value <= 63) {
            return 1;
        } else if (value <= 16383) {
            return 2;
        } else if (value <= 1073741823) {
            return 4;
        } else {
            return 8;
        }
    }

    static int encode(int value, ByteBuffer buffer) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-20#section-16
        // | 2Bit | Length | Usable Bits | Range                 |
        // +------+--------+-------------+-----------------------+
        // | 00   | 1      | 6           | 0-63                  |
        // | 01   | 2      | 14          | 0-16383               |
        // | 10   | 4      | 30          | 0-1073741823          |
        if (value <= 63) {
            buffer.put((byte) value);
            return 1;
        } else if (value <= 16383) {
            buffer.put((byte) ((value / 256) | 0x40));
            buffer.put((byte) (value % 256));
            return 2;
        } else if (value <= 1073741823) {
            int initialPosition = buffer.position();
            buffer.putInt(value);
            buffer.put(initialPosition, (byte) (buffer.get(initialPosition) | (byte) 0x80));
            return 4;
        } else {
            int initialPosition = buffer.position();
            buffer.putLong(value);
            buffer.put(initialPosition, (byte) (buffer.get(initialPosition) | (byte) 0xc0));
            return 8;
        }
    }

    static int encodeInteger(byte[] data, int start, int length) {
        int value = 0;
        for (int i = start; i < start + length; i++) {
            value = (value << 8) + (data[i] & 0xFF);
        }
        return value;
    }

    static int encode(long value, ByteBuffer buffer) {
        if (value <= Integer.MAX_VALUE) {
            return encode((int) value, buffer);
        }
        // https://tools.ietf.org/html/draft-ietf-quic-transport-20#section-16
        // | 2Bit | Length | Usable Bits | Range                 |
        // +------+--------+-------------+-----------------------+
        // | 11   | 8      | 62          | 0-4611686018427387903 |
        else if (value <= 4611686018427387903L) {
            int initialPosition = buffer.position();
            buffer.putLong(value);
            buffer.put(initialPosition, (byte) (buffer.get(initialPosition) | (byte) 0xc0));
            return 8;
        } else {
            throw new IllegalStateException("value cannot be encoded in variable-length integer");
        }
    }

}
