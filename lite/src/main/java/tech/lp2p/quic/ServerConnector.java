package tech.lp2p.quic;


import static tech.lp2p.core.Status.CLOSE;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import javax.net.ssl.X509TrustManager;

import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.tls.TlsServerEngineFactory;
import tech.lp2p.utils.Utils;


/**
 * Listens for QUIC connections on a given port. Requires server certificate and corresponding private key.
 */
public final class ServerConnector implements ServerConnectionRegistry {


    private final TlsServerEngineFactory tlsServerEngineFactory;
    private final Receiver receiver;

    private final DatagramSocket serverSocket;
    private final X509TrustManager trustManager;
    private final Map<Integer, Connection> connections = new ConcurrentHashMap<>();
    private final ApplicationProtocolRegistry applicationProtocolRegistry =
            new ApplicationProtocolRegistry();

    private final Set<SocketAddress> expectation = ConcurrentHashMap.newKeySet();

    private final Consumer<Peeraddr> closedConsumer;

    public ServerConnector(DatagramSocket socket, X509TrustManager trustManager,
                           X509Certificate x509Certificate, PrivateKey privateKey,
                           Consumer<Peeraddr> closedConsumer) {
        this.serverSocket = socket;
        this.closedConsumer = closedConsumer;
        this.trustManager = trustManager;
        this.tlsServerEngineFactory = TlsServerEngineFactory.createTlsServerEngineFactory(
                x509Certificate, privateKey);
        this.receiver = new Receiver(socket, new DatagramPacketConsumer(), new ThrowableConsumer());
    }

    public int numConnections() {
        return connections().size();
    }


    public void punching(InetAddress address, int port, long expireTimeStamp) {

        // Upon expiry of the timer, B starts to send UDP packets filled with random bytes to A's
        // address. Packets should be sent repeatedly in random intervals between 10 and 200 ms.

        InetSocketAddress inetSocketAddress = new InetSocketAddress(address, port);
        expectation.add(inetSocketAddress);
        try {
            punch(inetSocketAddress, expireTimeStamp);
        } finally {
            expectation.remove(inetSocketAddress);
        }
    }


    public Collection<Connection> serverConnections(PeerId peerId) {
        Set<Connection> connectionSet = new HashSet<>();
        for (ServerConnection connection : serverConnections()) {
            if (connection.hasRemotePeerId()) {
                if (Objects.equals(connection.remotePeerId(), peerId)) {
                    connectionSet.add(connection);
                }
            }
        }
        return connectionSet;
    }


    public Collection<ServerConnection> serverConnections() {
        Set<ServerConnection> connectionSet = new HashSet<>();
        for (Connection conn : connections()) {
            if (conn instanceof ServerConnection serverConnection) {
                connectionSet.add(serverConnection);
            }
        }
        return connectionSet;
    }

    public Collection<Connection> connections() {
        Set<Connection> connectionSet = new HashSet<>();
        for (Connection connection : connections.values()) {
            if (connection.isConnected()) {
                connectionSet.add(connection);
            }
        }
        return connectionSet;
    }

    public Collection<Connection> clientConnections(PeerId peerId) {
        Set<Connection> connectionSet = new HashSet<>();
        for (ClientConnection clientConnection : clientConnections()) {
            if (clientConnection.hasRemotePeerId()) {
                if (Objects.equals(clientConnection.remotePeerId(), peerId)) {
                    connectionSet.add(clientConnection);
                }
            }
        }
        return connectionSet;
    }


    public Collection<ClientConnection> clientConnections() {
        Set<ClientConnection> connectionSet = new HashSet<>();
        for (Connection conn : connections()) {
            if (conn instanceof ClientConnection clientConnection) {
                if (clientConnection.isConnected()) {
                    connectionSet.add(clientConnection);
                }
            }
        }
        return connectionSet;
    }

    public void punch(InetSocketAddress inetSocketAddress, long expireTimeStamp) {

        // check expireTimeStamp
        if (System.currentTimeMillis() > expireTimeStamp) {
            return;
        }
        if (!expectation.contains(inetSocketAddress)) {
            return;
        }
        // check if there is a
        try {
            int length = 64;
            byte[] datagramData = new byte[length];
            Random rd = new Random();
            rd.nextBytes(datagramData);

            DatagramPacket datagram = new DatagramPacket(datagramData, length,
                    inetSocketAddress.getAddress(), inetSocketAddress.getPort());

            serverSocket.send(datagram);

            Thread.sleep(new Random().nextInt(190) + 10); // sleep for random value between 10 and 200

            if (!Thread.currentThread().isInterrupted()) {
                punch(inetSocketAddress, expireTimeStamp);
            }
            // interrupt exception should occur, but it simply ends the loop
        } catch (Throwable ignore) {
        }
    }

    public void shutdown() {
        try {
            connections().forEach(connection -> connection.close(CLOSE));
            ScheduledExecutorService scheduledExecutorService =
                    Executors.newSingleThreadScheduledExecutor();
            scheduledExecutorService.schedule(this::terminate, Settings.SHUTDOWN_TIME,
                    TimeUnit.MILLISECONDS);
            scheduledExecutorService.shutdown();
            boolean result = scheduledExecutorService.awaitTermination(
                    Integer.MAX_VALUE, TimeUnit.SECONDS);
            if (!result) {
                scheduledExecutorService.shutdownNow();
            }
        } catch (Throwable throwable) {
            Utils.error(throwable); // should not occur
        }
    }

    private void terminate() {
        try {
            connections.values().forEach(Connection::terminate);
            receiver.shutdown();
            serverSocket.close();
        } catch (Throwable throwable) {
            Utils.error(throwable); // should not occur
        } finally {
            connections.clear();
        }
    }

    public void registerApplicationProtocol(String protocol, ApplicationProtocolConnectionFactory protocolConnectionFactory) {
        applicationProtocolRegistry.registerApplicationProtocol(protocol, protocolConnectionFactory);
    }

    public void start() {
        receiver.start();
    }


    private void process(long timeReceived, DatagramPacket datagramPacket) {
        expectation.remove(datagramPacket.getSocketAddress());
        ByteBuffer data = ByteBuffer.wrap(datagramPacket.getData(), 0, datagramPacket.getLength());
        int posFlags = data.position();
        byte flags = data.get();

        if ((flags & 0x80) == 0x80) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.2
            // "Header Form:  The most significant bit (0x80) of byte 0 (the first byte) is set to 1 for long headers."
            processLongHeaderPacket(flags, posFlags, data, timeReceived,
                    (InetSocketAddress) datagramPacket.getSocketAddress());
        } else if ((flags & 0xc0) == 0x40) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.3
            // "Header Form:  The most significant bit (0x80) of byte 0 is set to 0 for the short header.
            processShortHeaderPacket(flags, posFlags, data, timeReceived);
        } else {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.2
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.3
            // "The next bit (0x40) of byte 0 is set to 1. Packets containing a zero value for
            // this bit are not valid packets in this version and MUST be discarded."
            System.err.printf(" Invalid Quic packet (flags: %02x) is discarded%n", flags);
        }
    }

    private void processLongHeaderPacket(byte flags, int posFlags, ByteBuffer buffer,
                                         long timeReceived, InetSocketAddress remoteAddress) {

        // when version does not fit (only V1 supported)
        Level level = PacketParser.parseLevel(flags, Version.V1, buffer);
        if (level == null) {
            return;
        }

        Number dcid = PacketParser.dcid(level, buffer);

        if (dcid instanceof Integer cid) {
            Connection connection = getConnection(cid);
            if (connection != null) {
                // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-8
                // "Therefore, after receiving packets from an address that is not yet validated, an
                // endpoint MUST limit the amount of payload it sends to the unvalidated address to three
                // times the amount of payload received from that address."
                // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-8.1
                // "For the purposes of avoiding amplification prior to address validation, servers MUST
                // count all of the payload bytes received in datagrams that are uniquely attributed to a
                // single connection. This includes datagrams that contain packets that are successfully
                // processed and datagrams that contain packets that are all discarded."
                // [NOT YET SUPPORTED]
                connection.parsePackets(level, dcid, flags, posFlags, buffer, timeReceived);
            }
            else {
                System.err.println("no lhp conn 1");
            }
        } else {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-7.2
            // "This Destination Connection ID MUST be at least 8 bytes in length."
            // -> reaching this point means, the dcid is 8 bytes in length
            if (level == Level.Initial) {
                parseInitialPacket(remoteAddress, dcid, flags, posFlags, buffer, timeReceived);
            } else {
                // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-6
                // "A server sends a Version Negotiation packet in response to each
                // packet that might initiate a new connection;"
                // [BUT NOT SUPPORTED HERE YET]
                System.err.println("initial new connection not supported  " + level.name());
            }
        }
    }

    private void parseInitialPacket(InetSocketAddress remoteAddress, Number dcid, byte flags,
                                    int posFlags, ByteBuffer buffer, long timeReceived) {

        try {
            PacketHeader initialPacket = PacketParser.parseInitialPacket(Version.V1, dcid,
                    flags, posFlags, buffer);
            if (initialPacket != null) {
                // Packet is valid. This is the moment to create a
                // server connection and continue processing.

                ServerConnection serverConnection = new ServerConnection(this,
                        initialPacket.version(), remoteAddress, initialPacket.scid(), dcid);
                serverConnection.updateKeysAndPackageNumber(initialPacket);

                // Pass the initial packet for processing, so it is processed on the server thread
                // (enabling thread confinement concurrency strategy)
                serverConnection.processPacket(initialPacket, timeReceived);

                // Register new connection with the new connection id (the one generated by the server)
                registerConnection(serverConnection, serverConnection.activeScid());

            }
        } catch (Throwable error) {
            // Drop packet without any action (i.e. do not send anything; do not change state;
            // avoid unnecessary processing)
            System.err.println("Dropped invalid initial packet " +
                    "(no connection created) " + remoteAddress);
        }

    }

    private void processShortHeaderPacket(byte flags, int posFlags, ByteBuffer data, long timeReceived) {

        Number dcid = PacketParser.dcid(Level.App, data);
        if (dcid == null) {
            return;
        }

        if (dcid instanceof Integer cid) {
            Connection connection = getConnection(cid);
            if (connection != null) {
                // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-8
                // "Therefore, after receiving packets from an address that is not yet validated, an
                // endpoint MUST limit the amount of payload it sends to the unvalidated address to three
                // times the amount of payload received from that address."
                // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-8.1
                // "For the purposes of avoiding amplification prior to address validation, servers MUST
                // count all of the payload bytes received in datagrams that are uniquely attributed to a
                // single connection. This includes datagrams that contain packets that are successfully
                // processed and datagrams that contain packets that are all discarded."
                // [NOT YET SUPPORTED]
                connection.parsePackets(Level.App, dcid, flags, posFlags, data, timeReceived);
            } else {
                System.err.println("no shp conn 1");
            }
        }else {
            System.err.println("no shp conn 2");
        }
    }


    void removeConnection(ServerConnection connection) {
        for (CidInfo info : connection.sourceConnectionIds()) {
            Objects.requireNonNull(info);
            connections.remove((Integer) info.cid());
        }

        if (!connection.isClosed()) {
            Utils.debug("Removed connection " + connection + " that is not closed...");
        }

        try {
            if (connection.hasRemotePeerId()) {
                closedConsumer.accept(connection.remotePeeraddr());
            }
        } catch (Throwable throwable) {
            Utils.error(throwable); // should not occur
        }
    }

    @Override
    public void registerConnection(Connection connection, Integer scid) {
        connections.put(scid, connection);
    }

    @Override
    public void deregisterConnection(Connection connection, Integer cid) {
        boolean removed = removeConnection(connection, cid);

        if (!removed && containsKey(cid)) {
            Utils.debug("Connection " + connection.remoteAddress() + " not removed, because "
                    + getConnection(cid) + " is still registered");
        }
    }

    private boolean containsKey(Integer cid) {
        return connections.containsKey(cid);
    }

    private boolean removeConnection(Connection connection, Integer cid) {
        return connections.remove(cid, connection);
    }

    private Connection getConnection(Integer cid) {
        return connections.get(cid);
    }

    @Override
    public void registerAdditionalConnectionId(Integer currentCid, Integer cid) {
        Connection connection = getConnection(currentCid);
        if (connection != null) {
            registerConnection(connection, cid);
        } else {
            Utils.debug("Cannot add additional cid to non-existing connection");
        }
    }

    @Override
    public void deregisterConnectionId(Integer cid) {
        connections.remove(cid);
    }

    DatagramSocket datagramSocket() {
        return serverSocket;
    }

    ApplicationProtocolRegistry applicationProtocolRegistry() {
        return applicationProtocolRegistry;
    }

    public X509TrustManager trustManager() {
        return trustManager;
    }

    public TlsServerEngineFactory tlsServerEngineFactory() {
        return tlsServerEngineFactory;
    }


    private static final class ThrowableConsumer implements Consumer<Throwable> {

        @Override
        public void accept(Throwable throwable) {
            Utils.error(throwable);
        }
    }


    private final class DatagramPacketConsumer implements Consumer<DatagramPacket> {

        @Override
        public void accept(DatagramPacket datagramPacket) {
            try {
                process(System.currentTimeMillis(), datagramPacket);
            } catch (Throwable throwable) {
                Utils.error(throwable);
            }
        }
    }
}
