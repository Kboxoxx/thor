package tech.lp2p.quic;

import static tech.lp2p.quic.Level.App;
import static tech.lp2p.quic.Level.Initial;
import static tech.lp2p.quic.TransportError.Code.CRYPTO_ERROR;
import static tech.lp2p.quic.TransportError.Code.INTERNAL_ERROR;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.PeerId;
import tech.lp2p.tls.DecryptErrorAlert;
import tech.lp2p.tls.ErrorAlert;
import tech.lp2p.utils.Utils;


public abstract class Connection extends ConnectionStreams {

    protected final AtomicReference<Status> connectionState = new AtomicReference<>(Connection.Status.Created);
    protected final AtomicReference<HandshakeState> handshakeState = new AtomicReference<>(HandshakeState.Initial);
    protected final AtomicReference<Integer> remoteDelayScale = new AtomicReference<>(Settings.ACK_DELAY_SCALE);
    protected final DatagramSocket datagramSocket;
    private final long[] largestPacketNumber = new long[Level.LENGTH];
    private final RateLimiter closeFramesSendRateLimiter;
    private final long flowControlIncrement;  // no concurrency
    private final AtomicLong idleTimeout = new AtomicLong(Settings.MAX_IDLE_TIMEOUT);
    private final AtomicLong lastIdleAction = new AtomicLong(Settings.NOT_DEFINED);
    private final AtomicBoolean enableKeepAlive = new AtomicBoolean(false);
    private final AtomicLong lastPingAction = new AtomicLong(Settings.NOT_DEFINED);
    private final AtomicBoolean enabledIdle = new AtomicBoolean(false);
    private final InetSocketAddress remoteAddress;
    private final Thread requesterThread;
    private final AtomicInteger idleCounter = new AtomicInteger(0);
    private final ReentrantLock lock = new ReentrantLock();
    private final Condition waitCondition = lock.newCondition();

    private final ConcurrentHashMap<String, Object> attributes = new ConcurrentHashMap<>();
    private long flowControlMax;  // no concurrency
    private long flowControlLastAdvertised;  // no concurrency
    // Using thread-confinement strategy for concurrency control: only the sender thread
    // created in this class accesses these members
    private volatile boolean shutdownRequester = false;

    private Consumer<Connection> closeConsumer = null;

    protected Connection(int version, boolean isClient, int initialMaxData, int initialRtt,
                         DatagramSocket datagramSocket, InetSocketAddress remoteAddress) {
        super(version, isClient, initialRtt);
        this.closeFramesSendRateLimiter = new RateLimiter();
        this.flowControlMax = initialMaxData;
        this.flowControlLastAdvertised = flowControlMax;
        this.flowControlIncrement = flowControlMax / 10;
        this.datagramSocket = datagramSocket;
        this.remoteAddress = remoteAddress;
        this.requesterThread = new Thread(this::run, "sender-loop");
        this.requesterThread.setDaemon(true);
    }

    private static String determineClosingErrorMessage(FrameReceived.ConnectionCloseFrame closing) {
        if (closing.hasTransportError()) {
            if (closing.hasTlsError()) {
                return "TLS error " + closing.getTlsError() + (closing.hasReasonPhrase() ? ": " + closing.getReasonPhrase() : "");
            } else {
                return "transport error " + closing.getErrorCode() + (closing.hasReasonPhrase() ? ": " + closing.getReasonPhrase() : "");
            }
        } else if (closing.hasApplicationProtocolError()) {
            return "application protocol error " + closing.getErrorCode() + (closing.hasReasonPhrase() ? ": " + closing.getReasonPhrase() : "");
        } else {
            return "";
        }
    }

    private static TransportError quicError(Throwable throwable) {
        if (throwable instanceof ErrorAlert) {
            // tlsError evaluate [and maybe in the future to support also ErrorAlert types]
            return new TransportError(CRYPTO_ERROR);
        } else if (throwable.getCause() instanceof TransportError) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-20.1
            return ((TransportError) throwable.getCause());
        } else {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-20.1
            // "INTERNAL_ERROR (0x1):  The endpoint encountered an internal error and cannot continue with the connection."
            return new TransportError(INTERNAL_ERROR);
        }
    }

    public abstract ALPN alpn();

    private void keepAlive() {
        if (enableKeepAlive.get()) {
            long now = System.currentTimeMillis();
            if (now > lastPingAction.get() + Settings.PING_INTERVAL) {
                addRequest(App, Frame.PING);
                lastPingAction.set(System.currentTimeMillis());
            }
        }
    }

    public final void enableKeepAlive() {
        if (enableKeepAlive.compareAndSet(false, true)) {
            lastPingAction.set(System.currentTimeMillis());
            idleCounter.set(0);
        }
    }

    public final void disableKeepAlive() {
        enableKeepAlive.set(false);
    }

    public boolean isConnected() {
        return this.connectionState.get().isConnected();
    }

    final void updateConnectionFlowControl(int size) {
        flowControlMax += size;
        if (flowControlMax - flowControlLastAdvertised > flowControlIncrement) {
            addRequest(App, Frame.createMaxDataFrame(flowControlMax));
            flowControlLastAdvertised = flowControlMax;
        }
    }

    public Stream createStream(StreamHandler streamHandler, boolean bidirectional)
            throws InterruptedException, TimeoutException {
        return createStream(this, bidirectional, stream -> streamHandler);
    }

    final void nextPacket(ByteBuffer buffer, long timeReceived) {
        if (buffer.remaining() < 2) {
            return;
        }
        // Iteration
        int posFlags = buffer.position();
        byte flags = buffer.get();

        Level level = PacketParser.parseLevel(flags, version, buffer);
        if (level == null) {
            return;
        }
        Number dcid = PacketParser.dcid(level, buffer);
        if (dcid == null) {
            return;
        }
        parsePackets(level, dcid, flags, posFlags, buffer, timeReceived);
    }

    final void parsePackets(Level level, Number dcid, byte flags, int posFlags,
                            ByteBuffer buffer, long timeReceived) {


        try {
            PacketHeader packetHeader = parsePacket(level, dcid, flags, posFlags, buffer);
            if (packetHeader == null) {
                return; // when not valid
            }
            processPacket(packetHeader, timeReceived);

            nextPacket(buffer, timeReceived);

        } catch (DecryptErrorAlert decryptErrorAlert) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-24#section-12.2
            // "if decryption fails (...), the receiver (...) MUST attempt to process the
            // remaining packets."
            Utils.error(decryptErrorAlert);
            if (checkForStatelessResetToken(buffer)) {
                if (enterDrainingState()) {
                    Utils.debug("Client " + isClient + " Entering draining state because " +
                            "stateless reset was received");
                } else {
                    Utils.debug("Client " + isClient + " Received stateless reset");
                }
            } else {
                nextPacket(buffer, timeReceived);
            }
        }
    }

    boolean checkForStatelessResetToken(ByteBuffer buffer) {
        return false;
    }


    protected PacketHeader parsePacket(Level level, Number dcid,
                                       byte flags, int posFlags, ByteBuffer buffer)
            throws DecryptErrorAlert {

        if (PacketParser.invalidFixBit(flags)) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-27#section-17.2
            // https://tools.ietf.org/html/draft-ietf-quic-transport-27#section-17.3
            // "Fixed Bit:  The next bit (0x40) of byte 0 is set to 1.  Packets
            // containing a zero value for this bit are not valid packets in this
            // version and MUST be discarded."
            return null;
        }

        // check if the level is not discarded (level can be null, to avoid expensive
        // exception handling
        if (isDiscarded(level)) {
            return null;
        }

        try {
            Keys keys = remoteSecrets(level);

            if (keys == null) {
                // Could happen when, due to packet reordering, the first short header
                // packet arrives before handshake is finished.
                // https://tools.ietf.org/html/draft-ietf-quic-tls-18#section-5.7
                // "Due to reordering and loss, protected packets might be received by an
                // endpoint before the final TLS handshake messages are received."
                throw new DecryptErrorAlert(level.name());
            }

            long lpn = largestPacketNumber[level.ordinal()];


            PacketHeader packetHeader = switch (level) {
                case Initial -> PacketParser.parseInitialPacketHeader(
                        buffer, dcid, flags, posFlags, version(), keys, lpn);
                case Handshake -> PacketParser.parseHandshakePackageHeader(
                        buffer, dcid, flags, posFlags, version(), keys, lpn);
                case App -> PacketParser.parseShortPacketHeader(
                        buffer, dcid, flags, posFlags, version(), keys, lpn);
            };

            if (packetHeader == null) {
                return null;
            }
            updateKeysAndPackageNumber(packetHeader);
            return packetHeader;
        } catch (IOException ioException) {
            Utils.error(ioException);
            return null;
        }
    }

    public final void updateKeysAndPackageNumber(PacketHeader packetHeader) {
        Level level = packetHeader.level();
        updateKeys(level, packetHeader);
        updatePackageNumber(level, packetHeader);
    }

    private void updateKeys(Level level, PacketHeader packetHeader) {
        if (packetHeader.hasUpdatedKeys()) {
            // update the secrets keys here (updated was set)
            remoteSecrets(level, packetHeader.updated());

            // now update own secrets
            Keys oldKeys = ownSecrets(level);
            Keys newKeys = Keys.computeKeyUpdate(version(), oldKeys);
            ownSecrets(level, newKeys);
        }
    }

    private void updatePackageNumber(Level level, PacketHeader packetHeader) {
        if (packetHeader.packetNumber() > largestPacketNumber[level.ordinal()]) {
            largestPacketNumber[level.ordinal()] = packetHeader.packetNumber();
        }
    }


    protected boolean processFrames(PacketHeader packetHeader, long timeReceived) throws IOException {

        // <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-terms-and-definitions">...</a>
        // "Ack-eliciting packet: A QUIC packet that contains frames other than ACK, PADDING,
        // and CONNECTION_CLOSE."
        boolean isAckEliciting = false;


        ByteBuffer buffer = ByteBuffer.wrap(packetHeader.framesBytes());

        byte frameType;

        while (buffer.remaining() > 0) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-16#section-12.4
            // "Each frame begins with a Frame Type, indicating its payloadType,
            // followed by additional payloadType-dependent fields"
            frameType = buffer.get();
            switch (frameType) {
                case 0x00 -> // isAckEliciting = false
                        FrameReceived.parsePaddingFrame(buffer);
                case 0x01 -> // ping frame nothing to parse
                        isAckEliciting = true;
                case 0x02, 0x03 -> // isAckEliciting = false
                        process(FrameReceived.parseAckFrame(frameType, buffer,
                                remoteDelayScale.get()), packetHeader.level(), timeReceived);
                case 0x04 -> {
                    isAckEliciting = true;
                    process(FrameReceived.parseResetStreamFrame(buffer));
                }
                case 0x05 -> {
                    isAckEliciting = true;
                    process(FrameReceived.parseStopSendingFrame(buffer));
                }
                case 0x06 -> {
                    isAckEliciting = true;
                    process(FrameReceived.parseCryptoFrame(buffer), packetHeader.level());
                }
                case 0x07 -> {
                    isAckEliciting = true;
                    process(FrameReceived.parseNewTokenFrame(buffer));
                    // type will be supported someday in the future
                }
                case 0x10 -> {
                    isAckEliciting = true;
                    process(FrameReceived.parseMaxDataFrame(buffer));
                }
                case 0x011 -> {
                    isAckEliciting = true;
                    process(FrameReceived.parseMaxStreamDataFrame(buffer));
                }
                case 0x12, 0x13 -> {
                    isAckEliciting = true;
                    process(FrameReceived.parseMaxStreamsFrame(frameType, buffer));
                }
                case 0x14 -> {
                    isAckEliciting = true;
                    System.err.println("parseDataBlockedFrame");
                    FrameReceived.parseDataBlockedFrame(buffer);
                    // type will be supported someday in the future
                }
                case 0x15 -> {
                    isAckEliciting = true;
                    System.err.println("parseStreamDataBlockedFrame");
                    FrameReceived.parseStreamDataBlockedFrame(buffer);
                    // type will be supported someday in the future
                }
                case 0x16, 0x17 -> {
                    isAckEliciting = true;
                    System.err.println("parseStreamDataBlockedFrame");
                    FrameReceived.parseStreamsBlockedFrame(frameType, buffer);
                    // type will be supported someday in the future
                }
                case 0x18 -> {
                    isAckEliciting = true;
                    process(FrameReceived.parseNewConnectionIdFrame(buffer));
                }
                case 0x19 -> {
                    isAckEliciting = true;
                    process(FrameReceived.parseRetireConnectionIdFrame(buffer),
                            packetHeader.dcid());
                }
                case 0x1a -> {
                    isAckEliciting = true;
                    process(FrameReceived.parsePathChallengeFrame(buffer));
                }
                case 0x1b -> {
                    isAckEliciting = true;
                    System.err.println("parsePathResponseFrame");
                    FrameReceived.parsePathResponseFrame(buffer);
                    // type will be supported someday in the future
                }
                case 0x1c, 0x1d -> // isAckEliciting is false;
                        process(FrameReceived.parseConnectionCloseFrame(
                                frameType, buffer), packetHeader.level());
                case 0x1e -> {
                    isAckEliciting = true;
                    process(FrameReceived.parseHandshakeDoneFrame());
                }
                default -> {
                    if ((frameType >= 0x08) && (frameType <= 0x0f)) {
                        isAckEliciting = true;
                        process(FrameReceived.parseStreamFrame(frameType, buffer));
                    } else {
                        // https://tools.ietf.org/html/draft-ietf-quic-transport-24#section-12.4
                        // "An endpoint MUST treat the receipt of a frame of unknown payloadType as a connection error of payloadType FRAME_ENCODING_ERROR."
                        throw new IOException("Receipt a frame of unknown payloadType");
                    }
                }
            }
        }

        return isAckEliciting;
    }

    final void flush() {
        if (lock.tryLock()) {
            try {
                waitCondition.signal();
            } finally {
                lock.unlock();
            }
        }
    }

    // https://www.rfc-editor.org/rfc/rfc9000.html#name-path_challenge-frames
    // "The recipient of this frame MUST generate a PATH_RESPONSE frame (...) containing the same Data value."
    abstract boolean process(PacketHeader packetHeader, long timeReceived) throws IOException;

    final void processPacket(PacketHeader packetHeader, long timeReceived) {

        try {
            if (!connectionState.get().closingOrDraining()) {

                Level level = packetHeader.level();
                boolean isAckEliciting = process(packetHeader, timeReceived);

                // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-13.1
                // "A packet MUST NOT be acknowledged until packet protection has been successfully
                // removed and all frames contained in the packet have been processed."
                // "Once the packet has been fully processed, a receiver acknowledges receipt by
                // sending one or more ACK frames containing the packet number of the received packet."
                // boolean isAckEliciting  = PacketHeader.isAckEliciting(frames);
                ackGenerator(level).packetReceived(level, isAckEliciting,
                        packetHeader.packetNumber(), timeReceived);


                // https://tools.ietf.org/html/draft-ietf-quic-transport-31#section-10.1
                // "An endpoint restarts its idle timer when a packet from its peer is received and processed successfully."
                packetIdleProcessed();
            } else if (connectionState.get().isClosing()) {
                // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2.1
                // "An endpoint in the closing state sends a packet containing a CONNECTION_CLOSE frame in response
                //  to any incoming packet that it attributes to the connection."
                handlePacketInClosingState(packetHeader.level());
            }
        } catch (Throwable throwable) {
            Utils.error(throwable); // ignore
        } finally {
            // Processed all packets in the datagram, so not expecting more.
            flush();
        }
    }

    abstract void process(FrameReceived.HandshakeDoneFrame ignoredHandshakeDoneFrame);

    abstract void process(FrameReceived.RetireConnectionIdFrame retireConnectionIdFrame,
                          Number dcid);

    abstract void process(FrameReceived.NewConnectionIdFrame newConnectionIdFrame);

    abstract void process(FrameReceived.NewTokenFrame ignoredNewTokenFrame);

    final void process(FrameReceived.CryptoFrame cryptoFrame, Level level) {
        try {
            getCryptoStream(level).add(cryptoFrame);
        } catch (Throwable throwable) {
            immediateCloseWithError(level, quicError(throwable));
        }
    }


    final void process(FrameReceived.MaxStreamDataFrame maxStreamDataFrame) {
        try {
            processMaxStreamDataFrame(maxStreamDataFrame);
        } catch (TransportError transportError) {
            immediateCloseWithError(Level.App, transportError);
        }
    }


    final void process(FrameReceived.PathChallengeFrame pathChallengeFrame) {
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-retransmission-of-informati
        // "Responses to path validation using PATH_RESPONSE frames are sent just once."
        addRequest(App, Frame.createPathResponseFrame(pathChallengeFrame.data()));
    }


    final void process(FrameReceived.StreamFrame streamFrame) {
        try {
            processStreamFrame(this, streamFrame);
        } catch (TransportError transportError) {
            immediateCloseWithError(Level.App, transportError);
        }
    }

    /**
     * @noinspection ExtractMethodRecommender
     */
    final void determineIdleTimeout(long maxIdleTimout, long peerMaxIdleTimeout) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-31#section-10.1
        // "If a max_idle_timeout is specified by either peer in its transport parameters (Section 18.2), the
        //  connection is silently closed and its state is discarded when it remains idle for longer than the minimum
        //  of both peers max_idle_timeout values."
        long idleTimeout = Long.min(maxIdleTimout, peerMaxIdleTimeout);
        if (idleTimeout == 0) {
            // Value of 0 is the same as not specified.
            idleTimeout = Long.max(maxIdleTimout, peerMaxIdleTimeout);
        }
        if (idleTimeout != 0) {
            // Initialise the idle timer that will take care of (silently) closing connection if idle longer than idle timeout
            setIdleTimeout(idleTimeout);
        } else {
            // Both or 0 or not set: [Note: this does not occur within this application]
            // https://tools.ietf.org/html/draft-ietf-quic-transport-31#section-18.2
            // "Idle timeout is disabled when both endpoints omit this transport parameter or specify a value of 0."
            setIdleTimeout(Long.MAX_VALUE);
        }


    }

    private void silentlyCloseConnection(long idleTime) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.1
        // "If a max_idle_timeout is specified by either peer (...), the connection is silently closed and its state is
        //  discarded when it remains idle for longer than the minimum of both peers max_idle_timeout values."
        clearRequests();
        Utils.debug("Idle timeout: silently closing connection after "
                + idleTime + " ms of inactivity");
        terminate();
    }

    /**
     * Immediately closes the connection (with or without error) and enters the "closing state".
     * Connection close frame with indicated error (or "NO_ERROR") is send to peer and after 3 x PTO,
     * the closing state is ended and all connection state is discarded.
     * If this method is called outside received-message-processing, post-processing actions
     * (including flushing the sender) should be performed by the caller.
     *
     * @param level The level that should be used for sending the connection close frame
     */

    protected void immediateCloseWithError(Level level, TransportError transportError) {
        if (connectionState.get().closingOrDraining()) {
            Utils.debug("Immediate close ignored because already closing");
            return;
        }

        disableKeepAlive();

        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2
        // "An endpoint sends a CONNECTION_CLOSE frame (Section 19.19) to terminate the connection immediately."
        clearRequests(); // all outgoing messages are cleared -> purpose send connection close
        addRequest(level, Frame.createConnectionCloseFrame(transportError));
        flush();

        // "After sending a CONNECTION_CLOSE frame, an endpoint immediately enters the closing state;"
        connectionState.set(Connection.Status.Closing);


        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2.3
        // "An endpoint that has not established state, such as a server that detects an error in an Initial packet,
        //  does not enter the closing state."
        if (level == Initial) {
            terminate(); // immediately terminate
        } else {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2
            // "The closing and draining connection states exist to ensure that connections close cleanly and that
            //  delayed or reordered packets are properly discarded. These states SHOULD persist for at least three
            //  times the current Probe Timeout (PTO) interval"
            int pto = getPto();
            scheduleTerminate(pto);
        }
    }

    private void scheduleTerminate(int pto) {
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.schedule(this::terminate, 3L * pto, TimeUnit.MILLISECONDS);
        scheduler.shutdown();
    }

    private void handlePacketInClosingState(Level level) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2.2
        // "An endpoint MAY enter the draining state from the closing state if it receives
        // a CONNECTION_CLOSE frame, which indicates that the peer is also closing or draining."
        // NOT DONE HERE (NO DRAINING)

        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2.1
        // "An endpoint in the closing state sends a packet containing a CONNECTION_CLOSE frame
        // in response to any incoming packet that it attributes to the connection."
        // "An endpoint SHOULD limit the rate at which it generates packets in the closing state."
        closeFramesSendRateLimiter.execute(() -> addRequest(level,
                Frame.createConnectionCloseFrame()));
        // No flush necessary, as this method is called while processing a received packet.
    }

    private void process(FrameReceived.ConnectionCloseFrame closing, Level level) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2.2
        // "The draining state is entered once an endpoint receives a CONNECTION_CLOSE frame,
        // which indicates that its peer is closing or draining."
        if (!connectionState.get().closingOrDraining()) {  // Can occur due to race condition (both peers closing simultaneously)
            if (closing.hasError()) {
                Utils.debug("Connection closed with " + determineClosingErrorMessage(closing));
            }
            clearRequests();

            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2.2
            // "An endpoint that receives a CONNECTION_CLOSE frame MAY send a single packet containing a CONNECTION_CLOSE
            //  frame before entering the draining state, using a CONNECTION_CLOSE frame and a NO_ERROR code if appropriate.
            //  An endpoint MUST NOT send further packets."
            addRequest(level, Frame.createConnectionCloseFrame());
            // No flush necessary, as this method is called while processing a received packet.

            drain();
        }
    }

    private void drain() {
        connectionState.set(Connection.Status.Draining);
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2
        // "The closing and draining connection states exist to ensure that connections close cleanly and that
        //  delayed or reordered packets are properly discarded. These states SHOULD persist for at least three
        //  times the current Probe Timeout (PTO) interval"
        int pto = getPto();
        scheduleTerminate(pto);
    }

    private boolean enterDrainingState() {
        if (connectionState.get().closingOrDraining()) {
            return false;
        } else {
            clearRequests();
            drain();
            return true;
        }
    }

    /**
     * Closes the connection by discarding all connection state. Do not call directly, should be called after
     * closing state or draining state ends.
     */
    void terminate() {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2
        // "Once its closing or draining state ends, an endpoint SHOULD discard all connection state."
        shutdownRequester();
        super.terminate(); // cleanup
        connectionState.set(Connection.Status.Closed);
        if (closeConsumer != null) {
            closeConsumer.accept(this); // should be done before clearing attributes
            closeConsumer = null; // not required anymore
        }
        attributes.clear();
    }

    public void setCloseConsumer(Consumer<Connection> closeConsumer) {
        this.closeConsumer = closeConsumer;
    }

    public final void close() {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2
        immediateCloseWithError(App, new TransportError(TransportError.Code.NO_ERROR));
    }

    public final void close(tech.lp2p.core.Status status) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-10.2
        immediateCloseWithError(App, new TransportError(status));
    }

    public final int version() {
        return version;
    }

    /**
     * Returns the connection ID of this connection. During handshake, this is a fixed ID, that is generated by this
     * endpoint. After handshaking, this is one of the active connection ID's; if there are multiple active connection
     * ID's, which one is returned is not determined (but this method should always return the same until it is not
     * active anymore). Note that after handshaking, the connection ID (of this endpoint) is not used for sending
     * packets (short header packets only contain the destination connection ID), only for routing received packets.
     */
    abstract Number activeScid();

    /**
     * Returns the current peer connection ID, i.e. the connection ID this endpoint uses as
     * destination connection id when sending packets. During handshake this is
     * a fixed ID generated by the peer (except for the first Initial packets send by the client).
     * After handshaking, there can be multiple active connection ID's supplied by the peer; which
     * one is current (thus, is being used when sending packets) is determined by the implementation.
     */
    abstract Number activeDcid();


    abstract void abortConnection(Throwable fatalError);

    private void setIdleTimeout(long idleTimeoutInMillis) {
        if (!enabledIdle.getAndSet(true)) {
            lastIdleAction.set(System.currentTimeMillis());
            // https://tools.ietf.org/html/draft-ietf-quic-transport-31#section-10.1
            // To avoid excessively small idle timeout periods, endpoints MUST increase the idle timeout period
            // to be at least three times the current Probe Timeout (PTO)
            idleTimeout.set(Math.max(idleTimeoutInMillis, 3L * getPto()));
        }
    }

    private void checkIdle() {
        if (enabledIdle.get()) {
            long now = System.currentTimeMillis();
            if (now > (lastIdleAction.get() + idleTimeout.get())) {
                enabledIdle.set(false);
                silentlyCloseConnection(idleTimeout.get());
            }
        }
    }

    private void packetIdleProcessed() {
        if (enabledIdle.get()) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-31#section-10.1
            // "An endpoint restarts its idle timer when a packet from its peer is received
            // and processed successfully."
            lastIdleAction.set(System.currentTimeMillis());
        }
    }

    private void packetIdleSent(Packet packet, long sendTime) {
        if (enabledIdle.get()) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-31#section-10.1
            // "An endpoint also restarts its idle timer when sending an ack-eliciting packet if no other ack-eliciting
            //  packets have been sent since last receiving and processing a packet. "
            if (Packet.isAckEliciting(packet)) {
                lastIdleAction.set(sendTime);
            }
        }
    }

    public InetSocketAddress remoteAddress() {
        return remoteAddress;
    }

    final void startRequester() {
        requesterThread.start();
    }

    final void shutdownRequester() {
        // Stopped should have be called before.
        // Stop cannot be called here (again),
        // because it would drop ConnectionCloseFrame still waiting to be sent.

        shutdownRequester = true;
        requesterThread.interrupt();
    }

    private void run() {

        try {
            // Determine whether this loop must be ended _before_ composing packets, to avoid race conditions with
            // items being queued just after the packet assembler (for that level) has executed.
            while (!shutdownRequester) {

                lossDetection();
                sendIfAny();

                keepAlive(); // only happens when enabled
                checkIdle(); // only happens when enabled

                lock.lock();
                try {
                    int time = Math.min(Settings.MAX_ACK_DELAY * (idleCounter.get() + 1),
                            Settings.PING_INTERVAL); // time is max PING_INTERVAL 5s
                    boolean result = waitCondition.await(time, TimeUnit.MILLISECONDS);
                    if (!result) {
                        idleCounter.incrementAndGet();
                        Utils.debug("Lock timeout (no interruption) " + "Client " + isClient +
                                " time " + time);
                    }
                } finally {
                    lock.unlock();
                }
            }
        } catch (Throwable fatalError) {
            if (!shutdownRequester) {
                abortConnection(fatalError);
            }
        }
    }

    private void sendIfAny() throws IOException {
        List<Packet> items;
        do {
            items = assemblePackets();
            if (!items.isEmpty()) {
                send(items);
            }
        } while (!items.isEmpty());
    }

    private void send(List<Packet> itemsToSend) throws IOException {

        for (Packet packet : itemsToSend) {
            Keys keys = ownSecrets(packet.level());
            if (keys != null) { // keys can be discard in between
                byte[] packetData = packet.generatePacketBytes(keys);

                DatagramPacket datagram = new DatagramPacket(packetData, packetData.length,
                        remoteAddress.getAddress(), remoteAddress.getPort());

                long timeSent = System.currentTimeMillis();
                datagramSocket.send(datagram);
                idleCounter.set(0);
                packetSent(packet, timeSent, packetData.length);
                packetIdleSent(packet, timeSent);
            }
        }
    }


    public void setAttribute(String key, Object value) {
        attributes.put(key, value);
    }


    public Object getAttribute(String key) {
        return attributes.get(key);
    }

    public boolean hasAttribute(String key) {
        return attributes.containsKey(key);
    }

    public void removeAttribute(String key) {
        attributes.remove(key);
    }


    /**
     * Assembles packets for sending in one datagram. The total size of the QUIC packets returned will never exceed
     * max packet size and for packets not containing probes, it will not exceed the remaining congestion window size.
     */
    private List<Packet> assemblePackets() {
        Number scid = activeScid();
        Number dcid = activeDcid();
        int dcidLength = VariableLengthInteger.length(dcid);

        List<Packet> packets = new ArrayList<>();
        int size = 0;

        int minPacketSize = 19 + dcidLength;  // Computed for short header packet
        int remaining = Integer.min((int) remainingCwnd(), Settings.MAX_PACKAGE_SIZE);

        for (Level level : Level.levels()) {
            if (!isDiscarded(level)) {
                PacketAssembler assembler = packetAssembler(level);

                Packet item = assembler.assemble(remaining,
                        Settings.MAX_PACKAGE_SIZE - size, scid, dcid);

                if (item != null) {
                    packets.add(item);
                    int packetSize = item.estimateLength();
                    size += packetSize;
                    remaining -= packetSize;
                }
                if (remaining < minPacketSize && (Settings.MAX_PACKAGE_SIZE - size) < minPacketSize) {
                    // Trying a next level to produce a packet is useless
                    break;
                }

            }
        }

        return packets;

    }

    public abstract PeerId remotePeerId();

    public enum Status {
        Created,
        Handshaking,
        Connected,
        Closing,
        Draining,
        Closed,
        Failed;

        @SuppressWarnings("BooleanMethodIsAlwaysInverted")
        boolean closingOrDraining() {
            return this == Closing || this == Draining;
        }

        boolean isClosing() {
            return this == Closing;
        }

        boolean isConnected() {
            return this == Connected;
        }

        public boolean isClosed() {
            return this == Closed;
        }
    }
}
