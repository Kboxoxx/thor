package tech.lp2p.quic;

import java.net.UnknownServiceException;
import java.nio.ByteBuffer;

import tech.lp2p.Lite;
import tech.lp2p.core.Status;

public record AlpnLibp2pRequester(Requester requester, StreamState streamState)
        implements StreamHandler {

    public static AlpnLibp2pRequester create(Requester requester) {
        return new AlpnLibp2pRequester(requester, new Libp2pState(requester));
    }

    @Override
    public void data(Stream stream, byte[] data) {
        try {
            StreamState.iteration(streamState, stream, ByteBuffer.wrap(data));
        } catch (UnknownServiceException unknownServiceException) {
            stream.resetStream(Status.PROTOCOL_NEGOTIATION_FAILED);
            throwable(unknownServiceException);
        } catch (Throwable throwable) {
            stream.resetStream(Status.INTERNAL_ERROR);
            throwable(throwable);
        }
    }

    @Override
    public long delimiter(Stream stream) {
        return Lite.CHUNK_DEFAULT;
    }

    @Override
    public void terminated(Stream stream) {
        streamState.reset();
        requester.terminated(stream);
    }

    @Override
    public void fin(Stream stream) {
        streamState.reset();
        requester.fin(stream);
    }

    public void throwable(Throwable throwable) {
        streamState.reset();
        requester.throwable(throwable);
    }

    @Override
    public boolean readFully(Stream stream) {
        return false;
    }


    static class Libp2pState extends StreamState {

        private final Requester requester;


        Libp2pState(Requester requester) {
            this.requester = requester;
        }

        public void accept(Stream stream, byte[] frame) throws Exception {
            if (frame.length > 0) {
                if (!StreamState.isProtocol(frame)) {
                    requester.data(stream, frame);
                }
            }
        }
    }
}
