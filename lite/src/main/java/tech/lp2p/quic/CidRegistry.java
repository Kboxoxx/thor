package tech.lp2p.quic;

import java.util.Collection;
import java.util.concurrent.ConcurrentSkipListSet;

import tech.lp2p.utils.Utils;

class CidRegistry {

    /**
     * Maps sequence number to connection ID (info)
     */
    private final ConcurrentSkipListSet<CidInfo> cidInfos = new ConcurrentSkipListSet<>();

    CidRegistry(CidInfo init) {
        cidInfos.add(init);
    }

    void initialConnectionId(Number cid) {
        cidInfos.first().cid(cid);
    }

    void initialStatelessResetToken(byte[] statelessResetToken) {
        cidInfos.first().statelessResetToken(statelessResetToken);
    }

    int maxSequenceNr() {
        int maxSequenceNr = 0;
        for (CidInfo cidInfo : cidInfos) {
            if (cidInfo.sequenceNumber() > maxSequenceNr) {
                maxSequenceNr = cidInfo.sequenceNumber();
            }
        }

        return maxSequenceNr;
    }


    Integer retireCid(int sequenceNr) {
        CidInfo cidInfo = cidInfo(sequenceNr);
        if (cidInfo != null) {
            if (cidInfo.cidStatus().active()) {
                cidInfo.cidStatus(CidStatus.RETIRED);
                Utils.checkTrue(cidInfo.cid() instanceof Integer, "Invalid number");
                return (Integer) cidInfo.cid();
            }
        }
        return null;
    }


    CidInfo cidInfo(int sequenceNr) {
        for (CidInfo cidInfo : cidInfos) {
            if (cidInfo.sequenceNumber() == sequenceNr) {
                return cidInfo;
            }
        }
        return null;
    }


    Number getInitial() {
        return cidInfos.first().cid();
    }

    /**
     * Get an active connection ID. There can be multiple active connection IDs, this method returns an arbitrary one.
     *
     * @return an active connection ID or null if non is active (which should never happen).
     */
    Number getActive() {
        for (CidInfo info : cidInfos) {
            if (info.cidStatus().active()) {
                return info.cid();
            }
        }
        throw new IllegalStateException("no active connection id");
    }


    int getActiveCids() {
        int active = 0;
        for (CidInfo info : cidInfos) {
            if (info.cidStatus().active()) {
                active++;
            }
        }
        return active;
    }


    Collection<CidInfo> cidInfos() {
        return cidInfos;
    }

}

