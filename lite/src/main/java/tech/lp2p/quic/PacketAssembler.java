package tech.lp2p.quic;

import java.util.ArrayList;
import java.util.List;


/**
 * Assembles QUIC packets for a given encryption level, based on "send requests" that are previously queued.
 * These send requests either contain a frame, or can produce a frame to be sent.
 */
final class PacketAssembler {

    private final int version;
    private final Level level;
    private final SendRequestQueue sendRequestQueue;
    private final AckGenerator ackGenerator;
    private long packetNumberGenerator = 0L; // no concurrency


    PacketAssembler(int version, Level level, SendRequestQueue sendRequestQueue,
                    AckGenerator ackGenerator) {
        this.version = version;
        this.level = level;
        this.sendRequestQueue = sendRequestQueue;
        this.ackGenerator = ackGenerator;
    }


    private static void addPadding(Level level, int dcidLength, List<Frame> frames) {
        if (level == Level.Initial) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-14
            // "A client MUST expand the payload of all UDP datagrams carrying Initial packets to at least the smallest
            //  allowed maximum datagram size of 1200 bytes... "
            // "Similarly, a server MUST expand the payload of all UDP datagrams carrying ack-eliciting Initial packets
            //  to at least the smallest allowed maximum datagram size of 1200 bytes."

            int requiredPadding = 1200 - estimateLength(level, dcidLength, frames,
                    0);
            if (requiredPadding > 0) {
                frames.add(Frame.createPaddingFrame(requiredPadding));
            }
        }
        if (hasPathChallengeOrResponse(frames)) {

            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-8.2.1
            // "An endpoint MUST expand datagrams that contain a PATH_CHALLENGE frame to at least the smallest allowed
            //  maximum datagram size of 1200 bytes."
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-8.2.2
            // "An endpoint MUST expand datagrams that contain a PATH_RESPONSE frame to at least the smallest allowed
            // maximum datagram size of 1200 bytes."


            int requiredPadding = 1200 - estimateLength(level,
                    dcidLength, frames, 0);
            if (requiredPadding > 0) {
                frames.add(Frame.createPaddingFrame(requiredPadding));
            }

        }
    }

    static Packet createPacket(int version, Level level, long packetNumber,
                               Number scid, Number dcid, List<Frame> frames) {

        Frame[] framesArray = new Frame[frames.size()];
        frames.toArray(framesArray);

        return switch (level) {
            case App -> PacketService.createShortHeader(version,
                    framesArray, packetNumber, dcid);
            case Handshake -> PacketService.createHandshake(version, framesArray,
                    packetNumber, scid, dcid);
            case Initial -> PacketService.createInitial(version, framesArray,
                    packetNumber, scid, dcid);
        };

    }

    private static int framesLength(List<Frame> frames) {
        int sum = 0;
        for (Frame frame : frames) {
            sum += frame.frameLength();
        }
        return sum;
    }

    private static boolean hasPathChallengeOrResponse(List<Frame> frames) {
        for (Frame frame : frames) {
            if (frame.frameType() == FrameType.PathResponseFrame
                    || frame.frameType() == FrameType.PathChallengeFrame) {
                return true;
            }
        }
        return false;
    }

    /**
     * Estimates what the length of this packet will be after it has been encrypted. The returned length must be
     * less then or equal the actual length after encryption. Length estimates are used when preparing packets for
     * sending, where certain limits must be met (e.g. congestion control, max datagram size, ...).
     *
     * @param additionalPayload when not 0, estimate the length if this amount of additional (frame) bytes were added.
     */
    private static int estimateLength(Level level, int dcidLength,
                                      List<Frame> frames, int additionalPayload) {
        return switch (level) {
            case App -> estimateShortHeaderPacketLength(dcidLength,
                    frames, additionalPayload);
            case Handshake -> estimateHandshakePacketLength(
                    dcidLength, frames, additionalPayload);
            case Initial -> estimateInitialPacketLength(
                    dcidLength, frames, additionalPayload);
        };
    }

    private static int estimateShortHeaderPacketLength(int dcidLength,
                                                       List<Frame> frames,
                                                       int additionalPayload) {
        int payloadLength = framesLength(frames) + additionalPayload;
        return 1
                + dcidLength
                + 1  // packet number length: will usually be just 1, actual value
                // cannot be computed until packet number is known
                + payloadLength
                // https://tools.ietf.org/html/draft-ietf-quic-tls-27#section-5.4.2
                // "The ciphersuites defined in [TLS13] - (...) - have 16-byte expansions..."
                + 16;
    }

    private static int estimateHandshakePacketLength(int dcidLength,
                                                     List<Frame> frames,
                                                     int additionalPayload) {
        int payloadLength = framesLength(frames) + additionalPayload;
        return 1
                + 4
                + 1 + dcidLength
                + 1 + Integer.BYTES // scid
                + (payloadLength + 1 > 63 ? 2 : 1)
                + 1  // packet number length: will usually be just 1, actual value cannot be
                // computed until packet number is known
                + payloadLength
                // https://tools.ietf.org/html/draft-ietf-quic-tls-27#section-5.4.2
                // "The ciphersuites defined in [TLS13] - (...) - have 16-byte expansions..."
                + 16;
    }

    private static int estimateInitialPacketLength(int dcidLength, List<Frame> frames,
                                                   int additionalPayload) {
        int payloadLength = framesLength(frames) + additionalPayload;
        return 1
                + 4
                + 1 + dcidLength
                + 1 + Integer.BYTES // scid
                + 1 // token length, which is not supported
                + (payloadLength + 1 > 63 ? 2 : 1)
                + 1  // packet number length: will usually be just 1, actual value cannot be
                // computed until packet number is known
                + payloadLength
                // https://tools.ietf.org/html/draft-ietf-quic-tls-27#section-5.4.2
                // "The ciphersuites defined in [TLS13] - (...) - have 16-byte expansions..."
                + 16;
    }

    /**
     * Assembles a QUIC packet for the encryption level handled by this instance.
     *
     * @param scid can be null when encryption level is 1-rtt; but not for the other levels; can be empty array though
     */


    Packet assemble(int remainingCwndSize, int availablePacketSize, Number scid, Number dcid) {

        if (level == Level.Initial && availablePacketSize < 1200) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-14
            // "A client MUST expand the payload of all UDP datagrams carrying Initial packets to at least the smallest
            //  allowed maximum datagram size of 1200 bytes... "
            // "Similarly, a server MUST expand the payload of all UDP datagrams carrying ack-eliciting Initial packets
            //  to at least the smallest allowed maximum datagram size of 1200 bytes."
            // Note that in case of an initial packet, the availablePacketSize equals the maximum datagram size; even
            // when different packets are coalesced, the initial packet is always the first that is assembled.
            return null;
        }
        // Packet can be 3 bytes larger than estimated size because of unknown packet number length.
        final int available = Integer.min(remainingCwndSize, availablePacketSize - 3);
        List<Frame> frames = new ArrayList<>();

        long packetNumber = packetNumberGenerator++;

        int dcidLength = VariableLengthInteger.length(dcid);

        // Check for an explicit ack, i.e. an ack on ack-eliciting packet that cannot be delayed (any longer)
        if (ackGenerator.mustSendAck(level)) {

            Frame ackFrame = ackGenerator.generateAck(packetNumber,
                    length -> estimateLength(level, dcidLength,
                            frames, length) <= availablePacketSize);

            // https://tools.ietf.org/html/draft-ietf-quic-transport-29#section-13.2
            // "... packets containing only ACK frames are not congestion controlled ..."
            // So: only check if it fits within available packet space
            if (ackFrame != null) {
                frames.add(ackFrame);
            } else {
                // If not even a mandatory ack can be added, don't bother about other frames: theoretically there might be frames
                // that can be fit, but this is very unlikely to happen (because limit packet size is caused by coalescing packets
                // in one datagram, which will only happen during handshake, when acks are still small) and even then: there
                // will be a next packet in due time.
                // AckFrame does not fit in availablePacketSize, so just return;
                return null;
            }
        }

        if (sendRequestQueue.hasRequests()) {
            // Must create packet here, to have an initial estimate of packet header overhead
            int estimatedSize = estimateLength(level,
                    dcidLength, frames, 1000) - 1000;  // Estimate length if large frame would have been added; this will give upper limit of packet overhead.

            while (estimatedSize < available) {
                // First try to find a frame that will leave space for optional frame (if any)
                int proposedSize = available - estimatedSize;
                SendRequest next = sendRequestQueue.next(proposedSize);

                if (next == null) {
                    // Nothing fits within available space
                    break;
                }
                Frame nextFrame = next.frameSupplier().apply(proposedSize);
                if (nextFrame != null) {
                    if (nextFrame.frameLength() > proposedSize) {
                        throw new IllegalStateException(
                                "supplier does not produce frame of right (max) size: " +
                                        nextFrame.frameLength() + " > " + (proposedSize)
                                        + " frame: " + nextFrame);
                    }
                    estimatedSize += nextFrame.frameLength();
                    frames.add(nextFrame);
                }
            }
        }


        Packet packet;
        if (frames.isEmpty()) {
            // Nothing could be added, discard packet and mark packet number as not used
            packetNumberGenerator--;
            packet = null;
        } else {
            addPadding(level, dcidLength, frames);
            packet = createPacket(version, level, packetNumber, scid, dcid, frames);
        }
        return packet;

    }

}

