package tech.lp2p.quic;

public interface StreamHandler {

    void terminated(Stream stream);

    void fin(Stream stream);

    boolean readFully(Stream stream);

    void data(Stream stream, byte[] data);

    long delimiter(Stream stream);
}
