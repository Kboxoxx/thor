package tech.lp2p.quic;


import java.util.concurrent.TimeoutException;

import tech.lp2p.core.ALPN;

public interface Requester {
    String PEER = "PEER";
    String RELAYED = "RELAYED";
    String STREAM = "STREAM";
    String ADDRS = "ADDRS";
    String TIMER = "TIMER";
    String INITIALIZED = "INITIALIZED";
    String RESERVATION = "RESERVATION";


    static Stream createStream(Connection connection, Requester requester)
            throws InterruptedException, TimeoutException {
        if (connection.alpn() == ALPN.libp2p) {
            return connection.createStream(AlpnLibp2pRequester.create(requester), true);
        }
        throw new InterruptedException("not supported alpn");
    }


    static Stream createStream(Connection connection, long delimiter)
            throws InterruptedException, TimeoutException {
        return connection.createStream(new RequestResponse(delimiter), true);
    }

    void throwable(Throwable throwable);

    void data(Stream stream, byte[] data) throws Exception;

    void terminated(Stream stream);

    void fin(Stream stream);

    record RequestResponse(long delimiter) implements StreamHandler {

        @Override
        public void terminated(Stream stream) {
        }

        @Override
        public void fin(Stream stream) {
        }

        @Override
        public boolean readFully(Stream stream) {
            return true;
        }

        @Override
        public void data(Stream stream, byte[] byteArray) {
            throw new IllegalStateException("should never be invoked");
        }

        @Override
        public long delimiter(Stream stream) {
            return delimiter;
        }
    }
}
