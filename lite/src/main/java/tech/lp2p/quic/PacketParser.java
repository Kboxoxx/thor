package tech.lp2p.quic;


import static tech.lp2p.quic.Level.App;
import static tech.lp2p.quic.Level.Handshake;
import static tech.lp2p.quic.Level.Initial;

import java.io.IOException;
import java.nio.ByteBuffer;

import tech.lp2p.tls.DecryptErrorAlert;
import tech.lp2p.utils.Utils;

interface PacketParser {
    int PACKET_NUMBER_OFFSET = 5;

    private static byte getInitialPacketType(int version) {
        if (Version.isV2(version)) {
            return (byte) Settings.INITIAL_V2_type;
        } else {
            return (byte) Settings.INITIAL_V1_type;
        }
    }

    static void checkInitialPacketType(int version, int type) {
        if (type != getInitialPacketType(version)) {
            // Programming error: this method shouldn't have been called if packet is not Initial
            throw new IllegalStateException();
        }
    }

    private static void parseToken(ByteBuffer buffer) throws IOException {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-16#section-17.5:
        // "An Initial packet (shown in Figure 13) has two additional header
        // fields that are added to the Long Header before the Length field."

        long tokenLength = VariableLengthInteger.parseLong(buffer);

        if (tokenLength > 0) {
            if (tokenLength <= buffer.remaining()) {
                byte[] token = new byte[(int) tokenLength];
                buffer.get(token);
            } else {
                throw new IOException("invalid packet length");
            }
        }
    }

    static boolean invalidFixBit(byte flags) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-27#section-17.2
        // https://tools.ietf.org/html/draft-ietf-quic-transport-27#section-17.3
        // "Fixed Bit:  The next bit (0x40) of byte 0 is set to 1.  Packets
        // containing a zero value for this bit are not valid packets in this
        // version and MUST be discarded."
        return (flags & 0x40) != 0x40;
    }

    static Level parseLevel(byte flags, int version, ByteBuffer data) {
        if ((flags & 0x80) == 0x80) {
            // Long header packet
            return PacketParser.parseLongHeaderLevel(flags, version, data);
        } else {
            // Short header packet
            if ((flags & 0xc0) == 0x40) {
                return App;
            }
        }
        return null;
    }

    static Level parseLongHeaderLevel(byte flags, int version, ByteBuffer data) {

        if (data.remaining() < 4) { // 4 bits required here
            return null;
        }

        int versionId = data.getInt(); // 4 bits

        // https://tools.ietf.org/html/draft-ietf-quic-transport-16#section-17.4:
        // "A Version Negotiation packet ... will appear to be a packet using the long header, but
        //  will be identified as a Version Negotiation packet based on the
        //  Version field having a value of 0."
        if (versionId == 0) {
            return null;
        }

        if (versionId != version) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-27#section-5.2
            // "... packets are discarded if they indicate a different protocol version than that of the connection..."
            return null;
        }

        // https://tools.ietf.org/html/draft-ietf-quic-transport-17#section-17.5
        // "An Initial packet uses long headers with a payloadType value of 0x0."
        if ((flags & 0xf0) == 0xc0) {  // 1100 0000
            return Initial;
        }
        // https://tools.ietf.org/html/draft-ietf-quic-transport-17#section-17.7
        // "A Retry packet uses a long packet header with a payloadType value of 0x3"
        else if ((flags & 0xf0) == 0xf0) {  // 1111 0000
            // Retry packet....
            Utils.debug("Retry packet is intentionally not supported");
            return null;
        }
        // https://tools.ietf.org/html/draft-ietf-quic-transport-17#section-17.6
        // "A Handshake packet uses long headers with a payloadType value of 0x2."
        else if ((flags & 0xf0) == 0xe0) {  // 1110 0000
            return Handshake;
        }
        // https://tools.ietf.org/html/draft-ietf-quic-transport-17#section-17.2
        // "|  0x1 | 0-RTT Protected | Section 12.1 |"
        else if ((flags & 0xf0) == 0xd0) {  // 1101 0000
            // 0-RTT Protected
            // "It is used to carry "early"
            // data from the client to the server as part of the first flight, prior
            // to handshake completion."

            // When such a packet arrives, consider it to be caused by network corruption, so
            Utils.debug("network corruption detected");
            return null;

        } else {
            Utils.debug("Should not happen, all cases should be covered above");
            return null;
        }
    }

    static Integer scid(ByteBuffer buffer) {

        int srcConnIdLength = buffer.get();
        if (srcConnIdLength != Integer.BYTES) {
            return null;
        }
        if (buffer.remaining() < srcConnIdLength) {
            return null;
        }
        return buffer.getInt();
    }

    static PacketHeader parseInitialPacket(int version, Number dcid, byte flags,
                                           int posFlags, ByteBuffer buffer)
            throws IOException, DecryptErrorAlert {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-14.1
        // "A server MUST discard an Initial packet that is carried in a UDP datagram with a payload
        // that is smaller than the smallest allowedmaximum datagram size of 1200 bytes."
        if (buffer.limit() < 1200) {
            return null;
        }

        if (PacketParser.invalidFixBit(flags)) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-27#section-17.2
            // https://tools.ietf.org/html/draft-ietf-quic-transport-27#section-17.3
            // "Fixed Bit:  The next bit (0x40) of byte 0 is set to 1.  Packets
            // containing a zero value for this bit are not valid packets in this
            // version and MUST be discarded."
            return null;
        }

        Keys keys = ConnectionSecrets.computeRemoteInitialKeys(version, dcid);
        return PacketParser.parseInitialPacketHeader(buffer, dcid, flags, posFlags,
                version, keys, 0);

    }

    static PacketHeader parseInitialPacketHeader(ByteBuffer buffer, Number dcid, byte flags,
                                                 int posFlags, int version, Keys keys,
                                                 long largestPacketNumber) throws IOException, DecryptErrorAlert {
        checkInitialPacketType(version, (flags & 0x30) >> 4);

        Integer scid = scid(buffer);
        if (scid == null) {
            return null;
        }

        parseToken(buffer); // token not used yet

        // "The length of the remainder of the packet (that is, the Packet Number and
        // Payload fields) in bytes"
        int length = VariableLengthInteger.parse(buffer);

        return parsePacketNumberAndPayload(Level.Initial, version, flags, posFlags, buffer,
                length, keys, largestPacketNumber, dcid, scid);

    }


    static PacketHeader parseShortPacketHeader(ByteBuffer buffer, Number dcid, byte flags,
                                               int posFlags, int version, Keys keys,
                                               long largestPacketNumber)
            throws DecryptErrorAlert {

        return parsePacketNumberAndPayload(Level.App, version, flags, posFlags, buffer,
                buffer.limit() - buffer.position(), keys,
                largestPacketNumber, dcid, null);
    }


    private static byte getHandshakePacketType(int version) {
        if (Version.isV2(version)) {
            return (byte) Settings.HANDSHAKE_V2_type;
        } else {
            return (byte) Settings.HANDSHAKE_V1_type;
        }
    }

    private static void checkHandshakePacketType(int version, int type) {
        if (type != getHandshakePacketType(version)) {
            throw new IllegalStateException("Programming error: this method shouldn't " +
                    "have been called if packet is not Initial");
        }
    }

    static Number dcid(Level level, ByteBuffer buffer) {
        return switch (level) {
            case App -> {
                if (buffer.remaining() < Integer.BYTES) {
                    yield null;
                }
                yield buffer.getInt();
            }
            case Initial, Handshake -> {
                int dstConnIdLength = buffer.get();
                // https://tools.ietf.org/html/draft-ietf-quic-transport-27#section-17.2
                // "In QUIC version 1, this value MUST NOT exceed 20.  Endpoints that receive a version 1
                // long header with a value larger than 20 MUST drop the packet."
                if (dstConnIdLength < 0 || dstConnIdLength > 20) {
                    yield null;
                }
                if (buffer.remaining() < dstConnIdLength) {
                    yield null;
                }
                if (dstConnIdLength == Integer.BYTES) {
                    yield buffer.getInt();
                }
                if (dstConnIdLength == Long.BYTES) {
                    yield buffer.getLong();
                }
                yield null;
            }
        };
    }

    static PacketHeader parseHandshakePackageHeader(ByteBuffer buffer, Number dcid, byte flags,
                                                    int posFlags, int version,
                                                    Keys keys, long largestPacketNumber)
            throws IOException, DecryptErrorAlert {
        checkHandshakePacketType(version, (flags & 0x30) >> 4);

        Integer scid = scid(buffer);
        if (scid == null) {
            return null;
        }

        // "The length of the remainder of the packet (that is, the Packet Number and Payload
        // fields) in bytes"
        int length = VariableLengthInteger.parse(buffer);

        return parsePacketNumberAndPayload(Level.Handshake, version, flags, posFlags, buffer,
                length, keys, largestPacketNumber, dcid, scid);

    }

    /**
     * @noinspection ExtractMethodRecommender
     */
    private static PacketHeader parsePacketNumberAndPayload(
            Level level, int version, byte flags, int posFlags, ByteBuffer buffer,
            int remainingLength, Keys keys, long largestPacketNumber, Number dcid, Integer scid)
            throws DecryptErrorAlert {

        if (buffer.remaining() < remainingLength) {
            return null;
        }

        // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.3
        // "When removing packet protection, an endpoint
        // first removes the header protection."

        int startPos = buffer.position();

        try {
            // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.4.2:
            // "The same number of bytes are always sampled, but an allowance needs
            // to be made for the endpoint removing protection, which will not know
            // the length of the Packet Number field. In sampling the packet
            // ciphertext, the Packet Number field is assumed to be 4 bytes long
            // (its maximum possible encoded length)."
            if (buffer.remaining() < 4) {
                return null;
            }
            buffer.position(startPos + 4);

            // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.4.2:
            // "This algorithm samples 16 bytes from the packet ciphertext."
            if (buffer.remaining() < 16) {
                return null;
            }
            byte[] sample = new byte[16];
            buffer.get(sample);


            // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.4.1:
            // "Header protection is applied after packet protection is applied (see
            // Section 5.3).  The ciphertext of the packet is sampled and used as
            // input to an encryption algorithm."
            byte[] mask = createHeaderProtectionMask(sample, keys);


            // "The output of this algorithm is a 5 byte mask which is applied to the
            // protected header fields using exclusive OR.  The least significant
            // bits of the first byte of the packet are masked by the least
            // significant bits of the first mask byte
            byte decryptedFlags;

            if ((flags & 0x80) == 0x80) {
                // Long header: 4 bits masked
                decryptedFlags = (byte) (flags ^ mask[0] & 0x0f);
            } else {
                // Short header: 5 bits masked
                decryptedFlags = (byte) (flags ^ mask[0] & 0x1f);
            }


            Keys updated = null;
            if (level == Level.App) {
                short keyPhaseBit = (short) ((decryptedFlags & 0x04) >> 2);
                if (keys.checkKeyPhase(keyPhaseBit)) {
                    keys = Keys.computeKeyUpdate(version, keys);
                    updated = keys;
                }
            }

            buffer.position(startPos);


            // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.4.1:
            // "pn_length = (packet[0] & 0x03) + 1"
            int protectedPackageNumberLength = (decryptedFlags & 0x03) + 1;

            byte[] protectedPackageNumber = new byte[protectedPackageNumberLength];
            buffer.get(protectedPackageNumber);

            byte[] unprotectedPacketNumber = new byte[protectedPackageNumberLength];
            for (int i = 0; i < protectedPackageNumberLength; i++) {
                // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.4.1:
                // " ...and the packet number is
                // masked with the remaining bytes.  Any unused bytes of mask that might
                // result from a shorter packet number encoding are unused."
                unprotectedPacketNumber[i] = (byte) (protectedPackageNumber[i] ^ mask[1 + i]);
            }
            long packetNumber = VariableLengthInteger.bytesToInt(unprotectedPacketNumber);

            packetNumber = decodePacketNumber(packetNumber, largestPacketNumber,
                    protectedPackageNumberLength * 8);

            // validate the packet number (minAllowed)
            long minAllowedPacketNumber = Math.max(largestPacketNumber - PACKET_NUMBER_OFFSET, 0);
            if (packetNumber < minAllowedPacketNumber) {
                return null; // can happen when decoding failed
            }
            // this is not according to spec [but it is required here]
            long maxAllowedPacketNumber = Math.min(largestPacketNumber + PACKET_NUMBER_OFFSET,
                    Long.MAX_VALUE);
            if (packetNumber > maxAllowedPacketNumber) {
                return null; // can happen when decoding failed
            }


            int currentPosition = buffer.position();
            // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.3
            // "The associated data, A, for the AEAD is the contents of the QUIC
            //   header, starting from the flags byte in either the short or long
            //   header, up to and including the unprotected packet number."
            byte[] frameHeader = new byte[buffer.position() - posFlags];
            buffer.position(posFlags);
            buffer.get(frameHeader);
            frameHeader[0] = decryptedFlags;
            buffer.position(currentPosition);

            // Copy unprotected (decrypted) packet number in frame header, before decrypting payload.
            System.arraycopy(unprotectedPacketNumber, 0, frameHeader, frameHeader.length -
                    (protectedPackageNumberLength), protectedPackageNumberLength);

            // "The input plaintext, P, for the AEAD is the payload of the QUIC
            // packet, as described in [QUIC-TRANSPORT]."
            // "The output ciphertext, C, of the AEAD is transmitted in place of P."
            int encryptedPayloadLength = remainingLength - protectedPackageNumberLength;
            if (encryptedPayloadLength < 1) {
                return null;
            }

            buffer.position(currentPosition + encryptedPayloadLength);
            byte[] frameBytes = decryptPayload(buffer.array(), currentPosition, encryptedPayloadLength,
                    frameHeader, packetNumber, keys);

            return new PacketHeader(level, version, dcid, scid, frameBytes, packetNumber, updated);
        } finally {
            buffer.position(startPos + remainingLength); // prepare for next iteration
        }
    }

    static byte[] createHeaderProtectionMask(byte[] sample, Keys secrets) {
        return PacketService.createHeaderProtectionMask(sample, 4, secrets);
    }


    static long decodePacketNumber(long truncatedPacketNumber, long largestPacketNumber, int bits) {
        // https://www.rfc-editor.org/rfc/rfc9000.html#sample-packet-number-decoding
        // "Figure 47: Sample Packet Number Decoding Algorithm"
        long expectedPacketNumber = largestPacketNumber + 1;
        long pnWindow = 1L << bits;
        long pnHalfWindow = pnWindow / 2;
        long pnMask = -pnWindow;

        long candidatePn = (expectedPacketNumber & pnMask) | truncatedPacketNumber;
        if (candidatePn <= expectedPacketNumber - pnHalfWindow && candidatePn < (1L << 62) - pnWindow) {
            return candidatePn + pnWindow;
        }
        if (candidatePn > expectedPacketNumber + pnHalfWindow && candidatePn >= pnWindow) {
            return candidatePn - pnWindow;
        }

        return candidatePn;
    }

    static byte[] decryptPayload(byte[] input, int inputOffset, int inputLength,
                                 byte[] associatedData, long packetNumber, Keys secrets) throws DecryptErrorAlert {

        ByteBuffer nonceInput = ByteBuffer.allocate(12);
        nonceInput.putInt(0);
        nonceInput.putLong(packetNumber);

        byte[] writeIV = secrets.getWriteIV();
        byte[] nonce = new byte[12];
        int i = 0;
        for (byte b : nonceInput.array())
            nonce[i] = (byte) (b ^ writeIV[i++]);

        return secrets.aeadDecrypt(associatedData, nonce, input, inputOffset, inputLength);
    }
}
