package tech.lp2p.quic;


import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import tech.lp2p.utils.Utils;

public sealed interface FrameReceived permits FrameReceived.AckFrame, FrameReceived.StreamFrame,
        FrameReceived.ConnectionCloseFrame, FrameReceived.CryptoFrame,
        FrameReceived.DataBlockedFrame,
        FrameReceived.HandshakeDoneFrame, FrameReceived.MaxDataFrame,
        FrameReceived.MaxStreamDataFrame, FrameReceived.MaxStreamsFrame,
        FrameReceived.NewConnectionIdFrame,
        FrameReceived.NewTokenFrame, FrameReceived.PaddingFrame, FrameReceived.PathChallengeFrame,
        FrameReceived.PathResponseFrame, FrameReceived.PingFrame, FrameReceived.StreamsBlockedFrame,
        FrameReceived.RetireConnectionIdFrame, FrameReceived.ResetStreamFrame,
        FrameReceived.StopSendingFrame, FrameReceived.StreamDataBlockedFrame {


    @SuppressWarnings("UnusedReturnValue")
    static StreamsBlockedFrame parseStreamsBlockedFrame(byte type, ByteBuffer buffer) throws IOException {
        return new StreamsBlockedFrame(type == 0x16, (int) VariableLengthInteger.parseLong(buffer));
    }

    static ConnectionCloseFrame parseConnectionCloseFrame(byte type, ByteBuffer buffer) throws IOException {

        int triggeringFrameType = 0;
        long errorCode = VariableLengthInteger.parseLong(buffer);
        if (type == 0x1c) {
            triggeringFrameType = VariableLengthInteger.parse(buffer);
        }
        byte[] reasonPhrase = Utils.BYTES_EMPTY;
        int reasonPhraseLength = VariableLengthInteger.parse(buffer);
        if (reasonPhraseLength > 0) {
            reasonPhrase = new byte[reasonPhraseLength];
            buffer.get(reasonPhrase);
        }
        int tlsError = -1;
        if (type == 0x1c && errorCode >= 0x0100 && errorCode < 0x0200) {
            tlsError = (int) (errorCode - 256);
        }

        return new ConnectionCloseFrame(type, triggeringFrameType, reasonPhrase,
                errorCode, tlsError);
    }

    static AckFrame parseAckFrame(byte type, ByteBuffer buffer, int delayScale)
            throws IOException {

        //  If the frame payloadType is 0x03, ACK frames also contain the cumulative count
        //  of QUIC packets with associated ECN marks received on the connection
        //  up until this point.
        if (type == 0x03) {
            Utils.debug("AckFrame of payloadType 0x03 is not yet fully supported");
        }

        // ACK frames are formatted as shown in Figure 25.
        //
        //   ACK Frame {
        //     Type (i) = 0x02..0x03,
        //     Largest Acknowledged (i),
        //     ACK Delay (i),
        //     ACK Range Count (i),
        //     First ACK Range (i),
        //     ACK Range (..) ...,
        //     [ECN Counts (..)],
        //   }


        // A variable length integer is an encoding of 64-bit unsigned
        // integers into between 1 and 9 bytes.

        // A variable-length integer representing the
        // largest packet number the peer is acknowledging; this is usually
        // the largest packet number that the peer has received prior to
        // generating the ACK frame.  Unlike the packet number in the QUIC
        //long or short header, the value in an ACK frame is not truncated.
        long largestAcknowledged = VariableLengthInteger.parseLong(buffer);

        // Parse as long to protect to against buggy peers. Convert to int as MAX_INT is large enough to hold the
        // largest ack delay that makes sense (even with an delay exponent of 0, MAX_INT is approx 2147 seconds, approx. half an hour).
        int ackDelay = ((int) VariableLengthInteger.parseLong(buffer) * delayScale) / 1000;

        int ackBlockCount = (int) VariableLengthInteger.parseLong(buffer);

        long[] acknowledgedRanges = new long[(ackBlockCount + 1) * 2];

        long currentSmallest = largestAcknowledged;
        // The smallest of the first block is the largest - (rangeSize - 1).
        int rangeSize = 1 + VariableLengthInteger.parse(buffer);
        int acknowledgedRangesIndex = 0;
        acknowledgedRanges[acknowledgedRangesIndex] = largestAcknowledged;
        acknowledgedRangesIndex++;
        acknowledgedRanges[acknowledgedRangesIndex] = largestAcknowledged - rangeSize - 1;
        currentSmallest -= rangeSize - 1;

        for (int i = 0; i < ackBlockCount; i++) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-17#section-19.3.1:
            // "Each Gap indicates a range of packets that are not being
            //   acknowledged.  The number of packets in the gap is one higher than
            //   the encoded value of the Gap Field."
            int gapSize = VariableLengthInteger.parse(buffer) + 1;
            // https://tools.ietf.org/html/draft-ietf-quic-transport-17#section-19.3.1:
            // "Each ACK Block acknowledges a contiguous range of packets by
            //   indicating the number of acknowledged packets that precede the
            //   largest packet number in that block.  A value of zero indicates that
            //   only the largest packet number is acknowledged."
            int contiguousPacketsPreceding = VariableLengthInteger.parse(buffer) + 1;
            // The largest of the next range is the current smallest - (gap size + 1), because the gap size counts the
            // ones not being present, and we need the first (below) being present.
            // The new current smallest is largest of the next range - (range size - 1)
            //                             == current smallest - (gap size + 1) - (range size - 1)
            //                             == current smallest - gap size - range size
            long largestOfRange = currentSmallest - gapSize - 1;

            acknowledgedRangesIndex++;
            acknowledgedRanges[acknowledgedRangesIndex] = largestOfRange;

            acknowledgedRangesIndex++;
            acknowledgedRanges[acknowledgedRangesIndex] = largestOfRange - contiguousPacketsPreceding + 1;

            currentSmallest -= (gapSize + contiguousPacketsPreceding);
        }

        // ECN Counts {
        //     ECT0 Count (i),
        //     ECT1 Count (i),
        //     ECN-CE Count (i),
        //   }
        //
        //                        Figure 27: ECN Count Format
        //
        //   The three ECN Counts are:
        //
        //   ECT0 Count:  A variable-length integer representing the total number
        //      of packets received with the ECT(0) codepoint in the packet number
        //      space of the ACK frame.
        //
        //   ECT1 Count:  A variable-length integer representing the total number
        //      of packets received with the ECT(1) codepoint in the packet number
        //      space of the ACK frame.
        //
        //   CE Count:  A variable-length integer representing the total number of
        //      packets received with the CE codepoint in the packet number space
        //      of the ACK frame.
        //
        //   ECN counts are maintained separately for each packet number space.

        return new AckFrame(acknowledgedRanges, largestAcknowledged, ackDelay);
    }

    static CryptoFrame parseCryptoFrame(ByteBuffer buffer) throws IOException {

        long offset = VariableLengthInteger.parseLong(buffer);
        int length = VariableLengthInteger.parse(buffer);

        byte[] cryptoData = new byte[length];
        buffer.get(cryptoData);

        return new CryptoFrame(offset, cryptoData, length);
    }

    @SuppressWarnings("UnusedReturnValue")
    static DataBlockedFrame parseDataBlockedFrame(ByteBuffer buffer) throws IOException {
        return new DataBlockedFrame(VariableLengthInteger.parseLong(buffer));
    }

    static MaxDataFrame parseMaxDataFrame(ByteBuffer buffer) throws IOException {

        return new MaxDataFrame(VariableLengthInteger.parseLong(buffer));
    }

    static MaxStreamDataFrame parseMaxStreamDataFrame(ByteBuffer buffer) throws IOException {

        return new MaxStreamDataFrame(
                (int) VariableLengthInteger.parseLong(buffer),
                VariableLengthInteger.parseLong(buffer));
    }

    static MaxStreamsFrame parseMaxStreamsFrame(byte type, ByteBuffer buffer) throws IOException {

        return new MaxStreamsFrame(VariableLengthInteger.parseLong(buffer),
                type == 0x12);
    }

    static NewConnectionIdFrame parseNewConnectionIdFrame(ByteBuffer buffer) throws IOException {

        int sequenceNr = VariableLengthInteger.parse(buffer);
        int retirePriorTo = VariableLengthInteger.parse(buffer);
        int length = buffer.get();
        if (length != Integer.BYTES) {
            throw new IOException("not supported length of connection id");
        }
        int connectionId = buffer.getInt();

        byte[] statelessResetToken = new byte[128 / 8];
        buffer.get(statelessResetToken);

        return new NewConnectionIdFrame(sequenceNr, retirePriorTo,
                connectionId, statelessResetToken);
    }


    static NewTokenFrame parseNewTokenFrame(ByteBuffer buffer) throws IOException {

        int tokenLength = VariableLengthInteger.parse(buffer);
        byte[] newToken = new byte[tokenLength];
        buffer.get(newToken);

        return new NewTokenFrame(newToken);
    }

    /**
     * Strictly speaking, a padding frame consists of one single byte. For convenience,
     * here all subsequent padding bytes are collected in one padding object.
     */
    @SuppressWarnings("UnusedReturnValue")
    static PaddingFrame parsePaddingFrame(ByteBuffer buffer) {
        int length = 0;
        byte lastByte = 0;
        while (buffer.position() < buffer.limit() && (lastByte = buffer.get()) == 0)
            length++;

        if (lastByte != 0) {
            // Set back one position
            buffer.position(buffer.position() - 1);
        }

        return new PaddingFrame(length);
    }

    static PathChallengeFrame parsePathChallengeFrame(ByteBuffer buffer) {
        byte[] data = new byte[8];
        buffer.get(data);
        return new PathChallengeFrame(data);
    }

    @SuppressWarnings("UnusedReturnValue")
    static PathResponseFrame parsePathResponseFrame(ByteBuffer buffer) {
        byte[] data = new byte[8];
        buffer.get(data);
        return new PathResponseFrame(data);
    }


    static ResetStreamFrame parseResetStreamFrame(ByteBuffer buffer) throws IOException {

        int streamId = VariableLengthInteger.parse(buffer);
        long errorCode = VariableLengthInteger.parseLong(buffer);
        long finalSize = VariableLengthInteger.parseLong(buffer);
        return new ResetStreamFrame(streamId, errorCode, finalSize);
    }

    static RetireConnectionIdFrame parseRetireConnectionIdFrame(ByteBuffer buffer) throws IOException {

        int sequenceNr = VariableLengthInteger.parse(buffer);
        return new RetireConnectionIdFrame(sequenceNr);
    }

    static StopSendingFrame parseStopSendingFrame(ByteBuffer buffer) throws IOException {

        int streamId = VariableLengthInteger.parse(buffer);
        long errorCode = VariableLengthInteger.parseLong(buffer);

        return new StopSendingFrame(streamId, errorCode);
    }

    @SuppressWarnings("UnusedReturnValue")
    static StreamDataBlockedFrame parseStreamDataBlockedFrame(ByteBuffer buffer) throws IOException {

        int streamId = VariableLengthInteger.parse(buffer);
        long streamDataLimit = VariableLengthInteger.parseLong(buffer);

        return new StreamDataBlockedFrame(streamId, streamDataLimit);
    }

    static StreamFrame parseStreamFrame(byte type, ByteBuffer buffer) throws IOException {

        boolean withOffset = ((type & 0x04) == 0x04);
        boolean withLength = ((type & 0x02) == 0x02);
        boolean isFinal = ((type & 0x01) == 0x01);

        int streamId = VariableLengthInteger.parse(buffer);

        long offset = 0;
        if (withOffset) {
            offset = VariableLengthInteger.parseLong(buffer);
        }
        int length;
        if (withLength) {
            length = VariableLengthInteger.parse(buffer);
        } else {
            length = buffer.limit() - buffer.position();
        }

        byte[] streamData = new byte[length];
        buffer.get(streamData);

        return new StreamFrame(streamId, isFinal, offset, length, streamData);
    }

    static HandshakeDoneFrame parseHandshakeDoneFrame() {
        return new HandshakeDoneFrame();
    }

    /**
     * Represents a connection close frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-connection_close-frames">...</a>
     */
    record ConnectionCloseFrame(int frameType, int triggeringFrameType,
                                byte[] reasonPhrase,
                                long errorCode, int tlsError) implements FrameReceived {


        public boolean hasTransportError() {
            return frameType == 0x1c && errorCode != 0;
        }

        public boolean hasTlsError() {
            return tlsError != -1;
        }

        public long getTlsError() {
            if (hasTlsError()) {
                return tlsError;
            } else {
                throw new IllegalStateException("Close does not have a TLS error");
            }
        }

        public long getErrorCode() {
            return errorCode;
        }

        public boolean hasReasonPhrase() {
            return reasonPhrase != null;
        }

        public String getReasonPhrase() {
            return new String(reasonPhrase, StandardCharsets.UTF_8);
        }

        public boolean hasApplicationProtocolError() {
            return frameType == 0x1d && errorCode != 0;
        }

        public boolean hasError() {
            return hasTransportError() || hasApplicationProtocolError();
        }


        @Override
        public String toString() {
            return "ConnectionCloseFrame["
                    + (hasTlsError() ? "TLS " + tlsError : errorCode) + "|"
                    + triggeringFrameType + "|"
                    + (reasonPhrase != null ? new String(reasonPhrase) : "-") + "]";
        }

    }

    // https://www.rfc-editor.org/rfc/rfc9000.html#name-transport-parameter-definit
    // "...  a default value of 3 is assumed (indicating a multiplier of 8)."
    record AckFrame(long[] acknowledgedRanges, long largestAcknowledged,
                    int ackDelay) implements FrameReceived {

    }

    /**
     * Represents a crypto frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-crypto-frames">...</a>
     */
    record CryptoFrame(long offset, byte[] payload, int length)
            implements FrameReceived, Comparable<FrameReceived.CryptoFrame> {


        public long offsetLength() {
            return offset + length;
        }

        @Override
        public int compareTo(CryptoFrame other) {
            if (this.offset == other.offset()) {
                return Long.compare(this.length, other.length());
            } else {
                return Long.compare(this.offset, other.offset());
            }
        }

    }

    /**
     * Represents a data blocked frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-data_blocked-frames">...</a>
     */
    record DataBlockedFrame(long streamDataLimit) implements FrameReceived {

    }

    /**
     * <a href="https://tools.ietf.org/html/draft-ietf-quic-transport-25#section-19.20">...</a>
     */
    record HandshakeDoneFrame() implements FrameReceived {

    }

    // https://tools.ietf.org/html/draft-ietf-quic-transport-20#section-19.9
    record MaxDataFrame(long maxData) implements FrameReceived {
    }

    /**
     * Represents a max stream data frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-max_stream_data-frames">...</a>
     */
    record MaxStreamDataFrame(int streamId, long maxData) implements FrameReceived {

    }

    /**
     * Represents a max streams frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-max_streams-frames">...</a>
     */
    record MaxStreamsFrame(long maxStreams,
                           boolean appliesToBidirectional) implements FrameReceived {

    }

    /**
     * Represents a new connection id frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-new_connection_id-frames">...</a>
     */
    record NewConnectionIdFrame(int sequenceNr, int retirePriorTo, Number connectionId,
                                byte[] statelessResetToken) implements FrameReceived {

    }

    /**
     * Represents a new token frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-new_token-frames">...</a>
     */
    record NewTokenFrame(byte[] token) implements FrameReceived {

    }

    /**
     * Represents a number of consecutive padding frames.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-padding-frames">...</a>
     * <p>
     * Usually, padding will consist of multiple padding frames, each being exactly one (zero) byte. This class can
     * represent an arbitrary number of consecutive padding frames, by recording padding length.
     */
    record PaddingFrame(int length) implements FrameReceived {

    }

    /**
     * Represents a path challenge frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-path_challenge-frames">...</a>
     */
    record PathChallengeFrame(byte[] data) implements FrameReceived {

    }

    /**
     * Represents a path response frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-path_response-frames">...</a>
     */
    record PathResponseFrame(byte[] data) implements FrameReceived {

    }

    /**
     * Represents a ping frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-ping-frames">...</a>
     */
    @SuppressWarnings("unused")
    record PingFrame() implements FrameReceived {
    }

    /**
     * Represents a reset stream frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-reset_stream-frames">...</a>
     */
    record ResetStreamFrame(int streamId, long errorCode, long finalSize) implements FrameReceived {

    }

    /**
     * Represents a retire connection id frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-retire_connection_id-frames">...</a>
     */
    record RetireConnectionIdFrame(int sequenceNumber) implements FrameReceived {

    }

    /**
     * Represents a stop sending frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-stop_sending-frames">...</a>
     */
    record StopSendingFrame(int streamId, long errorCode) implements FrameReceived {

    }

    /**
     * Represents a stream data blocked frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-stream_data_blocked-frames">...</a>
     */
    record StreamDataBlockedFrame(int streamId, long streamDataLimit) implements FrameReceived {

    }

    record StreamFrame(int streamId, boolean isFinal, long offset, int length, byte[] streamData)
            implements FrameReceived, Comparable<FrameReceived.StreamFrame> {

        @Override
        public int compareTo(FrameReceived.StreamFrame other) {
            if (this.offset == other.offset()) {
                return Long.compare(this.length, other.length());
            } else {
                return Long.compare(this.offset, other.offset());
            }
        }


        public long offsetLength() {
            return offset + length;
        }


    }

    /**
     * Represents a streams blocked frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-streams_blocked-frames">...</a>
     */
    record StreamsBlockedFrame(boolean bidirectional, int streamLimit) implements FrameReceived {
    }

}

