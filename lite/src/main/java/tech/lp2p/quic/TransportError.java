package tech.lp2p.quic;


import tech.lp2p.core.Status;

// https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-20.1
public final class TransportError extends Exception {

    private final long errorCode;

    public TransportError(Code errorCode) {
        super(errorCode.name());
        this.errorCode = errorCode.value();
        if (this.errorCode != 0) {
            System.err.println("TransportError " + this);
        }
    }

    public TransportError(Status status) {
        super(status.name());
        this.errorCode = status.code();
        if (errorCode != 0) {
            System.err.println("TransportError " + this);
        }
    }

    public String transportErrorMessage() {
        return getMessage();
    }

    public long errorCode() {
        return errorCode;
    }

    @Override

    public String toString() {
        return errorCode + " " + getMessage();
    }


    // https://www.rfc-editor.org/rfc/rfc9000.html#name-transport-error-codes
    @SuppressWarnings("unused")
    public enum Code {
        NO_ERROR(0x0),
        INTERNAL_ERROR(0x1),

        CONNECTION_REFUSED(0x2),

        FLOW_CONTROL_ERROR(0x3),
        STREAM_LIMIT_ERROR(0x4),
        STREAM_STATE_ERROR(0x5),

        FINAL_SIZE_ERROR(0x6),
        FRAME_ENCODING_ERROR(0x7),
        TRANSPORT_PARAMETER_ERROR(0x8),
        CONNECTION_ID_LIMIT_ERROR(0x9),
        PROTOCOL_VIOLATION(0xa),

        INVALID_TOKEN(0xb),

        APPLICATION_ERROR(0xc),

        CRYPTO_BUFFER_EXCEEDED(0xd),

        KEY_UPDATE_ERROR(0xe),

        AEAD_LIMIT_REACHED(0xf),

        NO_VIABLE_PATH(0x10),

        CRYPTO_ERROR(0x100),
        // https://www.ietf.org/archive/id/draft-ietf-quic-version-negotiation-08.html#iana-error
        VERSION_NEGOTIATION_ERROR(0x53F8);   // !! When this document is approved, it will request permanent allocation of a codepoint in the 0-63 range to replace the provisional codepoint described above.


        private final short value;

        Code(int value) {
            this.value = (short) value;
        }

        public short value() {
            return value;
        }
    }
}
