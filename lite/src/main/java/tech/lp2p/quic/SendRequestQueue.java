package tech.lp2p.quic;


import java.util.Deque;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.function.Function;


final class SendRequestQueue {
    private final Deque<SendRequest> requestQueue = new ConcurrentLinkedDeque<>();

    void addRequest(Frame fixedFrame) {
        requestQueue.addLast(new SendRequest(fixedFrame.frameLength(),
                actualMaxSize -> fixedFrame));
    }

    void insertRequest(Frame fixedFrame) {
        requestQueue.addFirst(new SendRequest(fixedFrame.frameLength(),
                actualMaxSize -> fixedFrame));
    }

    /**
     * @param estimatedSize The minimum size of the frame that the supplier can produce. When the supplier is
     *                      requested to produce a frame of that size, it must return a frame of the size or smaller.
     *                      This leaves room for the caller to handle uncertainty of how large the frame will be,
     *                      for example due to a var-length int value that may be larger at the moment the frame
     */
    void addRequest(Function<Integer, Frame> frameSupplier, int estimatedSize) {
        requestQueue.addLast(new SendRequest(estimatedSize, frameSupplier));
    }

    boolean hasRequests() {
        return !requestQueue.isEmpty();
    }


    public SendRequest next(int maxFrameLength) {
        if (maxFrameLength < 1) {  // Minimum frame size is 1: some frames (e.g. ping) are just a payloadType field.
            // Forget it
            return null;
        }

        for (Iterator<SendRequest> iterator = requestQueue.iterator(); iterator.hasNext(); ) {
            SendRequest next = iterator.next();
            if (next.estimatedSize() <= maxFrameLength) {
                iterator.remove();
                return next;
            }
        }
        // Couldn't find one.
        return null;

    }

    public void clear() {
        requestQueue.clear();
    }
}

