package tech.lp2p.core;

import java.net.UnknownServiceException;


public record Protocol(String name, boolean enabled) {

    // Libp2p protocols
    public static final Protocol MULTISTREAM_PROTOCOL = new Protocol("/multistream/1.0.0",
            true);
    public static final Protocol DHT_PROTOCOL = new Protocol("/ipfs/kad/1.0.0",
            false);
    public static final Protocol IDENTITY_PROTOCOL = new Protocol("/ipfs/id/1.0.0",
            true); // signed IDs
    public static final Protocol RELAY_PROTOCOL_HOP = new Protocol("/libp2p/circuit/relay/0.2.0/hop",
            true);
    public static final Protocol RELAY_PROTOCOL_STOP = new Protocol("/libp2p/circuit/relay/0.2.0/stop",
            true);
    public static final Protocol IDENTITY_PUSH_PROTOCOL = new Protocol("/ipfs/id/push/1.0.0",
            false); // signed IDs

    public static Protocol name(String protocol) throws UnknownServiceException {

        return switch (protocol) {
            case "/multistream/1.0.0" -> MULTISTREAM_PROTOCOL;
            case "/ipfs/kad/1.0.0" -> DHT_PROTOCOL;
            case "/ipfs/id/1.0.0" -> IDENTITY_PROTOCOL;
            case "/ipfs/id/push/1.0.0" -> IDENTITY_PUSH_PROTOCOL;
            case "/libp2p/circuit/relay/0.2.0/hop" -> RELAY_PROTOCOL_HOP;
            case "/libp2p/circuit/relay/0.2.0/stop" -> RELAY_PROTOCOL_STOP;
            default -> throw new UnknownServiceException("unhandled protocol " + protocol);
        };


    }

}
