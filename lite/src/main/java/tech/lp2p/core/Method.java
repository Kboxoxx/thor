package tech.lp2p.core;

@SuppressWarnings("unused")
public enum Method {
    GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE
}
