package tech.lp2p.core;


import com.google.protobuf.CodedInputStream;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.Objects;


public record Peeraddr(PeerId peerId, byte[] address, int port) implements Serializable {


    public static Peeraddr loopbackPeeraddr(PeerId peerId, int port) {
        InetAddress inetAddress = InetAddress.getLoopbackAddress();
        return Peeraddr.create(peerId, inetAddress.getAddress(), port);
    }


    public static Peeraddr create(PeerId peerId, byte[] raw) {
        return Objects.requireNonNull(Multiaddr.parseAddress(
                CodedInputStream.newInstance(raw), peerId), "Not supported peeraddr");
    }

    public static Peeraddr create(PeerId peerId, InetSocketAddress inetSocketAddress) {
        InetAddress inetAddress = inetSocketAddress.getAddress();
        return create(peerId, inetAddress.getAddress(), inetSocketAddress.getPort());
    }


    public static Peeraddr create(PeerId peerId, byte[] address, int port) {
        return new Peeraddr(peerId, address, port);
    }

    public byte[] encoded() {

        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            InetAddress inetAddress = InetAddress.getByAddress(address);
            if (inetAddress instanceof Inet4Address) {
                Multiaddr.encodeProtocol(Multiaddr.IP4, byteArrayOutputStream);
            } else {
                Multiaddr.encodeProtocol(Multiaddr.IP6, byteArrayOutputStream);
            }
            Multiaddr.encodePart(address, byteArrayOutputStream);
            Multiaddr.encodeProtocol(Multiaddr.UDP, byteArrayOutputStream);
            Multiaddr.encodePart(port, byteArrayOutputStream);
            Multiaddr.encodeProtocol(Multiaddr.QUICV1, byteArrayOutputStream);

            return byteArrayOutputStream.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Peeraddr peeraddr = (Peeraddr) o;
        return port == peeraddr.port && Objects.equals(peerId, peeraddr.peerId)
                && Arrays.equals(address, peeraddr.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(peerId, Arrays.hashCode(address), port);
    }

    public String hostName() {
        try {
            return InetAddress.getByAddress(address).getHostName();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    public String authority() {
        return hostName() + ":" + port;
    }

}
