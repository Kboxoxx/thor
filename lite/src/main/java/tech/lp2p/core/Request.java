package tech.lp2p.core;


import java.time.Duration;
import java.util.Objects;

import tech.lp2p.http3.HttpHeaders;
import tech.lp2p.utils.Utils;

public record Request(Peeraddr peeraddr, String path, Scheme scheme,
                      Method method, Headers headers, Duration timeout,
                      boolean keepAlive, int maxResponseSize, byte[] body) {

    public static Request create(Peeraddr peeraddr, String path, Method method,
                                 Headers headers, Duration timeout, boolean keepAlive,
                                 int maxResponseSize, byte[] body) {
        Objects.requireNonNull(peeraddr);
        Objects.requireNonNull(path);
        Objects.requireNonNull(method);
        Objects.requireNonNull(headers);
        Objects.requireNonNull(timeout);
        Objects.requireNonNull(body);
        Utils.checkTrue(maxResponseSize > 0, "Invalid response size");

        return new Request(peeraddr, path, Scheme.https,
                method, headers, timeout, keepAlive, maxResponseSize, body);
    }

    public static Request get(Peeraddr peeraddr, String path, Duration timeout,
                              boolean keepAlive, int maxResponseSize) {
        return create(peeraddr, path, Method.GET, createHeaders(), timeout, keepAlive,
                maxResponseSize, Utils.BYTES_EMPTY);
    }

    public static Request post(Peeraddr peeraddr, String path, Duration timeout,
                               boolean keepAlive, int maxResponseSize) {
        return create(peeraddr, path, Method.POST, createHeaders(), timeout, keepAlive,
                maxResponseSize, Utils.BYTES_EMPTY);
    }

    public static Request post(Peeraddr peeraddr, String path, Duration timeout, boolean keepAlive,
                               int maxResponseSize, byte[] body) {
        return create(peeraddr, path, Method.POST, createHeaders(), timeout, keepAlive,
                maxResponseSize, body);
    }

    public static Headers createHeaders() {
        return new HttpHeaders();
    }
}
