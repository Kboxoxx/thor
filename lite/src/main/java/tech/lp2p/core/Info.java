package tech.lp2p.core;


public sealed interface Info permits Raw, Fid, Dir {
    long size();

    Cid cid();

    String name();

    default boolean isDir() {
        return this instanceof Dir;
    }

    default boolean isRaw() {
        return this instanceof Raw;
    }

    default boolean isFid() {
        return this instanceof Fid;
    }
}
