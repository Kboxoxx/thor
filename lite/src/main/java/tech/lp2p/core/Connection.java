package tech.lp2p.core;

import java.net.InetSocketAddress;

public interface Connection {

    ALPN alpn();

    void close();

    InetSocketAddress remoteAddress();

    boolean isConnected();

    Response send(Request request);
}
