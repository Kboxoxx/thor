package tech.lp2p.core;


import java.util.Objects;

import tech.lp2p.Lite;
import tech.lp2p.quic.Stream;

public record Responder(Protocols protocols) {


    public static long delimiter() {
        return 2 * Lite.CHUNK_DATA;
    }

    public void protocol(Stream stream, Protocol protocol) {
        Handler handler = protocols.get(protocol);
        if (handler != null) {
            handler.protocol(stream);
        } else {
            stream.resetStream(Status.PROTOCOL_NEGOTIATION_FAILED);
        }
    }

    public void data(Stream stream, Protocol protocol, byte[] data) throws Exception {
        Objects.requireNonNull(protocol, "data unknown protocol");
        Handler handler = protocols.get(protocol);
        if (handler != null) {
            handler.data(stream, data);
        } else {
            stream.resetStream(Status.PROTOCOL_NEGOTIATION_FAILED);
        }

    }

    public void fin(Stream stream, Protocol protocol) {
        Objects.requireNonNull(protocol, "data unknown protocol");
        Handler handler = protocols.get(protocol);
        if (handler != null) {
            handler.fin(stream);
        } else {
            stream.resetStream(Status.PROTOCOL_NEGOTIATION_FAILED); // not sure if necessary
        }
    }

    public void terminated(Stream stream, Protocol protocol) {
        Objects.requireNonNull(protocol, "data unknown protocol");
        Handler handler = protocols.get(protocol);
        if (handler != null) {
            handler.terminated(stream);
        } else {
            stream.resetStream(Status.PROTOCOL_NEGOTIATION_FAILED); // not sure if necessary
        }

    }
}


