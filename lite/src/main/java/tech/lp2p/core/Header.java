package tech.lp2p.core;

@SuppressWarnings("unused")
public record Header(String name) {
    public static final Header USER_AGENT = new Header("User-Agent");
    public static final Header CONTENT_TYPE = new Header("Content-Type");
    public static final Header CONTENT_LENGTH = new Header("Content-Length");
}
