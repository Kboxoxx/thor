package tech.lp2p.core;

public record Raw(Cid cid, long size, String name) implements Info {
}
