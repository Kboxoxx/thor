package tech.lp2p.core;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

import tech.lp2p.utils.Base58;
import tech.lp2p.utils.Utils;


public final class FileStore implements BlockStore, AutoCloseable {

    private final File directory;

    public FileStore(File directory) {
        this.directory = directory;
        Utils.checkTrue(directory.exists(), "FileStore directory does not exists.");
    }

    public FileStore() throws IOException {
        directory = Files.createTempDirectory("").toFile();
    }

    public static File getTempDirectory() throws IOException {
        String property = System.getProperty("java.io.tmpdir");
        File temp = new File(property);
        if (!temp.exists()) {
            throw new IOException("Tmpdir does not exists");
        }
        return temp;
    }

    public static void deleteDirectory(File dir) {
        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    deleteDirectory(file);
                } else {
                    boolean result = file.delete();
                    if (Utils.DEBUG) {
                        Utils.debug(file.getName() + " deleting " + result);
                    }
                }
            }
        }
    }

    @Override
    public void close() {
        deleteDirectory(directory);
        boolean result = directory.delete();
        if (Utils.DEBUG) {
            Utils.debug(directory.getName() + " deleting " + result);
        }
    }

    @Override
    public boolean hasBlock(Hash hash) {
        return getFile(hash).exists();
    }


    @Override
    public byte[] getBlock(Hash hash) {
        try {
            File file = getFile(hash);
            if (file.exists()) {
                try (FileInputStream fileInputStream = new FileInputStream(file)) {
                    return fileInputStream.readAllBytes();
                }
            }
        } catch (Throwable throwable) {
            Utils.error(throwable);
        }
        return null;
    }

    @Override
    public void storeBlock(Hash hash, byte[] data) {
        try {
            writeFile(hash, data);
        } catch (Throwable throwable) {
            Utils.error(throwable);
        }
    }

    private File getFile(Hash hash) {
        return new File(directory, Base58.encode(hash.hash()));
    }

    private void writeFile(Hash hash, byte[] data) throws IOException {
        File file = getFile(hash);
        if (!file.exists()) {
            boolean result = file.createNewFile();
            if (Utils.DEBUG) {
                Utils.debug(file.getName() + " create " + result);
            }
        }
        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fileOutputStream.write(data);
        }
    }

    @Override
    public void deleteBlock(Hash hash) {
        File file = getFile(hash);
        if (file.exists()) {
            boolean result = file.delete();
            if (Utils.DEBUG) {
                Utils.debug(file.getName() + " deleting " + result);
            }
        }
    }
}
