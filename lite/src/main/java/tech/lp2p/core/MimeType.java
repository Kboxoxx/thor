package tech.lp2p.core;

@SuppressWarnings("unused")
public record MimeType(String name) {
    public static final MimeType PDF_MIME_TYPE = new MimeType("application/pdf");
    public static final MimeType OCTET_MIME_TYPE = new MimeType("application/octet-stream");
    public static final MimeType JSON_MIME_TYPE = new MimeType("application/json");
    public static final MimeType PLAIN_MIME_TYPE = new MimeType("text/plain");
    public static final MimeType TORRENT_MIME_TYPE = new MimeType("application/x-bittorrent");
    public static final MimeType CSV_MIME_TYPE = new MimeType("text/csv");
    public static final MimeType PGP_KEYS_MIME_TYPE = new MimeType("application/pgp-keys");
    public static final MimeType EXCEL_MIME_TYPE = new MimeType("application/msexcel");
    public static final MimeType OPEN_EXCEL_MIME_TYPE = new MimeType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    public static final MimeType WORD_MIME_TYPE = new MimeType("application/msword");
    public static final MimeType DIR_MIME_TYPE = new MimeType("vnd.android.document/directory");
    public static final MimeType MHT_MIME_TYPE = new MimeType("multipart/related");
    public static final MimeType HTML_MIME_TYPE = new MimeType("text/html");

    // general
    public static final MimeType AUDIO = new MimeType("audio");
    public static final MimeType VIDEO = new MimeType("video");
    public static final MimeType TEXT = new MimeType("text");
    public static final MimeType APPLICATION = new MimeType("application");
    public static final MimeType IMAGE = new MimeType("image");
    public static final MimeType ALL = new MimeType("*/*");
}
