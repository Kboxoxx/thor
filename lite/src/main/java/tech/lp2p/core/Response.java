package tech.lp2p.core;


import java.util.Objects;

import tech.lp2p.http3.HttpHeaders;
import tech.lp2p.utils.Utils;

public interface Response {

    static Response create(Status status, Headers headers, byte[] body) {
        Objects.requireNonNull(status);
        Objects.requireNonNull(headers);
        Objects.requireNonNull(body);
        return new Default(status, headers, body);
    }

    static Response create(Status status, Headers headers) {
        return create(status, headers, Utils.BYTES_EMPTY);
    }

    static Response create(Status status, byte[] payload) {
        return create(status, HttpHeaders.NO_HEADERS, payload);
    }

    static Response create(Status status) {
        return create(status, HttpHeaders.NO_HEADERS, Utils.BYTES_EMPTY);
    }

    static Headers createHeaders() {
        return new HttpHeaders();
    }

    byte[] body();

    Status status();

    Headers headers();

    record Default(Status status, Headers headers, byte[] body) implements Response {

    }
}
