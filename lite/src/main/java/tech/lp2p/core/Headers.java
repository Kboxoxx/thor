package tech.lp2p.core;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalLong;

public interface Headers {
    Map<String, List<String>> map();

    void add(String name, String value);

    OptionalLong firstValueAsLong(String name);

    Optional<String> firstValue(String name);
}
