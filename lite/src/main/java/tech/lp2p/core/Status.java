package tech.lp2p.core;

import java.util.HashMap;
import java.util.Map;

import tech.lp2p.utils.Utils;

@SuppressWarnings("unused")
public enum Status {

    CLOSE(0x0),
    UNKNOWN(9999),

    PROTOCOL_NEGOTIATION_FAILED(100),
    GATED(101),
    INTERNAL_ERROR(104),
    LIMIT_REACHED(105),
    STREAM_CLOSED(106),
    PROTOCOL_DATA_SIZE_VIOLATION(107),
    TIMEOUT(108),
    INTERRUPT(108),

    // HTML codes

    OK(200),
    BAD_REQUEST(400),
    NOT_FOUND(404),
    METHOD_NOT_ALLOWED(405),
    REQUEST_TIMEOUT(408),
    URI_TOO_LONG(414),
    CLIENT_CLOSED_REQUEST(499),
    SERVER_INTERNAL_ERROR(500),
    SERVICE_UNAVAILABLE(503),
    NOT_IMPLEMENTED(501),

    // An HTTP message was malformed and cannot be processed.
    H3_MESSAGE_ERROR(0x010e),
    // A frame that fails to satisfy layout requirements or with an invalid size was received.
    H3_FRAME_ERROR(0x0106),
    // "The endpoint detected that its peer created a stream that it will not accept."
    H3_STREAM_CREATION_ERROR(0x0103),
    // A stream required by the HTTP/3 connection was closed or reset.
    H3_CLOSED_CRITICAL_STREAM(0x0104),
    // No SETTINGS frame was received at the beginning of the control stream.
    H3_MISSING_SETTINGS(0x010a);
    private static final Map<Integer, Status> byCode = new HashMap<>();

    static {
        for (Status t : Status.values()) {
            byCode.put(t.code, t);
        }
    }

    private final int code;


    Status(int code) {
        this.code = code;
    }

    public static Status get(int value) {
        Status status = byCode.get(value);
        if (status == null) {
            if (Utils.DEBUG) {
                Utils.debug("Status not known  " + value);
            }
            status = Status.UNKNOWN;
        }
        return status;
    }

    public int code() {
        return code;
    }

}
