package tech.lp2p.core;

import com.google.protobuf.CodedInputStream;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;

import tech.lp2p.utils.Utils;

// CID is always of version 1, Multicodec.DAG_PB and Multihash.SHA2_256
public record Cid(byte[] hash) implements Hash, Serializable {


    public static Cid createCid(byte[] hash) {
        Utils.checkTrue(hash.length == 32, "Invalid hash length");
        return new Cid(hash);
    }


    static Cid parse(byte[] data) throws Exception {

        CodedInputStream codedInputStream = CodedInputStream.newInstance(data);
        int version = codedInputStream.readRawVarint32(); // skip version, because it is 1
        Utils.checkArgument(version, 1, "cid version 1 is only supported");

        int codecType = codedInputStream.readRawVarint32();
        Utils.checkArgument(codecType, Multicodec.DAG_PB, "invalid cid codec type");

        int type = codedInputStream.readRawVarint32();
        Utils.checkArgument(type, Multihash.SHA2_256, "invalid cid hash type");

        int len = codedInputStream.readRawVarint32();
        Utils.checkArgument(len, 32, "invalid cid length");

        byte[] hash = codedInputStream.readRawBytes(len);
        Utils.checkTrue(codedInputStream.isAtEnd(), "still data available");

        return new Cid(hash);

    }

    /**
     * Decode a string representation of a Cid object
     *
     * @param name Encoded cid as string
     * @return decoded Cid object
     * @throws Exception in case of invalid encoded cid
     */

    public static Cid decodeCid(String name) throws Exception {
        if (name.length() < 2) {
            throw new IllegalStateException("invalid cid");
        }
        byte[] raw = Multibase.decode(name);
        return Cid.parse(raw);
    }


    public static Cid toCid(byte[] data) {
        Objects.requireNonNull(data, "data can not be null");
        Utils.checkArgument(data.length, 32, "data size must be 32");
        return new Cid(data);
    }

    public static byte[] toArray(Cid cid) {
        Objects.requireNonNull(cid, "cid can not be null");
        return cid.hash();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cid cid = (Cid) o;
        return Arrays.equals(hash, cid.hash);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(hash); // ok, checked, maybe opt
    }


    public String toBase32() {
        return Multibase.encode(Multibase.BASE32, encoded());
    }

    private byte[] encoded() {

        int versionLength = Utils.unsignedVariantSize(1);
        int codecLength = Utils.unsignedVariantSize(Multicodec.DAG_PB);
        byte[] multihash = multihash();

        ByteBuffer buffer = ByteBuffer.allocate(
                versionLength + codecLength + multihash.length);
        Utils.writeUnsignedVariant(buffer, 1);
        Utils.writeUnsignedVariant(buffer, Multicodec.DAG_PB);
        buffer.put(multihash);
        return buffer.array();

    }

    public Key createKey() {
        return Key.convertKey(hash());
    }

    public byte[] multihash() {
        Utils.checkArgument(hash.length, 32, "invalid cid");

        int typeLength = Utils.unsignedVariantSize(Multihash.SHA2_256);
        int hashLength = Utils.unsignedVariantSize(hash.length);

        ByteBuffer buffer = ByteBuffer.allocate(typeLength + hashLength + hash.length);
        Utils.writeUnsignedVariant(buffer, Multihash.SHA2_256);
        Utils.writeUnsignedVariant(buffer, hash.length);
        buffer.put(hash);
        return buffer.array();
    }
}