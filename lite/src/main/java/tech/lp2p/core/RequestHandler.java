package tech.lp2p.core;


public interface RequestHandler {

    Response handleRequest(Request request);
}
