package tech.lp2p.core;

import com.google.protobuf.ByteString;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import tech.lp2p.dag.DagInputStream;
import tech.lp2p.dag.DagResolver;
import tech.lp2p.dag.DagService;
import tech.lp2p.dag.DagStream;
import tech.lp2p.lite.LiteReader;
import tech.lp2p.lite.LiteReaderStream;

public interface BlockStore {

    boolean hasBlock(Hash hash);

    byte[] getBlock(Hash hash);

    void deleteBlock(Hash hash);

    void storeBlock(Hash hash, byte[] data);

    default Info info(Cid cid) throws IOException {
        return DagStream.info(this, cid);
    }

    default boolean hasChild(Dir dir, String name) throws IOException {
        return DagStream.hasChild(this, dir, name);
    }

    default List<Raw> raws(Fid fid) throws IOException {
        return DagStream.raws(this, fid);
    }

    default List<Info> childs(Dir dir) throws IOException {
        return DagStream.childs(this, dir);
    }

    default List<Cid> blocks(Cid cid) throws IOException {
        return DagStream.blocks(this, cid);
    }

    // Note: remove the cid block (add all links blocks recursively) from the blockStore
    default void removeBlocks(Cid cid) throws IOException {
        DagStream.removeBlocks(this, cid);
    }

    default Dir removeFromDirectory(Dir dir, Dir child) throws IOException {
        return DagStream.removeFromDirectory(this, dir.cid(), child);
    }

    default Dir removeFromDirectory(Dir dir, Fid child) throws IOException {
        return DagStream.removeFromDirectory(this, dir.cid(), child);
    }

    default Dir addToDirectory(Dir dir, Dir child) throws IOException {
        return DagStream.addToDirectory(this, dir.cid(), child);
    }

    default Dir addToDirectory(Dir dir, Fid child) throws Exception {
        return DagStream.addToDirectory(this, dir.cid(), child);
    }

    default Dir updateDirectory(Dir dir, Fid child) throws IOException {
        return DagStream.updateDirectory(this, dir.cid(), child);
    }

    default Dir updateDirectory(Dir dir, Dir child) throws IOException {
        return DagStream.updateDirectory(this, dir.cid(), child);
    }

    default Dir createDirectory(String name, List<Info> children) {
        return DagStream.createDirectory(this, name, children);
    }

    default Dir createEmptyDirectory(String name) {
        return DagStream.createEmptyDirectory(this, name);
    }

    default Dir renameDirectory(Dir dir, String name) throws IOException {
        return DagStream.renameDirectory(this, dir.cid(), name);
    }

    default Info resolvePath(Cid root, List<String> path) throws IOException {
        return DagResolver.resolvePath(this, root, path);
    }

    // Note: data is limited to Lite.CHUNK_SIZE
    default Raw storeData(byte[] data) {
        return DagService.createRaw(this, data);
    }

    // Note: data is limited to Lite.CHUNK_SIZE
    default Raw storeText(String data) {
        return DagService.createRaw(this, data.getBytes());
    }


    default Fid storeFile(File file) throws IOException {
        try (FileInputStream inputStream = new FileInputStream(file)) {
            return storeInputStream(file.getName(), inputStream);
        }
    }


    default void fetchToOutputStream(OutputStream outputStream, Cid cid) throws IOException {
        LiteReader liteReader = LiteReader.getReader(this, cid);
        do {
            ByteString buffer = liteReader.loadNextData();
            if (buffer.isEmpty()) {
                return;
            }
            outputStream.write(buffer.toByteArray());
        } while (true);
    }

    default void fetchToFile(File file, Cid cid) throws IOException {
        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fetchToOutputStream(fileOutputStream, cid);
        }
    }


    default InputStream getInputStream(Cid cid) throws IOException {
        LiteReader liteReader = LiteReader.getReader(this, cid);
        return new LiteReaderStream(liteReader);
    }


    default Fid storeInputStream(String name, InputStream inputStream,
                                 Progress progress, long size) throws IOException {

        return DagStream.readInputStream(this, name,
                new DagInputStream(inputStream, progress, size));

    }

    default Fid storeInputStream(String name, InputStream inputStream) throws IOException {
        return DagStream.readInputStream(this, name, new DagInputStream(inputStream, 0));
    }

    default byte[] fetchData(Raw raw) throws IOException {
        return fetchData(raw.cid());
    }

    default byte[] fetchData(Cid cid) throws IOException {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(outputStream, cid);
            return outputStream.toByteArray();
        }
    }

    default String fetchText(Raw raw) throws IOException {
        return fetchText(raw.cid());
    }

    default String fetchText(Cid cid) throws IOException {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(outputStream, cid);
            return outputStream.toString();
        }
    }

    default Dir storeDirectory(File directory) throws IOException {
        List<Info> children = new ArrayList<>();
        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    Dir child = storeDirectory(file);
                    children.add(child);
                } else {
                    Fid fid = storeFile(file);
                    children.add(fid);
                }
            }
        }
        return createDirectory(directory.getName(), children);
    }
}


