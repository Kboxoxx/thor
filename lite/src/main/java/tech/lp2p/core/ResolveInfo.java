package tech.lp2p.core;

public record ResolveInfo(Peeraddr target, Peeraddr relay) {
}
