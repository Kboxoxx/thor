package tech.lp2p.core;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Objects;
import java.util.Set;

import javax.net.ssl.X509TrustManager;

import tech.lp2p.utils.Utils;

public final class TrustManager implements X509TrustManager {

    public static void validCertificate(X509Certificate cert) throws Exception {
        // Checks that the certificate is currently valid. It is if
        // the current date and time are within the validity period given in the
        // certificate.
        //
        // The validity period consists of two date/time values:
        // the first and last dates (and times) on which the certificate
        // is valid.
        cert.checkValidity();


        // check if the certificate’s self-signature is valid (verified)
        // Verifies that this certificate was signed using the
        // private key that corresponds to the specified public key.
        cert.verify(cert.getPublicKey());

        boolean found = false;
        Set<String> critical = cert.getCriticalExtensionOIDs();
        if (critical.contains(Certificate.getLiteExtension())) {
            found = true;
            Utils.checkTrue(critical.size() == 1, "unknown critical extensions");
        } else {
            Utils.checkTrue(critical.isEmpty(), "unknown critical extensions");
        }

        if (!found) {
            Set<String> nonCritical = cert.getNonCriticalExtensionOIDs();
            if (nonCritical.contains(Certificate.getLiteExtension())) {
                found = true;
            }
        }
        Utils.checkTrue(found, "libp2p Public Key Extension is missing");

        // Certificates MUST omit the deprecated subjectUniqueId and issuerUniqueId fields.
        // Endpoints MAY abort the connection attempt if either is present.

        // Could be done, but left out is not required
        // Utils.error(TAG, Arrays.toString(cert.getSubjectUniqueID()));

        // Could be done, but left out is not required
        // Utils.error(TAG, Arrays.toString( cert.getIssuerUniqueID()));

    }

    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType)
            throws CertificateException {
        if (chain.length != 1) {
            throw new CertificateException("only one certificate allowed");
        }
        try {
            // here the expected peerId is null (because it is simply not known)
            // just check if the extracted peerId is not null
            for (X509Certificate cert : chain) {
                validCertificate(cert);

                PeerId peerId = PeerId.extractPeerId(cert);
                Objects.requireNonNull(peerId);

            }
        } catch (Throwable throwable) {
            Utils.error(throwable);
            throw new CertificateException(throwable);
        }
    }

    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType)
            throws CertificateException {
        if (chain.length != 1) {
            throw new CertificateException("only one certificate allowed");
        }
        try {
            // now check the extracted peer ID with the expected value
            for (X509Certificate cert : chain) {
                validCertificate(cert);

                PeerId peerId = PeerId.extractPeerId(cert);
                Objects.requireNonNull(peerId);

            }
        } catch (Throwable throwable) {
            Utils.error(throwable);
            throw new CertificateException(throwable);
        }
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return Utils.CERTIFICATES_EMPTY;
    }
}
