package tech.lp2p.dht;


import com.google.protobuf.ByteString;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeoutException;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Acceptor;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Key;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.lite.LiteConnection;
import tech.lp2p.lite.LiteHandler;
import tech.lp2p.proto.Dht;
import tech.lp2p.quic.ConnectionBuilder;
import tech.lp2p.quic.Requester;
import tech.lp2p.utils.Utils;

public interface DhtService {

    static void addToPeerStore(Lite host, DhtPeer dhtPeer) {
        if (dhtPeer.replaceable()) {
            host.peerStore().storePeeraddr(dhtPeer.peeraddr());
        }
    }

    static void removeFromPeerStore(Lite host, DhtPeer peer) {
        host.peerStore().removePeeraddr(peer.peeraddr());
    }

    private static List<DhtPeer> evalClosestPeers(Dht.Message pms, DhtPeers peers, Key key) {

        List<DhtPeer> dhtPeers = new ArrayList<>();
        for (Dht.Message.Peer entry : pms.getCloserPeersList()) {
            if (entry.getAddrsCount() > 0) {
                Optional<Peeraddr> reduced = Peeraddrs.reduce(
                        entry.getId().toByteArray(), entry.getAddrsList());

                if (reduced.isPresent()) {
                    DhtPeer dhtPeer = DhtPeer.create(reduced.get(), true, key);
                    boolean result = peers.enhance(dhtPeer);
                    if (result) {
                        dhtPeers.add(dhtPeer);
                    }
                }
            }
        }
        return dhtPeers;
    }


    private static List<DhtPeer> bootstrap(Lite host, Key key) {
        List<DhtPeer> peers = new ArrayList<>();

        Peeraddrs peeraddrs = host.bootstrap();
        for (Peeraddr peeraddr : peeraddrs) {
            peers.add(DhtPeer.create(peeraddr, false, key));
        }

        List<Peeraddr> stored = host.peerStore().peeraddrs(Lite.DHT_ALPHA);

        for (Peeraddr peeraddr : stored) {
            peers.add(DhtPeer.create(peeraddr, true, key));
        }

        return peers;
    }


    static void findClosestPeers(Lite host, Key key, Acceptor acceptor) {

        Dht.Message findNodeMessage = DhtService.createFindNodeMessage(key);
        closestPeers(host, key, findNodeMessage, peers -> {

            for (DhtPeer dhtPeer : peers) {
                if (Thread.currentThread().isInterrupted()) {
                    return;
                }
                acceptor.consume(dhtPeer.peeraddr());
            }
        });

    }

    private static void closestPeers(Lite host, Key key, Dht.Message message, QueryClosest closest) {

        runQuery(host, key, (peers, peer) -> {

            if (Thread.currentThread().isInterrupted()) {
                return;
            }
            Dht.Message pms = request(host, peer, message);

            List<DhtPeer> res = evalClosestPeers(pms, peers, key);
            if (!res.isEmpty()) {
                DhtService.addToPeerStore(host, peer);
                closest.evaluate(res);
            }
        });
    }


    static void providers(Lite host, Key key, Acceptor acceptor) {

        Dht.Message message = DhtService.createGetProvidersMessage(key);

        Set<Peeraddr> handled = ConcurrentHashMap.newKeySet();
        runQuery(host, key, (peers, peer) -> {

            if (Thread.currentThread().isInterrupted()) {
                return;
            }

            Dht.Message pms = request(host, peer, message);

            List<Dht.Message.Peer> list = pms.getProviderPeersList();

            for (Dht.Message.Peer entry : list) {
                if (entry.getAddrsCount() > 0) {

                    Optional<Peeraddr> reduced = Peeraddrs.reduce(
                            entry.getId().toByteArray(), entry.getAddrsList());
                    if (reduced.isPresent()) {
                        if (handled.add(reduced.get())) {
                            if (Thread.currentThread().isInterrupted()) {
                                return;
                            }
                            acceptor.consume(reduced.get());
                        }
                    }

                }
            }
            if (Thread.currentThread().isInterrupted()) {
                return;
            }

            List<DhtPeer> res = evalClosestPeers(pms, peers, key); // next iteration
            if (!res.isEmpty()) {
                DhtService.addToPeerStore(host, peer);
            }
        });

    }


    static void provideKey(Lite host, Acceptor acceptor, Key key) {

        Dht.Message findNodeMessage = DhtService.createFindNodeMessage(key);
        Dht.Message message = DhtService.createAddProviderMessage(host, key);

        closestPeers(host, key, findNodeMessage, peers -> {

            for (DhtPeer dhtPeer : peers) {

                if (Thread.currentThread().isInterrupted()) {
                    return;
                }

                Connection connection = ConnectionBuilder.connect(
                        host, ALPN.libp2p, dhtPeer.peeraddr(), dhtPeer.replaceable());

                DhtService.message(connection, message);

                if (Thread.currentThread().isInterrupted()) {
                    return;
                }

                acceptor.consume(dhtPeer.peeraddr());
                connection.close();
            }

        });
    }


    private static Dht.Message request(Lite host, DhtPeer dhtPeer, Dht.Message message)
            throws InterruptedException, ConnectException, TimeoutException {
        Connection connection = null;
        try {
            connection = ConnectionBuilder.connect(host, ALPN.libp2p,
                    dhtPeer.peeraddr(), dhtPeer.replaceable());
            return DhtService.request(connection, message);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }


    private static DhtPeers nearestPeers(Lite host, Key key) {

        // now get the best result to limit 'Lite.DHT_ALPHA'
        DhtPeers dhtPeers = new DhtPeers(Lite.DHT_ALPHA);
        List<DhtPeer> pds = bootstrap(host, key);

        for (DhtPeer dhtPeer : pds) {
            dhtPeers.fill(dhtPeer);
        }
        return dhtPeers;
    }

    private static void runQuery(Lite host, Key key, QueryFunc queryFn) {
        DhtQuery.runQuery(host, nearestPeers(host, key), queryFn);
    }

    static Dht.Message createFindNodeMessage(Key key) {
        return Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.FIND_NODE)
                .setKey(ByteString.copyFrom(key.target()))
                .setClusterLevelRaw(0).build();
    }

    static Dht.Message createGetProvidersMessage(Key key) {
        return Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.GET_PROVIDERS)
                .setKey(ByteString.copyFrom(key.target()))
                .setClusterLevelRaw(0).build();
    }

    static Dht.Message createAddProviderMessage(Lite host, Key key) {

        Dht.Message.Builder builder = Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.ADD_PROVIDER)
                .setKey(ByteString.copyFrom(key.target()))
                .setClusterLevelRaw(0);

        Dht.Message.Peer.Builder peerBuilder = Dht.Message.Peer.newBuilder()
                .setId(ByteString.copyFrom(PeerId.multihash(host.peerId())));
        for (Peeraddr peeraddr : host.peeraddrs()) {
            peerBuilder.addAddrs(ByteString.copyFrom(peeraddr.encoded()));
        }
        builder.addProviderPeers(peerBuilder.build());

        return builder.build();
    }

    @SuppressWarnings("CommentedOutCode")
    static Peeraddrs providers(Connection connection, Key key) throws Exception {
        Utils.checkTrue(connection.alpn() == ALPN.libp2p, "wrong ALPN usage");

        Dht.Message message = DhtService.createGetProvidersMessage(key);

        /* connection.send(Request.create(null, Method.GET, Request.createHeaders(),
                Duration.ofSeconds(Lite.DHT_REQUEST_TIMEOUT), false,
                Protocol.DHT_PROTOCOL.readDelimiter(), Utils.encode(message,
                Protocol.MULTISTREAM_PROTOCOL, Protocol.DHT_PROTOCOL)));*/

        byte[] data = Requester.createStream(((LiteConnection) connection).quic(),
                        2 * Lite.CHUNK_DATA)
                .request(Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.DHT_PROTOCOL), Lite.TIMEOUT);

        byte[] response = LiteHandler.receiveResponse(data);


        Peeraddrs peeraddrs = new Peeraddrs();
        Dht.Message pms = Dht.Message.parseFrom(response);

        List<Dht.Message.Peer> list = pms.getProviderPeersList();
        for (Dht.Message.Peer entry : list) {
            if (entry.getAddrsCount() > 0) {
                Optional<Peeraddr> reduced = Peeraddrs.reduce(
                        entry.getId().toByteArray(), entry.getAddrsList());
                reduced.ifPresent(peeraddrs::add);
            }
        }

        return peeraddrs;
    }

    static Peeraddrs findPeeraddrs(Connection connection, Key key) throws Exception {
        Utils.checkTrue(connection.alpn() == ALPN.libp2p, "wrong ALPN usage");

        Dht.Message message = DhtService.createFindNodeMessage(key);


        byte[] data = Requester.createStream(((LiteConnection) connection).quic(),
                        2 * Lite.CHUNK_DATA)
                .request(Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.DHT_PROTOCOL), Lite.TIMEOUT);
        byte[] response = LiteHandler.receiveResponse(data);
        Dht.Message pms = Dht.Message.parseFrom(response);

        Peeraddrs peeraddrs = new Peeraddrs();
        List<Dht.Message.Peer> list = pms.getCloserPeersList();
        for (Dht.Message.Peer entry : list) {
            if (entry.getAddrsCount() > 0) {
                Optional<Peeraddr> reduced = Peeraddrs.reduce(
                        entry.getId().toByteArray(), entry.getAddrsList());
                reduced.ifPresent(peeraddrs::add);
            }
        }
        return peeraddrs;
    }

    static void message(Connection connection, Dht.Message message)
            throws InterruptedException, TimeoutException {
        Requester.createStream(((LiteConnection) connection).quic(),
                        2 * Lite.CHUNK_DATA)
                .request(Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.DHT_PROTOCOL), Lite.TIMEOUT);
    }

    static Dht.Message request(Connection connection, Dht.Message message)
            throws InterruptedException, TimeoutException, ConnectException {

        byte[] data = Requester.createStream(((LiteConnection) connection).quic(),
                        2 * Lite.CHUNK_DATA)
                .request(Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.DHT_PROTOCOL), Lite.TIMEOUT);

        try {
            byte[] response = LiteHandler.receiveResponse(data);
            return Dht.Message.parseFrom(response);
        } catch (Throwable throwable) {
            throw new ConnectException(throwable.getMessage());
        }

    }

    interface QueryFunc {

        void query(DhtPeers peers, DhtPeer peer)
                throws InterruptedException, ConnectException, TimeoutException;
    }


    interface QueryClosest {

        void evaluate(List<DhtPeer> dhtPeers)
                throws InterruptedException, ConnectException, TimeoutException;
    }

}
