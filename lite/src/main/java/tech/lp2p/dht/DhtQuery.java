package tech.lp2p.dht;

import java.net.ConnectException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import tech.lp2p.Lite;
import tech.lp2p.utils.Utils;


interface DhtQuery {

    static void runQuery(Lite host, DhtPeers dhtPeers, DhtService.QueryFunc queryFn) {
        iteration(host, dhtPeers, queryFn);
    }


    static void iteration(Lite host, DhtPeers dhtPeers, DhtService.QueryFunc queryFn) {
        if (Thread.currentThread().isInterrupted()) {
            return;
        }
        ExecutorService service = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());
        try {
            List<DhtPeer> nextPeersToQuery = dhtPeers.nextPeers();

            for (DhtPeer dhtPeer : nextPeersToQuery) {

                dhtPeer.start();

                service.execute(() -> {

                    try {
                        if (!Thread.currentThread().isInterrupted()) {
                            queryFn.query(dhtPeers, dhtPeer);
                        }
                        dhtPeer.done();
                        if (!Thread.currentThread().isInterrupted()) {
                            iteration(host, dhtPeers, queryFn);
                        }
                    } catch (InterruptedException ignore) {
                        dhtPeer.done();
                        service.shutdownNow();
                    } catch (ConnectException ignore) {
                        dhtPeer.done();
                    } catch (TimeoutException timeoutException) {
                        dhtPeer.done();
                        DhtService.removeFromPeerStore(host, dhtPeer);
                    } catch (Throwable throwable) {
                        Utils.error(throwable); // not expected
                        dhtPeer.done();
                        DhtService.removeFromPeerStore(host, dhtPeer);
                    }
                });
            }
            service.shutdown();
            if (!service.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS)) {
                service.shutdownNow();
            }
        } catch (InterruptedException interruptedException) {
            service.shutdownNow();
        }
    }


}
