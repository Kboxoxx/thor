package tech.lp2p.dht;

import java.math.BigInteger;

import tech.lp2p.core.Key;
import tech.lp2p.core.Peeraddr;


public record DhtPeer(Peeraddr peeraddr, boolean replaceable, BigInteger distance, State state)
        implements Comparable<DhtPeer> {

    public static DhtPeer create(Peeraddr peeraddr, boolean replaceable, Key key) {
        Key peerKey = peeraddr.peerId().createKey();
        BigInteger distance = Key.distance(key, peerKey);
        return new DhtPeer(peeraddr, replaceable, distance, new State());
    }

    public void done() {
        state.done = true;
    }

    public boolean isDone() {
        return state.done;
    }

    public boolean isStarted() {
        return state.started;
    }

    public void start() {
        state.started = true;
    }

    @Override
    public int compareTo(DhtPeer o) {
        return distance.compareTo(o.distance);
    }

    private static class State {
        volatile boolean started = false;
        volatile boolean done = false;
    }
}
