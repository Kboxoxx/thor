package tech.lp2p.dht;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import tech.lp2p.Lite;

public class DhtPeers {
    public final int saturation;
    private final ConcurrentSkipListSet<DhtPeer> peerSet = new ConcurrentSkipListSet<>();
    private final Lock lock = new ReentrantLock();

    public DhtPeers(int saturation) {
        this.saturation = saturation;
    }


    public List<DhtPeer> nextPeers() {
        // The peers we query next should be ones that we have only Heard about.
        List<DhtPeer> peers = new ArrayList<>();
        int count = 0;
        for (DhtPeer dhtPeer : peerSet) {
            if (!dhtPeer.isStarted()) {
                peers.add(dhtPeer);
                count++;
                if (count == Lite.DHT_CONCURRENCY) {
                    break;
                }
            }
        }
        return peers;
    }


    public boolean enhance(DhtPeer dhtPeer) {
        if (peerSet.size() < saturation) {
            return peerSet.add(dhtPeer);
        } else {
            lock.lock();
            try {
                DhtPeer last = last();
                Objects.requireNonNull(last);
                // check if dhtQueryPeer is closer
                int value = dhtPeer.compareTo(last);
                // if value is negative dhtPeer is closer to key
                if (value < 0) {
                    // it is save now to remove last and insert
                    boolean result = peerSet.add(dhtPeer);
                    if (result) {
                        // the new peer is added, now remove the last entry
                        // when it is possible
                        if (last.isDone()) {
                            peerSet.remove(last);
                        }
                    }
                    return result;
                }
                // else case not added
                return false;
            } finally {
                lock.unlock();
            }
        }
    }

    public int size() {
        return peerSet.size();
    }

    public void fill(DhtPeer dhtPeer) {
        peerSet.add(dhtPeer);

        if (peerSet.size() > saturation) {
            DhtPeer last = peerSet.last();
            peerSet.remove(last);
        }
    }

    public DhtPeer first() {
        return peerSet.first();
    }

    public DhtPeer last() {
        return peerSet.last();
    }
}
