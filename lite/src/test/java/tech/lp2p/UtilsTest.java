package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import junit.framework.TestCase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Fid;
import tech.lp2p.core.FileStore;
import tech.lp2p.core.Headers;
import tech.lp2p.core.Key;
import tech.lp2p.core.Keys;
import tech.lp2p.core.Limit;
import tech.lp2p.core.MemoryPeers;
import tech.lp2p.core.Network;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocols;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Response;
import tech.lp2p.core.Status;
import tech.lp2p.http3.HttpHeaders;
import tech.lp2p.utils.Utils;


public class UtilsTest {

    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }


    @Test
    public void testBuilder() throws Exception {
        Lite lite = Lite.newBuilder().agent("a").
                certificate(Lite.generateCertificate()).
                limit(new Limit(100, 100)).
                bootstrap(new Peeraddrs()).
                peerStore(new MemoryPeers()).build();
        assertNotNull(lite);
        assertEquals("a", lite.agent());
        assertNotNull(lite.certificate());
        assertNotNull(lite.bootstrap());
        assertNotNull(lite.peerStore());

    }

    @Test
    public void testResponse() {
        Headers headers = Response.createHeaders();
        headers.add("test", "3");
        Response response = Response.create(Status.TIMEOUT, headers);
        assertNotNull(response);
        assertTrue(response.headers().firstValue("test").isPresent());
    }

    @Test
    public void testHeaders() {
        Headers headers = new HttpHeaders();
        headers.add("test", "3");
        headers.add("hallo", "moin");
        OptionalLong value = headers.firstValueAsLong("test");
        assertTrue(value.isPresent());
        assertEquals(value.getAsLong(), 3);

        Optional<String> result = headers.firstValue("hallo");
        assertTrue(result.isPresent());
        assertEquals(result.get(), "moin");

    }

    @Test
    public void testAddress() throws Exception {
        PeerId peerId = TestEnv.random();
        Peeraddr peeraddr = Lite.createPeeraddr(peerId.toBase58(),
                "2804:d41:432f:3f00:ccbd:8e0d:a023:376b", 4001);
        Assert.assertNotNull(peeraddr);

        Peeraddr cmp = Peeraddr.create(peerId, peeraddr.encoded());
        Assert.assertNotNull(cmp);
        TestCase.assertEquals(peeraddr.peerId(), cmp.peerId());
        TestCase.assertEquals(peeraddr, cmp);
    }


    @Test
    public void rawTest() {
        Cid rawCid = Lite.rawCid("moin elf");
        assertNotNull(rawCid);
    }

    @Test
    public void peerIdRandom() {
        PeerId peerId = TestEnv.random();
        byte[] bytes = PeerId.toArray(peerId);
        TestCase.assertEquals(PeerId.toPeerId(bytes), peerId);
    }


    @Test
    public void network() {
        int port = 4001;
        List<InetAddress> siteLocalAddresses = Network.siteLocalAddresses();
        Assert.assertNotNull(siteLocalAddresses);
        TestEnv.error(siteLocalAddresses.toString());

        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(TestEnv.random(), port);
        Assert.assertNotNull(peeraddr);
        TestEnv.error(peeraddr.toString());
    }

    @Test
    public void distance() {

        PeerId peerId = TestEnv.random();


        Key a = peerId.createKey();
        Key b = peerId.createKey();


        BigInteger dist = Key.distance(a, b);
        TestCase.assertEquals(dist.longValue(), 0L);


        PeerId randrom = TestEnv.random();

        Key r1 = randrom.createKey();

        BigInteger distCmp = Key.distance(a, r1);
        assertNotEquals(distCmp.longValue(), 0L);

        Cid cid = Lite.rawCid("time");
        Assert.assertNotNull(cid);

    }


    @Test
    public void tinkEd25519() throws Exception {
        Keys keys = Lite.generateKeys();

        PeerId peerId = keys.peerId();

        byte[] msg = "moin moin".getBytes();
        byte[] signature = keys.sign(msg);

        Base64.Encoder encoder = Base64.getEncoder();
        String privateKeyAsString = encoder.encodeToString(keys.privateKey());
        Assert.assertNotNull(privateKeyAsString);
        String publicKeyAsString = encoder.encodeToString(keys.peerId().hash());
        Assert.assertNotNull(publicKeyAsString);

        Base64.Decoder decoder = Base64.getDecoder();

        byte[] privateKey = decoder.decode(privateKeyAsString);
        Assert.assertNotNull(privateKey);
        byte[] publicKey = decoder.decode(publicKeyAsString);

        PeerId peerIdCmp = PeerId.create(publicKey);

        TestCase.assertEquals(peerId, peerIdCmp);

        peerIdCmp.verify(msg, signature);

    }

    @Test
    public void cidConversion() {

        Cid cid = Lite.rawCid("moin welt");
        Assert.assertNotNull(cid);
        Assert.assertNotNull(cid.hash());
        assertNotNull(cid.toBase32());

        byte[] data = Cid.toArray(cid);
        Cid cmp = Cid.toCid(data);
        Assert.assertNotNull(cmp);
        TestCase.assertEquals(cmp, cid);
        assertArrayEquals(cmp.hash(), cid.hash());
    }

    @Test
    public void threadModel() throws InterruptedException {

        if (TestEnv.supportLongRunningTests()) {

            AtomicBoolean finishedService = new AtomicBoolean(false);
            ExecutorService controller = Executors.newFixedThreadPool(2);
            controller.execute(() -> Utils.runnable(() -> Utils.runnable(() -> {
                try {
                    Thread.sleep(Integer.MAX_VALUE);
                } catch (InterruptedException interruptedException) {
                    finishedService.set(true);
                }
            }, Integer.MAX_VALUE), Integer.MAX_VALUE));

            // this closes is for interruption
            controller.execute(() -> {
                try {
                    Thread.sleep(TestEnv.SLEEP);
                    controller.shutdownNow();
                } catch (Throwable throwable) {
                    TestEnv.error(throwable);
                }
            });

            controller.shutdown();
            AtomicBoolean finished = new AtomicBoolean(false);
            try {
                finished.set(controller.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS));
            } catch (Throwable throwable) {
                TestEnv.error(throwable);
            }
            assertTrue(finished.get());
            Thread.sleep(TestEnv.SLEEP);
            assertTrue(finishedService.get());
        }
    }

    @Test
    public void cidsDecoding() throws Exception {
        Cid cid = Lite.rawCid("hello");
        assertNotNull(cid);
        Cid cmp = Lite.decodeCid(cid.toBase32());
        assertNotNull(cmp);
        assertEquals(cid, cmp);
    }

    @Test
    public void peerIdsDecoding() throws Exception {
        PeerId random = TestEnv.random();

        // -- Peer ID (sha256) encoded as a raw base58btc multihash
        PeerId peerId = Lite.decodePeerId("QmYyQSo1c1Ym7orWxLYvCrM2EmxFTANf8wXmmE7DWjhx5N");
        assertNotNull(peerId);

        //  -- Peer ID (ed25519, using the "identity" multihash) encoded as a raw base58btc multihash
        peerId = Lite.decodePeerId("12D3KooWD3eckifWpRn9wQpMG9R9hX3sD158z7EqHWmweQAJU5SA");
        assertNotNull(peerId);

        peerId = Lite.decodePeerId(random.toBase36());
        assertNotNull(peerId);
        assertEquals(peerId, random);

        peerId = Lite.decodePeerId(random.toBase58());
        assertNotNull(peerId);
        assertEquals(peerId, random);
    }


    @Test
    public void ipv6Test() throws Exception {
        assertNotNull(Lite.createPeeraddr("12D3KooWQ6SJ5A3uX5WjxCNbEbdAu8ufKJ3TmcjReTLSGaFk4HDU",
                "2804:d41:432f:3f00:ccbd:8e0d:a023:376b", 4001));
    }

    @Test
    public void listenAddresses() throws Exception {
        try (Lite server = Lite.newLite()) {

            Peeraddrs result = server.peeraddrs();
            assertNotNull(result);
            for (Peeraddr ma : result) {
                TestEnv.error("Listen Address : " + ma.toString());
            }

            assertNotNull(server.peerId());

            assertEquals(server.agent(), Lite.AGENT);
            assertEquals(server.peerId(), server.peerId());

            Protocols protocols = server.protocols();
            for (String protocol : protocols.names()) {
                assertNotNull(protocol);
            }


        }
    }


    @Test
    public void fetchDataTest() throws Exception {

        try (FileStore blockStore = new FileStore()) {
            String test = "Moin";
            Raw cid = blockStore.storeText(test);
            assertNotNull(cid);
            byte[] bytes = blockStore.fetchData(cid.cid());
            assertNotNull(bytes);
            assertEquals(test, new String(bytes));

            try {
                Cid fault = Lite.decodeCid(TestEnv.random().toBase58());
                blockStore.fetchData(fault);
                fail();
            } catch (Exception ignore) {
                // ok
            }
        }
    }

    @Test
    public void textProgressTest() throws Exception {

        try (FileStore blockStore = new FileStore()) {

            String test = "Moin bla bla";
            Raw cid = blockStore.storeText(test);
            assertNotNull(cid);

            byte[] bytes = blockStore.fetchData(cid.cid());
            assertNotNull(bytes);
            assertEquals(test, new String(bytes));

            String text = blockStore.fetchText(cid.cid());
            assertNotNull(text);
            assertEquals(test, text);
        }

    }


    @Test
    public void addCat() throws Exception {

        try (FileStore blockStore = new FileStore()) {
            byte[] content = TestEnv.getRandomBytes(400000);
            Fid cid = TestEnv.createContent(blockStore, "random.bin", content);

            byte[] fileContents = blockStore.fetchData(cid.cid());
            assertNotNull(fileContents);
            assertEquals(content.length, fileContents.length);
            assertEquals(new String(content), new String(fileContents));

            blockStore.removeBlocks(cid.cid());
        }

    }

    @Test
    public void rawCid() throws IOException {

        try (FileStore blockStore = new FileStore()) {
            Raw cid = blockStore.storeText("hallo");
            assertNotNull(cid);
        }
    }

    @Test
    public void peerStoreTest() {

        int port = 4001;
        PeerId random = TestEnv.random();

        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(random, port);
        Assert.assertNotNull(peeraddr);
        byte[] data = peeraddr.encoded();
        Assert.assertNotNull(data);
        Peeraddr cmp = Peeraddr.create(random, data);
        Assert.assertNotNull(cmp);
        assertEquals(peeraddr, cmp);

    }
}
