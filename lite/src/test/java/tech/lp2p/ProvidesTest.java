package tech.lp2p;


import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;

import com.google.protobuf.ByteString;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Hash;
import tech.lp2p.core.Key;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.dht.DhtService;
import tech.lp2p.proto.Merkledag;
import tech.lp2p.proto.Unixfs;
import tech.lp2p.utils.Utils;

public class ProvidesTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void providersEmptyDirectory() throws Exception {
        Lite server = TestEnv.getTestInstance();
        assertNotNull(server);

        // just for testing create an empty directory
        byte[] unixData = Unixfs.Data.newBuilder()
                .setType(Unixfs.Data.DataType.Directory)
                .build().toByteArray();

        Merkledag.PBNode.Builder pbn = Merkledag.PBNode.newBuilder();
        pbn.setData(ByteString.copyFrom(unixData));
        byte[] bytes = pbn.build().toByteArray();
        Cid cid = Cid.createCid(Hash.createHash(bytes));


        long time = System.currentTimeMillis();

        Set<Peeraddr> provs = ConcurrentHashMap.newKeySet();
        Key key = Key.convertKey(cid.multihash());


        Utils.runnable(() -> DhtService.providers(server, key, provs::add), 120);

        if (TestEnv.hasNetwork()) {
            assertFalse(provs.isEmpty());
        }

        for (Peeraddr prov : provs) {
            TestEnv.error("Provider " + prov);
        }
        TestEnv.error("Time Providers : " + (System.currentTimeMillis() - time) + " [ms]");
    }
}
