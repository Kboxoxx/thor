package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.InputStream;
import java.util.Stack;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Dir;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Info;
import tech.lp2p.core.Peeraddr;


public class ResolvePathTest {


    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }


    @Test
    public void resolvePath() throws Throwable {

        try (Lite server = Lite.newLite()) {

            Peeraddr peeraddr = server.loopbackAddress();

            BlockStore blockStore = server.blockStore();

            Dir root;
            Stack<String> pathIndex = new Stack<>();
            Stack<String> pathData = new Stack<>();
            String content = "Moin Moin";
            byte[] bytes = TestEnv.getRandomBytes(5000);

            Fid fid = TestEnv.createContent(blockStore, "index.txt", content.getBytes());
            assertNotNull(fid);

            Fid bin = TestEnv.createContent(blockStore, "payload.bin", bytes);
            assertNotNull(bin);


            Dir a = blockStore.createEmptyDirectory("a");
            assertNotNull(a);
            Dir b = blockStore.createEmptyDirectory("b");
            assertNotNull(b);
            b = blockStore.addToDirectory(b, fid);
            assertNotNull(b);
            a = blockStore.addToDirectory(a, b);
            assertNotNull(a);
            a = blockStore.addToDirectory(a, bin);
            assertNotNull(a);

            pathIndex.push("b");
            pathIndex.push("index.txt");

            pathData.push("payload.bin");
            root = a;


            assertNotNull(root);
            assertFalse(pathIndex.isEmpty());
            assertFalse(pathData.isEmpty());


            try (Lite client = Lite.newLite()) {

                Info info = client.resolvePath(peeraddr, root.cid(), pathIndex);
                assertNotNull(info);
                assertEquals(info.name(), "index.txt");

                byte[] data;
                try (InputStream inputStream = client.getInputStream(peeraddr, info.cid())) {
                    data = inputStream.readAllBytes();
                }

                assertNotNull(data);
                assertArrayEquals(data, content.getBytes());

                info = client.resolvePath(peeraddr, root.cid(), pathData);
                assertNotNull(info);
                assertEquals(info.name(), "payload.bin");


                try (InputStream inputStream = client.getInputStream(peeraddr,
                        info.cid(), new TestEnv.DummyProgress())) {
                    data = inputStream.readAllBytes();
                }
                assertNotNull(data);
                assertArrayEquals(data, bytes);
            }

        }
    }


    @Test
    public void hasChild() throws Throwable {

        try (Lite server = Lite.newLite()) {

            Peeraddr peeraddr = server.loopbackAddress();
            BlockStore blockStore = server.blockStore();

            Dir root;

            Fid fid = TestEnv.createContent(blockStore, "index.txt", "Moin".getBytes());
            assertNotNull(fid);

            Dir a = blockStore.createEmptyDirectory("a");
            assertNotNull(a);

            a = blockStore.addToDirectory(a, fid);
            assertNotNull(a);

            root = a;
            assertNotNull(root);

            try (Lite client = Lite.newLite()) {

                boolean result = client.hasChild(peeraddr, root, "index.txt");
                assertTrue(result);

                result = client.hasChild(peeraddr, root, "not_there");
                assertFalse(result);

            }
        }

    }

}
