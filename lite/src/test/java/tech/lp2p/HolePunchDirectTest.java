package tech.lp2p;


import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertArrayEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Reservation;


public class HolePunchDirectTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void testHolepunch() throws Throwable {

        Lite server = TestEnv.getTestInstance();
        assertNotNull(server);
        BlockStore blockStore = server.blockStore();
        byte[] original = TestEnv.getRandomBytes(100);
        Raw raw = blockStore.storeData(original);


        if (!server.hasReservations()) {
            TestEnv.error("nothing to test no dialable addresses");
            return;
        }

        Set<Reservation> reservations = server.reservations();

        AtomicInteger success = new AtomicInteger(0);

        for (Reservation reservation : reservations) {

            PeerId relayID = reservation.peerId();
            assertNotNull(relayID);
            Peeraddr relay = reservation.peeraddr();
            assertNotNull(relay);

            assertTrue(reservation.limitData() > 0);
            assertTrue(reservation.limitDuration() > 0);


            TestEnv.error(reservation.toString());

            try (Lite client = Lite.newLite()) {
                Peeraddr peeraddr = client.resolveAddress(reservation.peeraddr(), server.peerId());
                Objects.requireNonNull(peeraddr);

                byte[] data = client.fetchData(peeraddr, raw);
                assertArrayEquals(data, original);

                success.incrementAndGet();

            } catch (Throwable throwable) {
                TestEnv.error("Failed " + throwable.getMessage());
            }
        }
        assertTrue(server.hasReservations());
        TestEnv.error("Success " + success.get() + " Total " + reservations.size());

        assertTrue(success.get() > 0);
    }
}
