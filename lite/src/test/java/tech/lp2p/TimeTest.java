package tech.lp2p;

import static org.junit.Assert.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.InputStream;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Fid;
import tech.lp2p.core.FileStore;
import tech.lp2p.core.Hash;


public class TimeTest {


    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void test_adding_performance() throws Exception {

        long start = System.currentTimeMillis();
        // create dummy session
        int maxNumberBytes = 100 * 1000 * 1000; // 100 MB


        AtomicInteger counter = new AtomicInteger(0);
        BlockStore blockStore = new BlockStore() {

            @Override
            public boolean hasBlock(Hash hash) {
                return false;
            }

            @Override
            public byte[] getBlock(Hash hash) {
                return new byte[0];
            }

            @Override
            public void deleteBlock(Hash hash) {

            }

            @Override
            public void storeBlock(Hash hash, byte[] data) {

            }
        };
        //noinspection AnonymousInnerClassMayBeStatic,AnonymousInnerClass
        Fid cid = blockStore.storeInputStream("random.bin", new InputStream() {
            @Override
            public int read() {
                int count = counter.incrementAndGet();
                if (count > maxNumberBytes) {
                    return -1;
                }
                return 99;
            }
        });
        assertNotNull(cid);


        long end = System.currentTimeMillis();
        TestEnv.error("Time for hashing " + (end - start) / 1000 + "[s]");


        start = System.currentTimeMillis();

        try (FileStore fileStore = new FileStore()) {

            counter.set(0);
            //noinspection AnonymousInnerClassMayBeStatic,AnonymousInnerClass
            cid = fileStore.storeInputStream("test.bin", new InputStream() {
                @Override
                public int read() {
                    int count = counter.incrementAndGet();
                    if (count > maxNumberBytes) {
                        return -1;
                    }
                    return 99;
                }
            });
            assertNotNull(cid);
        }
        end = System.currentTimeMillis();
        TestEnv.error("Time for hashing and storing " + (end - start) / 1000 + "[s]");
    }

}
