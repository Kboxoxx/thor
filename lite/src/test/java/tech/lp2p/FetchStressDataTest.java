package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;

import junit.framework.TestCase;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Peeraddr;

public class FetchStressDataTest {


    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }


    @Test
    public void stressFetchData() throws Exception {

        try (Lite server = Lite.newLite()) {

            long now = System.currentTimeMillis();
            long length = 50000000; // 50 MB
            Fid fid;
            BlockStore blockStore = server.blockStore();

            AtomicInteger write = new AtomicInteger(0);
            fid = blockStore.storeInputStream("test.bin", new InputStream() {
                @Override
                public int read() {
                    int count = write.incrementAndGet();
                    if (count > length) {
                        return -1;
                    }
                    return 0; // only zeros, just test the fetch performance
                }
            });


            assertNotNull(fid);

            TestEnv.error("Store Input Stream : " + fid +
                    " Time : " + ((System.currentTimeMillis() - now) / 1000) + "[s]");
            now = System.currentTimeMillis();


            Peeraddr peeraddr = server.loopbackAddress();


            try (Lite client = Lite.newLite()) {

                AtomicInteger read = new AtomicInteger(0);
                client.fetchToOutputStream(peeraddr, new OutputStream() {
                            @Override
                            public void write(int b) {
                                read.incrementAndGet();
                            }
                        },
                        fid.cid(), new TestEnv.DummyProgress());

                TestCase.assertEquals(read.get(), length);
            }

            TestEnv.error("Read Data : " + fid +
                    " Time : " + ((System.currentTimeMillis() - now) / 1000) + "[s]");

        }
    }
}
