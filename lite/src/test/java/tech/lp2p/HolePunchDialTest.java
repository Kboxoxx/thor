package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Optional;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Info;
import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.ResolveInfo;

public class HolePunchDialTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void testDialLite() throws Throwable {

        Lite server = TestEnv.getTestInstance();
        assertNotNull(server);

        PeerStore peerStore = server.peerStore();
        BlockStore blockStore = server.blockStore();

        Info info = blockStore.createEmptyDirectory("Home");
        server.blockStore().storeBlock(server.peerId(), info.cid().hash());

        if (!server.hasReservations()) {
            TestEnv.error("nothing to test no dialable addresses");
            return;
        }

        try (Lite client = Lite.newBuilder().peerStore(peerStore).bootstrap(TestEnv.BOOTSTRAP).build()) {

            ResolveInfo resolveInfo = client.resolveAddress(server.peerId(),
                    peeraddr -> TestEnv.debug("Try " + peeraddr.toString()),
                    peeraddr -> TestEnv.debug("Failure " + peeraddr.toString()), 120);

            assertNotNull(resolveInfo);
            assertNotNull(resolveInfo.relay());
            assertNotNull(resolveInfo.target());

            Peeraddr target = resolveInfo.target();

            Optional<Cid> optional = client.fetchRoot(target);
            assertTrue(optional.isPresent());
            Cid value = optional.get();
            assertEquals(value, info.cid());

        }
    }
}