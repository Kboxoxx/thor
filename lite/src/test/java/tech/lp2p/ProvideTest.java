package tech.lp2p;


import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Key;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Raw;
import tech.lp2p.dht.DhtService;
import tech.lp2p.utils.Utils;

public class ProvideTest {


    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void testProvideCidKey() throws Exception {
        try (Lite server = Lite.newBuilder().bootstrap(TestEnv.BOOTSTRAP).build()) {


            BlockStore blockStore = server.blockStore();


            byte[] data = TestEnv.getRandomBytes(100);
            Raw cid = blockStore.storeData(data);
            assertNotNull(cid);
            assertEquals(cid.name(), Utils.STRING_EMPTY);

            long start = System.currentTimeMillis();

            Set<Peeraddr> providers = ConcurrentHashMap.newKeySet();
            Key key = cid.cid().createKey();


            Utils.runnable(() -> DhtService.provideKey(server, providers::add, key), 60);


            TestEnv.error("Time provide " + (System.currentTimeMillis() - start) +
                    " number of providers " + providers.size());


            Set<Peeraddr> peeraddrs = ConcurrentHashMap.newKeySet();

            Utils.runnable(() -> DhtService.providers(server, key, peeraddrs::add), 60);


            TestEnv.error("Time provide " + (System.currentTimeMillis() - start) +
                    " number of peeraddrs " + peeraddrs.size());
            assertTrue(peeraddrs.isEmpty()); // random cid, should not be found

        }
    }


    @Test
    public void testProvidePeerIdKey() throws Exception {
        try (Lite server = Lite.newBuilder().bootstrap(TestEnv.BOOTSTRAP).build()) {

            PeerId self = server.peerId();

            long start = System.currentTimeMillis();

            Set<Peeraddr> providers = ConcurrentHashMap.newKeySet();
            Key key = self.createKey();

            Utils.runnable(() -> DhtService.provideKey(server, providers::add, key), 60);


            TestEnv.error("Time provide " + (System.currentTimeMillis() - start) +
                    " number of providers " + providers.size());

            Set<Peeraddr> peeraddrs = ConcurrentHashMap.newKeySet();

            Utils.runnable(() -> DhtService.providers(server, key, peeraddrs::add), 60);

            if (TestEnv.hasNetwork()) {
                assertFalse(peeraddrs.isEmpty());
            }
        }
    }
}
