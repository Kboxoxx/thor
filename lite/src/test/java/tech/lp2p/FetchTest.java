package tech.lp2p;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.InputStream;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Peeraddr;

public class FetchTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void testClientServerDownload() throws Exception {

        try (Lite server = Lite.newLite()) {


            // create dummy session
            int maxNumberBytes = 10 * 1000 * 1000; // 10 MB

            Fid cid;
            BlockStore blockStore = server.blockStore();

            AtomicInteger counter = new AtomicInteger(0);
            //noinspection AnonymousInnerClassMayBeStatic,AnonymousInnerClass
            cid = blockStore.storeInputStream("random.bin", new InputStream() {
                @Override
                public int read() {
                    int count = counter.incrementAndGet();
                    if (count > maxNumberBytes) {
                        return -1;
                    }
                    return 99;
                }
            });


            assertNotNull(cid);
            long start = System.currentTimeMillis();

            Peeraddr peeraddr = server.loopbackAddress();


            File file = TestEnv.createCacheFile();

            try (Lite client = Lite.newLite()) {
                client.fetchToFile(peeraddr, file, cid.cid(),
                        new TestEnv.DummyProgress());

                Optional<Cid> root = client.fetchRoot(peeraddr);
                assertFalse(root.isPresent()); // not expected
            }


            assertEquals(file.length(), maxNumberBytes);

            long end = System.currentTimeMillis();
            TestEnv.error("Time for downloading " + (end - start) / 1000 +
                    "[s]" + " " + file.length() / 1000000 + " [MB]");
            file.deleteOnExit();
        }
    }
}
