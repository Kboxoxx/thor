package tech.lp2p;


import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

import com.google.protobuf.ByteString;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.List;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Dir;
import tech.lp2p.core.Fid;
import tech.lp2p.core.FileStore;
import tech.lp2p.core.Info;
import tech.lp2p.core.Raw;
import tech.lp2p.lite.LiteReader;

public class AddTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test(expected = Exception.class)
    public void add_and_remove() throws Exception {

        try (FileStore blockStore = new FileStore()) {

            String content = "Hallo dfsadf";
            Raw raw = blockStore.storeText(content);
            Cid text = raw.cid();
            assertNotNull(text);
            assertTrue(blockStore.hasBlock(text));
            blockStore.removeBlocks(text);
            assertFalse(blockStore.hasBlock(text));

            blockStore.fetchText(text); // closed exception expected
        }
    }

    @Test
    public void add_dir() throws Exception {
        try (FileStore blockStore = new FileStore()) {

            Dir dir = blockStore.createEmptyDirectory("Moin");
            assertNotNull(dir);
            Info info = blockStore.info(dir.cid());
            assertTrue(info instanceof Dir);
            Dir infoDir = (Dir) info;
            assertEquals(infoDir.name(), "Moin");
            assertEquals(info.name(), "Moin");

            dir = blockStore.renameDirectory(dir, "Hallo");
            assertNotNull(dir);
            info = blockStore.info(dir.cid());
            assertTrue(info instanceof Dir);
            infoDir = (Dir) info;
            assertEquals(infoDir.name(), "Hallo");
            assertEquals(info.name(), "Hallo");


            String content = "Hallo";
            Fid text = TestEnv.createContent(blockStore, "text.txt", content.getBytes());
            assertNotNull(text);

            byte[] data = blockStore.fetchData(text.cid());
            assertEquals(content, new String(data));

            dir = blockStore.addToDirectory(dir, text);
            assertNotNull(dir);
            assertTrue(dir.size() > 0);

            boolean exists = blockStore.hasChild(dir, "text.txt");
            assertTrue(exists);

            Info resolved = blockStore.resolvePath(dir.cid(), List.of("text.txt"));
            assertNotNull(resolved);
            assertEquals(resolved.cid(), text.cid());


            exists = blockStore.hasChild(dir, "text2.txt");
            assertFalse(exists);

            List<Info> childs = blockStore.childs(dir);
            assertNotNull(childs);
            assertEquals(childs.size(), 1);

            dir = blockStore.removeFromDirectory(dir, text);
            assertNotNull(dir);

            childs = blockStore.childs(dir);
            assertNotNull(childs);
            assertEquals(childs.size(), 0);
        }

    }

    @Test
    public void update_dir() throws Exception {
        try (FileStore blockStore = new FileStore()) {

            Dir dir = blockStore.createEmptyDirectory("a");
            assertNotNull(dir);
            assertEquals(dir.size(), 0);

            String fileName = "test.txt";

            // TEST 1
            String content = "Hallo";
            Fid text = TestEnv.createContent(blockStore, fileName, content.getBytes());
            assertNotNull(text);

            dir = blockStore.addToDirectory(dir, text);
            assertNotNull(dir);
            assertEquals(dir.size(), content.length());

            List<Info> childs = blockStore.childs(dir);
            assertNotNull(childs);
            assertEquals(childs.size(), 1);
            assertEquals(childs.get(0).cid(), text.cid());

            // TEST 2
            String contentNew = "Hallo Moin";
            Fid cmp = TestEnv.createContent(blockStore, fileName, contentNew.getBytes());
            Cid textNew = cmp.cid();
            assertNotNull(textNew);

            dir = blockStore.updateDirectory(dir, cmp);
            assertNotNull(dir);
            assertEquals(dir.size(), contentNew.length());

            childs = blockStore.childs(dir);
            assertNotNull(childs);
            assertEquals(childs.size(), 1);
            assertEquals(childs.get(0).cid(), textNew);
        }
    }

    @Test
    public void create_dir() throws Exception {

        try (FileStore blockStore = new FileStore()) {


            String content1 = "Hallo 1";
            Fid text1 = TestEnv.createContent(blockStore, "b.txt", content1.getBytes());
            assertNotNull(text1);

            String content2 = "Hallo 12";
            Fid text2 = TestEnv.createContent(blockStore, "a.txt", content2.getBytes());
            assertNotNull(text2);


            Dir dir = blockStore.createDirectory("zeit", List.of(text1, text2));
            assertNotNull(dir);

            List<Info> childs = blockStore.childs(dir);
            assertNotNull(childs);
            assertEquals(childs.size(), 2);

            assertEquals(childs.get(0).cid().toBase32(), text1.cid().toBase32());
            assertEquals(childs.get(1).cid(), text2.cid());
        }
    }

    @Test
    public void add_wrap_test() throws Exception {
        try (FileStore blockStore = new FileStore()) {

            int packetSize = 1000;
            long maxData = 1000;
            File inputFile = TestEnv.createCacheFile();
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            long size = inputFile.length();

            TestEnv.error("Bytes : " + inputFile.length() / 1000 + "[kb]");

            Fid fid = blockStore.storeFile(inputFile);
            assertNotNull(fid);

            List<Raw> links = blockStore.raws(fid);
            assertNotNull(links);
            assertEquals(links.size(), 20);

            byte[] bytes = blockStore.fetchData(fid.cid());
            assertNotNull(bytes);
            assertEquals(bytes.length, size);

        }
    }

    @Test
    public void add_dir_test() throws Exception {
        try (FileStore blockStore = new FileStore()) {

            File inputFile = TestEnv.createCacheFile();
            assertTrue(inputFile.exists());
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < 10; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(1000);
                    outputStream.write(randomBytes);
                }
            }

            Fid fid = blockStore.storeFile(inputFile);
            assertNotNull(fid);

            List<Raw> links = blockStore.raws(fid);
            assertNotNull(links);

            assertEquals(links.size(), 0);
        }
    }


    @Test
    public void add_test() throws Exception {

        int packetSize = 1000;
        long maxData = 1000;

        try (FileStore blockStore = new FileStore()) {

            File inputFile = TestEnv.createCacheFile();
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            long size = inputFile.length();

            TestEnv.error("Bytes : " + inputFile.length() / 1000 + "[kb]");

            Fid fid = blockStore.storeFile(inputFile);
            assertNotNull(fid);

            List<Raw> links = blockStore.raws(fid);
            assertNotNull(links);
            assertEquals(links.size(), 20);
            Raw link = links.get(0);
            assertNotEquals(link.cid(), fid.cid());

            byte[] bytes = blockStore.fetchData(fid.cid());
            assertNotNull(bytes);
            assertEquals(bytes.length, size);
        }

    }


    @Test
    public void add_wrap_small_test() throws Exception {

        int packetSize = 200;
        long maxData = 1000;

        try (FileStore blockStore = new FileStore()) {

            File inputFile = TestEnv.createCacheFile();
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            long size = inputFile.length();


            TestEnv.error("Bytes : " + inputFile.length() / 1000 + "[kb]");

            Fid fid = blockStore.storeFile(inputFile);
            assertNotNull(fid);

            List<Raw> links = blockStore.raws(fid);
            assertNotNull(links);
            assertEquals(links.size(), 4);

            byte[] bytes = blockStore.fetchData(fid.cid());
            assertNotNull(bytes);
            assertEquals(bytes.length, size);

        }
    }

    @Test
    public void add_small_test() throws Exception {

        int packetSize = 200;
        long maxData = 1000;

        try (FileStore blockStore = new FileStore()) {

            File inputFile = TestEnv.createCacheFile();
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            long size = inputFile.length();

            TestEnv.error("Bytes : " + inputFile.length() / 1000 + "[kb]");

            Fid fid = blockStore.storeFile(inputFile);
            assertNotNull(fid);

            List<Raw> links = blockStore.raws(fid);
            assertNotNull(links);
            assertEquals(links.size(), 4);

            byte[] bytes = blockStore.fetchData(fid.cid());
            assertNotNull(bytes);
            assertEquals(bytes.length, size);
        }

    }


    @Test
    public void test_inputStream() throws Exception {

        try (FileStore blockStore = new FileStore()) {

            String text = "moin zehn";
            Raw cid = blockStore.storeText(text);
            assertTrue(blockStore.hasBlock(cid.cid()));

            byte[] bytes = blockStore.fetchData(cid.cid());
            assertNotNull(bytes);
            assertEquals(bytes.length, text.length());

            try (InputStream stream = blockStore.getInputStream(cid.cid())) {
                assertEquals(text, new String(stream.readAllBytes()));
            }
        }
    }


    @Test
    public void test_inputStreamBig() throws Exception {

        try (FileStore blockStore = new FileStore()) {
            byte[] text = TestEnv.getRandomBytes((Lite.CHUNK_DATA * 2) - 50);
            Fid fid = TestEnv.createContent(blockStore, "random.bin", text);

            byte[] bytes = blockStore.fetchData(fid.cid());
            assertNotNull(bytes);
            assertEquals(bytes.length, text.length);


            try (InputStream stream = blockStore.getInputStream(fid.cid())) {
                assertArrayEquals(text, stream.readAllBytes());
            }
        }
    }

    @Test
    public void test_reader() throws Exception {

        try (FileStore blockStore = new FileStore()) {

            String text = "0123456789 jjjjjjjj";
            Raw cid = blockStore.storeText(text);
            assertTrue(blockStore.hasBlock(cid.cid()));

            LiteReader liteReader = LiteReader.getReader(blockStore, cid.cid());
            liteReader.seek(0);
            ByteString buffer = liteReader.loadNextData();
            assertNotNull(buffer);
            assertEquals(text, new String(buffer.toByteArray()));

            int pos = 11;
            liteReader.seek(pos);
            buffer = liteReader.loadNextData();
            assertNotNull(buffer);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            stream.write(buffer.toByteArray());
            assertEquals(text.substring(pos), stream.toString());

            pos = 5;
            liteReader.seek(pos);
            buffer = liteReader.loadNextData();
            assertNotNull(buffer);
            stream = new ByteArrayOutputStream();
            stream.write(buffer.toByteArray());
            assertEquals(text.substring(pos), stream.toString());
        }

    }

    @Test
    public void test_readerBig() throws Exception {

        try (FileStore blockStore = new FileStore()) {

            byte[] text = TestEnv.getRandomBytes((Lite.CHUNK_DATA * 2) - 50);
            Fid fid = TestEnv.createContent(blockStore, "random.bin", text);

            assertTrue(blockStore.hasBlock(fid.cid()));

            LiteReader liteReader = LiteReader.getReader(blockStore, fid.cid());
            liteReader.seek(0);
            ByteString buffer = liteReader.loadNextData();
            assertNotNull(buffer);
            assertEquals(Lite.CHUNK_DATA, buffer.size());
            buffer = liteReader.loadNextData();
            assertNotNull(buffer);
            assertEquals(Lite.CHUNK_DATA - 50, buffer.size());

            int pos = Lite.CHUNK_DATA + 50;
            liteReader.seek(pos);
            buffer = liteReader.loadNextData();
            assertNotNull(buffer);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            stream.write(buffer.toByteArray());

            assertEquals(Lite.CHUNK_DATA - 100, stream.size());

            pos = Lite.CHUNK_DATA - 50;
            liteReader.seek(pos);
            buffer = liteReader.loadNextData();
            assertNotNull(buffer);
            stream = new ByteArrayOutputStream();
            stream.write(buffer.toByteArray());
            assertEquals(50, stream.size());
        }
    }

    @Test
    public void storeDirectoryTest() throws Exception {
        try (FileStore blockStore = new FileStore()) {

            File directory = Files.createTempDirectory("").toFile();

            File child = new File(directory, "test.bin");
            boolean created = child.createNewFile();
            assertTrue(created);

            Dir dir = blockStore.storeDirectory(directory);
            assertNotNull(dir);


            List<Info> childs = blockStore.childs(dir);
            assertNotNull(childs);
            assertEquals(childs.size(), 1);
            assertEquals(childs.get(0).name(), "test.bin");


            FileStore.deleteDirectory(directory);
        }
    }

    @Test
    public void directoryTest() throws Throwable {

        try (FileStore blockStore = new FileStore()) {

            Fid fid = TestEnv.createContent(blockStore, "index.txt", "Moin Moin".getBytes());
            assertNotNull(fid);


            Dir a = blockStore.createEmptyDirectory("a");
            assertNotNull(a);
            Dir b = blockStore.createEmptyDirectory("b");
            assertNotNull(b);
            b = blockStore.addToDirectory(b, fid);
            assertNotNull(b);
            a = blockStore.addToDirectory(a, b);
            assertNotNull(a);

            assertEquals(a.size(), fid.size()); // test if same size

            // now change the "index.txt"
            fid = TestEnv.createContent(blockStore, "index.txt", "Moin Moin Moin".getBytes());
            assertNotNull(fid);

            b = blockStore.updateDirectory(b, fid);
            assertNotNull(b);
            a = blockStore.updateDirectory(a, b);
            assertNotNull(a);

            assertEquals(a.size(), fid.size()); // test if same size


            a = blockStore.removeFromDirectory(a, b);
            assertNotNull(a);

            assertEquals(a.size(), 0); // test if same size
        }
    }
}
