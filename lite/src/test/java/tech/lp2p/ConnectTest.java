package tech.lp2p;


import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.InetAddress;
import java.util.Optional;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Cid;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.quic.ConnectionBuilder;


public class ConnectTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }


    @Test(expected = Exception.class) // can be Timeout or Connect Exception dependent of Network
    public void swarmPeer() throws Exception {

        PeerId peerId = Lite.decodePeerId("12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC7cUR");

        String host = "139.178.68.146";
        Peeraddr peeraddr = Lite.createPeeraddr(peerId, InetAddress.getByName(host), 4001);
        assertEquals(4001, peeraddr.port());

        assertEquals(peeraddr.peerId(), peerId);

        // peeraddr is just a fiction (will fail)
        Lite client = Lite.newLite();
        ConnectionBuilder.connect(client, ALPN.libp2p, peeraddr, true);
        fail(); // should not reached this point

    }

    @Test
    public void testServerIdle() throws Exception {

        if (!TestEnv.supportLongRunningTests()) {
            return;
        }

        try (Lite server = Lite.newLite()) {

            Peeraddr peeraddr = server.loopbackAddress();

            try (Lite client = Lite.newLite()) {

                Optional<Cid> optional = client.fetchRoot(peeraddr); // Intern it sets keep alive to true
                assertFalse(optional.isPresent());

                Thread.sleep(30000); // timeout is 10 sec (should be reached)
                assertTrue(server.hasConnection(client.peerId())); // but connection is still valid (keep alive is true)
            }
        }
    }

    @Test
    public void testNumConns() throws Exception {
        try (Lite server = Lite.newLite()) {

            Peeraddr peeraddr = server.loopbackAddress();

            try (Lite client = Lite.newLite()) {

                Optional<Cid> optional = client.fetchRoot(peeraddr); // Intern it sets keep alive to true
                assertFalse(optional.isPresent());

                assertEquals(server.numConnections(), 1);
                assertEquals(client.numConnections(), 1);

            }
        }

    }

}
