package tech.lp2p;


import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.Reservation;


public class RelaysRefreshTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void refreshReservations() throws Exception {

        Lite server = TestEnv.getTestInstance();
        assertNotNull(server);

        if (!server.hasReservations()) {
            TestEnv.error("nothing to test no dialable addresses");
            return;
        }

        for (Reservation reservation : server.reservations()) {
            TestEnv.error(reservation.toString());
            assertTrue(reservation.expireInMinutes() > 0);
            TestEnv.error("Update required " + reservation.updateRequired());

            Reservation refreshed = server.refreshReservation(reservation);
            assertNotNull(refreshed);
            TestEnv.error(refreshed.toString());
            assertTrue(refreshed.expireInMinutes() >= reservation.expireInMinutes());
            assertEquals(refreshed.peerId(), reservation.peerId());
        }

    }

}
