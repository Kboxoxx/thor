package tech.lp2p;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import junit.framework.TestCase;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;
import java.util.Objects;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Handler;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Protocol;
import tech.lp2p.ident.IdentifyService;
import tech.lp2p.quic.ConnectionBuilder;


public class ProtocolTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void protocol_test() throws Exception {
        try (Lite server = Lite.newLite()) {

            int libp2pServerProtocols = 4;

            Map<Protocol, Handler> serverProtocols = server.protocols();
            assertNotNull(serverProtocols);

            assertTrue(serverProtocols.containsKey(Protocol.MULTISTREAM_PROTOCOL));
            assertTrue(serverProtocols.containsKey(Protocol.IDENTITY_PROTOCOL));
            assertTrue(serverProtocols.containsKey(Protocol.RELAY_PROTOCOL_HOP));
            assertTrue(serverProtocols.containsKey(Protocol.RELAY_PROTOCOL_STOP));
            assertEquals(serverProtocols.size(), libp2pServerProtocols);


            // for testing we are connecting to our own server
            Peeraddr peeraddr = server.loopbackAddress();
            Objects.requireNonNull(peeraddr);

            Connection connection = ConnectionBuilder.connect(server, ALPN.libp2p,
                    peeraddr, true);

            Identify info = IdentifyService.identify(connection);
            TestCase.assertNotNull(info);

            assertEquals(libp2pServerProtocols, info.protocols().length);

            connection.close();


            Thread.sleep(TestEnv.SLEEP);
            assertFalse(server.hasConnection(server.peerId()));

        }

    }
}
