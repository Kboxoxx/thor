package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;

import junit.framework.TestCase;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Peeraddr;

public class FetchStressTest {

    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void stressFetchCalls() throws Exception {

        try (Lite server = Lite.newLite()) {

            BlockStore blockStore = server.blockStore();
            long now = System.currentTimeMillis();

            int dataSize = Lite.CHUNK_DATA;
            Fid fid = TestEnv.createContent(blockStore, "text.bin",
                    TestEnv.getRandomBytes(dataSize));
            assertNotNull(fid);

            TestEnv.error("Store Input Stream : " + fid +
                    " Time : " + ((System.currentTimeMillis() - now) / 1000) + "[s]");
            now = System.currentTimeMillis();


            Peeraddr peeraddr = server.loopbackAddress();


            try (Lite client = Lite.newLite()) {

                long step = System.currentTimeMillis();

                int finalRead = 0;
                for (int i = 0; i < TestEnv.ITERATIONS; i++) {

                    AtomicInteger read = new AtomicInteger(0);
                    client.fetchToOutputStream(peeraddr, new OutputStream() {
                        @Override
                        public void write(int b) {
                            read.incrementAndGet();
                        }
                    }, fid.cid(), new TestEnv.DummyProgress());

                    TestCase.assertEquals(read.get(), dataSize);
                    finalRead += read.get();

                    if (Math.floorMod(i, 25) == 0) {
                        TestEnv.error("Read Data Iteration : " + i +
                                " Time : " + ((System.currentTimeMillis() - step)) + "[ms]");
                        step = System.currentTimeMillis();
                    }

                }

                TestEnv.error("Read Data : " + finalRead +
                        "[bytes] Time : " + ((System.currentTimeMillis() - now) / 1000) + "[s]");
            }
        }
    }
}
