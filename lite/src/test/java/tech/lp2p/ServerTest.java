package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.InetAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Network;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Reservation;


public class ServerTest {

    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }


    @Test
    public void fetchDataTest() throws Exception {

        try (Lite server = Lite.newLite()) {
            BlockStore blockStore = server.blockStore();

            Peeraddr peeraddr = server.loopbackAddress();

            byte[] input = TestEnv.getRandomBytes(10000000); // 10 MB

            Fid cid = TestEnv.createContent(blockStore, "random.bin", input);
            assertNotNull(cid);


            try (Lite client = Lite.newLite()) {
                byte[] output = client.fetchData(peeraddr, cid.cid(), new TestEnv.DummyProgress());
                assertArrayEquals(input, output);
            }
        }
    }


    @Test
    public void reachableTest() throws Exception {

        try (Lite server = Lite.newLite()) {

            Peeraddrs peeraddrs = server.peeraddrs();
            assertNotNull(peeraddrs);
            boolean reachable = false;
            for (Peeraddr peeraddr : peeraddrs) {
                InetAddress inetAddress = InetAddress.getByAddress(peeraddr.address());

                TestEnv.error("Well known ipv4 translatable " +
                        Network.isWellKnownIPv4Translatable(inetAddress));

                boolean reach = inetAddress.isReachable(50);
                if (reach) {
                    TestEnv.error("Reachable " + inetAddress.getHostAddress());
                    reachable = true;
                } else {
                    TestEnv.error("Not reachable " + inetAddress.getHostAddress());
                    TestEnv.error("Site Local " + inetAddress.isSiteLocalAddress());
                    TestEnv.error("Link Local " + inetAddress.isLinkLocalAddress());
                    TestEnv.error("Any Local " + inetAddress.isAnyLocalAddress());
                }
            }
            assertTrue(reachable);
        }

    }


    @Test
    public void serverTest() throws Exception {

        try (Lite server = Lite.newLite()) {

            Peeraddr peeraddr = server.loopbackAddress();
            assertNotNull(peeraddr);
            BlockStore blockStore = server.blockStore();

            String text = "Hallo das ist ein Test";
            Raw cid = blockStore.storeText(text);
            assertNotNull(cid);

            PeerId host = server.peerId();
            assertNotNull(host);


            try (Lite client = Lite.newLite()) {
                String cmpText = client.fetchText(peeraddr, cid.cid(), new TestEnv.DummyProgress());
                assertEquals(text, cmpText);
            }
        }

    }


    @Test
    public void multipleConns() throws Exception {

        try (Lite server = Lite.newLite()) {
            Peeraddr peeraddr = server.loopbackAddress();
            BlockStore blockStore = server.blockStore();

            byte[] input = TestEnv.getRandomBytes(50000);

            Raw raw = blockStore.storeData(input);
            assertNotNull(raw);

            ExecutorService executors = Executors.newFixedThreadPool(
                    Runtime.getRuntime().availableProcessors());

            AtomicInteger finished = new AtomicInteger();
            int instances = 100;

            for (int i = 0; i < instances; i++) {

                executors.execute(() -> {
                    try (Lite client = Lite.newLite()) {
                        byte[] output = client.fetchData(peeraddr, raw);
                        assertArrayEquals(input, output);
                        finished.incrementAndGet();
                    } catch (Throwable throwable) {
                        TestEnv.error(throwable);
                        fail();
                    }
                });
            }
            executors.shutdown();

            assertTrue(executors.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS));
            assertEquals(finished.get(), instances);

        }
    }


    @Test
    public void multipleReservations() throws Exception {

        try (Lite server = Lite.newLite()) {
            Peeraddr peeraddr = server.loopbackAddress();

            ExecutorService executors = Executors.newFixedThreadPool(
                    Runtime.getRuntime().availableProcessors());

            AtomicInteger finished = new AtomicInteger();
            int instances = 100;


            for (int i = 0; i < instances; i++) {

                executors.execute(() -> {
                    try (Lite client = Lite.newLite()) {
                        Reservation reservation = client.doReservation(peeraddr);
                        assertNotNull(reservation);
                        finished.incrementAndGet();
                    } catch (Throwable throwable) {
                        TestEnv.error(throwable);
                        fail();
                    }
                });
            }
            executors.shutdown();

            assertTrue(executors.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS));
            assertEquals(finished.get(), instances);

        }
    }
}