package tech.lp2p;


import static junit.framework.TestCase.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Reservation;


public class ReservationsTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void test_reservations() throws Exception {

        Lite server = TestEnv.getTestInstance();
        assertNotNull(server);

        if (!server.hasReservations()) {
            TestEnv.error("nothing to test no dialable addresses");
            return;
        }

        for (Peeraddr ma : server.reservationPeeraddrs()) {
            TestEnv.error(ma.toString());
        }


        int timeInMinutes = 1; // make higher for long run


        // test 1 minutes
        for (int i = 0; i < timeInMinutes; i++) {
            Thread.sleep(TimeUnit.MINUTES.toMillis(1));

            Set<Reservation> reservations = server.reservations();
            for (Reservation reservation : reservations) {
                TestEnv.error("Expire in minutes " +
                        reservation.expireInMinutes() + " " + reservation);
                assertNotNull(reservation.peeraddr());
                assertNotNull(reservation.peerId());
            }
        }

    }

}
