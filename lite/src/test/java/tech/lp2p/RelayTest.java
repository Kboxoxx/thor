package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import tech.lp2p.core.Limit;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Reservation;
import tech.lp2p.utils.Utils;

public class RelayTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test(expected = Exception.class)
    public void permissionDeniedReservation() throws Throwable {

        try (Lite server = Lite.newLite()) {
            Peeraddr relayPeeraddr = server.loopbackAddress();
            server.doReservation(relayPeeraddr); // exception expected
            fail();
        }

    }


    @Test(expected = Exception.class)
    public void permissionDeniedConnection() throws Throwable {

        try (Lite server = Lite.newLite()) {
            Peeraddr relayPeeraddr = server.loopbackAddress();
            server.resolveAddress(relayPeeraddr, server.peerId()); // exception expected
            fail();
        }
    }

    @Test
    public void positiveReservation() throws Throwable {

        try (Lite server = Lite.newLite()) {
            Peeraddr relayPeeraddr = server.loopbackAddress();

            int alicePort = Utils.nextFreePort();
            try (Lite alice = Lite.newBuilder().port(alicePort).build()) {

                String text = "Alice";
                Raw raw = alice.blockStore().storeText(text);
                assertNotNull(raw);

                Reservation reservation = alice.doReservation(relayPeeraddr);
                assertNotNull(reservation);
                assertEquals(relayPeeraddr, reservation.peeraddr());

                Set<Reservation> reservations = alice.reservations();
                assertEquals(reservations.size(), 1);


                assertTrue(alice.hasReservation(server.peerId()));
                assertTrue(server.hasHopReserve(alice.peerId()));

                Lite bob = Lite.newLite();

                Peeraddr peeraddr = bob.resolveAddress(relayPeeraddr, alice.peerId());
                assertNotNull(peeraddr);

                String content = bob.fetchText(peeraddr, raw.cid(), new TestEnv.DummyProgress());
                assertEquals(text, content);


                reservations = alice.reservations();
                assertFalse(reservations.isEmpty()); // should not be empty


                alice.closeReservation(reservation);
                reservations = alice.reservations();
                assertTrue(reservations.isEmpty()); // should be empty
            }
        }
    }


    @Test
    public void bobDeniedReservation() throws Throwable {

        try (Lite server = Lite.newLite()) {
            Peeraddr relayPeeraddr = server.loopbackAddress();

            int alicePort = Utils.nextFreePort();
            List<PeerId> aliceGated = new ArrayList<>();

            try (Lite alice = Lite.newBuilder().isGated(aliceGated::contains)
                    .port(alicePort).build()) {

                Reservation reservation = alice.doReservation(relayPeeraddr);
                assertNotNull(reservation);
                assertEquals(relayPeeraddr, reservation.peeraddr());

                Set<Reservation> reservations = alice.reservations();
                assertEquals(reservations.size(), 1);


                assertTrue(alice.hasReservation(server.peerId()));
                assertTrue(server.hasHopReserve(alice.peerId()));

                Lite bob = Lite.newBuilder().build();

                aliceGated.add(bob.peerId()); // bob is now gated for alice


                boolean failed = false;
                try {
                    // this will fail
                    bob.resolveAddress(relayPeeraddr, alice.peerId());
                } catch (Throwable expected) {
                    failed = true;
                }
                assertTrue(failed);

                reservations = alice.reservations();
                assertFalse(reservations.isEmpty()); // should not be empty


                alice.closeReservation(reservation);
                reservations = alice.reservations();
                assertTrue(reservations.isEmpty()); // should be empty
            }
        }
    }


    @Test
    public void reservationRefused() throws Throwable {

        try (Lite server = Lite.newLite()) {

            Peeraddr relayPeeraddr = server.loopbackAddress();

            int alicePort = Utils.nextFreePort();

            try (Lite alice = Lite.newBuilder().port(alicePort).build()) {

                Reservation reservation = alice.doReservation(relayPeeraddr);
                assertNotNull(reservation);
                assertEquals(relayPeeraddr, reservation.peeraddr());

                Set<Reservation> reservations = alice.reservations();
                assertEquals(reservations.size(), 1);

                Thread.sleep(TestEnv.SLEEP);
                boolean failed = false;
                try {
                    alice.doReservation(relayPeeraddr);
                } catch (Throwable expected) {
                    failed = true;
                }
                assertTrue(failed);

                reservations = alice.reservations();
                assertEquals(reservations.size(), 1);
                assertEquals(reservations.iterator().next(), reservation);

                alice.closeReservation(reservation);
                reservations = alice.reservations();
                assertTrue(reservations.isEmpty()); // should be empty

            }
        }
    }


    @Test
    public void refreshReservation() throws Throwable {

        try (Lite server = Lite.newLite()) {

            Peeraddr relayPeeraddr = server.loopbackAddress();

            int alicePort = Utils.nextFreePort();

            try (Lite alice = Lite.newBuilder().port(alicePort).build()) {

                Reservation reservation = alice.doReservation(relayPeeraddr);
                assertNotNull(reservation);
                assertEquals(relayPeeraddr, reservation.peeraddr());

                Set<Reservation> reservations = alice.reservations();
                assertEquals(reservations.size(), 1);

                Thread.sleep(TestEnv.SLEEP);

                Reservation refreshed = alice.refreshReservation(reservation);
                assertNotEquals(reservation, refreshed);
                assertTrue(refreshed.expireInMinutes() > reservation.expireInMinutes());

                reservations = alice.reservations();
                assertEquals(reservations.size(), 1);
                assertEquals(reservations.iterator().next(), refreshed);

                alice.closeReservation(refreshed);
                reservations = alice.reservations();
                assertEquals(reservations.size(), 0);

            }
        }
    }


    @Test
    public void reservationLost() throws Throwable {

        try (Lite server = Lite.newLite()) {

            Peeraddr relayPeeraddr = server.loopbackAddress();

            int alicePort = Utils.nextFreePort();

            try (Lite alice = Lite.newBuilder().port(alicePort).build()) {

                Reservation reservation = alice.doReservation(relayPeeraddr);
                assertNotNull(reservation);
                assertEquals(relayPeeraddr, reservation.peeraddr());

                Set<Reservation> reservations = alice.reservations();
                assertEquals(reservations.size(), 1);


                // get the connection with the peerId from alice from the lite server and close it
                PeerId alicePeerId = alice.peerId();
                assertNotNull(alicePeerId);

                assertTrue(alice.hasReservation(server.peerId()));
                assertTrue(server.hasHopReserve(alicePeerId));

                server.closeConnections(alicePeerId);


                Thread.sleep(TestEnv.SLEEP); // takes time to be recognized

                // now check that the reservation is not returned by the aliceServer
                reservations = alice.reservations();
                assertEquals(reservations.size(), 0);

            }
        }
    }


    @Test
    public void twoReservation() throws Throwable {

        try (Lite server = Lite.newLite()) {

            Peeraddr relayPeeraddr = server.loopbackAddress();

            int alicePort = Utils.nextFreePort();

            try (Lite alice = Lite.newBuilder().port(alicePort).build()) {

                int bobPort = Utils.nextFreePort();

                try (Lite bob = Lite.newBuilder().port(bobPort).build()) {

                    Reservation reservationAlice = alice.doReservation(relayPeeraddr);
                    assertNotNull(reservationAlice);
                    assertEquals(relayPeeraddr, reservationAlice.peeraddr());

                    Set<Reservation> reservations = alice.reservations();
                    assertEquals(reservations.size(), 1);


                    Reservation reservationBob = bob.doReservation(relayPeeraddr);
                    assertNotNull(reservationBob);
                    assertEquals(relayPeeraddr, reservationBob.peeraddr());

                    reservations = bob.reservations();
                    assertEquals(reservations.size(), 1);

                    // now closing
                    alice.closeReservation(reservationAlice);
                    reservations = alice.reservations();
                    assertTrue(reservations.isEmpty()); // should be empty


                    bob.closeReservation(reservationBob);
                    reservations = alice.reservations();
                    assertTrue(reservations.isEmpty()); // should be empty

                }
            }
        }
    }


    @Test(expected = ConnectException.class)
    public void limitedReservation() throws Throwable {

        // limit should fail for reservation
        try (Lite server = Lite.newBuilder().
                limit(new Limit(10, 10)).build()) {

            Peeraddr relayPeeraddr = server.loopbackAddress();

            int alicePort = Utils.nextFreePort();

            try (Lite alice = Lite.newBuilder().port(alicePort).build()) {

                Reservation reservation = alice.doReservation(relayPeeraddr);
                assertNotNull(reservation);
                assertEquals(relayPeeraddr, reservation.peeraddr());

                Set<Reservation> reservations = alice.reservations();
                assertEquals(reservations.size(), 1);


                Lite bob = Lite.newBuilder().build();

                try {
                    // this should fail
                    bob.resolveAddress(relayPeeraddr, alice.peerId());
                    fail();
                } finally {
                    // reservation is still valid
                    reservations = alice.reservations();
                    assertFalse(reservations.isEmpty()); // should not be empty

                    alice.closeReservation(reservation);
                    reservations = alice.reservations();
                    assertTrue(reservations.isEmpty()); // should be empty
                }
            }
        }
    }

    @Test(expected = ConnectException.class)
    public void serverShutdown() throws Throwable {

        try (Lite server = Lite.newLite()) {

            Peeraddr relayPeeraddr = server.loopbackAddress();

            int alicePort = Utils.nextFreePort();

            try (Lite alice = Lite.newBuilder().port(alicePort).build()) {
                Reservation reservation = alice.doReservation(relayPeeraddr);
                assertNotNull(reservation);
                assertEquals(relayPeeraddr, reservation.peeraddr());

                Set<Reservation> reservations = alice.reservations();
                assertEquals(reservations.size(), 1);

                alice.close(); // server shutdown

                // this is only to make the test run (the shutdown is not detected by the
                // reservation connection (because shutdown does not send anything)
                // but an refresh of the reservation will help
                alice.refreshReservation(reservation);
                fail(); // should never reach this point

            }
        }
    }
}
