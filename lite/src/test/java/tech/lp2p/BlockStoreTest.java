package tech.lp2p;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.FileStore;
import tech.lp2p.core.Raw;

public class BlockStoreTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void testString() throws Exception {

        try (FileStore blockStore = new FileStore()) {
            String text = "Hello Moin und Zehn Elf";
            Raw raw = blockStore.storeText(text);
            assertNotNull(raw);

            byte[] result = blockStore.fetchData(raw.cid());
            assertNotNull(result);
            assertEquals(text, new String(result));
        }
    }
}
