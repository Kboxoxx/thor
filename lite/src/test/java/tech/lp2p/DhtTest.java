package tech.lp2p;


import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Key;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.dht.DhtService;
import tech.lp2p.quic.ConnectionBuilder;

public class DhtTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }


    @Test
    public void providers() throws Throwable {

        try (Lite server = Lite.newLite()) {

            Peeraddr peeraddr = server.loopbackAddress();

            try (Lite client = Lite.newLite()) {

                Connection connection = ConnectionBuilder.connect(client, ALPN.libp2p,
                        peeraddr, true);

                Cid cid = Lite.rawCid("test");
                Key key = cid.createKey();

                // should return empty list (server does not support GET_PROVIDERS)
                Peeraddrs providers = DhtService.providers(connection, key);
                assertNotNull(providers);
                assertTrue(providers.isEmpty());

                connection.close();
            }
        }
    }

    @Test
    public void findPeer() throws Throwable {

        try (Lite server = Lite.newLite()) {
            Peeraddr peeraddr = server.loopbackAddress();

            try (Lite client = Lite.newLite()) {

                Connection connection = ConnectionBuilder.connect(client, ALPN.libp2p,
                        peeraddr, true);
                PeerId peerId = TestEnv.random();
                Peeraddrs peeraddrs = DhtService.findPeeraddrs(connection, peerId.createKey());
                assertTrue(peeraddrs.isEmpty()); // peerId not inside the DHT
                connection.close();
            }
        }
    }
}
