package tech.lp2p;

import static org.junit.Assert.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.InetAddress;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Identify;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.ident.IdentifyService;
import tech.lp2p.quic.ConnectionBuilder;


public class DnsAddressTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void testDnsBootstrap() throws Exception {

        // Multiaddr dns : ipfs.gecko.network.
        // Multiaddr DNS 12D3KooWCWaqYh2EnR1yy9rZ9rtBWCWVhQnvP4mqGbKrQ3tUntPB port4001
        if (!TestEnv.hasNetwork()) {
            return;
        }

        try (Lite lite = Lite.newLite()) {

            PeerId peerId = Lite.decodePeerId("12D3KooWCWaqYh2EnR1yy9rZ9rtBWCWVhQnvP4mqGbKrQ3tUntPB");
            InetAddress inetAddress = InetAddress.getByName("ipfs.gecko.network.");
            Peeraddr peeraddr = Lite.createPeeraddr(peerId, inetAddress, 4001);

            Connection connection = ConnectionBuilder.connect(lite, ALPN.libp2p,
                    peeraddr, true);
            assertNotNull(connection);

            Identify identify = IdentifyService.identify(connection);
            assertNotNull(identify);

            TestEnv.always(identify.toString());

            connection.close();
        }
    }
}
