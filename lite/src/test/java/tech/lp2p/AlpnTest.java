package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Info;
import tech.lp2p.core.Peeraddr;


public class AlpnTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void alpnTest() throws Exception {

        try (Lite server = Lite.newLite()) {

            BlockStore blockStore = server.blockStore();

            Info info = blockStore.createEmptyDirectory("Home");
            blockStore.storeBlock(server.peerId(), info.cid().hash());


            Peeraddr peeraddr = server.loopbackAddress();

            byte[] input = TestEnv.getRandomBytes(10000000); // 10 MB

            Fid fid = TestEnv.createContent(blockStore, "random.bin", input);
            assertNotNull(fid);

            byte[] cmp = blockStore.fetchData(fid.cid());
            assertArrayEquals(input, cmp);

            List<Cid> cids = blockStore.blocks(fid.cid());
            assertNotNull(cids);

            try (Lite client = Lite.newLite()) {

                Optional<Cid> optional = client.fetchRoot(peeraddr);
                assertTrue(optional.isPresent());
                Cid value = optional.get();
                assertEquals(value, info.cid());

                byte[] output = client.fetchData(peeraddr, fid.cid(),
                        new TestEnv.DummyProgress());
                assertArrayEquals(input, output);

            }
        }
    }
}
