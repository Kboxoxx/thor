package tech.lp2p;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.Duration;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Header;
import tech.lp2p.core.Headers;
import tech.lp2p.core.Method;
import tech.lp2p.core.MimeType;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Request;
import tech.lp2p.core.Response;
import tech.lp2p.core.Status;

public class ExamplesTest {

    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }


    @Test
    public void simpleRequestResponse() throws Exception {

        // create server instance with default values
        try (Lite server = Lite.newLite()) {

            BlockStore blockStore = server.blockStore();

            Raw raw = blockStore.storeText("Moin"); // store some text

            Peeraddr peeraddr = server.loopbackAddress();

            try (Lite client = Lite.newLite()) { // client instance default values
                String text = client.fetchText(peeraddr, raw); // fetch request
                assertEquals(text, "Moin");
            }
        } // this closes all connections and the running serever
    }


    @Test
    public void enhanceServer() throws Exception {

        // create a new lite instance with default settings
        // start the server on random port
        try (Lite server = Lite.newLite()) {

            // enhance the server with "/version" path request handler
            String protocol = "/version";
            server.addRequestHandler(protocol, request -> {
                String path = request.path();
                assertEquals(path, protocol);

                Method method = request.method();
                assertEquals(method, Method.GET); // method GET expected

                // print out the headers only (no reaction on the header settings)
                TestEnv.debug(request.headers().toString());

                // check peeraddr is not null
                assertNotNull(request.peeraddr());

                // Now prepare the result
                byte[] data = "Version 1.0.0".getBytes(); // result body

                // header for the response
                Headers headers = Response.createHeaders();
                headers.add(Header.CONTENT_TYPE.name(), MimeType.OCTET_MIME_TYPE.name());
                headers.add(Header.CONTENT_LENGTH.name(), String.valueOf(data.length));

                return Response.create(Status.OK, headers, data);
            });


            // create a client
            try (Lite client = Lite.newLite()) {

                // prepare the request [Important the protocol is in the URI path)
                Peeraddr loopbackAddress = server.loopbackAddress();

                Request request = Request.get(loopbackAddress, protocol,
                        Duration.ofSeconds(3), false, 128);
                Response response = client.send(request);

                // check return values
                byte[] data = response.body();
                assertEquals(new String(data), "Version 1.0.0");

                assertEquals(response.status(), Status.OK);
                assertFalse(response.headers().firstValue(Header.CONTENT_TYPE.name()).isEmpty());
                assertFalse(response.headers().firstValue(Header.CONTENT_LENGTH.name()).isEmpty());
            }
        }
    }
}
