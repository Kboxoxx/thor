package tech.lp2p;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;

import tech.lp2p.core.Key;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.dht.DhtPeer;
import tech.lp2p.dht.DhtPeers;


public class DhtPeersTest {

    @Test
    public void saturationModeNotReplaced() throws UnknownHostException {
        DhtPeers dhtPeers = new DhtPeers(2);
        assertNotNull(dhtPeers);

        Key key = Key.convertKey("Moin".getBytes());
        dhtPeers.enhance(random(key));
        dhtPeers.enhance(random(key));
        assertEquals(dhtPeers.size(), 2);

        dhtPeers.enhance(perfect(key));
        assertTrue(dhtPeers.size() >= 2); // can be three
    }

    @Test
    public void saturationModeReplaced() throws UnknownHostException {
        DhtPeers dhtPeers = new DhtPeers(2);
        assertNotNull(dhtPeers);

        Key key = Key.convertKey("Moin".getBytes());
        DhtPeer a = random(key);
        dhtPeers.enhance(a);
        DhtPeer b = random(key);
        dhtPeers.enhance(b);
        assertEquals(dhtPeers.size(), 2);

        a.done();
        b.done();

        dhtPeers.enhance(perfect(key));
        assertEquals(dhtPeers.size(), 2); // can be three
    }

    @Test
    public void random() throws UnknownHostException {
        DhtPeers dhtPeers = new DhtPeers(2);
        assertNotNull(dhtPeers);

        Key key = Key.convertKey("Moin".getBytes());
        for (int i = 0; i < 20; i++) {
            DhtPeer a = random(key);
            dhtPeers.enhance(a);
            a.done();
        }

        dhtPeers.enhance(perfect(key));
        assertEquals(dhtPeers.size(), 2);
    }


    @Test
    public void fill() throws UnknownHostException {
        DhtPeers dhtPeers = new DhtPeers(2);
        assertNotNull(dhtPeers);

        Key key = Key.convertKey("Moin".getBytes());
        for (int i = 0; i < 20; i++) {
            DhtPeer a = random(key);
            dhtPeers.fill(a);
            a.done();
        }

        assertEquals(dhtPeers.size(), 2);


        // check order
        DhtPeer first = dhtPeers.first();
        assertNotNull(first);
        DhtPeer last = dhtPeers.last();
        assertNotNull(last);

        int res = first.compareTo(last);
        // if value is negative first is closer to key (which is the way)
        assertTrue(res < 0);
    }


    private DhtPeer random(Key key) throws UnknownHostException {
        PeerId random = TestEnv.random();
        Peeraddr peeraddr = Lite.createPeeraddr(random,
                InetAddress.getByName("139.178.68.146"), 4001);

        return DhtPeer.create(peeraddr, false, key);
    }

    private DhtPeer perfect(Key key) throws UnknownHostException {
        PeerId random = PeerId.toPeerId(key.hash());
        Peeraddr peeraddr = Lite.createPeeraddr(random,
                InetAddress.getByName("139.178.68.146"), 4001);
        return DhtPeer.create(peeraddr, false, key);
    }
}
