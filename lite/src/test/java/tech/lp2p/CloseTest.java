package tech.lp2p;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Optional;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Info;
import tech.lp2p.core.Peeraddr;


public class CloseTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void closeConnect() throws Exception {

        try (Lite server = Lite.newLite()) {

            BlockStore blockStore = server.blockStore();

            Info info = blockStore.createEmptyDirectory("Home");
            server.blockStore().storeBlock(server.peerId(), info.cid().hash());

            Peeraddr peeraddr = server.loopbackAddress();


            try (Lite client = Lite.newLite()) {
                Optional<Cid> optional = client.fetchRoot(peeraddr);
                assertTrue(optional.isPresent());
                Cid value = optional.get();
                assertEquals(value, info.cid());
            }

            assertFalse(server.hasConnection(server.peerId()));
        }

    }
}
