package tech.lp2p;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Optional;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Info;
import tech.lp2p.core.Peeraddr;

public class FetchCidStressTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void fetchCidIterations() throws Exception {
        try (Lite server = Lite.newLite()) {

            Info info = server.blockStore().createEmptyDirectory("Home");
            server.blockStore().storeBlock(server.peerId(), info.cid().hash());


            Peeraddr peeraddr = server.loopbackAddress();

            try (Lite client = Lite.newLite()) {
                for (int i = 0; i < TestEnv.ITERATIONS; i++) {
                    Optional<Cid> cid = client.fetchRoot(peeraddr);
                    assertTrue(cid.isPresent());
                    Cid value = cid.get();
                    assertEquals(value, info.cid());
                }
            }

        }

    }
}
