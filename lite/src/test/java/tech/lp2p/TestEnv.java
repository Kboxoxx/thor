package tech.lp2p;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Fid;
import tech.lp2p.core.FileStore;
import tech.lp2p.core.Keys;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Progress;
import tech.lp2p.core.TrustManager;


final class TestEnv {


    public static final int ITERATIONS = Lite.CHUNK_DEFAULT;
    public static final Peeraddrs BOOTSTRAP = new Peeraddrs();
    public static final long SLEEP = 400;
    private static final boolean DEBUG = false;

    private static final ReentrantLock reserve = new ReentrantLock();
    private static volatile Lite INSTANCE = null;

    static {
        try {
            // "/ip4/104.131.131.82/udp/4001/quic/p2p/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ"
            BOOTSTRAP.add(Lite.createPeeraddr(
                    "QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ",
                    "104.131.131.82", 4001));
        } catch (Throwable throwable) {
            TestEnv.error(throwable);
        }
    }

    public static Lite getInstance() throws Exception {
        if (INSTANCE == null) {
            synchronized (Lite.class) {
                if (INSTANCE == null) {
                    INSTANCE = Lite.newBuilder().
                            bootstrap(BOOTSTRAP).
                            blockStore(new FileStore()).
                            trustManager(new TrustManager()).
                            connectionGain(peeraddr -> TestEnv.error("Incoming connection : "
                                    + peeraddr.toString())).
                            connectionLost(peeraddr -> TestEnv.error("Lost connection : "
                                    + peeraddr.toString())).
                            reservationGain(reservation -> always("Reservation gain " +
                                    reservation.toString())).
                            reservationLost(reservation -> always("Reservation lost " +
                                    reservation.toString())).build();

                    Runtime.getRuntime().addShutdownHook(new Thread(INSTANCE::close));
                    if (Lite.reservationFeaturePossible()) {
                        INSTANCE.doReservation(25, 120);

                        Peeraddrs peeraddrs = INSTANCE.reservationPeeraddrs();
                        for (Peeraddr addr : peeraddrs) {
                            TestEnv.error("Reservation Address " + addr.toString());
                        }
                    }
                }
            }
        }
        return INSTANCE;
    }

    public static PeerId random() {
        try {
            Keys keys = Lite.generateKeys();
            return keys.peerId();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    static File createCacheFile() throws IOException {
        return Files.createTempFile("temp", ".cid").toFile();
    }

    public static byte[] getRandomBytes(int number) {
        byte[] bytes = new byte[number];
        new Random().nextBytes(bytes);
        return bytes;
    }

    public static Fid createContent(BlockStore blockStore, String name, byte[] data) throws Exception {
        try (InputStream inputStream = new ByteArrayInputStream(data)) {
            return blockStore.storeInputStream(name, inputStream);
        }
    }

    /**
     * @noinspection BooleanMethodIsAlwaysInverted
     */
    public static boolean hasNetwork() {
        return Lite.reservationFeaturePossible(); // not 100 percent correct
    }

    public static Lite getTestInstance() throws Exception {
        reserve.lock();
        try {
            Lite server = TestEnv.getInstance();
            TestEnv.always("Current number of connections " + server.numConnections());
            return server;
        } finally {
            reserve.unlock();
        }
    }

    public static void cleanup() {
        try {
            FileStore.deleteDirectory(FileStore.getTempDirectory());
        } catch (Throwable throwable) {
            TestEnv.error(throwable);
        }
    }

    static void error(String message) {
        if (DEBUG) {
            System.err.println(message);
        }
    }

    static void error(Throwable throwable) {
        throwable.printStackTrace(System.err);
    }

    static void always(String message) {
        System.err.println(message);
    }

    static void debug(String message) {
        if (DEBUG) {
            System.out.println(message);
        }
    }

    public static boolean supportLongRunningTests() {
        return DEBUG;
    }

    public static class DummyProgress implements Progress {
        @Override
        public void setProgress(int progress) {
            TestEnv.error("Progress " + progress);
        }
    }
}
