package threads.thor;


import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.ident.IdentifyService;
import tech.lp2p.quic.ConnectionBuilder;
import threads.thor.model.API;


public class BootstrapTest {
    private static final String TAG = BootstrapTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_bootstrap() throws Exception {

        Lite lite = API.getInstance(context).lite();

        for (Peeraddr peeraddr : lite.bootstrap()) {
            LogUtils.error(TAG, "Routing Peer " + peeraddr.toString());

            Connection connection;
            try {
                connection = ConnectionBuilder.connect(lite, ALPN.libp2p, peeraddr,
                        false);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, "Connection failed " +
                        throwable.getClass().getSimpleName());
                continue;
            }
            try {
                assertNotNull(connection);
                Identify info = IdentifyService.identify(connection);
                assertNotNull(info);
                LogUtils.error(TAG, info.toString());
            } catch (Throwable throwable) {
                LogUtils.error(TAG, "PeerInfo failed " +
                        throwable.getClass().getSimpleName());
            }

        }
    }

    @Test
    public void test_bootstrap_peers() {

        Peeraddrs bootstrap = API.bootstrap();
        assertNotNull(bootstrap);


        for (Peeraddr address : bootstrap) {
            LogUtils.error(TAG, "Bootstrap " + address.toString());
        }
        assertTrue(bootstrap.size() >= 4);
    }

}
