package threads.thor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import threads.thor.model.API;

public class DocsTest {
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void parseContentDisposition() throws Exception {
        API api = API.getInstance(context);
        assertNotNull(api);
        String filename = API.parseContentDisposition("attachment; filename=\"README.md\"; filename=\"UTF-8 README.md\"");
        assertEquals(filename, "README.md");
    }

    @Test
    public void testTags() {
        API.Tags tags = new API.Tags("a", "b", 3);
        String test = tags.encoded();
        assertNotNull(test);
        API.Tags cmp = API.Tags.decode(test);
        assertNotNull(cmp);
        assertEquals(tags, cmp);
    }
}
