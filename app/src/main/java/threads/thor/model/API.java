package threads.thor.model;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.ArrayMap;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.TypeConverter;
import androidx.work.Data;
import androidx.work.WorkInfo;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tech.lp2p.Lite;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Dir;
import tech.lp2p.core.FileStore;
import tech.lp2p.core.Header;
import tech.lp2p.core.Info;
import tech.lp2p.core.MimeType;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.ResolveInfo;
import tech.lp2p.core.Scheme;
import tech.lp2p.utils.Utils;
import threads.magnet.magnet.MagnetUri;
import threads.magnet.magnet.MagnetUriParser;
import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.data.peers.PEERS;
import threads.thor.utils.MimeTypeService;

public class API {

    public static final String WORK_TAG = "Thor Works";
    public static final String WORK_FAILURE = "Failure";
    public static final String FILE_INFO = "FI:";
    public static final String APP_KEY = "AppKey";
    public static final String INDEX_HTML = "index.html";
    public static final int CLICK_OFFSET = 500;

    public static final String TAB = "tab";
    public static final String DOWNLOAD_URI = "uri";
    public static final String DOWNLOAD_NAME = "name";
    public static final String DOWNLOAD_SIZE = "size";
    public static final String DOWNLOAD_TYPE = "type";
    private static final String NO_NAME = "download-file.bin";
    private static final String JAVASCRIPT_KEY = "javascriptKey";
    private static final String HOMEPAGE_KEY = "homepageKey";
    private static final String SEARCH_ENGINE_KEY = "searchEngineKey";

    private static final Engine DUCKDUCKGO = new Engine("DuckDuckGo",
            Uri.parse("https://start.duckduckgo.com/"), "https://duckduckgo.com/?q=");
    private static final Engine GOOGLE = new Engine("Google",
            Uri.parse("https://www.google.com/"), "https://www.google.com/search?q=");
    private static final Engine ECOSIA = new Engine("Ecosia",
            Uri.parse("https://www.ecosia.org/"), "https://www.ecosia.org/search?q=");
    private static final Engine BING = new Engine("Bing",
            Uri.parse("https://www.bing.com/"), "https://www.bing.com/search?q=");

    @NonNull
    private static final Map<String, Engine> SEARCH_ENGINES = Map.of(
            DUCKDUCKGO.name, DUCKDUCKGO,
            GOOGLE.name, GOOGLE,
            ECOSIA.name, ECOSIA,
            BING.name, BING);

    private static final String TAG = API.class.getSimpleName();
    private static final int QR_CODE_SIZE = 250;
    @NonNull
    private static final ConcurrentHashMap<PeerId, Cid> resolves = new ConcurrentHashMap<>();
    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;
    private static final Pattern CONTENT_DISPOSITION_PATTERN =
            Pattern.compile("attachment;\\s*filename\\s*=\\s*(\"?)([^\"]*)\\1(\"?);",
                    Pattern.CASE_INSENSITIVE);
    private static volatile API INSTANCE = null;

    @NonNull
    private final ConcurrentHashMap<PeerId, Peeraddr> swarm = new ConcurrentHashMap<>();
    @NonNull
    private final ConcurrentHashMap<PeerId, ExecutorService> services = new ConcurrentHashMap<>();

    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final Lite lite;

    private API(@NonNull Context context) throws Exception {
        PEERS peers = PEERS.getInstance(context);
        blockStore = new FileStore(context.getFilesDir());
        lite = Lite.newBuilder().bootstrap(bootstrap()).
                blockStore(blockStore).
                peerStore(peers).build();

    }

    public static void setHomepage(Context context, @Nullable String uri) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(HOMEPAGE_KEY, uri);
        editor.apply();
    }

    @Nullable
    public static String getHomepage(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getString(HOMEPAGE_KEY, null);
    }

    public static void setJavascriptEnabled(Context context, boolean auto) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(JAVASCRIPT_KEY, auto);
        editor.apply();
    }

    public static boolean isJavascriptEnabled(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(JAVASCRIPT_KEY, true);

    }

    public static String getSearchEngine(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getString(SEARCH_ENGINE_KEY, "DuckDuckGo");
    }

    public static void setSearchEngine(@NonNull Context context, @NonNull String searchEngine) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(SEARCH_ENGINE_KEY, searchEngine);
        editor.apply();
    }

    @SuppressLint("SetJavaScriptEnabled")
    public static void setWebSettings(@NonNull WebView webView, boolean enableJavascript) {


        WebSettings settings = webView.getSettings();
        settings.setUserAgentString("Mozilla/5.0 (Linux; Android " + Build.VERSION.RELEASE + ")");

        settings.setJavaScriptEnabled(enableJavascript);
        settings.setJavaScriptCanOpenWindowsAutomatically(false);


        settings.setSafeBrowsingEnabled(true);
        settings.setAllowContentAccess(false);
        settings.setAllowFileAccess(true);    // set to true for mht files
        settings.setLoadsImagesAutomatically(true);
        settings.setBlockNetworkLoads(false);
        settings.setBlockNetworkImage(false);
        settings.setDomStorageEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        settings.setDatabaseEnabled(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        settings.setMixedContentMode(WebSettings.MIXED_CONTENT_NEVER_ALLOW);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setMediaPlaybackRequiresUserGesture(false); // set to false, required for camera permission
        settings.setSupportMultipleWindows(false);
        settings.setGeolocationEnabled(false);
    }

    @NonNull
    public static ArrayMap<String, Engine> searchEngines() {
        ArrayMap<String, Engine> map = new ArrayMap<>();
        map.putAll(SEARCH_ENGINES);
        return map;
    }

    @NonNull
    public static Engine getEngine(@NonNull Context context) {
        String searchEngine = getSearchEngine(context);
        Engine engine = SEARCH_ENGINES.get(searchEngine);
        if (engine == null) {
            return DUCKDUCKGO;
        }
        return engine;
    }

    public static byte[] bytes(@Nullable Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        Bitmap.Config config = Objects.requireNonNull(bitmap.getConfig());
        Bitmap copy = bitmap.copy(config, true);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        copy.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        copy.recycle();
        return byteArray;
    }

    public static API getInstance(@NonNull Context context) throws Exception {

        if (INSTANCE == null) {
            synchronized (API.class) {
                if (INSTANCE == null) {
                    INSTANCE = new API(context);
                }
            }
        }
        return INSTANCE;
    }

    @NonNull
    public static String getFileName(@NonNull Uri uri) {
        List<String> paths = uri.getPathSegments();
        if (!paths.isEmpty()) {
            return paths.get(paths.size() - 1);
        } else {
            return NO_NAME;
        }
    }


    public static Uri min(Uri uri, Uri child) {
        if (uri == null) {
            return child;
        } else {
            if (child != null) {
                if (uri.getPathSegments().size() >
                        child.getPathSegments().size()) {
                    return child;
                }
            }
        }
        return uri;
    }

    @NonNull
    public static String getUriTitle(@NonNull Uri uri) {
        String scheme = uri.getScheme();
        if (Objects.equals(scheme, Scheme.magnet.name())) {
            MagnetUri magnetUri = MagnetUriParser.parse(uri.toString());

            String name = uri.toString();
            if (magnetUri.getDisplayName().isPresent()) {
                name = magnetUri.getDisplayName().get();
            }
            return name;
        } else if (Objects.equals(scheme, Scheme.pns.name())) {
            List<String> paths = uri.getPathSegments();
            if (!paths.isEmpty()) {
                return paths.get(paths.size() - 1);
            } else {
                String host = uri.getHost();
                if (host != null && !host.isBlank()) {
                    return host;
                } else {
                    return "";
                }
            }
        } else {
            String host = uri.getHost();
            if (host != null && !host.isBlank()) {
                return host;
            } else {
                List<String> paths = uri.getPathSegments();
                if (!paths.isEmpty()) {
                    return paths.get(paths.size() - 1);
                } else {
                    return "";
                }
            }
        }
    }

    @DrawableRes
    public static int getImageResource(@NonNull Uri uri) {
        if (Objects.equals(uri.getScheme(), Scheme.file.name())) {
            return R.drawable.bookmark_mht; // it can only be mht right now
        } else if (Objects.equals(uri.getScheme(), Scheme.pns.name())) {
            return R.drawable.bookmark_pns;
        } else {
            return R.drawable.bookmark;
        }
    }

    public static Bitmap getBitmap(@NonNull String content) {
        try {
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix bitMatrix = qrCodeWriter.encode(content, BarcodeFormat.QR_CODE,
                    QR_CODE_SIZE, QR_CODE_SIZE);
            return createBitmap(bitMatrix);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public static Bitmap createBitmap(BitMatrix matrix) {
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = matrix.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    public static WebResourceResponse createEmptyResource() {
        return new WebResourceResponse(MimeType.PLAIN_MIME_TYPE.name(),
                StandardCharsets.UTF_8.name(), new ByteArrayInputStream("".getBytes()));
    }

    public static WebResourceResponse createErrorMessage(@NonNull Throwable throwable) {
        LogUtils.error(TAG, throwable);
        String message = throwable.getMessage();
        if (message == null || message.isEmpty()) {
            message = throwable.getClass().getSimpleName();
        }
        String report = generateErrorHtml(message);
        return new WebResourceResponse(MimeType.HTML_MIME_TYPE.name(),
                StandardCharsets.UTF_8.name(), new ByteArrayInputStream(report.getBytes()));
    }

    public static WebResourceResponse createErrorMessage(@NonNull String message) {
        String report = generateErrorHtml(message);
        return new WebResourceResponse(MimeType.HTML_MIME_TYPE.name(),
                StandardCharsets.UTF_8.name(), new ByteArrayInputStream(report.getBytes()));
    }

    private static String generateErrorHtml(@NonNull String message) {
        return "<html>" + "<head>" + MimeTypeService.META +
                "<title>" + "Error" + "</title>" +
                "</head>\n" + MimeTypeService.STYLE +
                "<body><div <div>" + message + "</div></body></html>";
    }


    @Nullable
    private static String getHost(@NonNull Uri uri) {
        try {
            if (Objects.equals(uri.getScheme(), Scheme.pns.name())) {
                return uri.getHost();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return null;
    }

    private static void addResolves(@NonNull PeerId peerId, @NonNull Cid root) {
        resolves.put(peerId, root);
    }

    public static WebResourceResponse createRedirectMessage(@NonNull Uri uri) {
        return new WebResourceResponse(MimeType.HTML_MIME_TYPE.name(),
                StandardCharsets.UTF_8.name(), new ByteArrayInputStream(("<!DOCTYPE HTML>\n" +
                "<html lang=\"en-US\">\n" +
                "    <head>\n" + MimeTypeService.META +
                "        <meta http-equiv=\"refresh\" content=\"0; url=" + uri + "\">\n" +
                "        <title>Page Redirection</title>\n" +
                "    </head>\n" + MimeTypeService.STYLE +
                "    <body>\n" +
                "        Automatically redirected to the <a style=\"word-wrap: break-word;\" href='" + uri + "'>" + uri + "</a> location\n" +
                "</html>").getBytes()));
    }

    private static String generateDirectoryHtml(@NonNull BlockStore blockStore,
                                                @NonNull Cid root,
                                                @NonNull Uri uri,
                                                @NonNull List<String> paths,
                                                @Nullable List<Info> infos) {
        String title = "";

        try {
            title = blockStore.info(root).name();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        if (paths.isEmpty()) {
            if (title == null || title.isBlank()) {
                title = uri.getHost();
            }
        } else {
            title = paths.get(paths.size() - 1); // get last
        }


        StringBuilder answer = new StringBuilder("<html>" + "<head>" + MimeTypeService.META +
                "<title>" + title + "</title>");

        answer.append("</head>");
        answer.append(MimeTypeService.STYLE);
        answer.append("<body>");

        if (infos != null) {
            if (!infos.isEmpty()) {
                answer.append("<form><table  width=\"100%\" style=\"border-spacing: 8px;\">");

                // folder up
                if (!paths.isEmpty()) {
                    Uri.Builder builder = new Uri.Builder();
                    builder.scheme(uri.getScheme()).authority(uri.getAuthority());
                    for (int i = 0; i < paths.size() - 1; i++) {
                        String path = paths.get(i);
                        builder.appendPath(path);
                    }

                    Uri linkUri = builder.build();
                    answer.append("<tr>");
                    answer.append("<td align=\"center\">");
                    answer.append("<a href=\"");
                    answer.append(linkUri.toString());
                    answer.append("\">");
                    answer.append(MimeTypeService.FOLDER_UP);
                    answer.append("</a>");

                    answer.append("<td width=\"100%\" style=\"word-break:break-word\">");
                    answer.append("..");
                    answer.append("</td>");
                    answer.append("<td/>");
                    answer.append("<td/>");
                    answer.append("</td>");
                    answer.append("</tr>");
                }


                for (Info info : infos) {

                    Uri.Builder builder = new Uri.Builder();
                    builder.scheme(uri.getScheme()).authority(uri.getAuthority());
                    for (String path : paths) {
                        builder.appendPath(path);
                    }
                    builder.appendPath(info.name());
                    builder.appendQueryParameter("download", "0");
                    Uri linkUri = builder.build();
                    answer.append("<tr>");

                    answer.append("<td>");
                    answer.append(MimeTypeService.getSvgResource(info));
                    answer.append("</td>");

                    answer.append("<td width=\"100%\" style=\"word-break:break-word\">");
                    answer.append("<a href=\"");
                    answer.append(linkUri.toString());
                    answer.append("\">");
                    answer.append(info.name());
                    answer.append("</a>");
                    answer.append("</td>");

                    answer.append("<td>");
                    answer.append(getFileSize(info.size()));
                    answer.append("</td>");

                    answer.append("<td align=\"center\">");
                    String text = "<button style=\"float:none!important;display:inline;\" " +
                            "name=\"download\" value=\"1\" formenctype=\"text/plain\" " +
                            "formmethod=\"get\" type=\"submit\" formaction=\"" +
                            linkUri + "\">" + MimeTypeService.getSvgDownload() + "</button>";
                    answer.append(text);
                    answer.append("</td>");
                    answer.append("</tr>");
                }
                answer.append("</table></form>");
            }

        }

        ZonedDateTime zoned = ZonedDateTime.now();
        DateTimeFormatter pattern = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);

        answer.append("</body><div class=\"footer\">")
                .append("<p>")
                .append(zoned.format(pattern))
                .append("</p>")
                .append("</div></html>");


        return answer.toString();
    }

    private static String getFileSize(long size) {

        String fileSize;

        if (size < 1000) {
            fileSize = String.valueOf(size);
            return fileSize.concat(" B");
        } else if (size < 1000 * 1000) {
            fileSize = String.valueOf((double) (size / 1000));
            return fileSize.concat(" KB");
        } else {
            fileSize = String.valueOf((double) (size / (1000 * 1000)));
            return fileSize.concat(" MB");
        }
    }

    public static void cleanupResolver(@NonNull Uri uri) {
        try {
            if (Objects.equals(uri.getScheme(), Scheme.pns.name())) {
                String host = getHost(uri);
                if (host != null) {
                    PeerId peerId = Lite.decodePeerId(host);
                    resolves.remove(peerId);
                }
            }
        } catch (Throwable ignore) {
            // ignore common failure
        }
    }

    @NonNull
    public static Peeraddrs bootstrap() {
        // "/ip4/104.131.131.82/udp/4001/quic/p2p/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ"
        Peeraddrs peeraddrs = new Peeraddrs();
        try {
            peeraddrs.add(Lite.createPeeraddr(
                    "QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ",
                    "104.131.131.82", 4001));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return peeraddrs;
    }

    @Nullable
    public static Uri downloadsUri(Context context, String mimeType, String name, String path) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.Downloads.DISPLAY_NAME, name);
        contentValues.put(MediaStore.Downloads.MIME_TYPE, mimeType);
        contentValues.put(MediaStore.Downloads.RELATIVE_PATH, path);

        ContentResolver contentResolver = context.getContentResolver();
        return contentResolver.insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, contentValues);
    }

    @Nullable
    public static String parseContentDisposition(String contentDisposition) {
        try {
            Matcher m = CONTENT_DISPOSITION_PATTERN.matcher(contentDisposition);
            if (m.find()) {
                return m.group(2);
            }
        } catch (IllegalStateException ex) {
            // This function is defined as returning null when it can't parse the header
        }
        return null;
    }

    public static boolean downloadActive(@Nullable String url) {
        if (url != null && !url.isEmpty()) {
            Uri uri = Uri.parse(url);
            return Objects.equals(uri.getScheme(), Scheme.pns.name()) ||
                    Objects.equals(uri.getScheme(), Scheme.http.name()) ||
                    Objects.equals(uri.getScheme(), Scheme.https.name());
        }
        return false;
    }

    public static boolean hasRunningWork(List<WorkInfo> workInfos) {
        for (WorkInfo workInfo : workInfos) {
            if (workInfo.getState() == WorkInfo.State.RUNNING) {
                return true;
            }
        }
        return false;
    }

    @NonNull
    @TypeConverter
    public static PeerId toPeerId(byte[] data) {
        return PeerId.toPeerId(data);
    }

    @NonNull
    @TypeConverter
    public static byte[] toArray(PeerId peerId) {
        return PeerId.toArray(peerId);
    }

    public static void storeRelay(Context context, PeerId peerId, @Nullable Peeraddr relay) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        Base64.Encoder encoder = Base64.getEncoder();
        if (relay != null) {
            editor.putString(peerId.toBase36(), encoder.encodeToString(relay.encoded()));
        } else {
            editor.putString(peerId.toBase36(), null);
        }
        editor.apply();
    }

    @Nullable
    public static Peeraddr getRelay(Context context, PeerId peerId) {
        try {
            SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
            String relay = sharedPref.getString(peerId.toBase36(), null);
            if (relay != null) {
                Base64.Decoder decoder = Base64.getDecoder();
                return Peeraddr.create(peerId, decoder.decode(relay));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return null;
    }

    public static void cleanData(@NonNull Context context) {
        deleteFile(context.getFilesDir(), false);
    }

    public static void cleanCache(@NonNull Context context) {
        deleteFile(context.getCacheDir(), false);
    }

    public static void deleteFile(@NonNull File file, boolean deleteWhenDir) {
        try {
            if (file.isDirectory()) {
                File[] children = file.listFiles();
                if (children != null) {
                    for (File child : children) {
                        deleteFile(child, true);
                    }
                }
                if (deleteWhenDir) {
                    LogUtils.error(TAG, "File delete " + file.getName());
                    boolean result = file.delete();
                    if (!result) {
                        LogUtils.error(TAG, "File " + file.getName() + " not deleted");
                    }
                }
            } else {
                LogUtils.error(TAG, "File delete " + file.getName());
                boolean result = file.delete();
                if (!result) {
                    LogUtils.error(TAG, "File " + file.getName() + " not deleted");
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public static File downloadsDir(@NonNull Context context) {
        File downloads = Objects.requireNonNull(
                context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS));
        if (!downloads.exists()) {
            boolean result = downloads.mkdir();
            if (!result) {
                throw new RuntimeException("downloads directory does not exists");
            }
        }
        return downloads;
    }

    public static void cleanDownloads(@NonNull Context context) {
        deleteFile(downloadsDir(context), false);
    }

    @NonNull
    public static String host(@NonNull Peeraddr peeraddr) throws UnknownHostException {
        return Objects.requireNonNull(InetAddress.getByAddress(peeraddr.address()).getHostAddress());
    }

    @Nullable
    public static Uri extractUri(WorkInfo workInfo) {

        Set<String> tags = workInfo.getTags();
        tags.remove(API.WORK_TAG);

        for (String tag : tags) {
            try {
                Uri uri = Uri.parse(tag);
                String scheme = uri.getScheme();
                if (Objects.equals(scheme, Scheme.pns.name()) ||
                        Objects.equals(scheme, Scheme.magnet.name()) ||
                        Objects.equals(scheme, Scheme.http.name()) ||
                        Objects.equals(scheme, Scheme.https.name()) ||
                        Objects.equals(scheme, Scheme.file.name())) {
                    return uri;
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
        return null;
    }

    @Nullable
    public static Tags extractTags(WorkInfo workInfo) {

        if (workInfo.getState() == WorkInfo.State.SUCCEEDED) {
            Data outputData = workInfo.getOutputData();
            if (outputData != Data.EMPTY) {
                String name = outputData.getString(DOWNLOAD_NAME);
                String mimeType = outputData.getString(DOWNLOAD_TYPE);
                long size = outputData.getLong(DOWNLOAD_SIZE, 0L);
                if (name != null && mimeType != null) {
                    return new Tags(name, mimeType, size);
                }
            }
        }

        Set<String> tags = workInfo.getTags();
        tags.remove(API.WORK_TAG);

        for (String tag : tags) {
            try {
                Tags test = Tags.decode(tag);
                if (test != null) {
                    return test;
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
        return null;
    }

    public static boolean isFileUri(@Nullable String url) {
        try {
            if (url != null) {
                Uri uri = Uri.parse(url);
                return Objects.equals(uri.getScheme(), Scheme.file.name());
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }

    @NonNull
    private WebResourceResponse getContentResponse(@NonNull Peeraddr peeraddr,
                                                   @NonNull Info info) throws IOException {

        String mimeType = MimeTypeService.getMimeType(info);
        try (InputStream in = lite.getInputStream(peeraddr, info.cid())) {

            Map<String, String> responseHeaders = new HashMap<>();

            responseHeaders.put(Header.CONTENT_LENGTH.name(), String.valueOf(info.size()));
            responseHeaders.put(Header.CONTENT_TYPE.name(), mimeType);

            return new WebResourceResponse(mimeType, StandardCharsets.UTF_8.name(), 200,
                    "OK", responseHeaders, new BufferedInputStream(in));
        }
    }

    @NonNull
    private Uri redirect(@NonNull Peeraddr peeraddr,
                         @NonNull Uri uri, @NonNull Cid root,
                         @NonNull List<String> paths)
            throws Exception {

        Info info = lite.resolvePath(peeraddr, root, paths);
        if (info instanceof Dir dir) {
            boolean exists = lite.hasChild(peeraddr, dir, INDEX_HTML);

            if (exists) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme(uri.getScheme())
                        .authority(uri.getAuthority());
                for (String path : paths) {
                    builder.appendPath(path);
                }
                builder.appendPath(INDEX_HTML);
                return builder.build();
            }
        }


        return uri;
    }

    @NonNull
    public Uri redirectUri(@NonNull Peeraddr peeraddr, @NonNull Cid root,
                           @NonNull Uri uri) throws Exception {
        List<String> paths = uri.getPathSegments();
        return redirect(peeraddr, uri, root, paths);
    }

    @NonNull
    private WebResourceResponse getContentResponse(@NonNull Info info) throws IOException {

        String mimeType = MimeTypeService.getMimeType(info);
        try (InputStream in = blockStore.getInputStream(info.cid())) {

            Map<String, String> responseHeaders = new HashMap<>();

            responseHeaders.put(Header.CONTENT_LENGTH.name(), String.valueOf(info.size()));
            responseHeaders.put(Header.CONTENT_TYPE.name(), mimeType);

            return new WebResourceResponse(mimeType, StandardCharsets.UTF_8.name(), 200,
                    "OK", responseHeaders, new BufferedInputStream(in));
        }
    }

    @NonNull
    private Uri redirect(@NonNull Uri uri, @NonNull Cid root, @NonNull List<String> paths)
            throws Exception {

        Info info = blockStore.resolvePath(root, paths);
        if (info instanceof Dir dir) {
            boolean exists = blockStore.hasChild(dir, INDEX_HTML);

            if (exists) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme(uri.getScheme())
                        .authority(uri.getAuthority());
                for (String path : paths) {
                    builder.appendPath(path);
                }
                builder.appendPath(INDEX_HTML);
                return builder.build();
            }
        }


        return uri;
    }

    @NonNull
    public Uri redirectUri(@NonNull Cid root, @NonNull Uri uri) throws Exception {
        List<String> paths = uri.getPathSegments();
        return redirect(uri, root, paths);
    }

    @NonNull
    public WebResourceResponse response(@NonNull Peeraddr peeraddr, @NonNull Cid root,
                                        @NonNull Uri uri) throws IOException {
        List<String> paths = uri.getPathSegments();

        Info info = lite.resolvePath(peeraddr, root, paths); // is resolved
        if (info instanceof Dir dir) {
            List<Info> childs = blockStore.childs(dir);
            String answer = generateDirectoryHtml(blockStore, root, uri, paths, childs);
            return new WebResourceResponse(MimeType.HTML_MIME_TYPE.name(),
                    StandardCharsets.UTF_8.name(), new ByteArrayInputStream(answer.getBytes()));

        } else {
            return getContentResponse(peeraddr, info);
        }

    }

    @NonNull
    public WebResourceResponse response(@NonNull Cid root, @NonNull Uri uri) throws Exception {
        List<String> paths = uri.getPathSegments();

        Info info = blockStore.resolvePath(root, paths); // is resolved
        if (info instanceof Dir dir) {
            List<Info> childs = blockStore.childs(dir);
            String answer = generateDirectoryHtml(blockStore, root, uri, paths, childs);
            return new WebResourceResponse(MimeType.HTML_MIME_TYPE.name(),
                    StandardCharsets.UTF_8.name(), new ByteArrayInputStream(answer.getBytes()));

        } else {
            return getContentResponse(info);
        }

    }

    @Nullable
    public Cid resolveName(@NonNull PeerId peerId) {
        byte[] data = blockStore.getBlock(peerId);
        if (data != null && data.length != 32) {
            return Cid.createCid(data);
        }
        return null;
    }

    @NonNull
    public Cid resolveName(@NonNull Peeraddr peeraddr, @NonNull PeerId peerId) {

        Cid resolved = resolves.get(peerId);
        if (resolved != null) {
            return resolved;
        }

        Optional<Cid> optionalCid = lite.fetchRoot(peeraddr);
        if (optionalCid.isPresent()) {
            Cid entry = optionalCid.get();
            blockStore.storeBlock(peerId, entry.hash());
            addResolves(peerId, entry);
            return entry;
        }
        throw new IllegalStateException("Cid couldn't be resolved");
    }


    @NonNull
    public Peeraddr resolveAddress(Context context, PeerId peerId,
                                   Consumer<Peeraddr> tries,
                                   Consumer<Peeraddr> failures) throws Exception {
        synchronized (peerId.toString().intern()) {
            Peeraddr peeraddr = swarm.get(peerId);
            if (peeraddr != null) {
                return peeraddr;
            }
            LogUtils.error(TAG, "connect to " + peerId.toBase58());

            peeraddr = resolveAddressIntern(context, peerId, tries, failures);
            if (peeraddr != null) {
                swarm.put(peerId, peeraddr);
                return peeraddr;
            }
            throw new ConnectException("Connection failed to host " + peerId.toBase36());
        }
    }

    @Nullable
    private Peeraddr resolveAddressIntern(Context context, PeerId peerId,
                                          Consumer<Peeraddr> tries, Consumer<Peeraddr> failures) {
        AtomicReference<Peeraddr> connection = new AtomicReference<>();


        Peeraddr relay = getRelay(context, peerId);
        int threads = 1;
        if (relay != null) {
            threads = 2;
        }
        ExecutorService service = Executors.newFixedThreadPool(threads);
        ExecutorService previous = services.put(peerId, service);
        if (previous != null) {
            throw new IllegalStateException("not allowed");
        }
        if (relay != null) {
            service.execute(() -> {
                try {
                    connection.set(lite.resolveAddress(relay, peerId));
                    service.shutdownNow();
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            });
        }
        service.execute(() -> {
            try {
                ResolveInfo info = lite.resolveAddress(peerId, tries, failures,
                        Integer.MAX_VALUE);

                storeRelay(context, peerId, info.relay());
                connection.set(info.target());
                service.shutdownNow();
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });
        service.shutdown();

        try {
            boolean result = service.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
            LogUtils.error(TAG, "Terminate normally " + result);
        } catch (InterruptedException ignore) {
        } finally {
            service.shutdownNow();
            services.remove(peerId);
        }

        return connection.get();
    }

    public boolean hasConnection(PeerId peerId) {
        Peeraddr connection = swarm.get(peerId);
        return connection != null;
    }

    public void abortConnection(PeerId peerId) {
        ExecutorService service = services.get(peerId);
        if (service != null) {
            LogUtils.error(TAG, "shutdown executor service of peerId " + peerId.toBase58());
            service.shutdownNow();
        }
    }

    public Lite lite() {
        return lite;
    }


    public record Engine(String name, Uri uri, String query) {
    }

    public record Tags(String name, String mimeType, long size) {

        @Nullable
        public static Tags decode(String tag) {
            if (tag.startsWith(API.FILE_INFO)) {
                String test = tag.replaceFirst(API.FILE_INFO, "");
                String[] tokens = test.split(";");
                Utils.checkArgument(tokens.length, 3, "three tags expected");
                return new Tags(tokens[0], tokens[1], Long.parseLong(tokens[2]));
            }
            return null;
        }

        public String encoded() {
            return API.FILE_INFO + name + ";" + mimeType + ";" + size();
        }
    }
}
