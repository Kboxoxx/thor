package threads.thor.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.window.layout.WindowMetrics;
import androidx.window.layout.WindowMetricsCalculator;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.sidesheet.SideSheetDialog;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.data.books.Bookmark;
import threads.thor.model.API;
import threads.thor.state.StateModel;
import threads.thor.utils.BookmarksAdapter;

public class BookmarksFragment extends DialogFragment {

    public static final String TAG = BookmarksFragment.class.getSimpleName();
    private WindowSizeClass widthWindowSizeClass = WindowSizeClass.COMPACT;

    public static BookmarksFragment newInstance(long tabItem) {
        BookmarksFragment fragment = new BookmarksFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(API.TAB, tabItem);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_booksmark, container, false);
        Bundle args = getArguments();
        Objects.requireNonNull(args);
        long tabItem = args.getLong(API.TAB);

        RecyclerView bookmarks = view.findViewById(R.id.bookmarks);
        Objects.requireNonNull(bookmarks);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(requireContext());
        linearLayoutManager.setReverseLayout(true);
        bookmarks.setLayoutManager(linearLayoutManager);

        StateModel stateModel =
                new ViewModelProvider(requireActivity()).get(StateModel.class);

        BookmarksAdapter mBookmarksAdapter = new BookmarksAdapter(
                new BookmarksAdapter.BookmarkListener() {
                    @Override
                    public void onClick(@NonNull Bookmark bookmark) {
                        try {
                            // todo virtual thread
                            new Thread(() -> stateModel.updateTab(tabItem, bookmark)).start();

                            Objects.requireNonNull(getDialog()).dismiss();
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                    }

                    @Override
                    public void onDismiss(@NonNull Bookmark bookmark) {
                        try {
                            // todo virtual thread
                            new Thread(() -> stateModel.removeBookmark(bookmark)).start();
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                    }
                });
        bookmarks.setAdapter(mBookmarksAdapter);


        stateModel.bookmarks().observe(getViewLifecycleOwner(), (marks -> {
            try {
                if (marks != null) {
                    mBookmarksAdapter.updateData(marks);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }));

        stateModel.removed().observe(this, (bookmark) -> {
            try {
                if (bookmark != null) {
                    Snackbar snackbar = Snackbar.make(view,
                            getString(R.string.bookmark_removed, bookmark.title()),
                            Snackbar.LENGTH_LONG);
                    snackbar.setAction(getString(R.string.revert), (view1) -> {
                        try {
                            // todo virtual thread
                            new Thread(() -> stateModel.restoreBookmark(bookmark)).start();
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        } finally {
                            snackbar.dismiss();
                        }

                    });
                    snackbar.show();
                    stateModel.removed(null);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        return view;
    }

    private void computeWindowSizeClasses() {
        WindowMetrics metrics = WindowMetricsCalculator.getOrCreate()
                .computeCurrentWindowMetrics(requireActivity());

        float widthDp = metrics.getBounds().width() /
                getResources().getDisplayMetrics().density;


        if (widthDp < 600f) {
            widthWindowSizeClass = WindowSizeClass.COMPACT;
        } else {
            widthWindowSizeClass = WindowSizeClass.MEDIUM;
        }

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        computeWindowSizeClasses();
        if (widthWindowSizeClass == WindowSizeClass.MEDIUM) {
            return new SideSheetDialog(requireContext());
        } else {
            BottomSheetDialog dialog = new BottomSheetDialog(requireContext());
            BottomSheetBehavior<FrameLayout> behavior = dialog.getBehavior();
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            behavior.setPeekHeight(0);
            return dialog;
        }
    }

    public enum WindowSizeClass {COMPACT, MEDIUM}
}
