package threads.thor.work;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.OutOfQuotaPolicy;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.Scheme;
import tech.lp2p.utils.Utils;
import threads.magnet.Client;
import threads.magnet.magnet.MagnetUri;
import threads.magnet.magnet.MagnetUriParser;
import threads.thor.LogUtils;
import threads.thor.model.API;
import threads.thor.utils.MimeTypeService;

public final class DownloadMagnetWorker extends Worker {

    private static final String TAG = DownloadMagnetWorker.class.getSimpleName();

    /**
     * @noinspection WeakerAccess
     */
    public DownloadMagnetWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static OneTimeWorkRequest getWork(@NonNull Uri magnet) {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        Data.Builder data = new Data.Builder();
        data.putString(Scheme.magnet.name(), magnet.toString());

        return new OneTimeWorkRequest.Builder(DownloadMagnetWorker.class)
                .addTag(API.WORK_TAG)
                .addTag(magnet.toString())
                .setInputData(data.build())
                .setConstraints(builder.build())
                .setExpedited(OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST)
                .build();

    }

    public static void download(@NonNull Context context, @NonNull Uri uri) {
        WorkManager.getInstance(context).enqueueUniqueWork(uri.toString(),
                ExistingWorkPolicy.KEEP, getWork(uri));
    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.error(TAG, " start DownloadMagnetWorker [" +
                (System.currentTimeMillis() - start) + "]...");

        String uri = getInputData().getString(Scheme.magnet.name());
        Objects.requireNonNull(uri);
        Uri result = Uri.parse(uri);
        try {
            MagnetUri magnetUri = MagnetUriParser.parse(uri);
            LogUtils.error(TAG, magnetUri.toString());
            String name = uri;
            if (magnetUri.getDisplayName().isPresent()) {
                name = magnetUri.getDisplayName().get();
            }

            File dataDir = API.downloadsDir(getApplicationContext());

            File root = new File(dataDir, name);
            if (!root.exists()) {
                boolean success = root.mkdir();
                Utils.checkTrue(success, "Could not create directory");
            }

            Client client = Client.create(root, magnetUri);
            AtomicInteger progress = new AtomicInteger();

            client.start((torrentSessionState) -> {

                long completePieces = torrentSessionState.getPiecesComplete();
                long totalPieces = torrentSessionState.getPiecesTotal();

                // -1 because of final copy action
                progress.set(Math.max((int) ((completePieces * 100.0f) / totalPieces) - 1, 0));


                LogUtils.error(TAG, "progress : " + progress +
                        " pieces : " + completePieces + "/" + totalPieces);

                setProgressAsync(new Data.Builder().putInt(API.APP_KEY, progress.get()).build());

                if (isStopped()) {
                    client.stop();
                }
            }, 1000);

            if (!isStopped()) {
                Stack<String> dirs = new Stack<>();
                Utils.checkTrue(root.isDirectory(), "Directory is expected");
                result = uploadDirectory(root, dirs);
                API.deleteFile(root, true);

                setProgressAsync(new Data.Builder()
                        .putInt(API.APP_KEY, 100).build()); // progress 100%
            }
        } catch (Throwable throwable) {
            return Result.failure(new Data.Builder().putString(API.WORK_FAILURE,
                    throwable.getMessage()).build());
        } finally {
            LogUtils.error(TAG, " finish DownloadMagnetWorker [" +
                    (System.currentTimeMillis() - start) + "]...");
        }
        if (!isStopped()) {
            return Result.success(new Data.Builder().putString(API.DOWNLOAD_URI,
                    result.toString()).build());
        }
        return Result.retry();
    }

    private Uri uploadFile(@NonNull File file, @NonNull Stack<String> dirs) throws IOException {
        String name = file.getName();
        Objects.requireNonNull(name);

        String mimeType = MimeTypeService.getMimeType(name);
        String path = Environment.DIRECTORY_DOWNLOADS + File.separator + String.join((File.separator), dirs);

        Uri downloads = API.downloadsUri(getApplicationContext(), mimeType, name, path);
        Objects.requireNonNull(downloads);
        ContentResolver contentResolver = getApplicationContext().getContentResolver();

        try (InputStream is = new FileInputStream(file)) {
            try (OutputStream os = contentResolver.openOutputStream(downloads)) {
                is.transferTo(os);
            }
        } catch (Throwable throwable) {
            contentResolver.delete(downloads, null, null);
            throw throwable;
        }
        return downloads;
    }

    private Uri uploadDirectory(@NonNull File dir, Stack<String> dirs) throws IOException {
        dirs.add(dir.getName());
        String[] children = dir.list();
        Uri uri = null;
        if (children != null) {
            for (String child : children) {
                File fileChild = new File(dir, child);
                if (fileChild.isDirectory()) {
                    Uri childUri = uploadDirectory(fileChild, dirs);
                    uri = API.min(uri, childUri);
                } else {
                    Uri childUri = uploadFile(fileChild, dirs);
                    uri = API.min(uri, childUri);
                }
            }
        }
        dirs.pop();
        return uri;
    }
}
