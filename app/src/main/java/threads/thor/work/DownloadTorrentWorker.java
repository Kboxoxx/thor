package threads.thor.work;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.OutOfQuotaPolicy;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.Progress;
import tech.lp2p.utils.Utils;
import threads.magnet.Client;
import threads.magnet.metainfo.MetadataService;
import threads.magnet.metainfo.Torrent;
import threads.thor.LogUtils;
import threads.thor.model.API;
import threads.thor.utils.MimeTypeService;

public final class DownloadTorrentWorker extends Worker {

    private static final String SIZE = "size";
    private static final String NAME = "name";
    private static final String TYPE = "type";
    private static final String URI = "uri";
    private static final String TAG = DownloadTorrentWorker.class.getSimpleName();

    /**
     * @noinspection WeakerAccess
     */
    public DownloadTorrentWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static OneTimeWorkRequest getWork(@NonNull Uri uri, @NonNull API.Tags tags) {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);

        Data.Builder data = new Data.Builder();
        data.putString(NAME, tags.name());
        data.putString(TYPE, tags.mimeType());
        data.putLong(SIZE, tags.size());
        data.putString(URI, uri.toString());

        return new OneTimeWorkRequest.Builder(DownloadTorrentWorker.class)
                .addTag(API.WORK_TAG)
                .addTag(uri.toString())
                .addTag(tags.encoded())
                .setInputData(data.build())
                .setConstraints(builder.build())
                .setExpedited(OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST)
                .build();
    }

    public static void download(@NonNull Context context, @NonNull Uri uri,
                                @NonNull API.Tags tags) {
        WorkManager.getInstance(context).enqueueUniqueWork(uri.toString(),
                ExistingWorkPolicy.KEEP, getWork(uri, tags));
    }


    private static void downloadUrl(URL urlCon, OutputStream os,
                                    Progress progress, long size) throws IOException {
        HttpURLConnection.setFollowRedirects(false);

        HttpURLConnection huc = (HttpURLConnection) urlCon.openConnection();

        huc.setReadTimeout(30000); // 30 sec
        huc.connect();

        try (InputStream is = huc.getInputStream()) {
            Utils.copy(is, os, progress, size);
        }
    }

    private static String nameWithoutExtension(String name) {
        int index = name.lastIndexOf('.');
        if (index > 1) { // maybe '.gradle'
            return name.substring(0, index);
        }
        return name;
    }

    private static String evalName(Torrent torrent, String name) {
        String torrentName = torrent.name();
        if (torrentName.isEmpty()) {
            torrentName = nameWithoutExtension(name);
        }
        return torrentName;
    }

    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();
        LogUtils.error(TAG, " start ... ");

        String url = getInputData().getString(URI);
        Objects.requireNonNull(url);
        Uri uri = Uri.parse(url);
        Uri downloadUri = uri;


        long downloadSize;
        String downloadName;
        String downloadType;

        try {
            long size = getInputData().getLong(SIZE, 0);
            String name = getInputData().getString(NAME);
            Objects.requireNonNull(name);
            String mimeType = getInputData().getString(TYPE);
            Objects.requireNonNull(mimeType);

            URL urlCon = new URL(uri.toString());
            Uri downloads = API.downloadsUri(getApplicationContext(), mimeType, name,
                    Environment.DIRECTORY_DOWNLOADS);
            Objects.requireNonNull(downloads);

            try (ByteArrayOutputStream os = new ByteArrayOutputStream(200000)) {
                Objects.requireNonNull(os, "Failed to open output stream");
                downloadUrl(urlCon, os, progress -> {
                    if (isStopped()) {
                        throw new IllegalStateException();
                    }
                }, size);

                Torrent torrent = MetadataService.buildTorrent(os.toByteArray());
                Objects.requireNonNull(torrent);

                File dataDir = API.downloadsDir(getApplicationContext());

                downloadName = evalName(torrent, name);
                downloadType = MimeTypeService.getMimeType(downloadName);
                downloadSize = torrent.size();

                File root = new File(dataDir, downloadName);
                if (!torrent.singleFile()) {
                    if (!root.exists()) {
                        boolean success = root.mkdir();
                        Utils.checkTrue(success, "Could not create directory");
                    }
                    dataDir = root;
                }

                Client client = Client.create(dataDir, torrent);

                AtomicInteger progress = new AtomicInteger();
                // now start torrent download
                client.start((torrentSessionState) -> {

                    long completePieces = torrentSessionState.getPiecesComplete();
                    long totalPieces = torrentSessionState.getPiecesTotal();

                    // -1 because of final copy action
                    progress.set(Math.max((int) ((completePieces * 100.0f) / totalPieces) - 1, 0));

                    LogUtils.error(TAG, " pieces : " + completePieces + "/" + totalPieces);


                    setProgressAsync(new Data.Builder().putInt(API.APP_KEY, progress.get()).build());

                    if (isStopped()) {
                        client.stop();
                    }
                }, 1000);

                if (!isStopped()) {

                    Stack<String> dirs = new Stack<>();
                    if (root.isDirectory()) {
                        downloadUri = uploadDirectory(root, dirs);
                    } else {
                        downloadUri = uploadFile(root, dirs);
                    }
                    API.deleteFile(root, true);

                    setProgressAsync(new Data.Builder()
                            .putInt(API.APP_KEY, 100).build()); // progress 100%
                }
            }

        } catch (Throwable throwable) {
            return Result.failure(new Data.Builder().putString(API.WORK_FAILURE,
                    throwable.getMessage()).build());
        } finally {
            LogUtils.error(TAG, " finish onStart [" +
                    (System.currentTimeMillis() - start) + "]...");
        }
        if (!isStopped()) {
            return Result.success(new Data.Builder().
                    putString(API.DOWNLOAD_URI, downloadUri.toString()).
                    putString(API.DOWNLOAD_NAME, downloadName).
                    putString(API.DOWNLOAD_TYPE, downloadType).
                    putLong(API.DOWNLOAD_SIZE, downloadSize).build());
        }
        return Result.retry();

    }

    private Uri uploadFile(@NonNull File file, @NonNull Stack<String> dirs) throws IOException {
        String name = file.getName();
        Objects.requireNonNull(name);

        String mimeType = MimeTypeService.getMimeType(name);
        String path = Environment.DIRECTORY_DOWNLOADS + File.separator + String.join((File.separator), dirs);

        Uri downloads = API.downloadsUri(getApplicationContext(), mimeType, name, path);
        Objects.requireNonNull(downloads);
        ContentResolver contentResolver = getApplicationContext().getContentResolver();

        try (InputStream is = new FileInputStream(file)) {
            try (OutputStream os = contentResolver.openOutputStream(downloads)) {
                is.transferTo(os);
            }
        } catch (Throwable throwable) {
            contentResolver.delete(downloads, null, null);
            throw throwable;
        }
        return downloads;
    }

    private Uri uploadDirectory(@NonNull File dir, Stack<String> dirs) throws IOException {
        dirs.add(dir.getName());
        String[] children = dir.list();
        Uri uri = null;
        if (children != null) {
            for (String child : children) {
                File fileChild = new File(dir, child);
                if (fileChild.isDirectory()) {
                    Uri childUri = uploadDirectory(fileChild, dirs);
                    uri = API.min(uri, childUri);
                } else {
                    Uri childUri = uploadFile(fileChild, dirs);
                    uri = API.min(uri, childUri);
                }
            }
        }
        dirs.pop();
        return uri;
    }
}
