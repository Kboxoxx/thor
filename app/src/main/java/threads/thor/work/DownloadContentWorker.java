package threads.thor.work;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.OutOfQuotaPolicy;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.File;
import java.io.OutputStream;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Stack;

import tech.lp2p.Lite;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Dir;
import tech.lp2p.core.Info;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import threads.thor.LogUtils;
import threads.thor.model.API;
import threads.thor.utils.MimeTypeService;

public final class DownloadContentWorker extends Worker {

    private static final String TAG = DownloadContentWorker.class.getSimpleName();
    private static final String URI = "uri";

    /**
     * @noinspection WeakerAccess
     */
    public DownloadContentWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static OneTimeWorkRequest getWork(@NonNull Uri content) {
        Data.Builder data = new Data.Builder();
        data.putString(URI, content.toString());

        return new OneTimeWorkRequest.Builder(DownloadContentWorker.class)
                .addTag(API.WORK_TAG)
                .addTag(content.toString())
                .setInputData(data.build())
                .setExpedited(OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST)
                .build();
    }

    public static void download(@NonNull Context context, @NonNull Uri uri) {
        WorkManager.getInstance(context).enqueueUniqueWork(uri.toString(),
                ExistingWorkPolicy.KEEP, getWork(uri));
    }

    @NonNull
    @Override
    public Result doWork() {


        long start = System.currentTimeMillis();
        LogUtils.error(TAG, " start DownloadContentWorker ... ");

        Uri uri = Uri.parse(getInputData().getString(URI));
        Uri result;
        try {
            API api = API.getInstance(getApplicationContext());
            Lite lite = api.lite();
            String name = API.getUriTitle(uri);

            PeerId peerId = Lite.decodePeerId(Objects.requireNonNull(uri.getHost()));

            Peeraddr resolveAddress = api.resolveAddress(getApplicationContext(), peerId,
                    peeraddr -> LogUtils.error(TAG, "Try relay " + peeraddr.toString()),
                    peeraddr -> LogUtils.error(TAG, "Failure relay " + peeraddr.toString()));

            Cid root = api.resolveName(resolveAddress, peerId);

            Info info = lite.resolvePath(resolveAddress, root, uri.getPathSegments());
            Objects.requireNonNull(info); // is resolved
            Stack<String> paths = new Stack<>();
            paths.push(name);
            if (info instanceof Dir dir) {
                Objects.requireNonNull(dir);

                result = download(lite, resolveAddress, paths, dir);
            } else {
                result = download(lite, resolveAddress, paths, info.cid());
            }
            Objects.requireNonNull(result);
        } catch (Throwable throwable) {
            return Result.failure(new Data.Builder().putString(API.WORK_FAILURE,
                    throwable.getMessage()).build());
        } finally {
            LogUtils.error(TAG, " finish DownloadContentWorker onStart [" +
                    (System.currentTimeMillis() - start) + "]...");
        }
        if (!isStopped()) {
            return Result.success(new Data.Builder().putString(API.DOWNLOAD_URI,
                    result.toString()).build());
        }
        return Result.retry();

    }

    private Uri download(@NonNull Lite lite,
                         @NonNull Peeraddr connection,
                         @NonNull Stack<String> paths,
                         @NonNull Cid cid) throws Exception {


        String name = paths.pop();
        Objects.requireNonNull(name);

        String mimeType = MimeTypeService.getMimeType(name);
        String path = Environment.DIRECTORY_DOWNLOADS + File.separator +
                String.join((File.separator), paths);

        Uri downloads = API.downloadsUri(getApplicationContext(), mimeType, name, path);
        Objects.requireNonNull(downloads);
        ContentResolver contentResolver = getApplicationContext().getContentResolver();


        try (OutputStream os = contentResolver.openOutputStream(downloads)) {
            Objects.requireNonNull(os, "Failed to open output stream");
            lite.fetchToOutputStream(connection, os, cid, progress -> {
                if (isStopped()) {
                    throw new IllegalStateException();
                }
                setProgressAsync(new Data.Builder()
                        .putInt(API.APP_KEY, progress).build());
            });

        } catch (Throwable throwable) {
            contentResolver.delete(downloads, null, null);
            throw throwable;
        }
        return downloads;
    }

    private Uri download(@NonNull Lite lite,
                         @NonNull Peeraddr peeraddr,
                         @NonNull Stack<String> paths,
                         @NonNull Dir dir) throws Exception {

        List<Info> childs = lite.blockStore().childs(dir);

        Uri uri = null;
        for (Info child : childs) {
            if (!isStopped()) {
                Cid cid = child.cid(); // not yet resolved
                paths.push(child.name());
                Info info = lite.resolvePath(peeraddr, cid, Collections.emptyList());
                if (info instanceof Dir dirChild) { // now it is resolved
                    Uri childUri = download(lite, peeraddr, paths, dirChild);
                    uri = API.min(uri, childUri);
                } else {
                    Uri childUri = download(lite, peeraddr, paths, cid);
                    uri = API.min(uri, childUri);
                }
            }
        }
        Objects.requireNonNull(uri);
        return uri;
    }

}
