package threads.thor.data.relays;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import tech.lp2p.core.PeerId;

@Dao
public interface RelayDao {

    @Query("SELECT * FROM Relay")
    LiveData<List<Relay>> relays();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertRelay(Relay relay);

    @Query("DELETE FROM Relay WHERE peerId = :peerId")
    void removeRelay(PeerId peerId);
}
