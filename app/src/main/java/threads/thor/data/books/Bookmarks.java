package threads.thor.data.books;

import androidx.room.RoomDatabase;

@androidx.room.Database(entities = {Bookmark.class}, version = 1, exportSchema = false)
public abstract class Bookmarks extends RoomDatabase {


    public abstract BookmarkDao bookmarkDao();

}
